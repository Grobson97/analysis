import numpy as np
from astropy.io import fits
from source.models.fits.fits_cw import FitsCW
from source.models.fits.fits_cw_header import FitsCWHeader
from source.models.fits.fits_kid_sweep import FitsKIDSweep
from source.models.fits.fits_kid_sweep_header import FitsKIDSweepHeader
from source.models.fits.fits_vna_sweep import FitsVNASweep
from source.models.fits.fits_vna_sweep_header import FitsVNASweepHeader


class FitsFileLoadService:
    """
    Class of static methods used to load fits files into classes.
    """

    @staticmethod
    def load_fits_kid_sweep_file(file_path: str) -> FitsKIDSweep:
        """
        Function to extract the fits file header and data into a FitsKIDSweep object instance.

        :param file_path: Path to fits sweep file.
        """

        with fits.open(file_path) as hdu_list:
            header = hdu_list["SWEEP"].header
            return FitsKIDSweep(
                header=FitsKIDSweepHeader(
                    data_type=header["DATATYPE"],
                    date=header["DATE"],
                    time=header["TIME"],
                    data_form=header["DATAFORM"],
                    dut=header["DUT"],
                    channel=header["CHANNEL"],
                    ultra_cold_attenuation=header["ATT_UC"],
                    cold_attenuation=header["ATT_C"],
                    room_temperature_attenuation=header["ATT_RT"],
                    input_attenuation=header["INPUTATT"],
                    output_attenuation=header["OUTPUTAT"],
                    f0_found=header["F0FOUND"],
                    sweep_samples=header["SW_SAMPS"],
                    sweep_blanking=header["SW_BLANK"],
                    sweep_points=header["SW_PTS"],
                    tone_name=header["TONE"],
                ),
                data=np.array(hdu_list["SWEEP"].data),
            )

    @staticmethod
    def load_fits_cw_file(file_path: str) -> FitsCW:
        """
        Function to extract the fits file header and data into a FitsCW object instance.
        """

        with fits.open(file_path) as hdu_list:
            header = hdu_list["CW"].header
            return FitsCW(
                header=FitsCWHeader(
                    data_type=header["DATATYPE"],
                    date=header["DATE"],
                    time=header["TIME"],
                    data_form=header["DATAFORM"],
                    dut=header["DUT"],
                    channel=header["CHANNEL"],
                    ultra_cold_attenuation=header["ATT_UC"],
                    cold_attenuation=header["ATT_C"],
                    room_temperature_attenuation=header["ATT_RT"],
                    input_attenuation=header["INPUTATT"],
                    output_attenuation=header["OUTPUTAT"],
                    tone_name=header["TONE"],
                    n_timestreams=header["TFIELDS"],
                    sample_rate=header["SAMPLERA"],
                    measurement_length=header["SAMPLELE"],
                    tone_frequency=header["SYNTHFRE"],
                    i_f0=header["IF0"],
                    q_f0=header["QF0"],
                    di_df=header["DIDF"],
                    dq_df=header["DQDF"],
                ),
                data=np.asarray(hdu_list["CW"].data),
            )

    @staticmethod
    def load_fits_vna_sweep_file(file_path: str) -> FitsVNASweep:
        """
        Function to extract the fits file header and data into a FitsVNASweep object instance.

        :param file_path: Path to fits sweep file.
        """

        with fits.open(file_path) as hdu_list:
            header = hdu_list["VNASWEEP"].header
            return FitsVNASweep(
                header=FitsVNASweepHeader(
                    data_type=header["DATATYPE"],
                    date=header["DATE"],
                    time=header["TIME"],
                    data_form=header["DATAFORM"],
                    dut=header["DUT"],
                    channel=header["CHANNEL"],
                    ultra_cold_attenuation=header["ATT_UC"],
                    cold_attenuation=header["ATT_C"],
                    room_temperature_attenuation=header["ATT_RT"],
                    input_attenuation=header["INPUTATT"],
                    output_attenuation=header["OUTPUTAT"],
                    vna_power=header["VNAPOWER"],
                    vna_bandwidth=header["VNABANDW"]
                ),
                data=np.asarray(hdu_list["VNASWEEP"].data),
            )
