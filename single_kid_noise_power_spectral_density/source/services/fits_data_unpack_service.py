import numpy as np

from source.models.fits.fits_cw import FitsCW
from source.models.fits.fits_kid_sweep import FitsKIDSweep
from source.models.fits.fits_vna_sweep import FitsVNASweep
from source.models.resonator_measurement.cw_data import CWData
from source.models.resonator_measurement.sweep_data import SweepData


class FitsDataUnpackService:
    """
    Class of static methods to unpack a fits data array into the corresponding sub-arrays
    """

    @staticmethod
    def unpack_sweep_data(fits_sweep: FitsKIDSweep or FitsVNASweep) -> SweepData:
        """
        Function to unpack a fits sweep data array into frequency, real and imaginary s21 arrays, and returns a
        SweepData object.

        :param fits_sweep: Array containing all of the data points from a fits sweep measurement.
        """
        data = fits_sweep.data
        frequency_array = np.empty(shape=len(data))
        real_s21_array = np.empty(shape=len(data))
        imag_s21_array = np.empty(shape=len(data))

        for count, entry in enumerate(data):
            frequency_array[count] = entry[0]
            real_s21_array[count] = entry[1]
            imag_s21_array[count] = entry[2]

        return SweepData(
            frequency_array=frequency_array,
            real_s21_array=real_s21_array,
            imag_s21_array=imag_s21_array,
        )

    @staticmethod
    def unpack_cw_data(fits_cw: FitsCW) -> CWData:
        """
        Function to unpack a fits cw data array into an equivalent numpy array of shape: (N, M) where N is the number
        of timestreams, and M is the number of samples.

        :param fits_cw: Array containing all of the data points from a fits CW measurement.
        """
        data = fits_cw.data
        dt = 1 / fits_cw.header.sample_rate  # Time between each sample measurement

        # create numpy array to allow numpy operations
        numpy_data_array = np.empty(shape=(len(data), len(data[0])))
        # Create array of times relative to start of each timestream to correspond to each sample.
        time_array = np.empty(shape=(len(data)))
        for count, entry in enumerate(data):
            time_array[count] = count * dt
            for index, value in enumerate(entry):
                numpy_data_array[count][index] = value

        numpy_data_array = np.transpose(numpy_data_array)

        # Return CWData instance with i and q data.
        return CWData(
            n_timestreams=int(fits_cw.header.n_timestreams) / 2,
            sample_rate=float(fits_cw.header.sample_rate),
            measurement_length=float(fits_cw.header.measurement_length),
            tone_frequency=float(fits_cw.header.tone_frequency),
            timestream_i_data=numpy_data_array[::2],
            timestream_q_data=numpy_data_array[1::2],
            timestream_time_data=time_array,
        )
