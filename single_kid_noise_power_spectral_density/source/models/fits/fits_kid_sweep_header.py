import numpy as np
from dataclasses import dataclass

from source.models.fits.fits_header import FitsHeader


@dataclass
class FitsKIDSweepHeader(FitsHeader):
    """
    Class to represent all the fields within a fits sweep file header that are unique to a Sweep file.
    """

    tone_name: str  # Tone name
    f0_found: float
    sweep_samples: int  # Sweep samples per step
    sweep_blanking: int  # Sweep blanking per step
    sweep_points: int  # Number of points for sweep.
