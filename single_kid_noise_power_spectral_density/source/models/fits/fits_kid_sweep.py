import numpy as np
from dataclasses import dataclass
from astropy.io import fits

from source.models.fits.fits import Fits
from source.models.fits.fits_kid_sweep_header import FitsKIDSweepHeader


@dataclass
class FitsKIDSweep(Fits):
    """
    Class to represent a fits file that contains sweep data/information.
    """

    header: FitsKIDSweepHeader
