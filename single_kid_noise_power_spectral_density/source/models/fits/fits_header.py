import numpy as np
from dataclasses import dataclass


@dataclass
class FitsHeader:
    """
    Class to represent all the fields of a fits header that are constant across all fits files.
    """

    data_type: str  # Type of measurement, e.g. sweep or CW (continuous wave)...
    date: str  # Date of measurement.
    time: str  # Time of measurement
    data_form: str  # Format of data array (column titles).
    dut: str  # Device under test
    channel: str  # Channel description
    ultra_cold_attenuation: float
    cold_attenuation: float
    room_temperature_attenuation: float
    input_attenuation: float
    output_attenuation: float
