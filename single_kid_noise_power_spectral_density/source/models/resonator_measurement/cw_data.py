import numpy as np
from dataclasses import dataclass


@dataclass
class CWData:
    n_timestreams: float  # Number of repeated timestreams per measurement.
    sample_rate: float  # Sample rate of timestream in Hz.
    measurement_length: float  # Length of time each timestream is measured for (s).
    tone_frequency: float  # Frequency the tone is set to.
    timestream_i_data: np.ndarray  # Time stream in phase (I) data in numpy array of shape (N, M). Where N is the
    # number of repeats (timestreams) and M is the number of samples per timestream.
    timestream_q_data: np.ndarray  # Time stream in quadrature (Q) data in numpy array of shape (N, M). Where N is the
    # number of repeats (timestreams) and M is the number of samples per timestream.
    timestream_time_data: np.ndarray  # Array of times relative to start of each timestream to correspond to each
    # sample. Length of array should be the same as the number of columns (M) in the timestream i and q data.
