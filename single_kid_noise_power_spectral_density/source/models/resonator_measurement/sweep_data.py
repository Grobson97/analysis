import numpy as np
from dataclasses import dataclass


@dataclass
class SweepData:
    frequency_array: np.ndarray
    real_s21_array: np.ndarray
    imag_s21_array: np.ndarray

    def get_iq_array(self) -> np.ndarray:
        """
        :return: complex array of I and Q data.
        """

        return np.array(self.real_s21_array + 1j * self.imag_s21_array)
