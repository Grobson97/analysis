import numpy as np
from matplotlib import pyplot as plt


def find_demodulation_indices(modulating_signal: np.ndarray) -> np.ndarray:
    """
    Function to return the indices that correspond to the indices bounding each on period and each off period in a
    perfectly a square wave signal (only on off states).

    :param modulating_signal: array of data for the modulating signal.
    :return: Array of indices: np.array([[on_start], [on_stop], [off_start], [off_stop]])
    """

    # When state changes from one to the other gradient has two values which are positive or negative, we only want the
    # first value
    if modulating_signal[0] > 0:
        on_first = True
    if modulating_signal[0] < 0:
        on_first = False

    non_zeros_gradient = np.where(np.gradient(modulating_signal) != 0)[0]

    if on_first:
        on_start_indices = np.hstack(
            (np.array([0]), np.array(non_zeros_gradient[3::4]))
        )
        on_stop_indices = np.array(non_zeros_gradient[::4])
        off_start_indices = np.array(non_zeros_gradient[1::4])
        off_stop_indices = np.array(non_zeros_gradient[2::4])
    if not on_first:
        on_start_indices = np.array(non_zeros_gradient[1::4])
        on_stop_indices = np.array(non_zeros_gradient[2::4])
        off_start_indices = np.hstack(
            (np.array([0]), np.array(non_zeros_gradient[3::4]))
        )
        off_stop_indices = np.array(non_zeros_gradient[::4])

    # Check if all arrays are same length, and shortens to the modal size
    sizes = np.array(
        [
            on_start_indices.size,
            on_stop_indices.size,
            off_start_indices.size,
            off_stop_indices.size,
        ]
    )
    values, counts = np.unique(sizes, return_counts=True)
    mode_size = values[np.argmax(counts)]

    on_start_indices = on_start_indices[:mode_size]
    on_stop_indices = on_stop_indices[:mode_size]
    off_start_indices = off_start_indices[:mode_size]
    off_stop_indices = off_stop_indices[:mode_size]

    return np.array(
        [on_start_indices, on_stop_indices, off_start_indices, off_stop_indices]
    )


def find_optimal_index_delay(
    signal_array: np.array,
    time_array: np.ndarray,
    demodulation_indices: np.ndarray,
    index_delay_step: int,
    plot_graph: bool,
) -> int:
    """
    Function to shift the demodulation indices by a fixed value and determine the optimal one, maximising the mean
    mean demodulated signal.

    :param signal_array: Array to be demodulated
    :param time_array: Corresponding time array for signal time stream.
    :param demodulation_indices: Array of four arrays of the indices corresponding the the start and stop of an on
    period, and the start and stop of an on period  np.array([[on_start], [on_stop], [off_start], [off_stop]]).
    :param index_delay_step: Integer step size to use between index delay values. Higher is faster but less precise.
    :param plot_graph: Boolean to plot the graph showing the mean demodulated signal vs. index delay.
    :return Optimal index delay
    """

    on_off_cycle_points = demodulation_indices[3][1] - demodulation_indices[0][1]
    index_delay_list = np.arange(0, 2 * on_off_cycle_points, index_delay_step)
    mean_demodulated_signal = []

    for count, index_delay in enumerate(index_delay_list):
        (
            demodulated_signal,
            demodulated_python_time,
            demodulated_signal_error,
        ) = demodulate_signal_dumb(
            signal_array=signal_array,
            time_array=time_array,
            demodulation_indices=demodulation_indices + index_delay,
            plot_graph=False,
        )
        mean_demodulated_signal.append(np.nanmean(demodulated_signal))

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(index_delay_list, mean_demodulated_signal)
        plt.xlabel("Index delay"),
        plt.ylabel("Mean value of demodulated signal")
        plt.show()

    return index_delay_list[np.argmax(np.array(mean_demodulated_signal))]


def demodulate_signal_dumb(
    signal_array: np.array,
    time_array: np.ndarray,
    demodulation_indices: np.ndarray,
    plot_graph: bool,
):
    """
    Function to demodulate a signal given and array of indices indicating the on-off cycles. The demodulated signal
    is the mean value of the signal in each on-off cycle, as is the demodulated time. The demodulated signal error is
    the standard deviation divided by the square root of the number of points in the cycle.

    :param signal_array: Array to be demodulated
    :param time_array: Corresponding time array for signal time stream.
    :param demodulation_indices: Array of four arrays of the indices corresponding the the start and stop of an on
    period, and the start and stop of an on period  np.array([[on_start], [on_stop], [off_start], [off_stop]]).
    :param plot_graph: Boolean to plot the graph showing the demodulation.
    :return:
    """
    demodulated_signal = []
    demodulated_time = []
    demodulated_signal_error = []

    for count in range(demodulation_indices[0].size):
        on_mean = np.mean(
            signal_array[
                demodulation_indices[0][count] : demodulation_indices[1][count]
            ]
        )
        off_mean = np.mean(
            signal_array[
                demodulation_indices[2][count] : demodulation_indices[3][count]
            ]
        )
        number_of_points = (
            demodulation_indices[3][count] - demodulation_indices[0][count]
        )
        on_std = np.std(
            signal_array[
                demodulation_indices[0][count] : demodulation_indices[1][count]
            ]
        )
        off_std = np.std(
            signal_array[
                demodulation_indices[2][count] : demodulation_indices[3][count]
            ]
        )

        demodulated_signal.append(on_mean - off_mean)
        demodulated_time.append(
            np.mean(
                time_array[
                    demodulation_indices[0][count] : demodulation_indices[3][count]
                ]
            )
        )
        demodulated_signal_error = (on_std + off_std) / (2 * np.sqrt(number_of_points))

    demodulated_signal_error = np.array(demodulated_signal_error)
    demodulated_signal = np.array(demodulated_signal)
    demodulated_time = np.array(demodulated_time)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(time_array, np.real(signal_array), color="b", label="Signal")
        plt.errorbar(
            x=demodulated_time,
            y=np.real(demodulated_signal),
            yerr=np.real(demodulated_signal_error),
            linestyle="none",
            marker="o",
            color="r",
            label="Demodulated Signal",
        )
        plt.xlabel("Time")
        plt.ylabel("Signal")
        plt.title("Dumb Signal demodulation")
        plt.legend()
        plt.show()

    return demodulated_signal, demodulated_time, demodulated_signal_error


def within_threshold(value: float, target_value: float, threshold: float) -> bool:
    """
    Function to check if a value is within a +/- threshold of a given target value.

    :param value: Value to check if it is within the threshold.
    :param target_value: Value to compare the value to.
    :param threshold: +/- threshold
    :return:
    """

    if target_value - threshold < value < target_value + threshold:
        return True
    if value < target_value - threshold or value > target_value + threshold:
        return False


def find_plateaus(
    x_array: np.ndarray, y_array: np.ndarray, threshold: float, plot_graph: bool
) -> np.ndarray:
    """
    Function to search for plateaus in the y array using the second derivative. Returns array of indices, such that
    plateau_indices[::2] gives all the indices at the start of each plateau and plateau_indices[1::2] gives the ends.

    :param x_array: Array for which the array with plateaus is plotted against.
    :param y_array: Array to search for plateaus in.
    :param threshold: value to use as the threshold for which the gradient is non_zero.
    :param plot_graph: Boolean to plot a graph of the y vs x with an overlay of the plateaus
    :return:
    """

    plateau_indices = []
    if within_threshold(float(np.gradient(y_array)[0]), 0, threshold):
        in_plateau = True
        plateau_indices.append(0)
    else:
        in_plateau = False

    for count, gradient in enumerate(np.gradient(y_array)):
        if within_threshold(gradient, 0, threshold) and not in_plateau:
            plateau_indices.append(count)
            in_plateau = True
        if within_threshold(gradient, 0, threshold) and in_plateau:
            continue
        if not within_threshold(gradient, 0, threshold) and in_plateau:
            plateau_indices.append(count)
            in_plateau = False
        if not within_threshold(gradient, 0, threshold) and not in_plateau:
            continue

    # If still in plateau at end of loop, add final index to put an ending bound to last plateau
    if in_plateau:
        plateau_indices.append(y_array.size - 1)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(x_array, y_array, label="Original Data", color="b", linestyle="--")
        plt.plot(
            x_array[plateau_indices],
            y_array[plateau_indices],
            color="r",
            label="Plateaus",
            linestyle="none",
            marker="o",
        )
        plt.xlabel("x_array")
        plt.ylabel("y_array")
        plt.show()

    return np.array(plateau_indices)


def within_plateau_time_bounds(value: float, time_bound_array: np.ndarray) -> bool:
    """
    Function to check if a given value is within the certain time bounds provided by the time_bound_array. This must be
    of the format such that the slice [::2] gives the start of the various time bounds and [1::2] gives the
    corresponding bound ends

    :param value: value to check
    :param time_bound_array: Array of time bounds of the format such that the slice [::2] gives the start of the
    various time bounds and [1::2] gives the corresponding bound ends.
    :return:
    """

    for index in range(time_bound_array[::2].size - 1):
        # If value is found within a time bound return true
        if time_bound_array[::2][index] < value < time_bound_array[1::2][index]:
            return True
        else:
            continue

    # If not found within any time bounds return false.
    return False


def get_cut_indices(array_to_cut: np.ndarray, bounds_array: np.ndarray) -> np.ndarray:
    """
    Function to find the indices of a given array that correspond to values within the bounding array. The bounds are
    defined in an array of the format such that the slice [::2] gives the start of the various bounds and [1::2] gives
    the corresponding bound ends.

    :param array_to_cut: Array to remove the values not found within the given bounds.
    :param bounds_array: Array of time bounds of the format such that the slice [::2] gives the start of the
    various time bounds and [1::2] gives the corresponding bound ends.
    :return:
    """

    good_indices = np.array([])
    for count, bound_start in enumerate(bounds_array[::2]):
        loop_indices = np.where(
            (bound_start < array_to_cut) & (array_to_cut < bounds_array[1::2][count])
        )[0]
        good_indices = np.concatenate((good_indices, loop_indices))

    return good_indices.astype(int)
