import matplotlib.pyplot as plt
import numpy as np
from lmfit import Model
from lmfit import Parameters


def solve_for_y(y0: np.ndarray, a: float, up_sweep=True) -> np.ndarray:
    """
    Function to return an array of the correct generator detuning in line-widths
    by solving the cubic function, equation 13 in arXiv 1305.4281. Each value
    in the returned array corresponds to the solution for each y0 value.

    NB: If VNA sweep is carried out with amp downwards sweep, up_sweep must be
    changed to False.

    """

    solved_y_array = np.empty(shape=y0.shape)

    for count, value in enumerate(y0):
        # Define Cubic Equation:
        cubic = np.poly1d([4.0, -4.0 * value, 1.0, -(value + a)])
        # Solve for roots:
        roots = cubic.r
        # Discard imaginary roots:
        real_roots = []
        for solution in roots:
            if solution.imag == 0:
                real_roots.append(solution.real)

        real_roots = np.array(real_roots)  # Make np array.
        # If VNA sweep is upwards select the minimum valued root, else the max.
        if up_sweep:
            solved_y_array[count] = np.amin(real_roots)
        else:
            solved_y_array[count] = np.amax(real_roots)

    return solved_y_array


def s21_non_linear(f, f0, qr, qc_real, qc_imag, a, amp):
    """
    Function to return the S21 spectrum for frequency array f for amp KID with amp
    resonant frequency f0, resonator quality factor qr, real and imaginary
    coupling quality factors qc_real and qc_imag, bifurcation factor amp and amplitude amp.
    """

    from math import pi

    y0 = qr * (f - f0) / f0
    y = solve_for_y(y0, a)

    return abs(amp * (1 - qr / (qc_real + 1j * qc_imag) / (1 + 2j * y)))


def fit_non_linear_mag_s21(
    frequency_array: np.ndarray,
    data_array: np.ndarray,
    qr_guess=1e5,
    a_guess=0.7,
    normalise=True,
    plot_db=False,
    plot_title="",
    plot_graph=False,
):
    """
    Function to fit to a single resonance feature of a KID using lmfit.
    Returns the best fit variables as a dictionary with keys:
    dict_keys(['f0', 'qr', 'qc_real', 'qc_imag', 'a', 'amp', 'qc', 'qi']).
    followed by the chi squared for the fit and amp plot of the fit if specified.

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex or magnitude array of N S21 data points.
    :param qr_guess: Guess for the qr of the lorentzian.
    :param a_guess: Guess for the bifurcation factor.
    :param normalise: Boolean to normalise the data_array array before fitting.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.
    """

    if normalise:
        data_array = data_array / data_array[-1]

    # Define model:
    s21_model = Model(s21_non_linear)

    # Assuming minimum value of data_array is f0, hence approx f0.
    minimum_index = np.argmin(data_array)
    approx_f0 = frequency_array[minimum_index]

    # Create an array to be used to weight the fit to give higher importance to date around
    # the resonance.
    weights = np.ones(data_array.shape)
    weights[minimum_index - int(len(data_array)/40) : minimum_index + int(len(data_array)/40)] = 5

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=approx_f0, vary=True)
    params.add("qr", value=qr_guess / 2, min=0.0, vary=True)
    params.add(
        "qc_real", value=qr_guess, min=0.0, vary=True
    )  # NB: qr should be less than qc
    params.add("qc_imag", value=100.0, min=0.0, vary=True)
    params.add("a", value=a_guess, vary=True, min=0)
    params.add("amp", value=abs(data_array).mean())

    # Fit and extract best values and fitting statistics
    result = s21_model.fit(data_array, params, f=frequency_array, weights=weights)
    best_fit = result.best_fit
    chi_squared = result.chisqr

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    # Calculate Qc and Qi (and errors) and add to result dict.
    result_dict["qc"] = [
        abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
        / result_dict["qc_real"][0],
        np.sqrt(
            result_dict["qc_real"][1] ** 2
            + (
                2
                * result_dict["qc_imag"][1]
                * result_dict["qc_imag"][0]
                / result_dict["qc_real"][0]
            )
            ** 2
            + result_dict["qc_real"][1]
            * result_dict["qc_imag"][0] ** 2
            / result_dict["qc_real"][0] ** 2
        ),
    ]
    qi = 1 / ((1 / result_dict["qr"][0]) - (1 / result_dict["qc"][0]))
    result_dict["qi"] = [
        qi,
        np.sqrt(
            qi**4
            * (
                result_dict["qr"][1] ** 2 / result_dict["qr"][0] ** 4
                + result_dict["qc"][1] ** 2 / result_dict["qc"][0] ** 4
            )
        ),
    ]

    # Plot formation
    if plot_graph:
        plt.figure(figsize=(8, 6))

        # If to convert data_array to plot_db:
        if plot_db:
            best_fit = 20 * np.log10(best_fit)
            data_array = 20 * np.log10(abs(data_array))
        plt.plot(
            frequency_array * 1e-9,
            data_array,
            linestyle="none",
            marker=".",
            color="b",
            label="Data",
        )
        plt.plot(
            frequency_array * 1e-9,
            best_fit,
            linestyle="--",
            color="r",
            label="Best Fit:\nf0 = %0.2e\nqr = %.2e\nqc = %.2e\nqi = %.2e\namp = %.3f\na = %.3f\n$\chi^2$ = %.2e"
            % (
                result_dict["f0"][0],
                result_dict["qr"][0],
                result_dict["qc"][0],
                result_dict["qi"][0],
                result_dict["amp"][0],
                result_dict["a"][0],
                chi_squared,
            ),
        )
        plt.xlabel("Frequency (GHz)")

        if plot_db:
            plt.ylabel("S21 Magnitude (dB)")
        else:
            plt.ylabel("S21 Magnitude")

        plt.legend()
        plt.title(plot_title)
        plt.grid()
        plt.show()

    return result_dict


########################################################################################################################


def lorentzian_s21(
    frequency_array: np.ndarray, qr: float, qc: float, f0: float, a=1.0
) -> np.array:
    """
    Function defining the model of amp lorentzian resonance in an S21 array.

    :param np.array frequency_array: Array of N frequency points.
    :param float qr: Resonator quality factor.
    :param float qc: Coupling quality factor.
    :param float f0: Resonant frequency.
    :param float a: Amplitude/ background level.
    """
    x = (frequency_array - f0) / f0
    b = 2.0 * qr * x
    real_s21 = a - (qr / qc) * (1.0 / (b**2 + 1.0))
    imaginary_s21 = (qr / qc) * (b / (b**2 + 1.0))
    magnitude_s21 = np.sqrt(real_s21**2 + imaginary_s21**2)
    return magnitude_s21


def fit_lorentzian_s21(
    frequency_array: np.array,
    s21_array: np.array,
    qr_guess: float,
    qc_guess: float,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> list:
    """
    Function to fit qr, qc and f0 to amp  somewhat simple lorentzian resonance in S21.\n
    Returns fit parameters as amp list: [qr, qc, f0, amp].

    :param frequency_array: Array of N frequency points.
    :param s21_array: Complex array of N S21 data points.
    :param qr_guess: Guess of resonators quality factor.
    :param qc_guess: Guess of coupling quality factor.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S21 in plot_db.
    :param plot_title: Title to give to the plot. Default is an empty string.

    """

    s21_mag_array = np.abs(s21_array)

    f0_index = np.argmin(s21_mag_array)  # index of minimum point.
    resonance_data_points = (
        300  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(lorentzian_s21)

    # Define Parameters:
    params = Parameters()
    params.add("qc", value=qc_guess, min=0, vary=True)
    params.add("qr", value=qr_guess, expr="qc")
    params.add(
        "f0", value=frequency_array[np.argmin(s21_mag_array)], min=0, vary=True
    )  # Guesses f0 is min of data_array
    params.add(
        "amp", value=np.mean(np.abs(s21_array)), vary=True
    )  # Guesses background level as mean of data_array.

    result = l_model.fit(
        data=np.abs(s21_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    best_fit_parameters = [
        result.best_values["qr"],
        result.best_values["qc"],
        result.best_values["f0"],
        result.best_values["amp"],
    ]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=100000,
        )

        lorentzian_fit = lorentzian_s21(
            frequency_array=f_array,
            qr=result.best_values["qr"],
            qc=result.best_values["qc"],
            f0=result.best_values["f0"],
            a=result.best_values["amp"],
        )

        y_label = "S21 Magnitude"
        if plot_db:
            y_label = "S21 Magnitude  (plot_db)"
            s21_mag_array = 20 * np.log10(s21_mag_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            s21_mag_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nf0=%0.2e"
            % (
                result.best_values["qr"],
                result.best_values["qc"],
                result.best_values["f0"],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return best_fit_parameters


########################################################################################################################


def simple_lorentzian(
    frequency_array: np.array, a: float, bw: float, f0: float, c: float
) -> np.array:
    """
    Function defining amp very simple model of amp lorentzian.
    Returns

    :param np.array frequency_array: Array of N frequency points.
    :param float a: Amplitude.
    :param float bw: Full width half maximum.
    :param float f0: Resonant frequency.
    :param float c: Offset in y.
    """
    x = (frequency_array - f0) / (0.5 * bw)
    lorentzian = a / (1 + x**2) + c
    return lorentzian


def fit_simple_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    bw_guess: np.array,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> dict:
    """
    Function to fit amp very simple lorentzian bw, f0, amp, amp to amp resonance in S21.\n
    Returns fit parameters as amp list: [q, bw, f0, amp, amp].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param bw_guess: Guess for the bandwidth of the lorentzian.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmax(magnitude_array)  # index of minimum point.
    resonance_data_points = (
        500  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(simple_lorentzian)

    # Define Parameters:
    params = Parameters()
    params.add("bw", value=bw_guess, vary=True)
    params.add("amp", value=np.max(magnitude_array), vary=True)
    params.add("f0", value=frequency_array[np.argmax(magnitude_array)], vary=True)
    # Guesses background level as mean of data_array.
    params.add("c", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    result_dict["q"] = [
        result_dict["f0"][0] / result_dict["bw"][0],
        (result_dict["f0"][0] / result_dict["bw"][0])
        * np.sqrt(
            (result_dict["f0"][1] / result_dict["f0"][0]) ** 2
            + (result_dict["bw"][1] / result_dict["bw"][0]) ** 2
        ),
    ]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = simple_lorentzian(
            frequency_array=f_array,
            a=result_dict["amp"][0],
            bw=result_dict["bw"][0],
            f0=result_dict["f0"][0],
            c=result_dict["c"][0],
        )

        y_label = "S21 Magnitude"
        if plot_db:
            y_label = "S21 Magnitude  (plot_db)"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nq=%0.2e" % result_dict["q"][0],
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return result_dict


########################################################################################################################


def skewed_lorentzian(
    frequency_array: np.ndarray,
    f0: float,
    qr: float,
    qc_real: float,
    qc_imag: float,
    amp: float,
) -> np.ndarray:
    """
    Model of a skewed lorentzian using a complex qc.

    :param frequency_array: Array of N frequency points.
    :param f0: Resonant frequency
    :param qr: Total resonator quality factor
    :param qc_real: Real coupling quality factor
    :param qc_imag: Imaginary coupling quality factor
    :param amp: Amplitude
    :return:
    """

    x = (frequency_array - f0) / f0
    return abs(amp * (1 - qr / (qc_real + 1j * qc_imag) / (1 + 2j * qr * x)))


def fit_skewed_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    qr_guess: float,
    f0_guess: float or None,
    normalise=True,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> dict:
    """
    Function to fit amp very simple lorentzian bw, f0, amp, amp to amp resonance in S21.\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qr"] = [value, error].
    Result dictionary keys: dict_keys(['f0', 'qr', 'qc_real', 'qc_imag', 'amp', 'qc', 'qi']).

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex or magnitude array of N S21 data points.
    :param qr_guess: Guess for the qr of the lorentzian.
    :param f0_guess: Guess for the resonant frequency.
    :param normalise: Boolean to normalise the data_array array before fitting.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    # TODO: Solve issue with s21 array in this function and non linear fitting function.

    if normalise:
        data_array = data_array / np.max(data_array)

    f0_index = np.argmin(data_array)  # index of minimum point.
    resonance_data_points = round(
        len(data_array) / 2
    )  # define number of data points either side of resonance to sample
    start_index = f0_index - round(resonance_data_points)
    end_index = f0_index + round(resonance_data_points)
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size - 1:
        end_index = -1

    # Define model:
    l_model = Model(skewed_lorentzian)

    f0_first_guess = frequency_array[f0_index]
    if f0_guess is not None:
        f0_first_guess = f0_guess

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=f0_first_guess, vary=True)
    params.add("qr", value=qr_guess, vary=True, min=0)
    params.add("qc_real", value=qr_guess * 2, vary=True, min=0)
    params.add("qc_imag", value=100.0, vary=True)
    # Guesses background level as mean of data_array.
    params.add("amp", value=data_array[-1], vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(data_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    try:
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
            / result_dict["qc_real"][0],
            np.sqrt(
                result_dict["qc_real"][1] ** 2
                + (
                    2
                    * result_dict["qc_imag"][1]
                    * result_dict["qc_imag"][0]
                    / result_dict["qc_real"][0]
                )
                ** 2
                + result_dict["qc_real"][1]
                * result_dict["qc_imag"][0] ** 2
                / result_dict["qc_real"][0] ** 2
            ),
        ]
    except TypeError:
        print("Possible error: The fit value for Qc has None type standard errors.")
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
            / result_dict["qc_real"][0],
            None,
        ]

    qi = 1 / ((1 / result_dict["qr"][0]) - (1 / result_dict["qc"][0]))
    try:
        result_dict["qi"] = [
            qi,
            np.sqrt(
                qi**4
                * (
                    result_dict["qr"][1] ** 2 / result_dict["qr"][0] ** 4
                    + result_dict["qc"][1] ** 2 / result_dict["qc"][0] ** 4
                )
            ),
        ]
    except TypeError:
        print(
            "Possible error: The fit value for Qr or Qc has None type standard errors."
        )
        result_dict["qi"] = [qi, None]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=1000000,
        )

        lorentzian_fit = skewed_lorentzian(
            frequency_array=f_array,
            f0=result_dict["f0"][0],
            qr=result_dict["qr"][0],
            qc_real=result_dict["qc_real"][0],
            qc_imag=result_dict["qc_imag"][0],
            amp=result_dict["amp"][0],
        )

        if plot_db:
            y_label = "S21 Magnitude (plot_db)"
            data_array = 20 * np.log10(data_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        if not plot_db:
            y_label = "S21 Magnitude"

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            data_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nQi=%0.2e\nf0=%0.2e"
            % (
                result_dict["qr"][0],
                result_dict["qc"][0],
                result_dict["qi"][0],
                result_dict["f0"][0],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return result_dict


########################################################################################################################


def interpolate_poly_fit(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit amp polynomial of amp specified degree to x and y data and interpolate
    at amp given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate amp y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial(xi)
