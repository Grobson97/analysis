# Single KID Noise Power Spectral Density

This is a self-contained project directory to load the data for a power and noise sweep of a single kid in the "data" subdirectory and produce various levels of analysis. See details on example scripts for further details but the analysis typically produces power spectral density plots or lorentzian fits.

## Setup

1. Load the relevant data files into a "data" subdirectory for a  "unit" of noise measurement (or point the script to the desired directory), for example this should be the following files (names will vary):
   * Sweep.fits
   * sweep_HR.fits
   * TS_2000_Hz_OFF_RES_x010.fits
   * TS_2000_Hz_ON_RES_x010.fits
   * TS_200000_Hz_OFF_RES_x010.fits
   * TS_200000_Hz_ON_RES_x010.fits

2. Create a python virtual environment within the project directory
   ```
   python -m venv venv
   ```
3. Install the project requirements. After activating the venv in the command line:
   ```
   pip install -r /path/to/requirements.txt
   ```
4. Run whichever example script you like, or make your own.

## Examples

1. **iq_circle_fit_example.py:** Demonstrates the circle fitting algorithm to a Sweep.fits file.
2. **plot_and_fit_fits_sweep.py:** Loads a Sweep.fits file and fits a lorentzian to the data using the non-linear model
or the skewed (Pete fitter) model. **NB: The skewed model fitter is currently not working**.
3. **plot_full_range_psd.py:** Circle fits the sweep data and uses this to interpolate a df timestream for each IQ
timestream and obtain a mean power spectral density for each timestream file (Low frequency, on and off resonance and
high frequency, on and off resonance. There is an option to turn diagnostic plots on and off to see all steps of the
analysis in more detail.
4. **plot_power_spectral_density.py:** For a single timestream file: Circle fits the sweep data and uses this to
interpolate a df timestream for each IQ timestream and obtain a mean power spectral density.
5. **plot_timestreams.py:** For a single timestream file: Plots an elected number of I and Q timestreams.
