import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import iq_circle_fit_tools


def main(
    plot_diagnostics=True,  # To see plots of fits, sweeps and all of the timestreams rather than just the mean psd.
    directory=r"..\\data",
):

    sweep_filename = "Sweep.fits"  # This shouldn't change.
    sweep_file_path = os.path.join(directory, sweep_filename)

    # Loop to find the two sample frequencies to identify which is high res and which is low res.
    sample_frequencies = []
    for file_name in os.listdir(directory):
        if "TS" in file_name:
            string_search_result = re.search("TS_(.*)_Hz", file_name)
            sample_frequency = float(string_search_result.group(1))
            if sample_frequency not in sample_frequencies:
                sample_frequencies.append(sample_frequency)

    ########################################################################################################################

    # Process Sweep Data:

    # Create instance of FitsVNAS21Sweep class:
    fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=sweep_file_path)
    sweep_data = FitsDataUnpackService.unpack_sweep_data(fits_sweep=fits_sweep)
    sweep_iq_array = sweep_data.real_s21_array + 1j * sweep_data.imag_s21_array

    if plot_diagnostics:
        figure, axes = plt.subplots(1, 3, figsize=(10, 4))
        axes[0].plot(sweep_data.frequency_array * 1e-9, np.abs(sweep_iq_array))
        axes[0].set_xlabel("Frequency (GHz)")
        axes[0].set_ylabel("S21 Magnitude (Arb)")
        axes[1].plot(
            sweep_data.frequency_array * 1e-9, 20 * np.log10(np.abs(sweep_iq_array))
        )
        axes[1].set_xlabel("Frequency (GHz)")
        axes[1].set_ylabel("S21 Magnitude (dB)")
        axes[2].plot(sweep_data.real_s21_array, sweep_data.imag_s21_array)
        axes[2].set_xlabel("I (V)")
        axes[2].set_ylabel("Q (V)")
        axes[2].axis("equal")
        plt.tight_layout()
        plt.show()

    iq_circle_fit = iq_circle_fit_tools.process_sweep_data_with_circle_fit(
        sweep_data=sweep_data,
        phase_polynomial_degree=10,
        phase_fit_fraction=1 / 8,
        plot_cable_delay_fit=plot_diagnostics,
        plot_calibrated_data=plot_diagnostics,
        plot_phase_polynomial_fit=plot_diagnostics,
    )
    print("Completed processing the Sweep data")

    ########################################################################################################################

    # Process Timestreams

    mean_psd_data_dict = {"Label": [], "Mean PSD Data": [], "Frequencies": []}
    for file_name in os.listdir(directory):
        if "TS" in file_name:
            timestream_file_path = os.path.join(directory, file_name)

            # Create instance of FitsCW
            fits_cw = FitsFileLoadService.load_fits_cw_file(
                file_path=timestream_file_path
            )
            cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

            psd_label = ""
            if str(max(sample_frequencies)) in file_name:
                psd_label += "High Frequency, "
            else:
                psd_label += "Low Frequency, "
            if "ON" in file_name:
                psd_label += "On Res"
            else:
                psd_label += "Off Res"

            # Section to calibrate the I and Q data via the same processes that happened to the sweep data for the
            # cirlce fit, Then extract the df timestreams and plot the mean power spectral density.

            calibrated_i = []
            calibrated_q = []
            df = []

            for index in range(int(cw_data.n_timestreams)):
                i, q = iq_circle_fit_tools.calibrate_iq_points_to_circle_fit(
                    i=cw_data.timestream_i_data[index],
                    q=cw_data.timestream_q_data[index],
                    tone_frequency=fits_cw.header.tone_frequency,
                    iq_circle_fit=iq_circle_fit,
                )
                calibrated_i.append(i)
                calibrated_q.append(q)
                timestream_phase = np.angle(i + 1j * q, deg=True)
                f = iq_circle_fit.phase_polynomial(timestream_phase)
                df.append((fits_cw.header.tone_frequency - f) / f)

            calibrated_i = np.array(calibrated_i)
            calibrated_q = np.array(calibrated_q)
            df = np.array(df)

            if plot_diagnostics:
                plt.figure(figsize=(8, 6))
                plt.plot(
                    iq_circle_fit.calibrated_i_array,
                    iq_circle_fit.calibrated_q_array,
                    label="Sweep Data",
                )
                plt.plot(
                    calibrated_i,
                    calibrated_q,
                    label="Timestream",
                    linestyle="none",
                    marker="o",
                )
                plt.title(
                    "Checking timestream tone is sitting on the IQ circle for "
                    + psd_label
                    + " data"
                )
                plt.show()

                plt.figure(figsize=(8, 6))
                for index in range(int(cw_data.n_timestreams)):
                    plt.plot(cw_data.timestream_time_data, df[index], label=index)
                plt.xlabel("Time (s)")
                plt.ylabel("df/f")
                plt.legend(title="Timestream number:")
                plt.title("Checking Timestream df/f for" + psd_label + " data")
                plt.show()

            # Get power spectral densities using scipy's periodogram function.
            psd_array = []
            if plot_diagnostics:
                plt.figure(figsize=(8, 6))
            for index in range(int(cw_data.n_timestreams)):
                (frequencies, psd) = scipy.signal.periodogram(
                    df[index], cw_data.sample_rate, scaling="density"
                )
                psd_array.append(psd)
                if plot_diagnostics:
                    plt.plot(frequencies, np.abs(psd))
            if plot_diagnostics:
                plt.plot(
                    frequencies, np.mean(psd_array, axis=0), color="k", label="Mean"
                )
                plt.xlabel("Frequency (Hz)")
                plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
                plt.xscale("log")
                plt.yscale("log")
                plt.title("Checking PSD data and mean value for " + psd_label + " data")
                plt.legend()
                plt.show()

            mean_psd_data_dict["Label"].append(psd_label)
            mean_psd_data_dict["Mean PSD Data"].append(np.mean(psd_array, axis=0))
            mean_psd_data_dict["Frequencies"].append(frequencies)

            print("Completed processing" + psd_label + " data")

    plt.figure(figsize=(8, 6))
    for count, mean_psd in enumerate(mean_psd_data_dict["Mean PSD Data"]):
        plt.plot(
            mean_psd_data_dict["Frequencies"][count],
            mean_psd,
            label=mean_psd_data_dict["Label"][count],
        )
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
