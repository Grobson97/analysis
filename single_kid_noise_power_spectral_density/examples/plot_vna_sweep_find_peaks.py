import os.path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.signal import savgol_filter

from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\mux1_v1_ANLw001\quad\20221128_Dark_Data_Prelim\VNA_Sweeps"
filename = "S21_Continuous_BTEMP0090_mK_POW0.0_dB_ATT10.0_dB.fits"
file_path = os.path.join(directory, filename)

fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(file_path=file_path)
vna_data = FitsDataUnpackService.unpack_sweep_data(fits_vna_sweep)
s21_magnitude = vna_data.get_iq_array()
s21_magnitude_db = 20 * np.log10(s21_magnitude)

plt.figure(figsize=(8, 6))
plt.plot(vna_data.frequency_array*1e-9, s21_magnitude_db)
plt.title(fits_vna_sweep.header.dut + " VNA Sweep with ~" + str(fits_vna_sweep.header.input_attenuation+11) + "dB")
plt.xlabel("Frequency (GHz)")
plt.ylabel("S21 Magnitude (dB)")
plt.savefig("..\\Figures\\" + fits_vna_sweep.header.dut + "_VNA_Sweep_" + str(fits_vna_sweep.header.input_attenuation).replace(".", "p") + "dB.png")
plt.show()

peaks, _ = find_peaks(x=-s21_magnitude_db, distance=50, prominence=2)

plt.figure(figsize=(8, 6))
plt.plot(vna_data.frequency_array*1e-9, s21_magnitude_db)
plt.plot(vna_data.frequency_array[peaks]*1e-9, s21_magnitude_db[peaks], "o", markerfacecolor="none", markersize=3, color="r")
plt.title("KIDs found = %i" % len(peaks))
plt.xlabel("Frequency (GHz)")
plt.ylabel("S21 Magnitude (dB)")
# plt.savefig("..\\Figures\\" + fits_vna_sweep.header.dut + "_VNA_Sweep_" + str(fits_vna_sweep.header.input_attenuation).replace(".", "p") + "dB.png")
plt.show()
