import os.path

import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools, iq_circle_fit_tools


def main():
    directory = r"..\\data"
    sweep_filename = "Sweep.fits"
    sweep_file_path = os.path.join(directory, sweep_filename)
    timestream_filename = "TS_200000_Hz_ON_RES_x010.fits"
    timestream_file_path = os.path.join(directory, timestream_filename)

    # Create instance of FitsVNAS21Sweep class:
    fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=sweep_file_path)
    sweep_data = FitsDataUnpackService.unpack_sweep_data(fits_sweep=fits_sweep)
    iq_array = sweep_data.real_s21_array + 1j * sweep_data.imag_s21_array

    # Create instance of FitsCW
    fits_cw = FitsFileLoadService.load_fits_cw_file(file_path=timestream_file_path)
    cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

    figure, axes = plt.subplots(1, 3, figsize=(10, 4))
    axes[0].plot(sweep_data.frequency_array * 1e-9, np.abs(iq_array))
    axes[0].set_xlabel("Frequency (GHz)")
    axes[0].set_ylabel("S21 Magnitude (Arb)")
    axes[1].plot(sweep_data.frequency_array * 1e-9, 20 * np.log10(np.abs(iq_array)))
    axes[1].set_xlabel("Frequency (GHz)")
    axes[1].set_ylabel("S21 Magnitude (dB)")
    axes[2].plot(sweep_data.real_s21_array, sweep_data.imag_s21_array)
    axes[2].set_xlabel("I (V)")
    axes[2].set_ylabel("Q (V)")
    axes[2].axis("equal")
    plt.tight_layout()
    plt.show()

    cable_delay = iq_circle_fit_tools.fit_cable_delay(
        frequency_array=sweep_data.frequency_array,
        complex_data_array=iq_array,
        amplitude_guess=np.sqrt(
            sweep_data.real_s21_array[0] ** 2 + sweep_data.imag_s21_array[0] ** 2
        ),
        tau_guess=80e-9,
        plot_graph=True,
    )

    # Remove cable delay
    cable_delay_values = cable_delay.calculate_delay_at_frequency(
        sweep_data.frequency_array
    )
    iq_array = iq_array / cable_delay_values

    i_array = np.real(iq_array)
    q_array = np.imag(iq_array)

    plt.figure(figsize=(8, 6))
    plt.plot(i_array, q_array)
    plt.xlabel("I (V)")
    plt.ylabel("Q (V)")
    plt.title("IQ circle after cable delay is removed.")
    plt.axis("equal")
    plt.tight_layout()
    plt.show()

    # Fit circle and find centre and radius:
    iq_circle_fit = iq_circle_fit_tools.circle_fit(i_array, q_array)
    i_array, q_array = iq_circle_fit.centre_and_normalise_iq(i_array, q_array)

    plt.figure(figsize=(8, 6))
    plt.plot(i_array, q_array)
    plt.xlabel("I (V)")
    plt.ylabel("Q (V)")
    plt.title("IQ circle after cable delay is removed and centred.")
    plt.axis("equal")
    plt.tight_layout()
    plt.show()

    resonance_index = iq_circle_fit_tools.find_resonance_index(i_array, q_array)
    # Find angle of resonance w.r.t. centre of circle:
    resonance_angle = np.angle(
        i_array[resonance_index] + 1j * q_array[resonance_index], deg=True
    )

    # Rotate IQ loop to put resonance at (-1, 0)
    rotated_iq_array = iq_circle_fit_tools.rotate_2d_matrix(
        np.array([i_array, q_array]), resonance_angle
    )
    i_array_rotated = rotated_iq_array[0, :]
    q_array_rotated = rotated_iq_array[1, :]
    phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)

    rotated_angle = resonance_angle
    # Adjust rotation slightly to get continues phase vs frequency
    while iq_circle_fit_tools.discontinuity_presence(phase_array, threshold=125):
        rotated_iq_array = iq_circle_fit_tools.rotate_2d_matrix(
            np.array([i_array_rotated, q_array_rotated]), 0.1
        )
        i_array_rotated = rotated_iq_array[0, :]
        q_array_rotated = rotated_iq_array[1, :]
        phase_array = np.angle(i_array_rotated + 1j * q_array_rotated, deg=True)
        rotated_angle += 0.1

    plt.figure(figsize=(8, 6))
    plt.plot(i_array, q_array)
    plt.xlabel("I (V)")
    plt.ylabel("Q (V)")
    plt.title("IQ circle after cable delay is removed, centred and rotated.")
    plt.axis("equal")
    plt.tight_layout()
    plt.show()

    # fit polynomial to map phase angle to frequency
    eighth = int(sweep_data.frequency_array.size / 16)
    polynomial_fit = iq_circle_fit_tools.fit_polynomial(
        phase_array[resonance_index - eighth : resonance_index + eighth],
        sweep_data.frequency_array[resonance_index - eighth : resonance_index + eighth],
        degree=10,
        plot_graph=True,
    )

    calibrated_i, calibrated_q = iq_circle_fit_tools.calibrate_iq_points_to_circle_fit(
        i=cw_data.timestream_i_data[0],
        q=cw_data.timestream_q_data[0],
        tone_frequency=fits_cw.header.tone_frequency,
        cable_delay=cable_delay,
        circle_fit=iq_circle_fit,
        rotation_angle=rotated_angle,
    )

    timestream_phase = np.angle(calibrated_i + 1j * calibrated_q, deg=True)
    f = polynomial_fit(timestream_phase)
    df = (fits_cw.header.tone_frequency - f) / f

    plt.figure(figsize=(8, 6))
    plt.plot(cw_data.timestream_time_data, df)
    plt.xlabel("Time (s)")
    plt.ylabel("df/f")
    plt.show()

    print(df)


if __name__ == "__main__":
    main()
