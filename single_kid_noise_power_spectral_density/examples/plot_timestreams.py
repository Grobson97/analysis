import os.path

import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools
from source.util.s_param_transfer_functions import SParamTransferFunctions


def main():
    time_streams_to_plot = (
        5  # Define integer number of timestreams to plot. Must be less than the total.
    )
    directory = r"..\\data"
    filename = "TS_2000_Hz_ON_RES_x010.fits"
    file_path = os.path.join(directory, filename)

    # Create instance of FitsVNAS21Sweep class:
    fits_cw = FitsFileLoadService.load_fits_cw_file(file_path=file_path)
    cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

    figure, axes = plt.subplots(2, 1, sharex=True, figsize=(10, 4))
    for i in range(time_streams_to_plot):
        axes[0].plot(
            cw_data.timestream_time_data,
            cw_data.timestream_i_data[i],
            linestyle="none",
            marker="o",
            markersize=2,
            label=str(i),
        )
        axes[1].plot(
            cw_data.timestream_time_data,
            cw_data.timestream_q_data[i],
            linestyle="none",
            marker="o",
            markersize=2,
            label=str(i),
        )
    axes[0].set_ylabel("I (V)")
    axes[1].set_xlabel("Time (s)")
    axes[1].set_ylabel("Q (V)")
    plt.tight_layout()
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
