# Device Analysis

This is a repository for the Detector group's analysis code.
* To the extent possible, commit only text-encoded files in order to minimize massive diffs and repo bloat
(e.g. commit LaTeX, not PDFs; markdown, not jupyter notebooks; scripts that make plots, not pngs; etc.). **Do not commit 
virtual environments!**

## Contents
* [Repository structure](#Repository-structure)
* [Setup](#Setup)
* [How to use](#How-to-use)
* [Best practice](#Best-practice)

## Repository structure

## Setup

## How to use

## Best practice

* Work on a separate branch to the master branch.
* Commits should be done regularly and in small chunks of changes with suitable descriptions.
