"""
Purpose:
Script will plot the S21 in dB of a fits vna sweep data file and the same plot showing how many are peaks (KIDS) are
picked up by the scipy find peaks function.

How to use:
Point to the desired directory and filename.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService

directory = r"..\data"
filename = "S21_Continuous_BTEMP0080_mK_POW0.0_dB_ATT30.0_dB_0_wide.fits"
file_path = os.path.join(directory, filename)

fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(file_path=file_path)
s21_magnitude = np.abs(fits_vna_sweep.data.get_iq_array())
s21_magnitude_db = 20 * np.log10(s21_magnitude)

fits_vna_sweep.header.print_attenuation_values()
total_attenuation = fits_vna_sweep.header.get_total_attenuation(
    total_gain=60.0, room_temperature_attenuation=32.0, cable_attenuation=15
)

plt.figure(figsize=(8, 6))
plt.plot(fits_vna_sweep.data.frequency_array * 1e-9, s21_magnitude_db)
plt.title(fits_vna_sweep.header.dut + f" VNA Sweep with ~ {total_attenuation}dB")
plt.xlabel("Frequency (GHz)")
plt.ylabel("S21 Magnitude (dB)")
plt.savefig(
    "..\\..\\Figures\\"
    + fits_vna_sweep.header.dut
    + f"_VNA_Sweep_{total_attenuation}dB".replace(".", "p")
    + "dB.png"
)
plt.show()

peaks, _ = find_peaks(x=-s21_magnitude_db, distance=50, prominence=2)

plt.figure(figsize=(8, 6))
plt.plot(vna_data.frequency_array * 1e-9, s21_magnitude_db)
plt.plot(
    vna_data.frequency_array[peaks] * 1e-9,
    s21_magnitude_db[peaks],
    "o",
    markerfacecolor="none",
    markersize=3,
    color="r",
)
plt.title("KIDs found = %i" % len(peaks))
plt.xlabel("Frequency (GHz)")
plt.ylabel("S21 Magnitude (dB)")
# plt.savefig("..\\..\\Figures\\" + fits_vna_sweep.header.dut + f"_VNA_Sweep_{total_attenuation}dB".replace(".", "p") + "dB.png")
plt.show()
