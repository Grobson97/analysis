"""
Purpose:
Script will plot the S21 in dB of various fits vna sweeps data files. If there are multiple files in one directory the
mean value at each frequency point will be used.
The number of directories defines how many different series are plotted.

How to use:
Load desired data into data folders, or point to the directories. Directories should only contain the data you wish to
plot. e.g. If you want to plot one vna sweep file, that should be the only file in the folder. If you want to average 3
repeats of the same measurement, the 3 data files need to be in the folder.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main():

    directory_list = [r"..\..\data_2", r"..\..\data_1"]
    directory_series_names = ["Cold", "Warm"]

    plt.figure(figsize=(10, 6))
    for count, directory in enumerate(directory_list):
        s21_magnitude_db_arrays = []
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            file_path = os.path.join(directory, filename)

            fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(
                file_path=file_path
            )
            s21_magnitude = np.abs(fits_vna_sweep.data.get_iq_array())
            s21_magnitude_db = 20 * np.log10(s21_magnitude)
            s21_magnitude_db_arrays.append(s21_magnitude_db)

        # Take mean s21 at each frequency.
        s21_magnitude_db = np.mean(np.array(s21_magnitude_db_arrays), axis=0)

        fits_vna_sweep.header.print_attenuation_values()
        total_attenuation = fits_vna_sweep.header.get_total_attenuation(
            total_gain=60.0, cable_attenuation=15
        )
        print(f"Total attenuation = {total_attenuation} dB")

        plt.plot(
            fits_vna_sweep.data.frequency_array * 1e-9,
            s21_magnitude_db,
            label=directory_series_names[count],
            alpha=0.6,
        )

    plt.title("Comparison of multiple VNA sweeps")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig(
        "..\\..\\Figures\\" + f"vna_sweep_comparison_of_{directory_series_names}"
    )
    plt.show()


if __name__ == "__main__":
    main()
