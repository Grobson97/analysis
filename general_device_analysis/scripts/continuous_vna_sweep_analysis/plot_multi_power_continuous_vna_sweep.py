"""
Purpose:
Script will plot the S21 in dB of fits vna sweep data files for the same device with different attenuations.

How to use:
Load desired data into data folder, or point to the directory. Directory should only contain the data you wish to plot.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main():
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SO-UK\AIG_SOUK_6INCH_HYB_290820231030_chip0\20230918_Dark_Data_Prelim\VNA_Sweeps"
    s21_magnitude_db_arrays = []
    frequency_arrays = []
    attenuation_array = []
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(
            file_path=file_path
        )
        s21_magnitude_db = 20 * np.log10(np.abs(fits_vna_sweep.data.get_iq_array()))
        s21_magnitude_db_arrays.append(s21_magnitude_db)
        frequency_arrays.append(fits_vna_sweep.data.frequency_array)
        attenuation_array.append(fits_vna_sweep.header.input_attenuation)

    plt.figure(figsize=(10, 6))
    for count, s21 in enumerate(s21_magnitude_db_arrays):
        plt.plot(
            frequency_arrays[count] * 1e-9, s21, label=f"{attenuation_array[count]} dB"
        )
    plt.title(f"Multi-power VNA sweeps of {fits_vna_sweep.header.dut}")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig(
        "..\\..\\Figures\\" + fits_vna_sweep.header.dut + "_multi_power_VNA_sweeps.png"
    )
    plt.show()


if __name__ == "__main__":
    main()
