"""
Purpose:
Script will plot the S21 in dB of a fits vna sweep data file. If there are multiple files in the directory the mean
value at each frequency point will be used.

How to use:
Load desired data into data folder, or point to the directory. Directory should only contain the data you wish to plot.
e.g. If you want to plot one vna sweep file, that should be the only file in the folder. If you want to average 3
repeats of the same measurement, the 3 data files need to be in the folder.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main():
    directory = r"..\..\data"
    s21_magnitude_db_arrays = []
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(
            file_path=file_path
        )
        s21_magnitude = np.abs(fits_vna_sweep.data.get_iq_array())
        s21_magnitude_db = 20 * np.log10(s21_magnitude)
        s21_magnitude_db_arrays.append(s21_magnitude_db)

    # Take mean s21 at each frequency.
    s21_magnitude_db = np.mean(np.array(s21_magnitude_db_arrays), axis=0)

    fits_vna_sweep.header.print_attenuation_values()
    total_attenuation = fits_vna_sweep.header.get_total_attenuation(
        total_gain=50.0, cable_attenuation=10
    )

    plt.figure(figsize=(10, 6))
    plt.plot(fits_vna_sweep.data.frequency_array * 1e-9, s21_magnitude_db)
    plt.title(
        fits_vna_sweep.header.dut
        + f" VNA Sweep with ~{total_attenuation} dB total attenuation"
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.savefig(
        "..\\..\\Figures\\"
        + fits_vna_sweep.header.dut
        + "_VNA_Sweep_"
        + str(fits_vna_sweep.header.input_attenuation).replace(".", "p")
        + "dB.png"
    )
    plt.show()


if __name__ == "__main__":
    main()
