"""
Purpose:
Script will plot the S21 in dB of a fits segmented vna sweep data file.

How to use:
Load desired data into data folder, or point to the directory.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main():

    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim22_simple_optical_v1\20240303_Dark_Data_Auto\VNA_Sweeps"
    filename = "S21_Segmented_BTEMP0240_mK_POW0.0_dB_ATT46.0_dB_1.fits"
    file_path = os.path.join(directory, filename)

    fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(file_path=file_path)
    s21_magnitude = fits_vna_sweep.data.get_iq_array()
    s21_magnitude_db = 20 * np.log10(s21_magnitude)

    number_of_tones = fits_vna_sweep.header.number_of_tones
    fits_vna_sweep.header.print_attenuation_values()
    total_attenuation = fits_vna_sweep.header.get_total_attenuation(
        total_gain=60.0, cable_attenuation=15
    )

    plt.figure(figsize=(10, 6))
    plt.plot(fits_vna_sweep.data.frequency_array * 1e-9, s21_magnitude_db)
    plt.title(
        fits_vna_sweep.header.dut
        + f": {number_of_tones} KID Segmented VNA Sweep with ~{total_attenuation} dB total attenuation"
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.savefig(
        "..\\..\\Figures\\"
        + fits_vna_sweep.header.dut
        + f"segmented_VNA_Sweep_{total_attenuation}".replace(".", "p")
        + "dB.png"
    )
    plt.show()


if __name__ == "__main__":
    main()
