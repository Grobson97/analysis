import math
import os.path

import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools
from source.util.s_param_transfer_functions import SParamTransferFunctions


def main():
    time_streams_to_plot = (
        50  # Define integer number of timestreams to plot. Must be less than the total.
    )
    directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Data\20230822_Dark_Data_Prelim\KID_K008\Set_Temperature_086_mK\Set_Attenuation_42dB_without_PT"

    for file_name in os.listdir(directory):
        if "TS" in file_name:
            timestream_file_path = os.path.join(directory, file_name)
            # Create instance of FitsCW
            fits_cw = FitsFileLoadService.load_fits_cw_file(
                file_path=timestream_file_path
            )
            cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

            figure, axes = plt.subplots(
                math.ceil(cw_data.n_timestreams / 5),
                5,
                sharex=True,
                sharey=True,
                figsize=(20, 10),
            )
            m = 0
            n = 0
            for i in range(cw_data.n_timestreams):
                axes[m][n].plot(
                    cw_data.timestream_time_data,
                    cw_data.timestream_i_data[i],
                    linestyle="none",
                    marker="o",
                    markersize=2,
                    label="I",
                )
                axes[m][n].plot(
                    cw_data.timestream_time_data,
                    cw_data.timestream_q_data[i],
                    linestyle="none",
                    marker="o",
                    markersize=2,
                    label="Q",
                )
                axes[m][n].set_title(f"TS Index: {i}")
                if n == 0:
                    axes[m][n].set_ylabel("Volts (V)")
                if m == math.ceil(cw_data.n_timestreams / 5) - 1:
                    axes[m][n].set_xlabel("Time (s)")

                n += 1
                if n > 4:
                    n = 0
                    m += 1

            plt.tight_layout()
            plt.legend()
            plt.show()


if __name__ == "__main__":
    main()
