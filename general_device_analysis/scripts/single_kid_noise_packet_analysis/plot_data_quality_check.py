import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main(
    plot_diagnostics=True,  # To see plots of fits, sweeps and all of the timestreams rather than just the mean psd.
    directory=r"..\..\data",
    # directory=r"C:\Users\c2047423\Transfer\20230405_Other_Data_Auto\KID_K012\Set_Temperature_090_mK\Set_Attenuation_32dB",
    save_figures=True,
):

    sweep_filename = "Sweep.fits"  # This shouldn't change.
    sweep_file_path = os.path.join(directory, sweep_filename)

    ########################################################################################################################

    # Create instance of FitsVNAS21Sweep class:
    fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=sweep_file_path)
    sweep_data = FitsDataUnpackService.unpack_sweep_data(fits_sweep=fits_sweep)
    sweep_iq_array = sweep_data.i + 1j * sweep_data.q

    figure, axes = plt.subplots(1, 2, figsize=(10, 4))
    axes[0].plot(sweep_data.frequency_array * 1e-9, np.abs(sweep_iq_array))
    axes[0].set_xlabel("Frequency (GHz)")
    axes[0].set_ylabel("S21 Magnitude (Arb)")
    axes[1].plot(
        sweep_data.frequency_array * 1e-9, 20 * np.log10(np.abs(sweep_iq_array))
    )
    axes[1].set_xlabel("Frequency (GHz)")
    axes[1].set_ylabel("S21 Magnitude (dB)")
    plt.tight_layout()
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"{fits_sweep.header.dut}_{fits_sweep.header.tone_name}_sweep_data"
        )
    plt.show()

    # Loop to find the two sample frequencies to identify which is high res and which is low res.
    sample_frequencies = []
    for file_name in os.listdir(directory):
        if "TS" in file_name:
            string_search_result = re.search("TS_(.*)_Hz", file_name)
            sample_frequency = float(string_search_result.group(1))
            if sample_frequency not in sample_frequencies:
                sample_frequencies.append(int(sample_frequency))

    for file_name in os.listdir(directory):
        if "TS" in file_name:
            timestream_file_path = os.path.join(directory, file_name)
            # Create instance of FitsCW
            fits_cw = FitsFileLoadService.load_fits_cw_file(
                file_path=timestream_file_path
            )
            cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

            psd_label = ""
            if str(max(sample_frequencies)) in file_name:
                psd_label += "High Frequency, "
            else:
                psd_label += "Low Frequency, "
            if "ON" in file_name:
                psd_label += "On Res"
            else:
                psd_label += "Off Res"

            plt.figure(figsize=(8, 6))
            plt.plot(
                sweep_data.i,
                sweep_data.q,
                label="Sweep Data",
            )
            for index in range(int(cw_data.n_timestreams)):
                plt.plot(
                    cw_data.timestream_i_data[index][::100],
                    cw_data.timestream_q_data[index][::100],
                    label=f"Timestream {index}",
                    linestyle="none",
                    marker="o",
                )
            plt.legend()
            plt.xlabel("I")
            plt.ylabel("Q")
            plt.axis("equal")
            plt.title(
                "Checking timestream tone is sitting on the IQ circle for "
                + psd_label
                + " data"
            )
            plt.show()


if __name__ == "__main__":
    main()
