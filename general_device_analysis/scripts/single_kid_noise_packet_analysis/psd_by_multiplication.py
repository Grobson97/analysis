import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy

from scipy.fft import fft, fftfreq, fftshift

from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import noise_multiplication_method_tools
from source.util import lekid_analysis_tools
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim20_mm_loss\20240110_Optical_Data_Auto\KID_K005\Set_Temperature_240_mK\Set_Attenuation_42dB",
    save_figures=True,
    plot_low_frequencies=False,
):

    try:
        noise_packet_psd = (
            noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
                directory=directory,
                down_sample_psd=True,
                down_sample_bins=200,
                plot_sweep_figure=True,
                save_figures=save_figures,
                save_to_directory="..\\..\\Figures",
            )
        )
    except:
        print(f"Error with getting PSD for noise packet")
    else:
        print("PSD complete")

    plt.figure(figsize=(10, 6))

    plt.plot(
        noise_packet_psd.on_res_low_frequencies,
        noise_packet_psd.psd_on_res_low,
        label="On Res Low Freq",
    )
    plt.plot(
        noise_packet_psd.on_res_high_frequencies,
        noise_packet_psd.psd_on_res_high,
        label="On Res High Freq",
    )
    plt.plot(
        noise_packet_psd.off_res_low_frequencies,
        noise_packet_psd.psd_off_res_low,
        label="Off Res Low Freq",
    )
    plt.plot(
        noise_packet_psd.off_res_high_frequencies,
        noise_packet_psd.psd_off_res_high,
        label="Off Res High Freq",
    )
    plt.plot(
        noise_packet_psd.psd_on_res_combined_frequencies,
        noise_packet_psd.psd_on_res_combined,
        label="Combined On Res",
    )
    plt.plot(
        noise_packet_psd.psd_off_res_combined_frequencies,
        noise_packet_psd.psd_off_res_combined,
        label="Combined Off Res",
    )

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()
    plt.title(
        f"Noise PSD for {noise_packet_psd.dut} {noise_packet_psd.tone_name} at T={noise_packet_psd.temperature}K with ~"
        f"{noise_packet_psd.input_attenuation}dB variable attenuation"
    )
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Noise_PSD_for_{noise_packet_psd.dut}_{noise_packet_psd.tone_name}_{noise_packet_psd.temperature}K_"
            f"{noise_packet_psd.input_attenuation}dB.png"
        )
    plt.show()

    figure, axes = plt.subplots(1, 2, sharex=True, sharey=False, figsize=(14, 10))
    ax_inside = inset_axes(axes[0], width="30%", height="30%")
    axes[0].plot(
        noise_packet_psd.psd_on_res_combined_frequencies,
        noise_packet_psd.psd_on_res_combined,
        label="Combined On Res",
    )
    axes[0].plot(
        noise_packet_psd.psd_off_res_combined_frequencies,
        noise_packet_psd.psd_off_res_combined,
        label="Combined Off Res",
    )
    ax_inside.plot(
        noise_packet_psd.noise_sweep.sweep_data.i,
        noise_packet_psd.noise_sweep.sweep_data.q,
    )
    ax_inside.set_ylabel("I")
    ax_inside.set_xlabel("Q")
    ax_inside.axis("equal")

    axes[0].set_xlabel("Frequency (Hz)")
    axes[0].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
    axes[0].set_xscale("log")
    axes[0].set_yscale("log")
    plt.show()


if __name__ == "__main__":
    main()
