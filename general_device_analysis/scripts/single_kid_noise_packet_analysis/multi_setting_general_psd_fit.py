import math
import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy

from skimage.measure import block_reduce
from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import (
    noise_multiplication_method_tools,
    resonator_noise_analysis_tools,
)
from scipy.fft import fft, fftfreq, fftshift


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Data\20230830_Dark_Data_Prelim\KID_K018",
    save_figures=False,
    plot_low_frequencies=False,
):

    temperature_directories = os.listdir(directory)
    # Logic to get all possible power directories (in case different powers used at different temperatures).
    power_directories = []
    for temperature_directory in temperature_directories:
        power_directory_list = os.listdir(
            os.path.join(directory, temperature_directory)
        )

        for power_directory in power_directory_list:
            if power_directory not in power_directories:
                power_directories.append(power_directory)

    noise_packet_list = []

    for temperature_directory in temperature_directories:
        temperature = float(temperature_directory[16:19])
        for power_directory in power_directories:
            power = float(power_directory[16:-2])
            path = os.path.join(directory, temperature_directory, power_directory)

            print(f"Getting data for {temperature}mK, {power}dB")

            try:
                noise_packet_psd = (
                    noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
                        directory=path,
                        down_sample_psd=True,
                        down_sample_bins=100,
                        plot_sweep_figure=False,
                        save_figures=False,
                    )
                )
                if not noise_packet_psd.is_bifurcated:
                    noise_packet_list.append(noise_packet_psd)
            except:
                print(
                    f"Error with getting PSD for noise data with T={temperature}mK, P={power}dB"
                )
                print(f"Possible error: No data available")
            else:
                print("PSD complete")
                # noise_packet_psd.plot_psd()

    temperature_array = []
    input_attenuation_array = []
    roll_off_lifetime_array = []

    n_rows = math.ceil(len(noise_packet_list) / 4)
    m = 0
    n = 0

    figure, axes = plt.subplots(nrows=n_rows, ncols=4, sharex=True, figsize=(14, 10))
    for count, noise_packet_psd in enumerate(noise_packet_list):

        (
            processed_frequencies,
            processed_psd,
        ) = resonator_noise_analysis_tools.remove_psd_peaks(
            noise_packet_psd.psd_on_res_combined_frequencies,
            noise_packet_psd.psd_on_res_combined,
            max_iterations=5,
            show_plots=False,
        )

        # Min and max frequencies to fit data between:
        fit_min_frequency = 1.0
        fit_max_frequency = 9e4
        fit_dictionary = resonator_noise_analysis_tools.fit_dark_general_ffs_psd(
            frequency_array=processed_frequencies,
            fit_min_frequency=fit_min_frequency,
            fit_max_frequency=fit_max_frequency,
            data_array=processed_psd,
            a_guess=1e-18,
            b_guess=1e-15,
            n_guess=0.5,
            c_guess=1e-19,
            roll_off_lifetime_guess=1.0e-4,
            plot_graph=False,
            plot_title=f"General FFS PSD fit for {noise_packet_psd.dut} at {noise_packet_psd.temperature}K and"
            f"{noise_packet_psd.input_attenuation}dB",
        )

        # Append fit results and noise packet settings:
        temperature_array.append(noise_packet_psd.temperature)
        input_attenuation_array.append(noise_packet_psd.input_attenuation)
        roll_off_lifetime_array.append(fit_dictionary["roll_off_lifetime"])

        # Plot fit on axes subplot:
        mask = np.where(
            (processed_frequencies > fit_min_frequency)
            & (processed_frequencies < fit_max_frequency)
        )
        fit_frequency_array = processed_frequencies[mask]
        f_array = np.linspace(
            start=fit_frequency_array[0],
            stop=fit_frequency_array[-1],
            num=10000,
        )
        psd_fit = resonator_noise_analysis_tools.general_ffs_psd(
            frequency_array=f_array,
            a=fit_dictionary["a"][0],
            b=fit_dictionary["b"][0],
            n=fit_dictionary["n"][0],
            c=fit_dictionary["c"][0],
            roll_off_lifetime=fit_dictionary["roll_off_lifetime"][0],
        )

        axes[m][n].plot(
            processed_frequencies,
            processed_psd,
            linestyle="-",
            linewidth=2,
            color="r",
            label="On-Res Data",
        )
        axes[m][n].plot(
            noise_packet_psd.psd_off_res_combined_frequencies,
            noise_packet_psd.psd_off_res_combined,
            linestyle="-",
            linewidth=2,
            color="b",
            label="Off-Res Data",
        )
        axes[m][n].plot(
            f_array,
            psd_fit,
            linestyle="-",
            color="k",
            label="Fit",
        )
        axes[m][n].set_title(
            f"T={noise_packet_psd.temperature * 1e3}mK, {noise_packet_psd.input_attenuation}dB"
        )
        axes[m][n].set_xscale("log")
        axes[m][n].set_yscale("log")
        if n == 0:
            axes[m][n].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")

        if m == n_rows - 1:
            axes[m][n].set_xlabel("Frequency (Hz)")

        n += 1
        if n == 4:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    remaining_subplots = (4 * n_rows) - len(noise_packet_list)
    if remaining_subplots > 0:
        for remaining_column in range(remaining_subplots):
            axes[m][n + remaining_column].remove()

    handles, labels = axes[0][0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="lower center",
        ncol=3,
    )
    plt.tight_layout(rect=(0, 0.1, 1, 1))
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_PSD_fit_subplots_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}"
        )
    plt.show()

    # Convert to numpy arrays:
    temperature_array = np.array(temperature_array)
    input_attenuation_array = np.array(input_attenuation_array)
    roll_off_lifetime_array = np.array(roll_off_lifetime_array)

    ####################################################################################################################

    # plot lifetime as function of temperature and power:

    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(input_attenuation_array)

    plt.figure(figsize=(8, 6))
    for count, unique_power in enumerate(unique_powers):
        series_indices = np.where(input_attenuation_array == unique_power)[0]
        ordered_indices = np.argsort(
            temperature_array[series_indices]
        )  # Order series indices in ascending temperature
        series_indices = series_indices[ordered_indices]
        plt.errorbar(
            temperature_array[series_indices] * 1e3,
            roll_off_lifetime_array[series_indices][:, 0] * 1e6,
            yerr=roll_off_lifetime_array[series_indices][:, 1] * 1e6,
            linestyle="--",
            marker="o",
            label=f"{unique_power}dB",
        )
    plt.legend()
    plt.xlabel("Temperature (mK)")
    plt.ylabel("Roll-off lifetime ($\mu$s)")
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_PSD_fit_tau_vs_temperature_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}"
        )
    plt.show()

    plt.figure(figsize=(8, 6))
    for count, unique_temperature in enumerate(unique_temperatures):
        series_indices = np.where(temperature_array == unique_temperature)[0]
        ordered_indices = np.argsort(
            input_attenuation_array[series_indices]
        )  # Order series indices inascending power
        series_indices = series_indices[ordered_indices]
        plt.errorbar(
            input_attenuation_array[series_indices],
            roll_off_lifetime_array[series_indices][:, 0] * 1e6,
            yerr=roll_off_lifetime_array[series_indices][:, 1] * 1e6,
            linestyle="--",
            marker="o",
            label=f"{unique_temperature*1e3}mK",
        )
    plt.legend()
    plt.xlabel("Input attenuation (dB)")
    plt.ylabel("Roll-off lifetime ($\mu$s)")
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_PSD_fit_tau_vs_power_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}"
        )
    plt.show()


if __name__ == "__main__":
    main()
