import math
import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy

from source.util import noise_multiplication_method_tools
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Data\20230831_Dark_Data_Prelim\KID_K299",
    save_figures=True,
):

    temperature_directories = os.listdir(directory)

    # Logic to get all possible power directories (in case different powers used at different temperatures).
    power_directories = []
    for temperature_directory in temperature_directories:
        power_directory_list = os.listdir(
            os.path.join(directory, temperature_directory)
        )

        for power_directory in power_directory_list:
            if power_directory not in power_directories:
                power_directories.append(power_directory)

    noise_packet_list = []
    temperature_array = []
    input_attenuation_array = []

    for temperature_directory in temperature_directories:
        temperature = float(temperature_directory[16:19])
        for power_directory in power_directories:
            power = float(power_directory[16:-2])
            path = os.path.join(directory, temperature_directory, power_directory)

            print(f"Getting data for {temperature}mK, {power}dB")

            try:
                noise_packet_psd = (
                    noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
                        directory=path,
                        down_sample_psd=True,
                        down_sample_bins=100,
                        plot_sweep_figure=False,
                        save_figures=False,
                    )
                )
                noise_packet_list.append(noise_packet_psd)
                temperature_array.append(noise_packet_psd.temperature)
                input_attenuation_array.append(noise_packet_psd.input_attenuation)
            except:
                print(
                    f"Error with getting PSD for noise data with T={temperature}mK, P={power}dB"
                )
                print(f"Possible error: No data available")
            else:
                print("PSD complete")
                # noise_packet_psd.plot_psd()

    # convert to numpy arrays:
    temperature_array = np.array(temperature_array)
    input_attenuation_array = np.array(input_attenuation_array)
    noise_packet_list = np.array(noise_packet_list)

    colors = plt.cm.rainbow(np.linspace(0, 1, len(noise_packet_list)))

    plt.figure(figsize=(10, 6))
    for count, noise_packet_psd in enumerate(noise_packet_list):
        line_style = "-"
        if noise_packet_psd.is_bifurcated:
            line_style = ":"
        plt.plot(
            noise_packet_psd.psd_on_res_combined_frequencies,
            noise_packet_psd.psd_on_res_combined,
            label=f"{noise_packet_psd.temperature}mK, {noise_packet_psd.input_attenuation}dB",
            color=colors[count],
            linestyle=line_style,
        )

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()
    plt.title(
        f"Noise PSD  at different temperatures and powers for {noise_packet_list[0].dut} {noise_packet_list[0].tone_name}"
    )
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_Noise_PSD_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}"
        )
    plt.show()

    ####################################################################################################################

    unique_temperatures = np.unique(temperature_array)
    unique_temperatures = np.sort(unique_temperatures)
    unique_attenuations = np.unique(input_attenuation_array)
    unique_attenuations = np.sort(unique_attenuations)

    # Plot noise at each temperature on subplots:
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    # Assign a color to each unique attenuation.
    colors = plt.cm.rainbow(np.linspace(0, 1, len(unique_attenuations)))
    all_handles = []
    all_labels = []

    n_rows = math.ceil(unique_temperatures.size / 2)
    figure, axes = plt.subplots(
        2,
        sharex=True,
        sharey=False,
        figsize=(14, 10),
    )

    for unique_temperature in unique_temperatures:

        # Get indices referring to each unique power to plot data series with same input attenuation.
        series_indices = np.where(temperature_array == unique_temperature)[0].tolist()

        # Add IQ inset
        ax_inside = inset_axes(axes[m][n], width=1.0, height=1.0)

        # Go through each noise packet at the current temperature and plot each noise associated with an attenuation:
        for count, noise_packet_psd in enumerate(noise_packet_list[series_indices]):
            line_style = "-"
            if noise_packet_psd.is_bifurcated:
                line_style = ":"
            axes[m][n].plot(
                noise_packet_psd.psd_on_res_combined_frequencies,
                noise_packet_psd.psd_on_res_combined,
                label=f"{noise_packet_psd.input_attenuation}dB",
                linestyle=line_style,
                color=colors[
                    np.where(unique_attenuations == noise_packet_psd.input_attenuation)[
                        0
                    ]
                ],
            )

            # plot IQ circle on inset axis:
            ax_inside.plot(
                np.real(noise_packet_psd.noise_sweep.sweep_iq_array),
                np.imag(noise_packet_psd.noise_sweep.sweep_iq_array),
                label=f"{noise_packet_psd.input_attenuation}dB",
                color=colors[
                    np.where(unique_attenuations == noise_packet_psd.input_attenuation)[
                        0
                    ]
                ],
            )

        ax_inside.set_ylabel("I")
        ax_inside.set_xlabel("Q")
        ax_inside.axis("equal")

        # Check if handles and labels not already in list, if not add them so they will be in the legend.
        handles, labels = ax_inside.get_legend_handles_labels()
        for count, label in enumerate(labels):
            if label not in all_labels:
                all_labels.append(label)
                all_handles.append(handles[count])

        axes[m][n].set_title(f"T={unique_temperature * 1e3}mK")
        axes[m][n].set_xscale("log")
        axes[m][n].set_yscale("log")
        if n == 0:
            axes[m][n].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
        if m == math.ceil(unique_temperatures.size / 2) - 1:
            axes[m][n].set_xlabel("Frequency (Hz)")

        n += 1
        if n == 2:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    remaining_subplots = (2 * n_rows) - unique_temperatures.size
    if remaining_subplots > 0:
        for remaining_column in range(remaining_subplots):
            axes[m][n + remaining_column].remove()

    # sort labels so they're in order
    label_values = np.char.strip(np.array(all_labels), "dB").astype(float)
    label_order = np.argsort(label_values)
    all_handles = np.array(all_handles)[label_order]
    all_labels = np.array(all_labels)[label_order]

    figure.legend(
        handles=all_handles.tolist(),
        labels=all_labels.tolist(),
        loc="center right",
        ncol=1,
        bbox_to_anchor=(1.1, 0.5),
    )

    plt.tight_layout()
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_Noise_PSD_temp_subplots_for_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}",
            bbox_inches="tight",
        )
    plt.show()

    # Plot noise at each temperature on subplots:
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    # Assign a color to each unique temperature.
    colors = plt.cm.rainbow(np.linspace(0, 1, len(unique_temperatures)))
    all_handles = []
    all_labels = []

    figure, axes = plt.subplots(
        math.ceil(unique_attenuations.size / 2),
        2,
        sharex=True,
        sharey=False,
        figsize=(14, 10),
    )
    for unique_attenuation in unique_attenuations:

        # Get indices referring to each unique power to plot data series with same input attenuation.
        series_indices = np.where(input_attenuation_array == unique_attenuation)[
            0
        ].tolist()

        # Add IQ inset
        ax_inside = inset_axes(axes[m][n], width=1.0, height=1.0)

        # Go through each noise packet at the current temperature and plot each noise associated with an attenuation:
        for count, noise_packet_psd in enumerate(noise_packet_list[series_indices]):
            line_style = "-"
            if noise_packet_psd.is_bifurcated:
                line_style = ":"
            axes[m][n].plot(
                noise_packet_psd.psd_on_res_combined_frequencies,
                noise_packet_psd.psd_on_res_combined,
                label=f"{noise_packet_psd.temperature * 1e3}mK",
                linestyle=line_style,
                color=colors[
                    np.where(unique_temperatures == noise_packet_psd.temperature)[0]
                ],
            )

            # plot IQ circle on inset axis:
            ax_inside.plot(
                np.real(noise_packet_psd.noise_sweep.sweep_iq_array),
                np.imag(noise_packet_psd.noise_sweep.sweep_iq_array),
                label=f"{noise_packet_psd.temperature * 1e3}mK",
                color=colors[
                    np.where(unique_temperatures == noise_packet_psd.temperature)[0]
                ],
            )

        ax_inside.set_ylabel("I")
        ax_inside.set_xlabel("Q")
        ax_inside.axis("equal")

        # Check if handles and labels not already in list, if not add them so they will be in the legend.
        handles, labels = ax_inside.get_legend_handles_labels()
        for count, label in enumerate(labels):
            if label not in all_labels:
                all_labels.append(label)
                all_handles.append(handles[count])

        axes[m][n].set_title(f"{unique_attenuation}dB")
        axes[m][n].set_xscale("log")
        axes[m][n].set_yscale("log")
        if n == 0:
            axes[m][n].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
        if m == math.ceil(unique_attenuations.size / 2) - 1:
            axes[m][n].set_xlabel("Frequency (Hz)")

        n += 1
        if n == 2:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    remaining_subplots = unique_attenuations.size % 2
    if remaining_subplots > 0:
        for remaining_column in range(remaining_subplots):
            axes[m][n + remaining_column].remove()

    # sort labels so they're in order
    label_values = np.char.strip(np.array(all_labels), "mK").astype(float)
    label_order = np.argsort(label_values)
    all_handles = np.array(all_handles)[label_order]
    all_labels = np.array(all_labels)[label_order]

    figure.legend(
        handles=all_handles.tolist(),
        labels=all_labels.tolist(),
        loc="center right",
        ncol=1,
        bbox_to_anchor=(1.1, 0.5),
    )

    plt.tight_layout()
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Multi_setting_Noise_PSD_power_subplots_for_{noise_packet_list[0].dut}_{noise_packet_list[0].tone_name}",
            bbox_inches="tight",
        )
    plt.show()


if __name__ == "__main__":
    main()
