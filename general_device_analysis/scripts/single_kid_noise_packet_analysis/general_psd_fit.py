import os

import numpy as np
import scipy
from matplotlib import pyplot as plt

from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import (
    noise_multiplication_method_tools,
    lekid_analysis_tools,
    resonator_noise_analysis_tools,
)
from scipy.fft import fft, fftfreq, fftshift


def main(
    plot_diagnostics=False,  # To see plots of fits, sweeps and all of the timestreams rather than just the mean psd.
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Data\20230830_Dark_Data_Prelim\KID_K320\Set_Temperature_300_mK\Set_Attenuation_46dB",
    save_figures=True,
):
    noise_packet_psd = (
        noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
            directory=directory,
            down_sample_psd=True,
            down_sample_bins=100,
        )
    )
    print("PSD complete")

    (
        processed_frequencies,
        processed_psd,
    ) = resonator_noise_analysis_tools.remove_psd_peaks(
        noise_packet_psd.psd_on_res_combined_frequencies,
        noise_packet_psd.psd_on_res_combined,
        max_iterations=5,
        show_plots=False,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(processed_frequencies, processed_psd)
    plt.xscale("log")
    plt.yscale("log")
    plt.show()

    fit_dictionary = resonator_noise_analysis_tools.fit_dark_general_ffs_psd(
        frequency_array=processed_frequencies,
        fit_min_frequency=1,
        fit_max_frequency=9e4,
        data_array=processed_psd,
        a_guess=1e-18,
        b_guess=1e-15,
        n_guess=0.5,
        c_guess=1e-19,
        roll_off_lifetime_guess=1.0e-4,
        plot_graph=True,
        plot_title="",
    )


if __name__ == "__main__":
    main()
