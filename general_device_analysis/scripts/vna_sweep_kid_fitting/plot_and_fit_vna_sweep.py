import os.path
import numpy as np

from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\mux1_v1_ANLw001\sparse\20221128_Dark_Data_Prelim\VNA_Sweeps"
filename = "S21_Continuous_BTEMP0090_mK_POW0.0_dB_ATT10.0_dB_kid2.fits"
file_path = os.path.join(directory, filename)

fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(file_path=file_path)
vna_data = FitsDataUnpackService.unpack_sweep_data(fits_vna_sweep)
s21_magnitude = vna_data.get_iq_array()
s21_magnitude_db = 20 * np.log10(s21_magnitude)

minimum_index = np.argmin(s21_magnitude)
s21_array = s21_magnitude
frequency_array = vna_data.frequency_array

non_linear_result_dict = lekid_analysis_tools.fit_non_linear_mag_s21(
    frequency_array=frequency_array,
    data_array=s21_array,
    qr_guess=1e4,
    a_guess=0.7,
    normalise=True,
    plot_db=True,
    plot_title="Non linear fit",
    plot_graph=True,
)

skewed_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
    frequency_array=frequency_array,
    data_array=s21_array,
    qc_guess=1e5,
    f0_guess=2.1673e9,
    normalise=True,
    plot_db=True,
    plot_title="Skewed (Pete fitter) fit",
    plot_graph=True,
)
