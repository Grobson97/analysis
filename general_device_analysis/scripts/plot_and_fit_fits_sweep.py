import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools
from source.util.s_param_transfer_functions import SParamTransferFunctions


def main():
    directory = r"..\\data_2"
    filename = "Sweep.fits"
    file_path = directory + "\\" + filename

    # Create instance of FitsVNAS21Sweep class:
    fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=file_path)
    sweep_data = FitsDataUnpackService.unpack_sweep_data(fits_sweep=fits_sweep)
    s21_magnitude_array = SParamTransferFunctions.complex_to_magnitude(
        sweep_data.i, sweep_data.q
    )

    figure, axes = plt.subplots(1, 3, figsize=(10, 4))
    axes[0].plot(sweep_data.frequency_array * 1e-9, s21_magnitude_array)
    axes[0].set_xlabel("Frequency (GHz)")
    axes[0].set_ylabel("S21 Magnitude (Arb)")
    axes[1].plot(sweep_data.frequency_array * 1e-9, 20 * np.log10(s21_magnitude_array))
    axes[1].set_xlabel("Frequency (GHz)")
    axes[1].set_ylabel("S21 Magnitude (dB)")
    axes[2].plot(sweep_data.i, sweep_data.q)
    axes[2].set_xlabel("I (V)")
    axes[2].set_ylabel("Q (V)")
    axes[2].axis("equal")
    plt.tight_layout()
    plt.show()

    non_linear_result_dict = lekid_analysis_tools.fit_non_linear_mag_s21(
        frequency_array=sweep_data.frequency_array,
        data_array=s21_magnitude_array**2,
        qr_guess=1e5,
        a_guess=0.7,
        normalise=True,
        plot_db=True,
        plot_title="Non linear fit",
        plot_graph=True,
    )

    skewed_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
        frequency_array=sweep_data.frequency_array,
        data_array=s21_magnitude_array,
        qc_guess=1e5,
        f0_guess=2e9,
        normalise=True,
        plot_db=True,
        plot_title="Skewed (Pete fitter) fit",
        plot_graph=True,
    )


if __name__ == "__main__":
    main()
