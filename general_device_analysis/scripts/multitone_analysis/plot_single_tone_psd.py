import os.path
import re

import numpy as np
import matplotlib.pyplot as plt

from source.models.noise.noise_sweep import NoiseSweep
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.services.hidfmux_pkl_file_load_service import HidfmuxFileLoadService
from source.util import noise_multiplication_method_tools
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Ice_board_data\140mK_0p001_amp",
    timestream_file_name="noise_timestream_K018.pkl",
    multisweep_file_name="res_detail_multi_sweep.pkl",
    dut="slim_v2_anlw001_ppcaps",
    tone_to_plot="K018",
    temperature=0.14,
    save_figures=False,
):

    multi_sweep_path = os.path.join(directory, multisweep_file_name)
    timestream_path = os.path.join(directory, timestream_file_name)

    ########################################################################################################################

    # Get sweep data for KID:
    multisweep = HidfmuxFileLoadService.load_pkl_multi_sweep_file(multi_sweep_path)
    sweep_data = multisweep.get_tone_sweep_data(tone=tone_to_plot)
    noise_sweep = NoiseSweep(
        dut=dut,
        tone_name=tone_to_plot,
        temperature=temperature,
        input_attenuation=multisweep.get_tone_bias_power(tone=tone_to_plot, db=True),
        sweep_data=sweep_data,
    )
    noise_sweep.plot_sweep(save_figure=save_figures)
    sweep_iq_array = sweep_data.i + 1j * sweep_data.q

    # *******************************************************************************************************************

    # Load timestream data:

    (
        on_res_timestream,
        off_res_timestream,
    ) = HidfmuxFileLoadService.load_pkl_timestream_file(
        timestream_file_path=timestream_path,
        multisweep=multisweep,
        tone_name=tone_to_plot,
    )

    noise_packet_psd = noise_multiplication_method_tools.get_simple_noise_packet_psd(
        noise_sweep=noise_sweep,
        on_res_timestream=on_res_timestream,
        off_res_timestream=off_res_timestream,
        down_sample_psd=True,
        down_sample_bins=100,
    )

    figure, axes = plt.subplots(1, 2, sharex=True, sharey=False, figsize=(14, 10))
    ax_inside = inset_axes(axes[0], width="30%", height="30%")
    axes[0].plot(
        noise_packet_psd.on_res_frequencies,
        noise_packet_psd.on_res_psd,
        label="On Res",
    )
    axes[0].plot(
        noise_packet_psd.off_res_frequencies,
        noise_packet_psd.off_res_psd,
        label="Combined Off Res",
    )
    ax_inside.plot(
        np.real(noise_packet_psd.noise_sweep.sweep_data.i),
        np.imag(noise_packet_psd.noise_sweep.sweep_data.q),
    )
    ax_inside.set_ylabel("I")
    ax_inside.set_xlabel("Q")
    ax_inside.axis("equal")

    axes[0].set_xlabel("Frequency (Hz)")
    axes[0].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
    axes[0].set_xscale("log")
    axes[0].set_yscale("log")
    plt.show()


if __name__ == "__main__":
    main()
