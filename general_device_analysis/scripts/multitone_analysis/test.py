import source.util.general_analysis_tools
import numpy as np


def get_clash_indices(f0_array: np.ndarray, threshold_spacing: float) -> list:
    """
    Function to find the indices of resonant frequencies that are within a given threshold of another f0.

    :param f0_array: 1D Array of f0 values.
    :param threshold_spacing: Minimum acceptable separation between f0's. Spacing's below this are flagged as clashes.
    """

    f0_spacings = np.diff(f0_array)
    print(f0_spacings)
    clash_indices = np.where(f0_spacings < threshold_spacing)[0]
    print(clash_indices)

    # Copy clash index array to insert extra clash indices into:
    full_clash_indices = clash_indices.copy()
    extras_added = 0  # Counter to track the number of inserts.
    for count, clash_index in enumerate(
        clash_indices
    ):  # For loop to add the other index of a clashed pair.
        if clash_index + 1 not in clash_indices:
            full_clash_indices = np.insert(
                full_clash_indices, count + 1 + extras_added, clash_index + 1
            )
            extras_added += 1

    print(f"Number of clashing f0's found: {len(full_clash_indices)}")
    print(f"Number of clashes: {int(len(full_clash_indices) / 2)}")

    return full_clash_indices


f0_array = np.array([10, 20, 30, 40, 50, 55, 60, 70, 80, 85, 90, 100, 110])
get_clash_indices(f0_array=f0_array, threshold_spacing=6)
