import os.path
import re

import numpy as np
import matplotlib.pyplot as plt

from source.models.noise.noise_sweep import NoiseSweep
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.services.hidfmux_pkl_file_load_service import HidfmuxFileLoadService


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Ice_board_data\140mK_0p005_amp",
    timestream_file_name="noise_timestream_K171.pkl",
    multisweep_file_name="res_detail_multi_sweep.pkl",
    dut="slim_v2_anlw001_idc_stepdown",
    tone_to_plot="K171",
    temperature=0.140,
    save_figures=True,
):

    multi_sweep_path = os.path.join(directory, multisweep_file_name)
    timestream_path = os.path.join(directory, timestream_file_name)

    multisweep = HidfmuxFileLoadService.load_pkl_multi_sweep_file(multi_sweep_path)

    ########################################################################################################################

    # Get sweep data for KID:
    sweep_data = multisweep.get_tone_sweep_data(tone=tone_to_plot)
    noise_sweep = NoiseSweep(
        dut=dut,
        tone_name=tone_to_plot,
        temperature=temperature,
        input_attenuation=multisweep.get_tone_bias_power(tone=tone_to_plot),
        sweep_data=sweep_data,
    )
    noise_sweep.plot_sweep()

    # *******************************************************************************************************************

    # Load timestream data:

    (
        on_res_timestream,
        off_res_timestream,
    ) = HidfmuxFileLoadService.load_pkl_timestream_file(
        timestream_file_path=timestream_path,
        multisweep=multisweep,
        tone_name=tone_to_plot,
    )

    plt.figure(figsize=(8, 6))
    plt.plot(
        sweep_data.i,
        sweep_data.q,
        label="Sweep Data",
    )
    plt.plot(
        on_res_timestream.timestream_i_data[::100],
        on_res_timestream.timestream_q_data[::100],
        label=f"On-Res",
        linestyle="none",
        marker="o",
    )
    plt.plot(
        off_res_timestream.timestream_i_data[::100],
        off_res_timestream.timestream_q_data[::100],
        label=f"Off-Res",
        linestyle="none",
        marker="o",
    )
    plt.legend()
    plt.xlabel("I")
    plt.ylabel("Q")
    plt.axis("equal")
    plt.title(f"Checking timestream tone is sitting on the IQ circle for {dut} data")
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"Hidfmux_timestream_data_check_for_{noise_sweep.dut}_{noise_sweep.tone_name}_{noise_sweep.temperature}K_"
            f"{noise_sweep.input_attenuation}amp.png"
        )
    plt.show()


if __name__ == "__main__":
    main()
