"""
Purpose:
Script will plot the summaries of the fit results for each kid from the segmented sweep. If desired, each fit will be
plotted.

How to use:
Load desired data into data folder, or point to the directory.

Ensure the attenuation and gain values are correct, then run the script.
"""
import math
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools
from source.util import data_array_analysis_tools


def main():

    directory = r"..\..\..\data_2"

    plot_fits = False  # Boolean to plot each kids fit.
    save_figures = True  # Boolean to save the plots.
    show_kid_labels = True  # Boolean to add kid labels to the segmented vna sweep plot.
    total_amplifier_gain = 78
    number_of_tones = 3

    temperature_array = []
    power_array = []
    full_f0_array = []  # Array to be filled with each files f0_array.
    full_qr_array = []  # Array to be filled with each files Qr array.
    full_qc_array = []  # Array to be filled with each files Qc array.
    full_qi_array = []  # Array to be filled with each files Qi array.

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        file_path = os.path.join(directory, filename)

        fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(
            file_path=file_path, total_amplifier_gain=total_amplifier_gain
        )
        vna_data = FitsDataUnpackService.unpack_sweep_data(fits_vna_sweep)
        frequency_array = vna_data.frequency_array
        s21_complex = vna_data.get_iq_array()

        if fits_vna_sweep.header.number_of_tones != None:
            number_of_tones = fits_vna_sweep.header.number_of_tones

        print(f"Number of KID tones: {number_of_tones}")
        fits_vna_sweep.header.print_attenuation_values()
        total_power = fits_vna_sweep.header.get_total_attenuation(cable_attenuation=10)
        print(f"Total Power = {total_power}dB")

        # Split data into sub arrays corresponding to each kid:
        frequency_array = np.split(frequency_array, number_of_tones)
        s21_magnitude = np.abs(np.split(s21_complex, number_of_tones))

        f0_array = []
        qr_array = []
        qc_array = []
        qi_array = []

        if plot_fits:
            plt.figure(figsize=(10, 6))
        for count, kid_s21 in enumerate(s21_magnitude):
            # Fit S21 data:
            fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency_array[count],
                data_array=kid_s21,
                qc_guess=1e5,
                qi_guess=1e5,
                f0_guess=None,
                normalise=False,
                plot_db=True,
                plot_title=f"Skewed (Pete fitter) fit: KID {count}",
                plot_graph=plot_fits,
            )

            s21_fit = lekid_analysis_tools.skewed_lorentzian(
                frequency_array=frequency_array[count],
                f0=fit_result_dict["f0"][0],
                qi=fit_result_dict["qi"][0],
                qc_real=fit_result_dict["qc_real"][0],
                qc_imag=fit_result_dict["qc_imag"][0],
                amp=fit_result_dict["amp"][0],
            )

            if plot_fits:
                plt.plot(
                    frequency_array[count] * 1e-9,
                    20 * np.log10(kid_s21),
                    linestyle="none",
                    marker="o",
                    color="b",
                    markersize="1",
                )
                if show_kid_labels:
                    plt.text(
                        np.median(frequency_array[count]) * 1e-9,
                        np.max(20 * np.log10(kid_s21)) + 0.5,
                        f"KID{count}",
                        fontsize=10,
                        color="b",
                        horizontalalignment="center",
                    )

                plt.plot(
                    frequency_array[count] * 1e-9,
                    20 * np.log10(s21_fit),
                    linewidth=2,
                    linestyle="--",
                    color="r",
                )

            if fit_result_dict["qi"][0] > 1e6:
                qi = fit_result_dict["qi"][0]
                print(f"KID{count}: Qi = {qi:.2E}")

            # Store f0 and quality factor data.
            f0_array.append(fit_result_dict["f0"])
            qr_array.append(fit_result_dict["qr"])
            qc_array.append(fit_result_dict["qc"])
            qi_array.append(fit_result_dict["qi"])

        if plot_fits:
            plt.xlabel("Frequency (GHz)")
            plt.ylabel("S21 Magnitude (dB)")
            plt.show()

        temperature_array.append(fits_vna_sweep.header.set_temperature)
        power_array.append(total_power)
        full_f0_array.append(f0_array)
        full_qr_array.append(qr_array)
        full_qc_array.append(qc_array)
        full_qi_array.append(qi_array)

    # Convert to numpy arrays:
    temperature_array = np.array(temperature_array)
    power_array = np.array(power_array)
    full_f0_array = np.array(full_f0_array)
    full_qr_array = np.array(full_qr_array)
    full_qc_array = np.array(full_qc_array)
    full_qi_array = np.array(full_qi_array)

    # Find unique temperatures and powers:
    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(power_array)

    # *******************************************************************************************************************

    # Plot parameters as a function of temperature:
    figure, axes = plt.subplots(
        4, number_of_tones, sharex=True, sharey=False, figsize=(14, 10)
    )
    for count, unique_powers in enumerate(unique_powers):

        # Get indices referring to each unique power to plot data series with same input attenuation.
        series_indices = np.where(power_array == unique_powers)

        for tone_index in range(number_of_tones):
            axes[0][tone_index].errorbar(
                temperature_array[series_indices] * 1e3,
                full_f0_array[series_indices][:, tone_index][:, 0] * 1e-3,
                yerr=full_f0_array[series_indices][:, tone_index][:, 1] * 1e-3,
                linestyle="--",
                marker="o",
                label=f"{unique_powers}dB",
            )
            axes[0][tone_index].set_title(f"KID {tone_index}")

            axes[1][tone_index].errorbar(
                temperature_array[series_indices] * 1e3,
                full_qr_array[series_indices][:, tone_index][:, 0],
                yerr=full_qr_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_powers}dB",
            )
            axes[1][tone_index].set_yscale("log")

            axes[2][tone_index].errorbar(
                temperature_array[series_indices] * 1e3,
                full_qc_array[series_indices][:, tone_index][:, 0],
                yerr=full_qc_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_powers}dB",
            )
            axes[2][tone_index].set_yscale("log")

            axes[3][tone_index].errorbar(
                temperature_array[series_indices] * 1e3,
                full_qi_array[series_indices][:, tone_index][:, 0],
                yerr=full_qi_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_powers}dB",
            )
            axes[3][tone_index].set_xlabel("Temperature (mK)")
            axes[3][tone_index].set_yscale("log")

    axes[0][0].set_ylabel("F0 (kHz)")
    axes[0][0].legend()
    axes[1][0].set_ylabel("Qr")
    axes[2][0].set_ylabel("Qc")
    axes[3][0].set_ylabel("Qi")
    plt.legend()
    plt.tight_layout()
    if save_figures:
        plt.savefig(
            "..\\..\\..\\Figures\\"
            + f"vna_sweep_fits_vs_temp_for_{fits_vna_sweep.header.dut}"
        )
    plt.show()

    # Plot parameters as a function of power:
    figure, axes = plt.subplots(
        4, number_of_tones, sharex=True, sharey=False, figsize=(14, 10)
    )
    for count, unique_temperature in enumerate(unique_temperatures):

        # Get indices referring to each unique power to plot data series with same input attenuation.
        series_indices = np.where(temperature_array == unique_temperature)

        for tone_index in range(number_of_tones):
            axes[0][tone_index].errorbar(
                power_array[series_indices],
                full_f0_array[series_indices][:, tone_index][:, 0] * 1e-3,
                yerr=full_f0_array[series_indices][:, tone_index][:, 1] * 1e-3,
                linestyle="--",
                marker="o",
                label=f"{unique_temperature*1e3}mK",
            )
            axes[0][tone_index].set_title(f"KID {tone_index}")

            axes[1][tone_index].errorbar(
                power_array[series_indices],
                full_qr_array[series_indices][:, tone_index][:, 0],
                yerr=full_qr_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_temperature*1e3}mK",
            )
            axes[1][tone_index].set_yscale("log")

            axes[2][tone_index].errorbar(
                power_array[series_indices],
                full_qc_array[series_indices][:, tone_index][:, 0],
                yerr=full_qc_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_temperature*1e3}mK",
            )
            axes[2][tone_index].set_yscale("log")

            axes[3][tone_index].errorbar(
                power_array[series_indices],
                full_qi_array[series_indices][:, tone_index][:, 0],
                yerr=full_qi_array[series_indices][:, tone_index][:, 1],
                linestyle="--",
                marker="o",
                label=f"{unique_temperature*1e3}mK",
            )
            axes[3][tone_index].set_xlabel("Power (dB)")
            axes[3][tone_index].set_yscale("log")

    axes[0][0].set_ylabel("F0 (kHz)")
    axes[0][0].legend()
    axes[1][0].set_ylabel("Qr")
    axes[2][0].set_ylabel("Qc")
    axes[3][0].set_ylabel("Qi")
    plt.legend()
    plt.tight_layout()
    if save_figures:
        plt.savefig(
            "..\\..\\..\\Figures\\"
            + f"vna_sweep_fits_vs_power_for_{fits_vna_sweep.header.dut}"
        )
    plt.show()


if __name__ == "__main__":
    main()
