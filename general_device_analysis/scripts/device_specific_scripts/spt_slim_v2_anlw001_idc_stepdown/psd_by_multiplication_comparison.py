import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy

from skimage.measure import block_reduce
from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import noise_multiplication_method_tools
from scipy.fft import fft, fftfreq, fftshift


def main(
    directory_1=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Data\20230816_Dark_Data_Prelim\KID_K008\Set_Temperature_100_mK\Set_Attenuation_50dB",
    directory_2=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Data\20230822_Dark_Data_Prelim\KID_K008\Set_Temperature_086_mK\Set_Attenuation_42dB_with_PT",
    directory_3=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Data\20230822_Dark_Data_Prelim\KID_K008\Set_Temperature_086_mK\Set_Attenuation_42dB_without_PT",
    save_figures=True,
):

    directories_list = [directory_1, directory_2, directory_3]
    label_list = ["Original", "With Pogos", "With Pogos and without Pulse Tube"]
    colors_list = ["r", "g", "b"]

    noise_packet_list = []

    for directory in directories_list:
        noise_packet_psd = (
            noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
                directory=directory,
                down_sample_psd=True,
                down_sample_bins=100,
            )
        )
        noise_packet_list.append(noise_packet_psd)
        print("PSD complete")

    plt.figure(figsize=(10, 6))
    for count, noise_packet_psd in enumerate(noise_packet_list):
        fft_frequencies = fftshift(noise_packet_psd.on_res_low_frequencies)
        frequency_order = np.argsort(
            fft_frequencies
        )  # Index order to put negative frequencies first
        plt.plot(
            fft_frequencies[frequency_order],
            fftshift(noise_packet_psd.psd_on_res_low)[frequency_order],
            label=label_list[count],
            color=colors_list[count],
        )

        fft_frequencies = fftshift(noise_packet_psd.off_res_low_frequencies)
        frequency_order = np.argsort(fft_frequencies)
        plt.plot(
            fft_frequencies[frequency_order],
            fftshift(noise_packet_psd.psd_off_res_low)[frequency_order],
            color=colors_list[count],
        )

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
    plt.xscale("log")
    plt.yscale("log")
    # plt.ylim(bottom=1e-23)
    plt.legend()
    plt.title(f"Noise PSD  for {noise_packet_list[0].dut} in different settings")
    if save_figures:
        plt.savefig(
            "..\\..\\..\\Figures\\"
            + f"Noise_PSD_comparison_{noise_packet_list[0].dut}_{label_list}_low_freq"
        )
    plt.show()


if __name__ == "__main__":
    main()
