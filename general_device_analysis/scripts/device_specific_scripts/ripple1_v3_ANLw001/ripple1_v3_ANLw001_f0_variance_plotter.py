import numpy as np
import os
import matplotlib.pyplot as plt
import source.util.general_analysis_tools as general_analysis_tools


def get_al_idc_length(f0: float or np.ndarray) -> float or np.ndarray:
    """Function to return the variable idc length to build aluminium inductor ortho-IDC Lekids for mask

    :param f0: F0 in Hz of LEKID.
    """
    return -2.558e-25 * f0**3 + 2.237e-15 * f0**2 - 7.461e-06 * f0 + 9096


def get_nb_f0(idc_length: float or np.ndarray) -> float or np.ndarray:
    """Function to return the f0 value of a niobium inductor ortho-IDC Lekid give the variable idc length.

    :param idc_length: variable IDC length in um.
    """
    return (
        -0.07501 * idc_length**3
        + 462.4 * idc_length**2
        - 1.451e6 * idc_length
        + 3.236e9
    )


data_directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\ripple_v3_ANLw001\slim_ebeam_test\slim_ebeam_test"

device_b_data_filename = "freq_scheduling_B_C.pk"
device_b_file_path = os.path.join(data_directory, device_b_data_filename)

devices_data_dict = np.load(device_b_file_path, allow_pickle=True)
device_b_f0_array = np.array(devices_data_dict["B"])
device_c_f0_array = np.array(devices_data_dict["C"])

al_target_data_filename = "target_detector_f0_array.txt"
al_target_data_file_path = os.path.join(data_directory, al_target_data_filename)

al_target_f0_array = np.loadtxt(al_target_data_file_path)
al_idc_lengths = get_al_idc_length(al_target_f0_array)

nb_target_f0_array = get_nb_f0(al_idc_lengths)

plt.figure(figsize=(8, 6))
plt.plot(np.diff(nb_target_f0_array * 1e-6), linestyle="none", marker="o", markersize=2)
plt.ylabel("Expected f0 difference (MHz)")
plt.xlabel("Index")
plt.title("Difference between expected f0's in the same device")
plt.show()
print(np.mean(np.diff(nb_target_f0_array * 1e-6)))

device_c_spacing = np.diff(device_c_f0_array) * 1e3
device_b_spacing = np.diff(device_b_f0_array) * 1e3

device_b_clash_indexes = np.where(device_b_spacing < 1.2)[0]
device_c_clash_indexes = np.where(device_c_spacing < 1.2)[0]

print(f"B clashes: {len(device_b_clash_indexes)}")
print(f"c clashes: {len(device_c_clash_indexes)}")
device_c_clash_f0_array = device_c_f0_array[device_c_clash_indexes]
device_b_clash_f0_array = device_b_f0_array[device_b_clash_indexes]
print(device_c_clash_f0_array)

plt.figure(figsize=(8, 6))
plt.plot(
    nb_target_f0_array * 1e-9,
    linestyle="none",
    marker="o",
    color="k",
    markersize=2,
    label="Target Nb F0's",
)
plt.plot(
    device_b_f0_array,
    linestyle="none",
    marker="o",
    color="tab:orange",
    markersize=2,
    label="Device B F0's",
)
plt.plot(
    device_c_f0_array,
    linestyle="none",
    marker="o",
    color="tab:blue",
    markersize=2,
    label="Device C F0's",
)
plt.vlines(device_b_clash_indexes, ymin=2.1, ymax=2.8, colors="tab:red")
plt.vlines(device_c_clash_indexes, ymin=2.1, ymax=2.8, colors="tab:green")
plt.xlabel("KID Index")
plt.ylabel("F0 (GHz)")
plt.legend()
plt.show()

# Create histogram of f0 spacing
# f0_spacing = np.abs(f0_array[:-1] - f0_array[1:])
plt.figure(figsize=(8, 6))
plt.hist(np.diff(nb_target_f0_array) * 1e-6, bins=40, alpha=0.7, label="Target Spacing")
plt.hist(device_c_spacing, bins=40, alpha=0.7, label="Device C Spacing")
plt.hist(device_b_spacing, bins=40, alpha=0.7, label="Device B Spacing")
plt.xlabel("F$_0$ spacing (MHz)")
plt.ylabel("Count")
plt.legend()
plt.show()

device_c_mag_data_filename = "C_freq_sweep.pk"
device_c_mag_file_path = os.path.join(data_directory, device_c_mag_data_filename)
device_c_mag_data_dict = np.load(device_c_mag_file_path, allow_pickle=True)

device_c_mag_frequency = device_c_mag_data_dict["frequency"]
device_c_mag_s21 = 20 * np.log10(device_c_mag_data_dict["magnitude"][0])

plt.figure(figsize=(8, 6))
plt.plot(device_c_mag_frequency * 1e-9, device_c_mag_s21)
plt.ylabel("S21 Magnitude (dB)")
plt.xlabel("Frequency (GHz)")
plt.show()

print(len(device_c_f0_array))

########################################################################################################################
# Find the difference to between an f0 in B and it's nearest f0 in C:
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


nearest_c_values = []
for value in device_b_f0_array:
    nearest_c_values.append(find_nearest(device_c_f0_array, value))

nearest_c_values = np.array(nearest_c_values)
nearest_difference = (nearest_c_values - device_b_f0_array) * 1e3

nearest_difference = np.abs(nearest_difference)
mean = np.mean(nearest_difference)
standard_deviation = np.std(nearest_difference)
standard_error = standard_deviation / np.sqrt(nearest_difference.size)

plt.figure(figsize=(8, 6))
plt.hist(nearest_difference, bins=40)
plt.text(
    x=1,
    y=10,
    s=f"mean: {mean:.2f}MHz\n$\sigma$: {standard_deviation:.2f}MHz\nstandard error: {standard_error:.2f}MHz",
)
plt.xlabel("Difference to nearest f0 (MHz)")
plt.ylabel("Count")
plt.title(
    "Histogram of the difference between device B f0's and their nearest value in device C"
)
plt.legend()
plt.show()

########################################################################################################################

for clash_f0 in device_c_clash_f0_array:
    clash_frequency_range_min = clash_f0 - 0.005
    clash_frequency_range_max = clash_f0 + 0.005

    clash_frequency_range_indices = np.where(
        (device_c_mag_frequency > clash_frequency_range_min * 1e9)
        & (device_c_mag_frequency < clash_frequency_range_max * 1e9)
    )[0]

    plt.figure(figsize=(8, 6))
    plt.plot(
        device_c_mag_frequency[clash_frequency_range_indices] * 1e-9,
        device_c_mag_s21[clash_frequency_range_indices],
    )
    plt.title(f"Clash F0: {clash_f0}")
    plt.ylabel("S21 Magnitude (dB)")
    plt.xlabel("Frequency (GHz)")
    plt.savefig(
        "..\\..\\..\\Clash Figures\\" + f"ripple1_v3_ANLw001_clash_{clash_f0}GHz.png"
    )
    plt.show()
