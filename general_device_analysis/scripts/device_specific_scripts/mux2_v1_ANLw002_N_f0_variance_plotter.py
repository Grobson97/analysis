import numpy as np
import matplotlib.pyplot as plt
import source.util.general_analysis_tools as general_analysis_tools


def get_al_idc_length(f0: float or np.ndarray) -> float or np.ndarray:
    """Function to return the variable idc length to build aluminium inductor ortho-IDC Lekids for mask

    :param f0: F0 in Hz of LEKID.
    """
    return -2.558e-25 * f0**3 + 2.237e-15 * f0**2 - 7.461e-06 * f0 + 9096


def get_nb_f0(idc_length: float or np.ndarray) -> float or np.ndarray:
    """Function to return the f0 value of a niobium inductor ortho-IDC Lekid give the variable idc length.

    :param idc_length: variable IDC length in um.
    """
    return (
        -0.07501 * idc_length**3
        + 462.4 * idc_length**2
        - 1.451e6 * idc_length
        + 3.236e9
    )


device_f0_array = np.array(
    [
        2.17938695e09,
        2.20714134e09,
        2.24800614e09,
        2.26956285e09,
        2.29577568e09,
        2.32464837e09,
        2.36267498e09,
        2.39321901e09,
        2.41279273e09,
        2.44649450e09,
        2.47116491e09,
        2.54234823e09,
        2.56821877e09,
        2.59515843e09,
        2.62546291e09,
        2.64967161e09,
        2.68593879e09,
        2.71850859e09,
        2.74173314e09,
    ]
)

al_target_f0_array = np.array(
    [
        2.01000000e09,
        2.03526316e09,
        2.06052632e09,
        2.08578947e09,
        2.11105263e09,
        2.13631579e09,
        2.16157895e09,
        2.18684211e09,
        2.21210526e09,
        2.23736842e09,
        2.26263158e09,
        2.31315789e09,
        2.33842105e09,
        2.36368421e09,
        2.38894737e09,
        2.41421053e09,
        2.43947368e09,
        2.46473684e09,
        2.49000000e09,
    ]
)

nb_f0_array = get_nb_f0(get_al_idc_length(al_target_f0_array))
print(nb_f0_array)

kid_number = np.array(
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19]
)

plt.figure(figsize=(8, 6))
plt.plot(
    kid_number,
    al_target_f0_array * 1e-9,
    linestyle="none",
    marker="o",
    markersize="2",
    label="Aluminiun inductor f0's",
)
plt.plot(
    kid_number,
    device_f0_array * 1e-9,
    linestyle="none",
    marker="o",
    markersize="2",
    label="Device f0's",
)
plt.plot(
    kid_number,
    nb_f0_array * 1e-9,
    linestyle="none",
    marker="o",
    markersize="2",
    label="Simulated Niobium inductor f0's",
)
plt.xlabel("KID")
plt.ylabel("F$_0$ (GHz)")
plt.legend()
plt.savefig("test.png")
plt.show()

device_f0_linear = general_analysis_tools.fit_linear_regression(
    x_data=kid_number,
    y_data=device_f0_array,
    plot_graph=True,
    plot_title="Linear fit to Device F0's",
    x_label="KID F0 Location",
    y_label="F0 (GHz)",
)

measured_slope = device_f0_linear.slope
measured_intercept = device_f0_linear.intercept

nb_f0_array_linear = general_analysis_tools.fit_linear_regression(
    x_data=kid_number,
    y_data=nb_f0_array,
    plot_graph=True,
    plot_title="Linear fit to Niobium target F0's",
    x_label="KID F0 Location",
    y_label="F0 (GHz)",
)

nb_slope = nb_f0_array_linear.slope
nb_intercept = nb_f0_array_linear.intercept

a = (measured_slope * kid_number + measured_intercept) / (
    nb_slope * kid_number + nb_intercept
) - 1

plt.figure(figsize=(8, 6))
plt.plot(kid_number, a, linestyle="None", marker="o")
plt.xlabel("KID F0 Location")
plt.ylabel("a")
plt.show()

a_polynomial = general_analysis_tools.fit_polynomial(
    x_data=kid_number,
    y_data=a,
    degree=3,
    plot_graph=True,
)

mapped_f0s = device_f0_array / (1 + a)

plt.figure(figsize=(8, 6))
plt.plot(
    kid_number,
    al_target_f0_array * 1e-9,
    linestyle="none",
    marker="o",
    markersize="2",
    label="Expected f0's",
)
plt.plot(
    kid_number,
    mapped_f0s * 1e-9,
    linestyle="none",
    marker="o",
    markersize="2",
    label="Mapped F0's",
)
plt.xlabel("KID")
plt.ylabel("F$_0$ (GHz)")
plt.legend()
plt.savefig("mapped_f0_comparison.png")
plt.show()

mapped_f0_variance = mapped_f0s - al_target_f0_array
standard_deviation = np.std(mapped_f0_variance)

plt.figure(figsize=(8, 6))
plt.plot(
    kid_number,
    mapped_f0_variance * 1e-6,
    linestyle="none",
    marker="o",
    markersize="4",
)
plt.xlabel("KID")
plt.ylabel("Mapped F$_0$ variance from target (MHz)")
plt.title(
    f"Measured F0 = Target F0 * (1 + A) with a {standard_deviation * 1e-6: .3f} MHz std"
)
plt.legend()
plt.savefig("f0_variance")
plt.show()

_ = plt.hist(mapped_f0_variance * 1e-6, bins=12)  # arguments are passed to np.histogram
plt.xlabel("KID Number")
plt.ylabel("Variance (GHz)")
plt.show()

# Calculation of alpha:

lg_array = (
    (al_target_f0_array**2 * 0.6 * 1e-12) - (nb_f0_array**2 * 0.03 * 1e-12)
) / (nb_f0_array**2 - al_target_f0_array**2)

plt.figure(figsize=(8, 6))
plt.plot(al_target_f0_array, lg_array, linestyle="none", marker="o")
plt.xlabel("Aluminium Target f0's")
plt.ylabel(" Geometric inductance")
plt.show()

mean_lg = np.mean(lg_array)
print(mean_lg)
al_alpha = 0.6 / (0.6 + mean_lg * 1e12)
nb_alpha = 0.03 / (0.03 + mean_lg * 1e12)
al_alpha_array = 0.6 / (0.6 + lg_array * 1e12)
nb_alpha_array = 0.03 / (0.03 + lg_array * 1e12)

plt.figure(figsize=(8, 6))
plt.plot(
    al_target_f0_array, al_alpha_array, linestyle="none", marker="o", label="Aluminium"
)
plt.plot(
    al_target_f0_array, nb_alpha_array, linestyle="none", marker="o", label="Niobium"
)
plt.xlabel("Target f0's")
plt.ylabel("Alpha")
plt.legend()
plt.show()

print(al_alpha)
print(nb_alpha)
