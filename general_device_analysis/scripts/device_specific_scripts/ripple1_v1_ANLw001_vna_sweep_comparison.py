"""
Purpose:
Script will plot the S21 in dB of various fits vna sweeps data files. If there are multiple files in one directory the
mean value at each frequency point will be used.
The number of directories defines how many different series are plotted.

How to use:
Load desired data into data folders, or point to the directories. Directories should only contain the data you wish to
plot. e.g. If you want to plot one vna sweep file, that should be the only file in the folder. If you want to average 3
repeats of the same measurement, the 3 data files need to be in the folder.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

import source.util.general_analysis_tools
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService


def main():

    directory_list = [r"..\..\data", r"..\..\data_1", r"..\..\data_2"]
    directory_series_names = ["Nominal", "Optimal", "Optimal - No Cold Amp"]
    mean_s21_magnitude_db_arrays = []
    frequency_arrays = []

    plt.figure(figsize=(10, 6))
    for count, directory in enumerate(directory_list):
        s21_magnitude_db_arrays = []
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            file_path = os.path.join(directory, filename)

            fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(
                file_path=file_path
            )
            vna_data = FitsDataUnpackService.unpack_sweep_data(fits_vna_sweep)
            s21_magnitude = np.abs(vna_data.get_iq_array())
            s21_magnitude_db = 20 * np.log10(s21_magnitude)
            s21_magnitude_db_arrays.append(s21_magnitude_db)

        # Take mean s21 at each frequency.
        s21_magnitude_db = np.mean(np.array(s21_magnitude_db_arrays), axis=0)

        fits_vna_sweep.header.print_attenuation_values()
        total_attenuation = fits_vna_sweep.header.get_total_attenuation(
            total_gain=60.0, cable_attenuation=15
        )
        print(f"Total attenuation = {total_attenuation} dB")

        plt.plot(
            vna_data.frequency_array * 1e-9,
            s21_magnitude_db,
            label=directory_series_names[count],
            alpha=0.6,
        )

        mean_s21_magnitude_db_arrays.append(s21_magnitude_db)
        frequency_arrays.append(vna_data.frequency_array)

    nominal_polynomial = source.util.general_analysis_tools.fit_polynomial(
        x_data=frequency_arrays[0],
        y_data=mean_s21_magnitude_db_arrays[0],
        degree=3,
        plot_graph=False,
    )
    nominal_fit = nominal_polynomial(frequency_arrays[0])
    plt.plot(
        frequency_arrays[0] * 1e-9, nominal_fit, label="Nominal 3rd Degree Poly Fit"
    )

    optimal_polynomial = source.util.general_analysis_tools.fit_polynomial(
        x_data=frequency_arrays[1],
        y_data=mean_s21_magnitude_db_arrays[1],
        degree=3,
        plot_graph=False,
    )
    optimal_fit = optimal_polynomial(frequency_arrays[1])
    plt.plot(
        frequency_arrays[1] * 1e-9, optimal_fit, label="Optimal 3rd Degree Poly Fit"
    )

    no_amp_polynomial = source.util.general_analysis_tools.fit_polynomial(
        x_data=frequency_arrays[2],
        y_data=mean_s21_magnitude_db_arrays[2],
        degree=3,
        plot_graph=False,
    )
    no_amp_fit = no_amp_polynomial(frequency_arrays[2])
    plt.plot(
        frequency_arrays[2] * 1e-9, no_amp_fit, label="No Cold Amp 3rd Degree Poly Fit"
    )

    plt.title("Comparison of multiple VNA sweeps")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    plt.legend()
    plt.savefig(
        "..\\..\\Figures\\" + f"vna_sweep_comparison_of_{directory_series_names}"
    )
    plt.show()

    # Remove slope from data:
    normalised_nominal_s21 = mean_s21_magnitude_db_arrays[0] - nominal_fit
    normalised_optimal_s21 = mean_s21_magnitude_db_arrays[1] - optimal_fit
    normalised_no_amp_s21 = mean_s21_magnitude_db_arrays[2] - no_amp_fit

    plt.figure(figsize=(8, 6))
    plt.plot(frequency_arrays[0] * 1e-9, normalised_nominal_s21, label="Nominal")
    plt.plot(frequency_arrays[1] * 1e-9, normalised_optimal_s21, label="Optimal")
    plt.plot(
        frequency_arrays[2] * 1e-9, normalised_no_amp_s21, label="Optimal - No Cold Amp"
    )
    plt.title("Comparison of nominal and optimal VNA sweeps after fit subtraction")
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude Difference (dB)")
    plt.legend()
    plt.savefig(
        "..\\..\\Figures\\"
        + f"vna_sweep_comparison_of_{directory_series_names}_after_fit_subtraction"
    )
    plt.show()

    nominal_peaks_positive, _ = find_peaks(
        x=normalised_nominal_s21, distance=1, prominence=1
    )
    nominal_peaks_negative, _ = find_peaks(
        x=-normalised_nominal_s21, distance=1, prominence=1
    )
    nominal_peaks = np.concatenate((nominal_peaks_negative, nominal_peaks_positive))

    optimal_peaks_positive, _ = find_peaks(
        x=normalised_optimal_s21, distance=1, prominence=1
    )
    optimal_peaks_negative, _ = find_peaks(
        x=-normalised_optimal_s21, distance=1, prominence=1
    )
    optimal_peaks = np.concatenate((optimal_peaks_negative, optimal_peaks_positive))

    no_amp_peaks_positive, _ = find_peaks(
        x=normalised_no_amp_s21, distance=1, prominence=1
    )
    no_amp_peaks_negative, _ = find_peaks(
        x=-normalised_no_amp_s21, distance=1, prominence=1
    )
    no_amp_peaks = np.concatenate((no_amp_peaks_negative, no_amp_peaks_positive))

    plt.figure(figsize=(8, 6))
    plt.plot(frequency_arrays[0] * 1e-9, normalised_nominal_s21)
    plt.plot(
        frequency_arrays[0][nominal_peaks] * 1e-9,
        normalised_nominal_s21[nominal_peaks],
        "o",
        markerfacecolor="none",
        markersize=3,
        color="r",
        label="Nominal",
    )
    plt.plot(frequency_arrays[1] * 1e-9, normalised_optimal_s21)
    plt.plot(
        frequency_arrays[1][optimal_peaks] * 1e-9,
        normalised_optimal_s21[optimal_peaks],
        "o",
        markerfacecolor="none",
        markersize=3,
        color="k",
        label="Optimal",
    )
    plt.plot(frequency_arrays[2] * 1e-9, normalised_no_amp_s21)
    plt.plot(
        frequency_arrays[2][no_amp_peaks] * 1e-9,
        normalised_no_amp_s21[no_amp_peaks],
        "o",
        markerfacecolor="none",
        markersize=3,
        color="k",
        label="Optimal - No Cold Amp",
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude Difference (dB)")
    plt.legend()
    plt.show()

    print(
        f"Minimum Nominal Peak: {np.min(np.abs(normalised_nominal_s21[nominal_peaks]))}"
    )
    print(
        f"Maximum Nominal Peak: {np.max(np.abs(normalised_nominal_s21[nominal_peaks]))}"
    )
    print(
        f"Mean Nominal Peak: {np.mean(np.abs(normalised_nominal_s21[nominal_peaks]))}"
    )
    print(f"Number of peaks: {nominal_peaks.size}")

    print(
        f"\nMinimum optimal Peak: {np.min(np.abs(normalised_optimal_s21[optimal_peaks]))}"
    )
    print(
        f"Maximum optimal Peak: {np.max(np.abs(normalised_optimal_s21[optimal_peaks]))}"
    )
    print(
        f"Mean optimal Peak: {np.mean(np.abs(normalised_optimal_s21[optimal_peaks]))}"
    )
    print(f"Number of peaks: {optimal_peaks.size}")

    print(
        f"\nMinimum optimal no cold amp Peak: {np.min(np.abs(normalised_no_amp_s21[no_amp_peaks]))}"
    )
    print(
        f"Maximum optimal no cold amp Peak: {np.max(np.abs(normalised_no_amp_s21[no_amp_peaks]))}"
    )
    print(
        f"Mean optimal no cold amp Peak: {np.mean(np.abs(normalised_no_amp_s21[no_amp_peaks]))}"
    )
    print(f"Number of peaks: {optimal_peaks.size}")

    normalised_optimal_s21[optimal_peaks],


if __name__ == "__main__":
    main()
