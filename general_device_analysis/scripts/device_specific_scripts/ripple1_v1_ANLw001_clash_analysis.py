"""
Purpose:
Script will plot the summaries of the fit results for each kid from the segmented sweep. If desired, each fit will be
plotted.

How to use:
Load desired data into data folder, or point to the directory.

Ensure the attenuation and gain values are correct, then run the script.
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import lekid_analysis_tools
from source.util import data_array_analysis_tools


def main():

    directory = r"..\..\data"
    filename = "S21_Segmented_BTEMP0250_mK_POW0.0_dB_ATT36.0_dB_1.fits"
    file_path = os.path.join(directory, filename)

    fit_model = "skewed"  # Boolean to define which model is used for fitting, "skewed" or "non-linear"
    plot_fits = False  # Boolean to plot each kids fit.
    save_figures = True  # Boolean to save the plots.
    show_kid_labels = True  # Boolean to add kid labels to the segmented vna sweep plot.
    qr_mask_min_bound = 0.0
    qr_mask_max_bound = 1e7
    qc_mask_min_bound = 0.0
    qc_mask_max_bound = 1e7
    qi_mask_min_bound = 0.0
    qi_mask_max_bound = 1e6

    fits_vna_sweep = FitsFileLoadService.load_fits_vna_sweep_file(file_path=file_path)
    vna_data = FitsDataUnpackService.unpack_sweep_data(fits_vna_sweep)
    frequency_array = vna_data.frequency_array
    s21_complex = vna_data.get_iq_array()

    number_of_tones = fits_vna_sweep.header.number_of_tones
    print(f"Number of KID tones: {number_of_tones}")
    fits_vna_sweep.header.print_attenuation_values()

    # Split data into sub arrays corresponding to each kid:
    frequency_array = np.split(frequency_array, number_of_tones)
    s21_magnitude = np.abs(np.split(s21_complex, number_of_tones))

    f0_array = []
    qr_array = []
    qc_array = []
    qi_array = []

    plt.figure(figsize=(10, 6))
    for count, kid_s21 in enumerate(s21_magnitude):
        plt.plot(
            frequency_array[count] * 1e-9,
            20 * np.log10(kid_s21),
            linestyle="none",
            marker="o",
            color="b",
            markersize="1",
        )
        if show_kid_labels:
            plt.text(
                np.median(frequency_array[count]) * 1e-9,
                np.max(20 * np.log10(kid_s21)) + 0.5,
                f"KID{count}",
                fontsize=10,
                color="b",
                horizontalalignment="center",
            )

        # TODO: Add logic to rerun fit with different Q if errors are too large.
        if fit_model == "skewed":
            fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
                frequency_array=frequency_array[count],
                data_array=kid_s21,
                qc_guess=1e5,
                qi_guess=1e5,
                f0_guess=None,
                normalise=False,
                plot_db=True,
                plot_title=f"Skewed (Pete fitter) fit: KID {count}",
                plot_graph=plot_fits,
            )
            s21_fit = lekid_analysis_tools.skewed_lorentzian(
                frequency_array=frequency_array[count],
                f0=fit_result_dict["f0"][0],
                qi=fit_result_dict["qi"][0],
                qc_real=fit_result_dict["qc_real"][0],
                qc_imag=fit_result_dict["qc_imag"][0],
                amp=fit_result_dict["amp"][0],
            )
            plt.plot(
                frequency_array[count] * 1e-9,
                20 * np.log10(s21_fit),
                linewidth=2,
                linestyle="--",
                color="r",
            )

            if fit_result_dict["qi"][0] > 1e6:
                qi = fit_result_dict["qi"][0]
                print(f"KID{count}: Qi = {qi:.2E}")

        # Store f0 and quality factor data.
        f0_array.append(fit_result_dict["f0"][0])
        qr_array.append(fit_result_dict["qr"][0])
        qc_array.append(fit_result_dict["qc"][0])
        qi_array.append(fit_result_dict["qi"][0])

    plt.xlabel("Frequency (GHz)")
    plt.ylabel("S21 Magnitude (dB)")
    if save_figures:
        plt.savefig(
            "..\\..\\Figures\\"
            + f"{fits_vna_sweep.header.dut}_segmented_vna_sweep_KID_Fits"
        )
    plt.show()

    f0_array = np.array(f0_array)
    qr_array = np.array(qr_array)
    qc_array = np.array(qc_array)
    qi_array = np.array(qi_array)

    ########################################################################################################################

    # Plot graph of f0's:
    # Define LEKID f0 arrays
    lekid_f0_array_1 = np.linspace(2.010e9, 2.410e9, 20)
    # Mix lekid f0 array. Split into 4 parts a, b, c, d. Then mix to get a1, c1, b1, d1, a2, c2, b2, d2... etc.
    lekid_f0_array_1 = np.split(lekid_f0_array_1, 5)
    lekid_f0_array_1 = np.dstack(
        (
            lekid_f0_array_1[0],
            lekid_f0_array_1[2],
            lekid_f0_array_1[4],
            lekid_f0_array_1[1],
            lekid_f0_array_1[3],
        )
    )
    lekid_f0_array_1 = lekid_f0_array_1.flatten()
    # Create array of lekid f0 arrays, one for each sub cascade.
    target_f0_array = [lekid_f0_array_1]
    for n in range(7):
        target_f0_array.append(target_f0_array[n] + 10e6)

    target_f0_array = np.array(target_f0_array).flatten()
    target_f0_array = np.sort(target_f0_array)

    def get_al_idc_length(f0: float or np.ndarray) -> float or np.ndarray:
        """Function to return the variable idc length to build aluminium inductor ortho-IDC Lekids for mask

        :param f0: F0 in Hz of LEKID.
        """
        return -2.558e-25 * f0**3 + 2.237e-15 * f0**2 - 7.461e-06 * f0 + 9096

    def get_nb_f0(idc_length: float or np.ndarray) -> float or np.ndarray:
        """Function to return the f0 value of a niobium inductor ortho-IDC Lekid give the variable idc length.

        :param idc_length: variable IDC length in um.
        """
        return (
            -0.07501 * idc_length**3
            + 462.4 * idc_length**2
            - 1.451e6 * idc_length
            + 3.236e9
        )

    al_idc_lengths = get_al_idc_length(target_f0_array)
    target_f0_array = get_nb_f0(al_idc_lengths)

    # Find Clashes:
    v1_clash_indexes = np.where(np.diff(f0_array) * 1e-6 < 1.2)[0]

    plt.figure(figsize=(8, 6))
    plt.plot(
        target_f0_array * 1e-9,
        linestyle="none",
        marker="o",
        markersize="2",
        label="Expected f0's",
    )
    plt.plot(
        f0_array * 1e-9,
        linestyle="none",
        marker="o",
        markersize="2",
        label="Device f0's",
    )
    plt.vlines(v1_clash_indexes, ymin=2.1, ymax=2.7, colors="r")
    plt.xlabel("KID")
    plt.ylabel("F$_0$ (GHz)")
    plt.legend()
    plt.show()

    ########################################################################################################################

    # Create histogram of f0 spacing

    plt.figure(figsize=(8, 6))
    plt.hist(
        np.diff(target_f0_array) * 1e-6, bins=40, alpha=0.7, label="Target Spacing"
    )
    plt.hist(np.diff(f0_array) * 1e-6, bins=40, alpha=0.7, label="Device C Spacing")
    plt.xlabel("F$_0$ spacing (MHz)")
    plt.ylabel("Count")
    plt.legend()
    plt.show()

    print(f"Number of detectors found: {len(f0_array)}")
    print(f"Number of clashes: {len(v1_clash_indexes)}")

    ####################################################################################################################

    # Comparison with Ripple1_v3_ANLw001_C:

    data_directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\ripple_v3_ANLw001\slim_ebeam_test\slim_ebeam_test"

    device_c_data_filename = "freq_scheduling_B_C.pk"
    device_b_file_path = os.path.join(data_directory, device_c_data_filename)
    devices_data_dict = np.load(device_b_file_path, allow_pickle=True)
    device_c_f0_array = np.array(devices_data_dict["C"])

    al_target_data_filename = "target_detector_f0_array.txt"
    al_target_data_file_path = os.path.join(data_directory, al_target_data_filename)
    al_target_f0_array = np.loadtxt(al_target_data_file_path)
    al_idc_lengths = get_al_idc_length(al_target_f0_array)

    device_c_spacing = np.diff(device_c_f0_array) * 1e3
    v3_clash_indexes = np.where(device_c_spacing < 1.2)[0]

    v3_clash_f0_array = device_c_f0_array[v3_clash_indexes]

    plt.figure(figsize=(8, 6))
    plt.plot(
        target_f0_array * 1e-9,
        linestyle="none",
        marker="o",
        markersize="2",
        color="k",
        label="Expected f0's",
    )
    plt.plot(
        f0_array * 1e-9,
        linestyle="none",
        marker="o",
        markersize="2",
        color="tab:orange",
        label="V1 Device f0's (MLA)",
    )
    plt.plot(
        device_c_f0_array,
        linestyle="none",
        marker="o",
        markersize="2",
        color="tab:blue",
        label="V3 Device f0's (E-Beam)",
    )
    plt.vlines(
        v1_clash_indexes, ymin=2.1, ymax=2.7, colors="tab:red", label="V1 Clashes"
    )
    plt.vlines(
        v3_clash_indexes, ymin=2.1, ymax=2.7, colors="tab:green", label="V3 Clashes"
    )
    plt.xlabel("KID")
    plt.ylabel("F$_0$ (GHz)")
    plt.legend()
    plt.show()

    plt.figure(figsize=(8, 6))
    plt.plot(v3_clash_indexes[:-1] - v1_clash_indexes, linestyle="none", marker="o")
    plt.ylabel("Clash index difference")
    plt.title(
        "Plot showing the difference in clash index between MLA and E-beam (v3 - v1)"
    )
    plt.show()


if __name__ == "__main__":
    main()
