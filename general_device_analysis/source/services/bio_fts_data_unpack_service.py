import numpy as np
from source.util import timestream_data_tools
from astropy.io import fits



def load_bio_fts_iq_data(file_path) -> tuple:
    """
    Function to load the IQ data for forward and reverse scans saved by the bio fts software.
    """
    data = np.loadtxt(file_path, skiprows=25)

    forward_i = data[:, 0]
    forward_q = data[:, 2]
    reverse_i = data[:, 1]
    reverse_q = data[:, 3]

    return forward_i + 1j*forward_q, reverse_i + 1j*reverse_q


def load_calibration_sweep_details(file_path) -> dict:
    """
    Function to load the header details from a calibration sweep fits file.
    """

    with fits.open(file_path) as hdul:
        sweep_details = {
            "IF0": hdul[1].header["IF0"],
            "QF0": hdul[1].header["IF0"],
            "DIDF": hdul[1].header["DIDF"],
            "DQDF": hdul[1].header["DQDF"],
            "F0": hdul[1].header["SYNTHFRE"],
        }

    return sweep_details


def load_df_f_interferograms_and_opd(fts_data_path: str, calibration_sweep_path: str) -> tuple:
    """
    Function to load in the FTS data and corresponding calibration sweep then compute the 
    fractional frequency shift and return the ffs interferogram and corresponding opd.
    """

    # load FTS data
    forward_iq, reverse_iq = load_bio_fts_iq_data(fts_data_path)
    # Load calibration sweep detals
    calibration_details = load_calibration_sweep_details(calibration_sweep_path)

    df_f_arrays = []
    for iq_array in [forward_iq, reverse_iq]:
        # Get forward and reverse df arrays:
        df_array = timestream_data_tools.calculate_timestream_df(
            di_df_r=calibration_details["DIDF"],
            dq_df_r=calibration_details["DQDF"],
            timestream_i_array=iq_array.real,
            timestream_q_array=iq_array.imag
        )
        df_f_array = df_array / calibration_details["F0"]
        df_f_arrays.append(df_f_array)

    return df_f_array[0], df_array[1]