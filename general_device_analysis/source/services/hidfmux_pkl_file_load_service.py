import os.path

import numpy as np
import source.util.hidfmux_transfer_functions as hidfmux_transfer_functions
import source.util.hidfmux_data_tools as hidfmux_data_tools

from scipy import interpolate
from source.models.fits.fits_cw import FitsCW
from source.models.hidfmux.hidfmux_multi_sweep import HidfmuxMultiSweep
from source.models.hidfmux.hidfmux_timestream import HidfmuxTimestream
from source.models.hidfmux.hidfmux_pfb_timestream import HidfmuxPFBTimestream
from source.models.resonator_measurement.timestream_data import TimestreamData
from source.models.resonator_measurement.sweep_data import SweepData
from source.models.resonator_measurement.measurement_header import ResonatorMeasurementHeader



def load_pkl_multi_sweep_file(
        file_path: str,
        dut="DUT",
        temperature=0,
        sweep_index=0 or np.ndarray,
    ) -> HidfmuxMultiSweep:
    """
    Function to extract the useful multi sweep information from a Pickle file processed by the hidfmux scripts for a
    multi-sweep.

    :param file_path: Path to Pickle sweep file.
    :param dut: Name of the device under test.
    :param temperature: Set temperature of the measured data.
    :param sweep_index: Index corresponding to the desired specific multisweep data values. If the multisweep file
    only contains one multisweep, 0 should be used. Can also accept an array of indices, which would extract the
    bias power, sweep data and calibration data from whichever sweep is desired for a specific KID.
    """

    multi_sweep = np.load(
        file_path,
        mmap_mode=None,
        allow_pickle=True,
        fix_imports=True,
        encoding="ASCII",
    )

    tone_name_array = np.array(list(multi_sweep["data"].keys()))
    if type(sweep_index) == int:
        sweep_index = np.full(shape=tone_name_array.shape, fill_value=sweep_index)
    bias_power_array = np.empty(shape=tone_name_array.shape)
    sweep_data_array = np.empty(shape=tone_name_array.shape, dtype=object)
    calibration_iq_array = []

    # For each tone extract the bias power, cal_iq array, frequency array and iq array:
    for count, tone_name in enumerate(tone_name_array):
        
        index = int(sweep_index[count])
        bias_power_array[count] = multi_sweep["data"][tone_name][index]["amp"]
        calibration_iq_array.append(
            np.array(multi_sweep["data"][tone_name][index]["cal_iq"], dtype=complex)
        )
        frequency = np.array(multi_sweep["data"][tone_name][index]["freq"])
        iq_array = hidfmux_transfer_functions.convert_roc_to_volts(
            np.array(multi_sweep["data"][tone_name][index]["iq"], dtype=complex)
        )

        date_start_index = multi_sweep["info"]["data_dir"].find("hidfmux_output") + 15
        measurent_header = ResonatorMeasurementHeader(
            dut=dut,
            tone_name=tone_name,
            date=multi_sweep["info"]["data_dir"][date_start_index: date_start_index+8],
            set_temperature=temperature,
            actual_temperature=temperature,
            power=bias_power_array[count],
        )

        sweep_data_array[count] = SweepData(
            measurement_header=measurent_header,
            frequency_array=frequency,
            i=np.real(iq_array),
            q=np.imag(iq_array),
        )

    calibration_iq_array = np.array(calibration_iq_array)

    return HidfmuxMultiSweep(
        bias_power_array=bias_power_array,
        tone_name_array=tone_name_array,
        calibration_iq_array=calibration_iq_array,
        sweep_data_array=sweep_data_array,
    )

def load_pkl_timestream_file(
    timestream_file_path: str,
    multisweep: HidfmuxMultiSweep,
    tone_name: str,
) -> tuple:
    """
    Function to load a timestream pkl file from a hidfmux noise timestream measurement. This function takes the
    corresponding multisweep which contains the sweep information for the tone used to record the noise. The
    timestream data from the pkl file is corrected by removing the phase shift due to the digital path length
    between the FPGA and demodulator.

    Returns a tuple of the on resonance timestream and the off resonance timestream.

    :param timestream_file_path: Path to pkl timestream file for particular tone.
    :param multisweep: HidfmuxMultiSweep object containing the desired tones sweep information.
    :param tone_name: Name of the tone. Must be in the tone_name_array in the multisweep.
    """

    time_stream_dict = np.load(
        timestream_file_path,
        mmap_mode=None,
        allow_pickle=True,
        fix_imports=True,
        # encoding="ASCII",
    )

    sweep_frequency_array = multisweep.get_tone_sweep_data(
        tone=tone_name
    ).frequency_array
    calibration_iq_array = multisweep.get_tone_calibration_iq_data(tone=tone_name)

    on_res_tone_frequency = np.array(
        time_stream_dict["onres"]["info"][tone_name]["bias_freq"]
    )
    off_res_tone_frequency = np.array(
        time_stream_dict["offres"]["info"][tone_name]["bias_freq"]
    )
    time_array = np.array(time_stream_dict["ts"])

    (
        on_res_timestream_i,
        on_res_timestream_q,
    ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
        i_array=np.array(time_stream_dict["onres"]["data"][tone_name]["i"]),
        q_array=np.array(time_stream_dict["onres"]["data"][tone_name]["q"]),
        tone_frequency=on_res_tone_frequency,
        cal_iq_at_bias_freq=None,
        sweep_f_array=sweep_frequency_array,
        cal_iq_array=calibration_iq_array,
    )

    (
        off_res_timestream_i,
        off_res_timestream_q,
    ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
        i_array=np.array(time_stream_dict["offres"]["data"][tone_name]["i"]),
        q_array=np.array(time_stream_dict["offres"]["data"][tone_name]["q"]),
        tone_frequency=off_res_tone_frequency,
        cal_iq_at_bias_freq=None,
        sweep_f_array=sweep_frequency_array,
        cal_iq_array=calibration_iq_array,
    )

    on_res_timestream = TimestreamData(
        n_timestreams=1,
        sample_rate=hidfmux_transfer_functions.DEMOD_SAMPLING_FREQ,
        measurement_length=time_array[-1],
        tone_frequency=on_res_tone_frequency,
        timestream_i_data=on_res_timestream_i,
        timestream_q_data=on_res_timestream_q,
        timestream_time_data=time_array,
    )

    off_res_timestream = TimestreamData(
        n_timestreams=1,
        sample_rate=hidfmux_transfer_functions.DEMOD_SAMPLING_FREQ,
        measurement_length=time_array[-1],
        tone_frequency=off_res_tone_frequency,
        timestream_i_data=off_res_timestream_i,
        timestream_q_data=off_res_timestream_q,
        timestream_time_data=time_array,
    )

    return on_res_timestream, off_res_timestream


def load_multi_tone_pkl_timestreams(
    timestream_directory: str,
    multisweep_file_name="res_detail_multi_sweep.pkl",
    time_stream_file_identifier="noise_timestream",
) -> HidfmuxTimestream:
    """
    Function to load in a directory of timestream pkl files, extracting the I and Q values for each one and also
    loads the multisweep data. Therefore,the directory must contain one high resolution multisweep file called
    "res_detail_multi_sweep.pkl" and a pkl file for each timestream with "noise_timestream" in the name.

    Returns a HidfmuxTimestream data object.

    :param timestream_directory: Directory containing the multisweep and timestream pickle files.
    :param multisweep_file_name: File name for the high resolution multisweep.
    :param time_stream_file_identifier: String that is common in all timestream pickle file names.
    """

    multi_sweep_path = os.path.join(timestream_directory, multisweep_file_name)

    # Get multisweep data:
    multisweep = load_pkl_multi_sweep_file(multi_sweep_path, sweep_index=0)

    on_res_timestreams_array = np.empty(
        shape=multisweep.tone_name_array.shape, dtype=object
    )
    off_res_timestreams_array = np.empty(
        shape=multisweep.tone_name_array.shape, dtype=object
    )

    timestream_count = 0
    for file in os.listdir(timestream_directory):
        file_path = os.path.join(timestream_directory, file)
        if time_stream_file_identifier in file:  # Ensure file is timestream.
            timestream_count += 1

            time_stream_dict = np.load(
                file_path,
                mmap_mode=None,
                allow_pickle=True,
                fix_imports=True,
                encoding="ASCII",
            )
            # Get tone name of timestream file:
            current_tone_name = list(time_stream_dict.keys())[0]
            sample_rate = time_stream_dict[current_tone_name]["fs"]

            # Get tone calibration sweep data to calibrate timestream IQ
            sweep_data = multisweep.get_tone_sweep_data(
                tone=current_tone_name
            )
            sweep_frequency_array = sweep_data.frequency_array
            calibration_iq_array = multisweep.get_tone_calibration_iq_data(
                tone=current_tone_name
            )

            on_res_tone_frequency = time_stream_dict[current_tone_name]["onres"]["info"]["bias_freq"]
            off_res_tone_frequency = time_stream_dict[current_tone_name]["offres"]["info"]["bias_freq"]

            on_res_rotation_angle = time_stream_dict[current_tone_name]["onres"]["info"]["theta_rot"]
            off_res_rotation_angle = time_stream_dict[current_tone_name]["offres"]["info"]["theta_rot"]
            
            on_res_timestream_i = time_stream_dict[current_tone_name]["onres"]["data"]["i"]
            on_res_timestream_q = time_stream_dict[current_tone_name]["onres"]["data"]["q"]

            # (
            #     on_res_timestream_i,
            #     on_res_timestream_q,
            # ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
            #     i_array=time_stream_dict[current_tone_name]["onres"]["data"]["i"],
            #     q_array=time_stream_dict[current_tone_name]["onres"]["data"]["q"],
            #     tone_frequency=on_res_tone_frequency,
            #     cal_iq_at_bias_freq=None,
            #     sweep_f_array=sweep_frequency_array,
            #     cal_iq_array=calibration_iq_array,
            # )

            off_res_timestream_i = time_stream_dict[current_tone_name]["offres"]["data"]["i"]
            off_res_timestream_q = time_stream_dict[current_tone_name]["offres"]["data"]["q"]

            # (
            #     off_res_timestream_i,
            #     off_res_timestream_q,
            # ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
            #     i_array=time_stream_dict[current_tone_name]["offres"]["data"]["i"],
            #     q_array=time_stream_dict[current_tone_name]["offres"]["data"]["q"],
            #     tone_frequency=off_res_tone_frequency,
            #     cal_iq_at_bias_freq=None,
            #     sweep_f_array=sweep_frequency_array,
            #     cal_iq_array=calibration_iq_array,
            # )

            n_samples = on_res_timestream_i.size

            on_res_timestream = TimestreamData(
                measurement_header=sweep_data.measurement_header,
                on_resonance=True,
                n_timestreams=1,
                sample_rate=sample_rate,
                measurement_length=n_samples / sample_rate,
                tone_frequency=on_res_tone_frequency,
                timestream_i_data=on_res_timestream_i,
                timestream_q_data=on_res_timestream_q,
                timestream_time_data=np.linspace(0, n_samples / sample_rate, n_samples),
            )

            off_res_timestream = TimestreamData(
                measurement_header=sweep_data.measurement_header,
                on_resonance=False,
                n_timestreams=1,
                sample_rate=sample_rate,
                measurement_length=n_samples / sample_rate,
                tone_frequency=off_res_tone_frequency,
                timestream_i_data=off_res_timestream_i,
                timestream_q_data=off_res_timestream_q,
                timestream_time_data=np.linspace(0, n_samples / sample_rate, n_samples),
            )

            # Find index of current tone name in multisweep array:
            tone_index = np.where(multisweep.tone_name_array == current_tone_name)[
                0
            ][0]

            # Assign timestreams to correct index in timestreams array:
            on_res_timestreams_array[tone_index] = on_res_timestream
            off_res_timestreams_array[tone_index] = off_res_timestream

            print(
                f"{timestream_count}/{multisweep.tone_name_array.size} Timestreams loaded"
            )

    # Remove any indices that don't have a corresponding time stream (in case only a subset of timestreams loaded.
    mask = np.where(on_res_timestreams_array != None)

    return HidfmuxTimestream(
        bias_power_array=multisweep.bias_power_array[mask],
        tone_name_array=multisweep.tone_name_array[mask],
        sweep_data_array=multisweep.sweep_data_array[mask],
        on_res_timestreams_array=on_res_timestreams_array[mask],
        off_res_timestreams_array=off_res_timestreams_array[mask],
    )



# def load_multi_tone_pkl_timestreams(
#     timestream_directory: str,
#     multisweep_file_name="res_detail_multi_sweep.pkl",
#     time_stream_file_identifier="noise_timestream",
# ) -> HidfmuxPFBTimestream:
#     """
#     Function to load in a directory of timestream pkl files, extracting the I and Q values for each one and also
#     loads the multisweep data. Therefore,the directory must contain one high resolution multisweep file called
#     "res_detail_multi_sweep.pkl" and a pkl file for each timestream with "noise_timestream" in the name.

#     Returns a HidfmuxTimestream data object.

#     :param timestream_directory: Directory containing the multisweep and timestream pickle files.
#     :param multisweep_file_name: File name for the high resolution multisweep.
#     :param time_stream_file_identifier: String that is common in all timestream pickle file names.
#     """

#     multi_sweep_path = os.path.join(timestream_directory, multisweep_file_name)

#     # Get multisweep data:
#     multisweep = load_pkl_multi_sweep_file(multi_sweep_path, sweep_index=0)

#     on_res_timestreams_array = np.empty(
#         shape=multisweep.tone_name_array.shape, dtype=object
#     )
#     off_res_timestreams_array = np.empty(
#         shape=multisweep.tone_name_array.shape, dtype=object
#     )

#     timestream_count = 0
#     for file in os.listdir(timestream_directory):
#         file_path = os.path.join(timestream_directory, file)
#         if time_stream_file_identifier in file:  # Ensure file is timestream.
#             timestream_count += 1

#             pfb_dict = np.load(
#                 file_path,
#                 mmap_mode=None,
#                 allow_pickle=True,
#                 fix_imports=True,
#                 encoding="ASCII",
#             )

#             # Get tone name of timestream file:
#             current_tone_name = list(pfb_dict.keys())[0]

#             sample_rate = time_stream_dict[current_tone_name]["fs"]

#             # Get tone calibration sweep data to calibrate timestream IQ
#             sweep_data = multisweep.get_tone_sweep_data(
#                 tone=current_tone_name
#             )
#             sweep_frequency_array = sweep_data.frequency_array
#             calibration_iq_array = multisweep.get_tone_calibration_iq_data(
#                 tone=current_tone_name
#             )

#             on_res_tone_frequency = time_stream_dict[current_tone_name]["onres"]["info"]["bias_freq"]
#             off_res_tone_frequency = time_stream_dict[current_tone_name]["offres"]["info"]["bias_freq"]

#             on_res_rotation_angle = time_stream_dict[current_tone_name]["onres"]["info"]["theta_rot"]
#             off_res_rotation_angle = time_stream_dict[current_tone_name]["offres"]["info"]["theta_rot"]
            
#             on_res_timestream_i = time_stream_dict[current_tone_name]["onres"]["data"]["i"]
#             on_res_timestream_q = time_stream_dict[current_tone_name]["onres"]["data"]["q"]

#             # (
#             #     on_res_timestream_i,
#             #     on_res_timestream_q,
#             # ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
#             #     i_array=time_stream_dict[current_tone_name]["onres"]["data"]["i"],
#             #     q_array=time_stream_dict[current_tone_name]["onres"]["data"]["q"],
#             #     tone_frequency=on_res_tone_frequency,
#             #     cal_iq_at_bias_freq=None,
#             #     sweep_f_array=sweep_frequency_array,
#             #     cal_iq_array=calibration_iq_array,
#             # )

#             off_res_timestream_i = time_stream_dict[current_tone_name]["offres"]["data"]["i"]
#             off_res_timestream_q = time_stream_dict[current_tone_name]["offres"]["data"]["q"]

#             # (
#             #     off_res_timestream_i,
#             #     off_res_timestream_q,
#             # ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
#             #     i_array=time_stream_dict[current_tone_name]["offres"]["data"]["i"],
#             #     q_array=time_stream_dict[current_tone_name]["offres"]["data"]["q"],
#             #     tone_frequency=off_res_tone_frequency,
#             #     cal_iq_at_bias_freq=None,
#             #     sweep_f_array=sweep_frequency_array,
#             #     cal_iq_array=calibration_iq_array,
#             # )

#             n_samples = on_res_timestream_i.size

#             on_res_timestream = TimestreamData(
#                 measurement_header=sweep_data.measurement_header,
#                 on_resonance=True,
#                 n_timestreams=1,
#                 sample_rate=sample_rate,
#                 measurement_length=n_samples / sample_rate,
#                 tone_frequency=on_res_tone_frequency,
#                 timestream_i_data=on_res_timestream_i,
#                 timestream_q_data=on_res_timestream_q,
#                 timestream_time_data=np.linspace(0, n_samples / sample_rate, n_samples),
#             )

#             off_res_timestream = TimestreamData(
#                 measurement_header=sweep_data.measurement_header,
#                 on_resonance=False,
#                 n_timestreams=1,
#                 sample_rate=sample_rate,
#                 measurement_length=n_samples / sample_rate,
#                 tone_frequency=off_res_tone_frequency,
#                 timestream_i_data=off_res_timestream_i,
#                 timestream_q_data=off_res_timestream_q,
#                 timestream_time_data=np.linspace(0, n_samples / sample_rate, n_samples),
#             )

#             # Find index of current tone name in multisweep array:
#             tone_index = np.where(multisweep.tone_name_array == current_tone_name)[
#                 0
#             ][0]

#             # Assign timestreams to correct index in timestreams array:
#             on_res_timestreams_array[tone_index] = on_res_timestream
#             off_res_timestreams_array[tone_index] = off_res_timestream

#             print(
#                 f"{timestream_count}/{multisweep.tone_name_array.size} Timestreams loaded"
#             )

#     # Remove any indices that don't have a corresponding time stream (in case only a subset of timestreams loaded.
#     mask = np.where(on_res_timestreams_array != None)

#     return HidfmuxTimestream(
#         bias_power_array=multisweep.bias_power_array[mask],
#         tone_name_array=multisweep.tone_name_array[mask],
#         sweep_data_array=multisweep.sweep_data_array[mask],
#         on_res_timestreams_array=on_res_timestreams_array[mask],
#         off_res_timestreams_array=off_res_timestreams_array[mask],
#     )
