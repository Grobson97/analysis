"""
Set of functions to open different data files and return useful data objects.
"""

import numpy as np
from source.models.resonator_measurement.sweep_data import SweepData
from source.models.resonator_measurement.measurement_header import ResonatorMeasurementHeader
from source.models.resonator_measurement.timestream_data import TimestreamData


def load_continuous_sweep(file_path: str) -> SweepData:
    """
    Function to load a continuous sweep .npz data  from the Eagle data acquisition system.

    Sweep data is returned as arrays with the following shape: (N repeats, M data points).

    :param file_path: Path to the data file.
    """
    continuous_sweep_data = np.load(file=file_path, allow_pickle=True)

    dut = continuous_sweep_data["dut"].item()
    date = continuous_sweep_data["date"].item()
    repeats = continuous_sweep_data["repeats"].item()
    state_variables = continuous_sweep_data["state_variables"].item()
    frequency_data = continuous_sweep_data["frequency_data"]
    complex_data = continuous_sweep_data["complex_data"]

    for key in state_variables.keys():
        if "cryostat_channel" in key:
            channel_key = key

    warm_input_attenuation = state_variables[channel_key]["parameters"]["warm_input_attenuation"]["value"]
    cold_input_attenuation = state_variables[channel_key]["parameters"]["cold_input_attenuation"]["value"]
    input_variable_attenuation = state_variables["input_attenuator"]["parameters"]["attenuation"]["value"]

    input_power = 0 - warm_input_attenuation - input_variable_attenuation - cold_input_attenuation

    temperature = state_variables["eagle"]["parameters"]["t_mxc"]["value"]

    measurement_header = ResonatorMeasurementHeader(
        dut=dut,
        date=date,
        tone_name=None,
        set_temperature=temperature,
        actual_temperature=temperature,
        power=input_power
    )

    sweep_data = SweepData(
        measurement_header=measurement_header,
        frequency_array=frequency_data,
        i=np.real(complex_data),
        q=np.imag(complex_data)
    )

    return sweep_data


def load_segmented_sweep(file_path: str) -> tuple:
    """
    Function to load a segmented sweep .npz data  from the Eagle data acquisition system.
    Returns the sweep data as well as the tone names and tone frequencies in a list.

    Sweep data is returned as arrays with the following shape: (N repeats, M tones, X data points).


    :param file_path: Path to the data file.
    """

    segmented_sweep_data = np.load(file=file_path, allow_pickle=True)

    dut = segmented_sweep_data["dut"].item()
    date = segmented_sweep_data["date"].item()
    repeats = segmented_sweep_data["repeats"].item()
    state_variables = segmented_sweep_data["state_variables"].item()
    tone_names = segmented_sweep_data["tone_names"]
    tone_frequencies = segmented_sweep_data["tone_frequencies"]
    frequency_data = segmented_sweep_data["frequency_data"]
    complex_data = segmented_sweep_data["complex_data"]

    for key in state_variables.keys():
        if "cryostat_channel" in key:
            channel_key = key

    warm_input_attenuation = state_variables[channel_key]["parameters"]["warm_input_attenuation"]["value"]
    cold_input_attenuation = state_variables[channel_key]["parameters"]["cold_input_attenuation"]["value"]
    input_variable_attenuation = state_variables["input_attenuator"]["parameters"]["attenuation"]["value"]

    input_power = 0 - warm_input_attenuation - input_variable_attenuation - cold_input_attenuation

    temperature = state_variables["eagle"]["parameters"]["t_mxc"]["value"]

    measurement_header = ResonatorMeasurementHeader(
        dut=dut,
        date=date,
        tone_name=None,
        set_temperature=temperature,
        actual_temperature=temperature,
        power=input_power
    )

    sweep_data = SweepData(
        measurement_header=measurement_header,
        frequency_array=frequency_data,
        i=np.real(complex_data),
        q=np.imag(complex_data)
    )

    return tone_names, tone_frequencies, sweep_data



def load_timestream_sweep(file_path: str) -> SweepData:
    """
    Function to load a timestream sweep .npz data from the Eagle data acquisition system.
    Returns the sweep data.

    Sweep data is returned as I and Q arrays with the following shape: (N repeats, X data points)
    and the frequency array is returned as an array with shape (X data points) and corresponds to
    all repeats.

    :param file_path: Path to the data file.
    """

    sweep_file_data = np.load(file=file_path, allow_pickle=True)

    dut = sweep_file_data["dut"]
    date = sweep_file_data["date"]
    tone_name = sweep_file_data["tone_name"]
    state_variables = sweep_file_data["state_variables"].item()
    frequency_data = sweep_file_data["frequency_data"]
    complex_data = sweep_file_data["complex_data"]


    for key in state_variables.keys():
        if "cryostat_channel" in key:
            channel_key = key

    warm_input_attenuation = state_variables[channel_key]["parameters"]["warm_input_attenuation"]["value"]
    cold_input_attenuation = state_variables[channel_key]["parameters"]["cold_input_attenuation"]["value"]
    input_variable_attenuation = state_variables["input_attenuator"]["parameters"]["attenuation"]["value"]

    input_power = 0 - warm_input_attenuation - input_variable_attenuation - cold_input_attenuation

    temperature = state_variables["eagle"]["parameters"]["t_mxc"]["value"]

    measurement_header = ResonatorMeasurementHeader(
        dut=dut,
        date=date,
        tone_name=tone_name,
        set_temperature=temperature,
        actual_temperature=temperature,
        power=input_power
    )

    sweep_data = SweepData(
        measurement_header=measurement_header,
        frequency_array=frequency_data,
        i=np.real(complex_data),
        q=np.imag(complex_data)
    )

    return sweep_data


def load_iq_timestream(file_path: str) -> TimestreamData:
    """
    Function to load a timestream .npz data file from the Eagle data acquisition system.
    Returns the timestream data as a TimestreamData object.

    timestream data is returned in a 2D array of shape (N repeats, M samples).

    :param file_path: Path to the data file.
    """

    sweep_file_data = np.load(file=file_path, allow_pickle=True)

    dut = sweep_file_data["dut"]
    date = sweep_file_data["date"]
    tone_name = sweep_file_data["tone_name"]
    state_variables = sweep_file_data["state_variables"].item()
    repeats = sweep_file_data["repeats"]
    tone_frequency = state_variables["smf"]["parameters"]["frequency"]["value"]
    n_samples = state_variables["daq"]["parameters"]["n_samples"]["value"]
    sampling_rate = state_variables["daq"]["parameters"]["sampling_rate"]["value"]
    complex_data = sweep_file_data["iq_timestream"]
    
    sampling_rate_from_file_name = int(file_path[file_path.find("0x") + 2: file_path.find("0Hz")+1])
    if sampling_rate != sampling_rate_from_file_name:
        sampling_rate = sampling_rate_from_file_name

    if "ONRES" in file_path:
        on_res = True
    else:
        on_res = False

    for key in state_variables.keys():
        if "cryostat_channel" in key:
            channel_key = key

    warm_input_attenuation = state_variables[channel_key]["parameters"]["warm_input_attenuation"]["value"]
    cold_input_attenuation = state_variables[channel_key]["parameters"]["cold_input_attenuation"]["value"]
    input_variable_attenuation = state_variables["input_attenuator"]["parameters"]["attenuation"]["value"]

    input_power = 0 - warm_input_attenuation - input_variable_attenuation - cold_input_attenuation

    temperature = state_variables["eagle"]["parameters"]["t_mxc"]["value"]

    measurement_header = ResonatorMeasurementHeader(
        dut=dut,
        date=date,
        tone_name=tone_name,
        set_temperature=temperature,
        actual_temperature=temperature,
        power=input_power
    )

    timestream_data = TimestreamData(
        measurement_header=measurement_header,
        on_resonance=on_res,
        n_timestreams=repeats,
        sample_rate=sampling_rate,
        measurement_length=n_samples / sampling_rate,
        tone_frequency=tone_frequency,
        timestream_i_data=np.real(complex_data),
        timestream_q_data=np.imag(complex_data),
        timestream_time_data=np.linspace(0, n_samples / sampling_rate, n_samples)
    )

    return timestream_data
