# TODO finish this class
class Resonator:
    def __init__(
        self,
        identifier: str,
        f0: float,
        qr: float,
        qc: float,
        qi: float,
        amplitude=1.0,
    ) -> None:

        """
        Creates new instance of a resonator.

        :param identifier: name or ID for resonator.
        :param f0: Resonant frequency of the resonator.
        :param qr: Total quality factor of resonator.
        :param qc: Coupling quality factor.
        :param qi: Loss quality factor.
        :param amplitude: Amplitude of resonator, i.e the baseline. default is 1.0.
        """

        self.identifier = identifier
        self.f0 = f0
        self.qr = qr
        self.qc = qc
        self.qi = qi
        self.amplitude = amplitude

    # TODO make function which plots the skewed lorentzian of the resonator
