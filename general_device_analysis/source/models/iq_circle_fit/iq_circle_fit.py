import numpy as np

from source.models.iq_circle_fit.cable_delay import CableDelay


class IQCircleFit:
    """
    Class to represent the fit parameters of a circle fit to an IQ circle sweep of a resonator.
    """

    def __init__(
        self,
        i_centre: float,
        q_centre: float,
        radius: float,
        calibrated_i_array=None,
        calibrated_q_array=None,
        cable_delay=None,
        rotation_angle=None,
        phase_polynomial=None,
    ):
        """
        Creates instance of IQCircleFit.

        :param i_centre: i coordinate of the circles centre.
        :param q_centre: q coordinate of the circles centre.
        :param radius: Radius of the circle.
        :param calibrated_i_array: I array after sweep I values have been calibrated via cable delay, rotation, etc.
        :param calibrated_q_array: Q array after sweep Q values have been calibrated via cable delay, rotation, etc.
        :param cable_delay: CableDelay object from fitting a cable delay.
        :param rotation_angle: Angle by which the IQ circle has been rotated.
        :param phase_polynomial: numpy Poly1D object for the Phase-frequency polynomial used to interpolate df.
        """

        self.i_centre = i_centre
        self.q_centre = q_centre
        self.radius = radius
        self.calibrated_i_array = calibrated_i_array
        self.calibrated_q_array = calibrated_q_array
        self.cable_delay = cable_delay
        self.rotation_angle = rotation_angle
        self.phase_polynomial = phase_polynomial

    def centre_and_normalise_iq(self, i: np.ndarray or float, q: np.ndarray or float):
        """
        Function to centre and normalise i and q coordinate(s) about the centre of the circle fit.
        Returns the normalised and centred values as the unpackable i, q.

        :param i: i coordinate or array to be centred and normalised.
        :param q: q coordinate or array to be centred and normalised.
        """

        return (i - self.i_centre) / self.radius, (q - self.q_centre) / self.radius
