import numpy as np


class CableDelay:
    """
    Class to represent the amplitude and tau values obtained from a cable delay fit on an IQ sweep of a resonator.
    """

    def __init__(self, amplitude: float, tau: float):
        """
        Create an instance of a cable delay.

        :param amplitude: Amplitude of cable delay.
        :param tau: Tau of cable delay.
        """

        self.amplitude = amplitude
        self.tau = tau

    def calculate_delay_at_frequency(
        self, frequency: np.ndarray or float
    ) -> np.ndarray or float:
        """
        Function to calculate the cable delay at a given frequency.

        :param frequency: Array or float to calculate the delay for.
        """
        return np.exp(-2 * np.pi * 1j * frequency * self.tau)
