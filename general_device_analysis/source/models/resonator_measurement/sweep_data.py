import numpy as np
from dataclasses import dataclass
from source.models.resonator_measurement.measurement_header import ResonatorMeasurementHeader


@dataclass
class SweepData:
    """
    Class to represent the data for any kind of frequency correlated in-phase (I) and quadrature (Q) measurements.
    """

    measurement_header: ResonatorMeasurementHeader
    frequency_array: np.ndarray
    i: np.ndarray
    q: np.ndarray

    def get_iq_array(self) -> np.ndarray:
        """
        :return: complex array of I and Q data.
        """

        return np.array(self.i + 1j * self.q)
    

    

