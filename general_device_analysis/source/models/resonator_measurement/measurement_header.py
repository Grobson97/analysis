from dataclasses import dataclass


@dataclass
class ResonatorMeasurementHeader:
    """
    A general measurement header class to contain only the key measurement information that is relevent for all
    measurements of a single KID/ tone. e.g. timestream, an IQ sweep of a KID.
    """

    dut: str or None  # Device under test
    tone_name: str or None  # Name assigned to the tone.
    date: str or None  # Date of measurement
    set_temperature: float or None  # The target/intended temperature
    actual_temperature: float or None  # The actual temperature of the measurement (if known).
    power: float or None  # Power value as reference for the measurement (this might be the ROC units for a HIDFMUX
    # sweep or the variable attenuation for a homodyne measurement or the calculated power at the resonator)
