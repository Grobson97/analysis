from dataclasses import dataclass
from source.models.fits.fits_header import FitsHeader


@dataclass
class FitsCWHeader(FitsHeader):
    """
    Class to represent all the fields within a fits CW file header that are unique to a CW file.
    """

    tone_name: str  # Tone name
    n_timestreams: float  # Number of repeated timestreams per measurement.
    sample_rate: float  # Sample rate in Hz.
    measurement_length: float  # Length of time each timestream is measured for (s).
    tone_frequency: float  # Frequency the tone is set to.
    i_f0: float  # I at f0.
    q_f0: float  # Q at f0.
    di_df: float  # change in I vs change in f.
    dq_df: float  # change in q vs change in f.
