import numpy as np
from dataclasses import dataclass


@dataclass
class FitsHeader:
    """
    Class to represent all the fields of a fits header that are constant across all fits files.
    """

    data_type: str  # Type of measurement, e.g. sweep or CW (continuous wave)...
    date: str  # Date of measurement.
    time: str  # Time of measurement
    data_form: str  # Format of data array (column titles).
    dut: str  # Device under test
    channel: str  # Channel description
    ultra_cold_attenuation: float
    cold_attenuation: float
    room_temperature_attenuation: float
    input_attenuation: float
    output_attenuation: float
    set_temperature: float
    total_amplifier_gain: float

    def print_attenuation_values(self) -> None:
        """
        Function to print the value of each attenuation
        """
        print("Data file attenuation values:")
        print(f"Room temperature attenuation = {self.room_temperature_attenuation}")
        print(f"Variable Room temperature attenuation = {self.input_attenuation}dB")
        print(f"Cold attenuation = {self.cold_attenuation}dB")
        print(f"Ultra-cold attenuation = {self.ultra_cold_attenuation}dB")
        print(f"Output attenuation = {self.output_attenuation}dB")
        print(f"Total Amplifier Gain = {self.total_amplifier_gain}dB")

        return None

    def get_total_attenuation(
        self,
        total_gain: float = None,
        ultra_cold_attenuation: float = None,
        cold_attenuation: float = None,
        room_temperature_attenuation: float = None,
        input_attenuation: float = None,
        output_attenuation: float = None,
        cable_attenuation=0.0,
    ) -> float:
        """
        Function to calculate the total attenuation at the time of measurement. If no argument given for each field
        the class attribute of the instance will be used.

        :param total_gain: Total gain in dB from amplifiers.
        :param ultra_cold_attenuation: Input attenuation at ultra cold stage.
        :param cold_attenuation: Input attenuation at cold stage.
        :param room_temperature_attenuation: Hard input attenuation at room temperature.
        :param input_attenuation: Variable room temperature input attenuation.
        :param output_attenuation: Output attenuation.
        :param cable_attenuation: Attenuation from cables, default is 0.
        """

        # If statements to allow overrides of attenuation values.
        if total_gain is None:
            total_gain = self.total_amplifier_gain
        if ultra_cold_attenuation is None:
            ultra_cold_attenuation = self.ultra_cold_attenuation
        if cold_attenuation is None:
            cold_attenuation = self.cold_attenuation
        if room_temperature_attenuation is None:
            room_temperature_attenuation = self.room_temperature_attenuation
        if input_attenuation is None:
            input_attenuation = self.input_attenuation
        if output_attenuation is None:
            output_attenuation = self.output_attenuation

        total_attenuation = (
            input_attenuation
            + room_temperature_attenuation
            + cold_attenuation
            + ultra_cold_attenuation
            + output_attenuation
            + cable_attenuation
        )

        return total_gain - total_attenuation
