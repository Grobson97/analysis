import numpy as np
from dataclasses import dataclass
from typing import Optional

from source.models.fits.fits_header import FitsHeader


@dataclass
class FitsVNASweepHeader(FitsHeader):
    """
    Class to represent all the fields within a fits sweep file header that are unique to a Sweep file.
    """

    vna_power: float  # POWER OF VNA (dBm). keyword: VNAPOWER
    vna_bandwidth: float  # Bandwidth of VNA (Hz). keyword: VNABANDW
    number_of_tones: Optional[float] = None  # Number of tones (If tones list is used).
