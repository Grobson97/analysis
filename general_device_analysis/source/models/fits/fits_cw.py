import numpy as np
from dataclasses import dataclass
from astropy.io import fits

from source.models.fits.fits import Fits
from source.models.fits.fits_cw_header import FitsCWHeader


@dataclass
class FitsCW(Fits):
    """
    Class to represent a fits file that contains CW data/information.
    """

    header: FitsCWHeader
