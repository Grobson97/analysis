import numpy as np
from dataclasses import dataclass
from astropy.io import fits

from source.models.fits.fits_header import FitsHeader
from source.models.resonator_measurement.sweep_data import SweepData


@dataclass
class Fits:
    """
    Represents the standard fields that are always in a sweep file.
    """

    data: SweepData
