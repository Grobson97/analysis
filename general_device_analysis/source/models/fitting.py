
import numpy as np
import lmfit 
import time
import sys
import multiprocessing

def linear_resonator(f, f0, qr, qc_real, qc_imag, amp):
    x0 = (f-f0)/f0            # fractional detuning
    qe = qc_real + 1j*qc_imag  # complex coupling quality factor
    y0 = qr*x0
    return amp*(1 - ( qr / qe / (1 + 2j * y0)))

class LinearResonatorModel(lmfit.model.Model):
    """ defines an lmfit model and applies a guess method to estimate initial parameters for fitting"""
    def __init__(self,*args, **kwargs):
        super(LinearResonatorModel, self).__init__(linear_resonator,*args, **kwargs)

    def guess(self, data, f=None, **kwargs):

        m          = np.abs(data)
        argmin_s21 = np.argmin(m)
        argmax_s21 = np.argmax(m)
        mminmag    = m[argmin_s21]
        fminmag    = f[argmin_s21]
        f0_guess   = fminmag
        amp_guess  = (m[0] + m[-1])/2.
        halfmax    = (amp_guess + mminmag)/2.
        fwhm       = 2*np.abs( fminmag - f[np.abs(m-halfmax).argmin()] )
        q_fwhm     = fminmag / fwhm
        qr_guess   = q_fwhm
        #qc_guess   = qr_guess/(1 - mminmag/amp_guess)
        qc_real_guess   = qr_guess/(1 - mminmag/amp_guess)
        qc_imag_guess = 0
        if abs(np.abs(qc_real_guess - qr_guess))<1:
            qc_real_guess = qr_guess+1

        qi_guess      = 1./(1./(qr_guess) - 1./(qc_real_guess))


        #be careful with zeros to avoid divbyzero screwing things up

        f_min = kwargs.pop('f0_min',1)
        f_max = kwargs.pop('f0_max',np.inf)
        amp_min = kwargs.pop('amp_min',1e-9) #A_guess/3
        amp_max = kwargs.pop('amp_max',np.inf) #A_guess*3
        qr_min = kwargs.pop('qr_min',1) #-np.inf #0
        qr_max = kwargs.pop('qr_max',np.inf) #
        #qc_min = kwargs.pop('qc_min',2)
        #qc_max = kwargs.pop('qc_max',np.inf)
        qc_real_min = kwargs.pop('qc_min',-np.inf) #2)
        qc_real_max = kwargs.pop('qc_max',np.inf)
        qc_imag_min = kwargs.pop('qc_imag_min',-np.inf) #Qe_real_min
        qc_imag_max = kwargs.pop('qc_imag_max',+np.inf) #Qe_real_max
        qi_min = kwargs.pop('qi_min',2)
        qi_max = kwargs.pop('qi_max',np.inf)


        params = lmfit.Parameters()
        #params.add('f0',value=f0_guess,min=f_min, max=f_max,vary=True)
        #params.add('qr',value=qr_guess,min=qr_min, max=qr_max,vary=True)
        #params.add('qr_minus_qc',value = qr_guess-qc_guess,max=-0.1,vary=True)
        #params.add('qc',value=qc_guess,min=qc_min,max=qc_max,expr='qr-qr_minus_qc')
        #params.add('qc_imag',value=qc_imag_guess,min=qc_imag_min,max=qc_imag_max,vary=True)
        #params.add('amp',value=amp_guess,min=amp_min, max=amp_max,vary=True)
        #params.add'qi',vary=False,expr='1./(1./qr - 1./qc)',min=qi_min,max=qi_max)

        params.add('f0',value=f0_guess,min=f_min, max=f_max,vary=True)
        params.add('qr',value=qr_guess,min=qr_min, max=qr_max,vary=True)
        params.add('qr_minus_qc_real',value = qr_guess-qc_real_guess,max=np.inf,vary=True)
        params.add('qc_real',value=qc_real_guess,min=qc_real_min,max=qc_real_max,expr='qr-qr_minus_qc_real')
        params.add('qc_imag',value=qc_imag_guess,min=qc_imag_min,max=qc_imag_max,vary=True)
        params.add('amp',value=amp_guess,min=amp_min, max=amp_max,vary=True)
        params.add('qi',vary=False,expr='1./(1./qr - 1./(qc_real+abs(qc_imag))',min=qi_min,max=qi_max)

        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


def fit_linear_resonator(f,z,**kwargs):
    baseline_polyorder = kwargs.pop('baseline_polyorder',1)
    #find nans
    finite = np.isfinite(z)
    #remove complex baseline
    ff       = f[finite]
    zf       = z[finite]
    zu       = np.unwrap(np.angle(zf))


    m = np.abs(zf)
    argmin_s21 = np.argmin(m)
    argmax_s21 = np.argmax(m)
    mminmag = m[argmin_s21]
    fminmag = ff[argmin_s21]
    f0_guess = fminmag
    A_guess = (m[0] + m[-1])/2.
    halfmax = (A_guess + mminmag)/2.
    fwhm = 2*np.abs( fminmag - f[np.abs(m-halfmax).argmin()] )
    offres_linewidths = kwargs.pop('offres_linewidths',10)
    offres = (ff<fminmag-offres_linewidths/2*fwhm) | (ff>fminmag+offres_linewidths/2*fwhm)
    baseline_iq = kwargs.pop('baseline_iq',False)
    if np.any(offres):
        if baseline_iq:
            basei     = np.polynomial.Polynomial.fit(ff[offres],zf[offres].real,baseline_polyorder)(ff)
            baseq     = np.polynomial.Polynomial.fit(ff[offres],zf[offres].imag,baseline_polyorder)(ff)
            basez     = basei+1j*baseq
            basemag   = np.abs(basez)
            basephase = np.angle(basez)
        else:
            basemag = np.polynomial.Polynomial.fit(ff[offres],abs(zf[offres]),baseline_polyorder)(ff)
            basephase = np.polynomial.Polynomial.fit(ff[offres],np.unwrap(np.angle((zf)))[offres],baseline_polyorder)(ff)
        #basemag = np.poly1d(np.polyfit(ff[offres],abs(zf[offres]),5))(ff)
        #basephase = np.poly1d(np.polyfit(ff[offres],np.unwrap(np.angle((zf[offres]))),3))(ff)
    else:
        if baseline_polyorder == 0:
            basemag = np.ones_like(zf)*0.5*(abs(zf[0])+abs(zf[-1]))
            #basephase = np.linspace(np.unwrap(np.angle(zf))[0],np.unwrap(np.angle(zf))[-1],len(zf))
            left = ff<(ff[0]+0.1*(ff[-1]-ff[0]))
            right = ff>(ff[0]+(1-0.1)*(ff[-1]-ff[0]))

            bpleft = np.polynomial.Polynomial.fit(ff[left],np.unwrap(np.angle((zf)))[left],1).convert()
            bpright = np.polynomial.Polynomial.fit(ff[right],np.unwrap(np.angle((zf)))[right],1).convert()
            basephase = 0.5*(bpleft.coef[0]+bpright.coef[0]) + ff*0.5*(bpleft.coef[1]+bpright.coef[1])
        else:
            #basephase = np.zeros(len(ff))
            #basemag = np.ones(len(ff))
            #basephase = np.full(len(zf),np.mean((np.unwrap(np.angle(zf))[0],np.unwrap(np.angle(zf))[-1])))
            basephase = np.linspace(np.unwrap(np.angle(zf))[0],np.unwrap(np.angle(zf))[-1],len(zf))

            #basemag = np.full(len(zf),np.mean((np.abs(zf[0]),np.abs(zf[-1]))))
            basemag = np.full(len(zf),np.max((np.abs(zf[0]),np.abs(zf[-1]))))

    baseline = basemag*np.exp(1j*basephase)

    #basemag = np.linspace(abs(zf[0]),abs(zf[-1]),len(zf))
    #basephase = np.linspace(zu[0],zu[-1],len(zu))
    #baseline = basemag*np.exp(1j*basephase)
    #baseline = np.linspace(abs(zf[0]),abs(zf[-1]),len(zf))*np.exp(1j*np.linspace(zu[0],zu[-1],len(zu)))

    z0=zf/baseline

    r      = LinearResonatorModel()

    #estimate intital parameters
    guess  = r.guess(z0,f=f[finite])

    #apply weighting
    weights = kwargs.pop('weights',None)
    if type(weights) is type(None):
        weights=np.ones_like(f)
    else:
        weights = weights[finite]

    #select method - don't use 'leastsq' as it is slow and inaccurate
    method = kwargs.pop('method','least_squares')

    #do the fitting, catch value errors due to bug in least_squares
    try:
        result = r.fit(z0[finite],
                       guess,
                       f=f[finite],
                       nan_policy='omit',
                       method=method,
                       weights=weights,**kwargs)
    except ValueError:
        guess_eval = r.eval(params=guess,f=f)

        return guess_eval*baseline, guess,None, guess,baseline
    except KeyboardInterrupt:
        raise KeyboardInterrupt

    #evaluate model based on fit parameters
    fit    = r.eval(params=result.params,f=f)

    #reapply baseline
    fit=fit*baseline

    #catch serious error case
    if np.all(~np.isfinite(fit)):
        fit = z

    fitparams = result.params

    return fit,fitparams,result, guess,baseline


def nonlinear_resonator(f, f0, Qr, Qe_real, Qe_imag, Amp, anl, Qi_dummy,Qc_dummy,sweep_direction='up', root=None,qr_minus_qc=None):
    """
    f: array of sweep frequencies
    f0: resonance frequency
    Qr: resonator quality factor
    Qe_real, Qe_im: real and imaginary parts of complex coupling quality factor
    A: baseline level
    a: non-linearity parameter, accounts for the power dependent non-linear kinetic inductance
    
    sweep_direction: either 'up' or 'down' depending on direction of swept source.
    
    Qi_dummy: not used, included for defining an expression in the lmfit model
    Qc_dummy: not used, included for defining an expression in the lmfit model
    
    root: for debugging, if value is 1, 2 or 3 will return the corresponding solution of the cubic.
    
    returns s21 of model resonator
    """

    #print('params:',f0, Qr, Qe_real, Qe_imag, Amp, anl)
    x0 = (f-f0)/f0            # fractional detuning
    Qe = Qe_real+ 1j*Qe_imag  # complex coupling quality factor
    y0 = Qr*x0 + 0j           # detuning in linewidths, must be complex to avoid nans when solving the cubic
    anl=anl+0j
    
    #need to solve y=y0 + a/(1+4y**2), for Swenson paper.
    #using general solution to cubic from https://en.wikipedia.org/wiki/Cubic_equation:

    #a primitive root of unity raised to some powers:
    xi  = 0.5*(-1 + np.sqrt(3)*1j)
    xi0 = xi**0 # 1+0j
    xi1 = xi**1 # -0.5 * (1 + sqrt(3)*j)
    xi2 = xi**2 # -0.5 * (1 - sqrt(3)*j)
    xi3 = xi**3 # 1+0j

    #write the cubic in the form Ax^3 + Bx^2 + Cx + D = 0: 4y^3 - 4*y0*y^2 + y - y0 - anl = 0
    A,B,C,D = 4,-4*y0,1,-y0-anl

    #compute some intermediate formulas:
    d0   = B**2-3*A*C
    d1   = 2*B**3 - 9*A*B*C + 27*A**2*D

    det  = np.sqrt(d1**2-4*d0**3)

    root1 = np.zeros_like(y0)
    root2 = np.zeros_like(y0)
    root3 = np.zeros_like(y0)

    #start with addition of the determinant and check for zeros and if any found, then change to subtraction of determinant. Check again and if zeros still found, then replace the fraction in the root with zero.
    cr = (0.5*(d1+det))**(1./3.) #adding det
    cz = (cr==0) # any zeros?
    ncz=~cz # where no zeros

    root1[ncz] = -1./3./A*(B[ncz]+cr[ncz]*xi0+d0[ncz]/cr[ncz]/xi0)
    root2[ncz] = -1./3./A*(B[ncz]+cr[ncz]*xi1+d0[ncz]/cr[ncz]/xi1)
    root3[ncz] = -1./3./A*(B[ncz]+cr[ncz]*xi2+d0[ncz]/cr[ncz]/xi2)


    if np.any(cz):
        #print('xxxx')
        crsub     = (0.5*(d1[cz]-det[cz]))**(1./3.) #subtracting det
        root1[cz] = -1./3./A*(B[cz]+crsub*xi0+d0[cz]/crsub/xi0)
        root2[cz] = -1./3./A*(B[cz]+crsub*xi1+d0[cz]/crsub/xi1)
        root3[cz] = -1./3./A*(B[cz]+crsub*xi2+d0[cz]/crsub/xi2)

        cr[cz]    = crsub
        cz = (cr==0) #still any zeros left?
        if np.any(cz):
            #print('yyyy')
            root1[cz] = -1./3./A*(B[cz])
            root2[cz] = -1./3./A*(B[cz])
            root3[cz] = -1./3./A*(B[cz])
    r1,r2,r3=root1,root2,root3

    if sweep_direction=='up':
        rup=np.copy(r1)
        where_r1 = np.isclose(r1.imag,0)
        rmax = np.maximum(r2[~where_r1].real,r3[~where_r1].real) #where r1 isnt real, use the max of the other two roots. seemingly, the root with the highest real part is always real
        rup[~where_r1]=rmax; #use the max of r1 and r2 where root1 isnt real
        s21 = Amp*(1 - ( Qr / Qe / (1 + 2j * rup)))
        return s21

    elif sweep_direction=='down':
        rdown=np.copy(r1)
        where_r2r3=np.isclose(r2.imag,0)|np.isclose(r3.imag,0)
        rmax = np.maximum(r2[where_r2r3].real,r3[where_r2r3].real) #where r2 or r3 are real, use max of r2,r3
        rdown[where_r2r3]=rmax
        s21 = Amp*(1 - ( Qr / Qe / (1 + 2j * rdown)))
        return s21


    if sweep_direction=='up':
        root=np.copy(root1)
        select1 = (~np.isclose(imag(root1),0))
        select2 = (~np.isclose(imag(root1),0)) & (np.isclose(imag(root2),0))
        root[select1] = root2[select2]
    elif sweep_direction=='down':
        root=np.copy(root2)
        select = (~np.isclose(imag(root2),0))
        root[select] = root1[select]


    s21 = Amp*(1 - ( Qr / Qe / (1 + 2j * root)))
    return s21


class NonlinearResonatorModel(lmfit.model.Model):
    """ defines an lmfit model and applies a guess method to estimate initial parameters for fitting"""


    def __init__(self,*args, **kwargs):
        
        super(NonlinearResonatorModel, self).__init__(nonlinear_resonator,*args, **kwargs)

    def guess(self, data, f=None, **kwargs):
                
        m = np.abs(data)
        argmin_s21 = np.argmin(m)
        argmax_s21 = np.argmax(m)
        mminmag = m[argmin_s21]
        fminmag = f[argmin_s21]
        
        #speed = np.abs(np.gradient(data))
        #argmax_speed = np.argmax(speed)
        #fmaxspeed = f[argmax_speed]
        
        f0_guess = fminmag
        #f0_guess = fmaxspeed
        #fmin = np.min(f)-1e6 #.1e9
        #fmax = np.max(f)+1e6 # 2e9

        A_guess = (m[0] + m[-1])/2.

        #halfmax = (A_guess + (A_guess + mminmag)/2.)/2
        halfmax = (A_guess + mminmag)/2.
        fwhm = 2*np.abs( fminmag - f[np.abs(m-halfmax).argmin()] )
        q_fwhm = fminmag / fwhm
        Qr_guess = q_fwhm

        Qe_real_guess = Qr_guess/(1 - mminmag/A_guess)
        if abs(Qe_real_guess - Qr_guess)<1:
            Qe_real_guess = Qr_guess+1
        #Qe_real_min = -np.inf #Qe_real_guess/2. #-np.inf
        #Qe_real_max = np.inf #Qe_real_guess*2. #+np.inf

        Qe_imag_guess = 0
        Qe_imag_min =  -np.inf
        Qe_imag_max = +np.inf
        #Qe_imag_min =  -10e6
        #Qe_imag_max = 10e6

        a_guess = 0.0#1e-5#0.001

        Qi_dummy_guess = 1./(1./(Qr_guess) - 1./(Qe_real_guess))
        #Qi_dummy_guess = 1./(1./(Qr_guess) - 1./np.sqrt(Qe_real_guess**2+Qe_imag_guess**2))
        #100e6#np.inf

        #Qc_dummy_guess = np.sqrt(Qe_real_guess**2+Qe_imag_guess**2)
        Qc_dummy_guess = Qe_real_guess

        #be careful with zeros to avoid divbyzero screwing things up

        f_min = 1
        f_max = np.inf
        #f_min = min(fminmag-fwhm/10,fminmag-(f[1]-f[0]))
        #f_max = max(fminmag+fwhm/10,fminmag+(f[1]-f[0]))
        A_min = 1e-9#A_guess/3
        A_max = np.inf#A_guess*3
        Qr_min = 1#-np.inf #0
        Qr_max = np.inf
        Qe_real_min = 2
        Qe_real_max = np.inf
        a_min = 0.0 # 1e-6
        a_max =  100
        Qc_dummy_min = Qe_real_min
        Qc_dummy_max = Qe_real_max#np.inf
        Qi_dummy_min = 2
        Qi_dummy_max = 1e8


        params = lmfit.Parameters()

        params.add('f0',value=f0_guess,min=f_min, max=f_max,vary=True)
        params.add('Qr',value=Qr_guess,min=Qr_min, max=Qr_max,vary=True)
        params.add('qr_minus_qc',value = Qr_guess-Qe_real_guess,max=-0.1,vary=True)
        params.add('Qe_real',value=Qe_real_guess,min=Qe_real_min,max=Qe_real_max,expr='Qr-qr_minus_qc')
        params.add('Qe_imag',value=Qe_imag_guess,min=Qe_imag_min,max=Qe_imag_max,vary=True)
        params.add('Amp',value=A_guess,min=A_min, max=A_max,vary=True)
        params.add('anl',value=a_guess,min=a_min, max=a_max,vary=True)
        params.add('Qi_dummy',vary=False,expr='1./(1./Qr - 1./Qe_real)',min=Qi_dummy_min,max=Qi_dummy_max)
        params.add('Qc_dummy',vary=False,expr='Qe_real',min=Qe_real_min,max=Qe_real_max)



        #params = self.make_params(Qr=Qr_guess,
                                  #Qe_real=Qe_real_guess,
                                  #Qe_imag=Qe_imag_guess,
                                  #f0=f0_guess,
                                  #A=A_guess,
                                  #a=a_guess,
                                  #Qi_dummy=Qi_dummy_guess,
                                  #Qc_dummy=Qc_dummy_guess)


        #params['%sQe_real' % self.prefix].set(min=Qe_real_min, max=Qe_real_max))
        #params['%sf0' % self.prefix].set(min=fmin, max=fmax)
        #params['%sQr' % self.prefix].set(min=Qr_min, max=Qr_max)
        ##params['%sQe_real' % self.prefix].set(min=Qe_real_min, max=Qe_real_max)
        #params['%sQe_real'%  self.prefix].set(expr='Qr-qr_minus_qc if Qr-qr_minus_qc>0 else 0')
        #params['%sQe_imag' % self.prefix].set(min=Qe_imag_min, max=Qe_imag_max)
        #params['%sA' % self.prefix].set(min=A_min, max=A_max)
        #params['%sa' % self.prefix].set(min=a_min,max=a_max)
        ##params['%sQi_dummy' % self.prefix].set(vary=False,
                                               ##min=Qi_min,max=Qi_max,
                                               ##expr="1./(1./Qr - 1./sqrt(Qe_real**2 + Qe_imag**2))")
        ##params['%sQc_dummy' % self.prefix].set(vary=False,
                                               ##min=Qc_min,max=Qc_max,
                                               ##expr="sqrt(Qe_real**2 + Qe_imag**2)")

        #params.add('%sqr_minus_qc'% self.prefix,vary=True,min=0,value=Qr_guess-Qe_real_guess)

        #params['%sQi_dummy' % self.prefix].set(vary=False,expr="1./(1./Qr - 1./Qe_real)",min=Qi_min)
        ##params.add('%sqr_minus_qi'% self.prefix,min=0,guess=Qr_guess-Qi_dummy_guess)
        ##params['%sQi_dummy'% self.prefix].set(vary=False,expr='Qr-qr_minus_qi if Qr-qr_minus_qi>0 else 0')

        ##params['%sQc_dummy' % self.prefix].set(vary=False,expr="sqrt(Qe_real**2 + Qe_imag**2)")
        #params['%sQc_dummy' % self.prefix].set(vary=False,expr="Qe_real")


        

        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)
    
    

def fit_nonlinear_resonator(f,z,sweep_direction='up',**kwargs):
    baseline_polyorder = kwargs.pop('baseline_polyorder',1)


    #find nans
    finite = np.isfinite(z) 
    #remove complex baseline
    ff       = f[finite]
    zf       = z[finite]
    zu       = np.unwrap(np.angle(zf))


    m = np.abs(zf)
    argmin_s21 = np.argmin(m)
    argmax_s21 = np.argmax(m)
    mminmag = m[argmin_s21]
    fminmag = ff[argmin_s21]
    f0_guess = fminmag
    A_guess = (m[0] + m[-1])/2.
    halfmax = (A_guess + mminmag)/2.
    fwhm = 2*np.abs( fminmag - f[np.abs(m-halfmax).argmin()] )
    offres_linewidths = kwargs.pop('offres_linewidths',10)
    offres = (ff<fminmag-offres_linewidths/2*fwhm) | (ff>fminmag+offres_linewidths/2*fwhm)
    if np.any(offres):
        basemag = np.polynomial.Polynomial.fit(ff[offres],abs(zf[offres]),baseline_polyorder)(ff)
        basephase = np.polynomial.Polynomial.fit(ff[offres],np.unwrap(np.angle((zf)))[offres],baseline_polyorder)(ff)
    else:
        basephase = np.linspace(np.unwrap(np.angle(zf))[0],np.unwrap(np.angle(zf))[-1],len(zf))

        basemag = np.full(len(zf),np.mean((np.abs(zf[0]),np.abs(zf[-1]))))

    baseline = basemag*np.exp(1j*basephase)

    z0=zf/baseline

    #define lmfit model
    r      = NonlinearResonatorModel(sweep_direction=sweep_direction)
    
    #estimate intital parameters
    #guess  = r.guess(z0[finite],f=f[finite])
    guess  = r.guess(z0,f=f[finite])

    #apply weighting
    #weights=None
    weights = kwargs.pop('weights',None)
    if type(weights) is type(None):
        weights=np.ones_like(f)
    else:
        weights = weights[finite]
    
    #select method - don't use 'leastsq' as it is slow and inaccurate
    method = kwargs.pop('method','least_squares')
    
    #do the fitting, catch value errors due to bug in least_squares
    try:
        result = r.fit(z0[finite],
                       guess,
                       f=f[finite],
                       nan_policy='omit',
                       method=method,
                       weights=weights,**kwargs)
    except ValueError:
        guess_eval = r.eval(params=guess,f=f)

        return guess_eval*baseline, guess,None, guess
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    
    #evaluate model based on fit parameters

    fit    = r.eval(params=result.params,f=f)

    ##reapply baseline
    ##tbd
    #fit = fit + baseline_trend - baseline_mean

    ##reapply rotation to fit
    #fit = fit*np.exp(1j*rot)
    
    #reapply baseline
    fit=fit*baseline

    #catch serious error case
    if np.all(~np.isfinite(fit)):
        fit = z

    fitparams = result.params

    return fit, fitparams, result, guess, baseline
    


def do_fit(freqs,s21):
    """fit a sweep and return evaluated model s21 and parameters"""
    numpoints = freqs.shape[0]
    fit,params,guess=fit_nonlinear_resonator(freqs,s21,method='least_squares')
    fit_s21    = fit
    fit_f0     = params['f0'].value
    fit_qr     = params['Qr'].value
    fit_qereal = params['Qe_real'].value
    fit_qeimag = params['Qe_imag'].value
    fit_A      = params['A'].value
    fit_a      = params['a'].value
    fit_qi     = params['Qi_dummy'].value
    fit_qc     = params['Qc_dummy'].value
    
    std_f0     = params['f0'].stderr
    std_qr     = params['Qr'].stderr
    std_qereal = params['Qe_real'].stderr
    std_qeimag = params['Qe_imag'].stderr
    std_A      = params['A'].stderr
    std_a      = params['a'].stderr
    std_qi     = params['Qi_dummy'].stderr
    std_qc     = params['Qc_dummy'].stderr
    
    return (fit_s21, 
            fit_f0, fit_qr, fit_qereal, fit_qeimag, fit_A, fit_a, fit_qi,fit_qc,
            std_f0, std_qr, std_qereal, std_qeimag, std_A, std_a, std_qi,std_qc)
       


def do_fits(freqs,data):
    """
    fit an array of sweeps and return arrays of evaluated model s21 and parameters
    no parallelisation, just runs in serial """
    numsweeps = freqs.shape[0]
    numpoints = freqs.shape[1]
    fit_s21       = np.full((numsweeps,numpoints),np.nan+1j*np.nan)
    fit_f0        = np.full(numsweeps,np.nan)
    fit_qr        = np.full(numsweeps,np.nan)
    fit_qereal    = np.full(numsweeps,np.nan)
    fit_qeimag    = np.full(numsweeps,np.nan)
    fit_A         = np.full(numsweeps,np.nan)
    fit_a         = np.full(numsweeps,np.nan)
    fit_qi        = np.full(numsweeps,np.nan)
    fit_qc        = np.full(numsweeps,np.nan)
    std_f0        = np.full(numsweeps,np.nan)
    std_qr        = np.full(numsweeps,np.nan)
    std_qereal    = np.full(numsweeps,np.nan)
    std_qeimag    = np.full(numsweeps,np.nan)
    std_A         = np.full(numsweeps,np.nan)
    std_a         = np.full(numsweeps,np.nan)
    std_qi        = np.full(numsweeps,np.nan)
    std_qc        = np.full(numsweeps,np.nan)
    
    print('Fitting: ', end=' ')
    for k,(ff,zz) in enumerate(zip(freqs,data)):
        print(k, end=' ');sys.stdout.flush()
        fit,params,guess=fit_nonlinear_resonator(ff,zz,method='least_squares')
        fit_s21[k]    = fit
        fit_f0[k]     = params['f0'].value
        fit_qr[k]     = params['Qr'].value
        fit_qereal[k] = params['Qe_real'].value
        fit_qeimag[k] = params['Qe_imag'].value
        fit_A[k]      = params['A'].value
        fit_a[k]      = params['a'].value
        fit_qi[k]     = params['Qi_dummy'].value
        fit_qc[k]     = params['Qc_dummy'].value
        
        std_f0[k]     = params['f0'].stderr
        std_qr[k]     = params['Qr'].stderr
        std_qereal[k] = params['Qe_real'].stderr
        std_qeimag[k] = params['Qe_imag'].stderr
        std_A[k]      = params['A'].stderr
        std_a[k]      = params['a'].stderr
        std_qi[k]     = params['Qi_dummy'].stderr
        std_qc[k]     = params['Qc_dummy'].stderr
    print(' Done fitting')
    
    return (fit_s21, 
            fit_f0, fit_qr, fit_qereal, fit_qeimag, fit_A, fit_a, fit_qi,fit_qc,
            std_f0, std_qr, std_qereal, std_qeimag, std_A, std_a, std_qi,std_qc)



def do_fits_parallel(freqs,s21):
    """ 
    run a bunch of fits of sweeps in parallel 
    freqs and s21 should be arrays of shape(num_sweeps,num_sweep_points)
    """
    numsweeps     = freqs.shape[0]
    numpoints     = freqs.shape[1]
    fit_s21       = np.full((numsweeps,numpoints),np.nan+1j*np.nan)
    fit_f0        = np.full(numsweeps,np.nan)
    fit_qr        = np.full(numsweeps,np.nan)
    fit_qereal    = np.full(numsweeps,np.nan)
    fit_qeimag    = np.full(numsweeps,np.nan)
    fit_A         = np.full(numsweeps,np.nan)
    fit_a         = np.full(numsweeps,np.nan)
    fit_qi        = np.full(numsweeps,np.nan)
    fit_qc        = np.full(numsweeps,np.nan)
    std_f0        = np.full(numsweeps,np.nan)
    std_qr        = np.full(numsweeps,np.nan)
    std_qereal    = np.full(numsweeps,np.nan)
    std_qeimag    = np.full(numsweeps,np.nan)
    std_A         = np.full(numsweeps,np.nan)
    std_a         = np.full(numsweeps,np.nan)
    std_qi        = np.full(numsweeps,np.nan)
    std_qc        = np.full(numsweeps,np.nan)
    
    pool = multiprocessing.Pool(processes=numsweeps) 
    res = [pool.apply_async(do_fit, (freqs[k],s21[k]) ) for k in range(num_sweeps) ]
    # wait for processing to finish 
    while not all([r.ready() for r in res]):
        time.sleep(0.5)
        
    for k in range(numsweeps):
        fit,params,guess = res[k].get()
        fit_s21[k]    = fit
        fit_f0[k]     = params['f0'].value
        fit_qr[k]     = params['Qr'].value
        fit_qereal[k] = params['Qe_real'].value
        fit_qeimag[k] = params['Qe_imag'].value
        fit_A[k]      = params['A'].value
        fit_a[k]      = params['a'].value
        fit_qi[k]     = params['Qi_dummy'].value
        fit_qc[k]     = params['Qc_dummy'].value
        
        std_f0[k]     = params['f0'].stderr
        std_qr[k]     = params['Qr'].stderr
        std_qereal[k] = params['Qe_real'].stderr
        std_qeimag[k] = params['Qe_imag'].stderr
        std_A[k]      = params['A'].stderr
        std_a[k]      = params['a'].stderr
        std_qi[k]     = params['Qi_dummy'].stderr
        std_qc[k]     = params['Qc_dummy'].stderr
    
    return (fit_s21, 
            fit_f0, fit_qr, fit_qereal, fit_qeimag, fit_A, fit_a, fit_qi,fit_qc,
            std_f0, std_qr, std_qereal, std_qeimag, std_A, std_a, std_qi,std_qc)
    
    
    
    
    

def muxchan_fit(muxchan):
    """
    convinient parallelised fitting of sweeps for a single mux channel
    
    one subprocess per kid in the mux channel
    
    takes about 8-9 seconds per mux channel in muscat
    """
    t0=time.time()
    
    numsweeps = muxchan.sweep.rf_freqs.shape[0]
    numpoints = muxchan.sweep.rf_freqs.shape[1]

    pool = multiprocessing.Pool(processes=numsweeps) 
    res = [pool.apply_async(do_fit, (muxchan.sweep.rf_freqs[k],muxchan.sweep.data[k]) )
           for k in range(numsweeps) ]
    # wait for processing to finish 
    while not all([r.ready() for r in res]):
        time.sleep(0.5)
    
    pool.close()
    pool.join()
   
    muxchan.sweep.fit_s21       = np.full((numsweeps,numpoints),np.nan+1j*np.nan)
    muxchan.sweep.fit_f0        = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_qr        = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_qereal    = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_qeimag    = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_A         = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_a         = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_qi        = np.full(numsweeps,np.nan)
    muxchan.sweep.fit_qc        = np.full(numsweeps,np.nan)
    muxchan.sweep.std_f0        = np.full(numsweeps,np.nan)
    muxchan.sweep.std_qr        = np.full(numsweeps,np.nan)
    muxchan.sweep.std_qereal    = np.full(numsweeps,np.nan)
    muxchan.sweep.std_qeimag    = np.full(numsweeps,np.nan)
    muxchan.sweep.std_A         = np.full(numsweeps,np.nan)
    muxchan.sweep.std_a         = np.full(numsweeps,np.nan)
    muxchan.sweep.std_qi        = np.full(numsweeps,np.nan)
    muxchan.sweep.std_qc        = np.full(numsweeps,np.nan)
    
    for j,r in enumerate(res):
        muxchan.sweep.fit_s21[j]       ,\
        muxchan.sweep.fit_f0[j]        ,\
        muxchan.sweep.fit_qr[j]        ,\
        muxchan.sweep.fit_qereal[j]    ,\
        muxchan.sweep.fit_qeimag[j]    ,\
        muxchan.sweep.fit_A[j]         ,\
        muxchan.sweep.fit_a[j]         ,\
        muxchan.sweep.fit_qi[j]        ,\
        muxchan.sweep.fit_qc[j]        ,\
        muxchan.sweep.std_f0[j]        ,\
        muxchan.sweep.std_qr[j]        ,\
        muxchan.sweep.std_qereal[j]    ,\
        muxchan.sweep.std_qeimag[j]    ,\
        muxchan.sweep.std_A[j]         ,\
        muxchan.sweep.std_a[j]         ,\
        muxchan.sweep.std_qi[j]        ,\
        muxchan.sweep.std_qc[j]        = r.get()
    dt = time.time()-t0
    print('Fitting complete: %.2f seconds for %d kids: %.2f sec/kid'%(dt,numsweeps,dt/numsweeps))
    

    
def muxchanlist_fit(muxchanlist):
    """
    parallelise fitting of sweeps for a mux channel list
    
    one subprocess per mux channel in the mux channel list
    
    takes about 4 seconds per muxchannel in muscat
    """
    t0=time.time()
    
    names = muxchanlist.ROACH_LIST
    n = len(names)
    
    chans = [getattr(muxchanlist, ch) for ch in names]
    print(chans)
    
    sweeps = [chan.sweep for chan in chans]
    print(sweeps)
    
    pool = multiprocessing.Pool(processes=n) 
    res = [pool.apply_async(do_fits, (sweep.rf_freqs,sweep.data) )  for sweep in sweeps ]
    ##print res
    
    # wait for processing to finish 
    while not all([r.ready() for r in res]):
        time.sleep(0.5)
    
    pool.close()
    pool.join()
    
    for ret,sweep in zip(res,sweeps):
        sweep.fit_s21   ,\
        sweep.fit_f0    ,\
        sweep.fit_qr    ,\
        sweep.fit_qereal,\
        sweep.fit_qeimag,\
        sweep.fit_A     ,\
        sweep.fit_a     ,\
        sweep.fit_qi    ,\
        sweep.fit_qc    ,\
        sweep.std_f0    ,\
        sweep.std_qr    ,\
        sweep.std_qereal,\
        sweep.std_qeimag,\
        sweep.std_A     ,\
        sweep.std_a     ,\
        sweep.std_qi    ,\
        sweep.std_qc    = ret.get()
    
    dt = time.time()-t0
    print('Fitting complete: %.2f seconds for %d mux channels: %.2f sec/chan'%(dt,n,dt/n))

    
