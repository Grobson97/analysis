import numpy as np
import matplotlib.pyplot as plt
import scipy

from source.models.noise.noise_sweep import NoiseSweep


class NoisePacketPSD:
    """
    Class to represent the data for a packet of noise measured at a given power and temperature.
    """

    def __init__(
        self,
        dut: str,
        tone_name: str,
        temperature: float,
        input_attenuation: float,
        noise_sweep: NoiseSweep,
        psd_on_res_low: np.ndarray,
        on_res_low_frequencies: np.ndarray,
        psd_off_res_low: np.ndarray,
        off_res_low_frequencies: np.ndarray,
        psd_on_res_high: np.ndarray,
        on_res_high_frequencies: np.ndarray,
        psd_off_res_high: np.ndarray,
        off_res_high_frequencies: np.ndarray,
    ):
        """
        Create an instance of a noise packet.

        :param dut: Device under test.
        :param tone_name: Tone name for KID. e.g. KID001
        :param temperature: Temperature of measurement.
        :param input_attenuation: Relative power of measurement. e.g. 6.0 = 6dB.
        :param noise_sweep: Instance of a NoiseSweep data class containing the sweep data.
        :param psd_on_res_low: Mean PSD array for the on resonance, low sampling timestream.
        :param on_res_low_frequencies: Frequency array for corresponding psd.
        :param psd_off_res_low: Mean PSD array for the off resonance, low sampling timestream.
        :param on_res_low_frequencies: Frequency array for corresponding psd.
        :param psd_on_res_high: Mean PSD array for the on resonance, high sampling timestream.
        :param on_res_low_frequencies: Frequency array for corresponding psd.
        :param psd_off_res_high: Mean PSD array for the off resonance, high sampling timestream.
        :param on_res_low_frequencies: Frequency array for corresponding psd.
        """

        self.dut = dut
        self.tone_name = tone_name
        self.temperature = temperature
        self.input_attenuation = input_attenuation
        self.noise_sweep = noise_sweep
        self.psd_on_res_low = psd_on_res_low
        self.on_res_low_frequencies = on_res_low_frequencies
        self.psd_off_res_low = psd_off_res_low
        self.off_res_low_frequencies = off_res_low_frequencies
        self.psd_on_res_high = psd_on_res_high
        self.on_res_high_frequencies = on_res_high_frequencies
        self.psd_off_res_high = psd_off_res_high
        self.off_res_high_frequencies = off_res_high_frequencies
        self.is_bifurcated = noise_sweep.is_bifurcated

        # Combine on res PSD data:
        psd_on_res_combined = np.concatenate(
            (self.psd_on_res_low[:-50], self.psd_on_res_high[:-50]), axis=0
        )
        psd_on_res_combined_frequencies = np.concatenate(
            (self.on_res_low_frequencies[:-50], self.on_res_high_frequencies[:-50]),
            axis=0,
        )
        on_res_frequency_order = np.argsort(psd_on_res_combined_frequencies)
        self.psd_on_res_combined = psd_on_res_combined[on_res_frequency_order]
        self.psd_on_res_combined_frequencies = psd_on_res_combined_frequencies[
            on_res_frequency_order
        ]

        # Combine off res PSD data:
        psd_off_res_combined = np.concatenate(
            (self.psd_off_res_low[:-50], self.psd_off_res_high[:-50]), axis=0
        )
        psd_off_res_combined_frequencies = np.concatenate(
            (self.off_res_low_frequencies[:-50], self.off_res_high_frequencies[:-50]),
            axis=0,
        )
        off_res_frequency_order = np.argsort(psd_off_res_combined_frequencies)
        self.psd_off_res_combined = psd_off_res_combined[off_res_frequency_order]
        self.psd_off_res_combined_frequencies = psd_off_res_combined_frequencies[
            off_res_frequency_order
        ]

    def plot_psd(self, show_individual_psd=False) -> None:
        """
        Function to plot the psd data.
        """

        plt.figure(figsize=(10, 6))
        if show_individual_psd:
            plt.plot(
                self.on_res_low_frequencies,
                self.psd_on_res_low,
                label="On Res Low Freq",
            )
            plt.plot(
                self.on_res_high_frequencies,
                self.psd_on_res_high,
                label="On Res High Freq",
            )
            plt.plot(
                self.off_res_low_frequencies,
                self.psd_off_res_low,
                label="Off Res Low Freq",
            )
            plt.plot(
                self.off_res_high_frequencies,
                self.psd_off_res_high,
                label="Off Res High Freq",
            )
        else:
            plt.plot(
                self.psd_on_res_combined_frequencies,
                self.psd_on_res_combined,
                label="On Res",
            )
            plt.plot(
                self.psd_off_res_combined_frequencies,
                self.psd_off_res_combined,
                label="Off Res",
            )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
        plt.xscale("log")
        plt.yscale("log")
        plt.ylim(bottom=1e-23)
        plt.legend()
        plt.title(
            f"Noise PSD for {self.dut} {self.tone_name} with {self.input_attenuation} dB at {self.temperature}K"
        )
        plt.show()
