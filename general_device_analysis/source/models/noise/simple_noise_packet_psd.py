import numpy as np
import matplotlib.pyplot as plt
import scipy

from source.models.noise.noise_sweep import NoiseSweep
from source.util import hidfmux_transfer_functions


class SimpleNoisePacketPSD:
    """
    A stripped down version of NoisePacketPSD to contain only one PSD and corresponding frequencies.
    """

    def __init__(
        self,
        dut: str,
        tone_name: str,
        temperature: float,
        input_attenuation: float,
        sampling_rate: float,
        noise_sweep: NoiseSweep,
        on_res_psd: np.array,
        on_res_frequencies: np.array,
        off_res_psd: np.array,
        off_res_frequencies: np.array,
    ):
        """
        Create an instance of a noise packet.

        :param dut: Device under test.
        :param tone_name: Tone name for KID. e.g. KID001
        :param temperature: Temperature of measurement.
        :param input_attenuation: Relative power of measurement. e.g. 6.0 = 6dB.
        :param sampling_rate: Sampling rate of timestreams.
        :param noise_sweep: Instance of a NoiseSweep data class containing the sweep data.
        :param on_res_psd: Array of on resonance PSD values corresponding to each frequency.
        :param on_res_frequencies: Array of on resonance frequency values corresponding to each PSD value.
        :param off_res_psd: Array of off resonance PSD values corresponding to each frequency.
        :param off_res_frequencies: Array of off resonance frequency values corresponding to each PSD value.
        """

        self.dut = dut
        self.tone_name = tone_name
        self.temperature = temperature
        self.input_attenuation = input_attenuation
        self.sampling_rate = sampling_rate
        self.noise_sweep = noise_sweep
        self.on_res_psd = on_res_psd
        self.on_res_frequencies = on_res_frequencies
        self.off_res_psd = off_res_psd
        self.off_res_frequencies = off_res_frequencies

    def plot_psd(self, show_individual_psd=False) -> None:
        """
        Function to plot the psd data.
        """

        plt.figure(figsize=(10, 6))
        plt.plot(self.on_res_frequencies, self.on_res_psd, label="On Resonance")
        plt.plot(self.off_res_frequencies, self.off_res_psd, label="Off Resonance")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
        plt.xscale("log")
        plt.yscale("log")
        plt.ylim(bottom=1e-23)
        plt.legend()
        plt.title(
            f"Noise PSD for {self.dut} {self.tone_name} with {self.input_attenuation} dB at {self.temperature}K"
        )
        plt.show()

    def correct_psd_for_cic_filter(self, trim_percentage=0.15) -> None:
        """
        If called, function will overwrite it's current on and off resonance psd arrays with corrected versions of
        themselves by compensating for the response of the second-stage cascaded-integrator comb (CIC) filter as a
        function of frequency in a power spectral density measurement.

        Because the channel response drops off quickly towards and above Nyquist, results will be less trustworthy at
        high frequencies, and so this trims the last trim-percent of the datapoints at high frequency.

        Function calls the apply_cic2_comp_psd() function from hidfmux.core.utils.transferfunctions.

        :param trim_percentage: Percent length of array to trim off at high frequencies, to avoid returning data where numerical
        uncertainties are higher in the compensation function.
        """

        (
            on_res_frequency_array,
            on_res_psd_array,
        ) = hidfmux_transfer_functions.apply_cic2_comp_psd(
            f_arr=self.on_res_frequencies,
            psd=self.on_res_psd,
            fs=self.sampling_rate,
            trim=trim_percentage,
        )

        (
            off_res_frequency_array,
            off_res_psd_array,
        ) = hidfmux_transfer_functions.apply_cic2_comp_psd(
            f_arr=self.off_res_frequencies,
            psd=self.off_res_psd,
            fs=self.sampling_rate,
            trim=trim_percentage,
        )

        self.on_res_psd = on_res_psd_array
        self.on_res_frequencies = on_res_frequency_array
        self.off_res_psd = off_res_psd_array
        self.off_res_frequencies = off_res_frequency_array

        return None
