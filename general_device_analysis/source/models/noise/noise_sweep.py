import numpy as np
import matplotlib.pyplot as plt
import scipy

from source.models.resonator_measurement.sweep_data import SweepData
from source.util import lekid_analysis_tools


class NoiseSweep:
    """
    Class to represent the tone sweep data from a noise packet / single tone readout.
    """

    def __init__(
        self,
        dut: str,
        tone_name: str,
        temperature: float,
        input_attenuation: float,
        sweep_data: SweepData,
    ):
        """
        Create an instance of a noise sweep and calculate attributes derived from sweep data.

        :param dut: Device under test.
        :param tone_name: Tone name for KID. e.g. KID001
        :param temperature: Temperature of measurement.
        :param input_attenuation: Relative power of measurement. e.g. 6.0 = 6dB.
        :param sweep_iq_array: Numpy array of complex data of the sweeps IQ data.
        :param sweep_frequency_array: Numpy array of the frequency values for the sweep data.

        :ivar sweep_di_df: dI/df numpy array.
        :ivar sweep_dq_df: dQ/df numpy array.
        :ivar sweep_di2_plus_dq2: Numpy array of Quadrature Sum of dI/df and dQ/df.
        :ivar sweep_f0_index: (Int) Index of dI^2 + dQ^2 which corresponds to the maximum value (e.g. f0).
        :ivar sweep_f0: (float) Frequency value at f0 index.
        :ivar di_df_r: (float) dI/df at resonant frequency.
        :ivar dq_df_r: (float) dQ/df at resonant frequency.
        """

        self.dut = dut
        self.tone_name = tone_name
        self.temperature = temperature
        self.input_attenuation = input_attenuation
        self.sweep_data = sweep_data

        (
            sweep_di_df,
            sweep_dq_df,
            sweep_di2_plus_dq2,
            sweep_f0_index,
            sweep_f0,
        ) = lekid_analysis_tools.calculate_sweep_frequency_derivatives(
            sweep_i_array=sweep_data.i,
            sweep_q_array=sweep_data.q,
            sweep_f_array=sweep_data.frequency_array,
        )
        # Get dI' and dQ' at resonance peak.
        di_df_r = sweep_di_df[sweep_f0_index]
        dq_df_r = sweep_dq_df[sweep_f0_index]

        self.sweep_di_df = sweep_di_df
        self.sweep_dq_df = sweep_dq_df
        self.sweep_di2_plus_dq2 = sweep_di2_plus_dq2
        self.sweep_f0_index = sweep_f0_index
        self.sweep_f0 = sweep_f0
        self.di_df_r = di_df_r
        self.dq_df_r = dq_df_r

        # Check if KID is bifurcated
        self.is_bifurcated = lekid_analysis_tools.is_bifurcated(
            sweep_data.get_iq_array()
        )

    def plot_sweep(
        self, save_figure=False, save_to_directory="..\\..\\Figures"
    ) -> None:
        """
        Function to plot the sweep data and fit to the S21.
        """

        iq_array = self.sweep_data.get_iq_array()
        # Fit Lorentzian to Sweep data
        fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
            frequency_array=self.sweep_data.frequency_array,
            data_array=np.abs(iq_array),
            qc_guess=1e5,
            qi_guess=1e5,
            f0_guess=None,
            fit_fraction=0.8,
            normalise=False,
            plot_db=True,
            plot_title="",
            plot_graph=False,
        )
        s21_fit = lekid_analysis_tools.skewed_lorentzian(
            frequency_array=self.sweep_data.frequency_array,
            f0=fit_result_dict["f0"][0],
            qi=fit_result_dict["qi"][0],
            qc_real=fit_result_dict["qc_real"][0],
            qc_imag=fit_result_dict["qc_imag"][0],
            amp=fit_result_dict["amp"][0],
        )

        figure, axes = plt.subplots(1, 4, figsize=(14, 4))
        axes[0].plot(self.sweep_data.frequency_array * 1e-9, np.abs(iq_array))
        axes[0].plot(
            self.sweep_data.frequency_array * 1e-9,
            s21_fit,
            color="r",
            linestyle="--",
            linewidth=2,
            label=f"Fit:\nQr={fit_result_dict['qr'][0]:.2E}"
            f"\nQc={fit_result_dict['qc'][0]:.2E}"
            f"\nQi={fit_result_dict['qi'][0]:.2E}",
        )
        axes[0].legend()
        axes[0].set_xlabel("Frequency (GHz)")
        axes[0].set_ylabel("S21 Magnitude (Arb)")
        axes[1].plot(
            self.sweep_data.frequency_array * 1e-9,
            20 * np.log10(np.abs(iq_array)),
        )
        axes[1].set_xlabel("Frequency (GHz)")
        axes[1].set_ylabel("S21 Magnitude (dB)")
        axes[2].plot(self.sweep_data.i, self.sweep_data.q)
        axes[2].set_xlabel("I (V)")
        axes[2].set_ylabel("Q (V)")
        axes[2].axis("equal")
        axes[3].plot(self.sweep_di_df * 1e9, label="dI'")
        axes[3].plot(self.sweep_dq_df * 1e9, label="dQ'")
        axes[3].plot(
            np.sqrt(self.sweep_di2_plus_dq2) * 1e9, label="$\sqrt{dI'^2+dQ'^2}$"
        )
        axes[3].vlines(
            self.sweep_f0_index,
            ymin=np.min(self.sweep_dq_df * 1e9),
            ymax=np.max(self.sweep_di2_plus_dq2 * 1e9),
            linestyle="--",
            color="k",
        )
        axes[3].set_xlabel("Index")
        axes[3].set_ylabel("d/df (V/GHz)")
        axes[3].legend()
        plt.tight_layout()
        if save_figure:
            plt.savefig(
                save_to_directory
                + f"\\{self.dut}_{self.tone_name}_{self.temperature}_{self.input_attenuation}amp_noise_sweep_data.png"
            )
        plt.show()
