import numpy as np
import math
import matplotlib.pyplot as plt
from dataclasses import dataclass
from matplotlib.axes import Axes

from source.util import timestream_data_tools
from source.util import hidfmux_data_tools
from source.util import data_processing_tools
from source.models.hidfmux.hidfmux_data import HidfmuxData
from source.models.noise.noise_sweep import NoiseSweep
from source.models.resonator_measurement.timestream_data import TimestreamData

def get_inset_axes(axis):
    """
    Function to get the inset axes from a matplotlib axis
    """
    return [c for c in axis.get_children() if isinstance(c, Axes)]

@dataclass
class HidfmuxPFBTimestream(HidfmuxData):
    """
    Class to represent the data for a multi-tone timestream measurement, including corresponding IQ sweeps (Multisweeps)
    for each tone.
    Inherits class attributes from HidfmuxMultiSweep.

    :param on_res_timestreams_array: Array of TimestreamData objects for the tone on resonance.
    :param off_res_timestreams_array: Array of TimestreamData objects for the tone off resonance.
    """

    on_res_timestreams_array: np.ndarray
    off_res_timestreams_array: np.ndarray

    def get_tone_timestream_data(
        self, tone: int or str, on_res: bool # type: ignore
    ) -> TimestreamData:
        """
        Function to return the timestream data of a specific tone in the measurement.

        :param tone: Either an integer representing the index of the tone in the measurement (e.g. 0 would be the first
        tone, 100 would be the 101st tone) or the corresponding tone name in the tone_name_array (e.g. "K001").
        :param on_res: Boolean to return the on resonance "on_res" timestream if true. If false then the off resonance
        "off_res" timestream is returned.
        """

        if on_res:
            timestreams = self.on_res_timestreams_array
        else:
            timestreams = self.off_res_timestreams_array

        if type(tone) == int:
            return timestreams[tone]

        if type(tone) == str or type(tone) ==  np.str_:
            tone_index = np.where(self.tone_name_array == tone)[0][0]
            return timestreams[tone_index]
        

    def get_df_timestreams(self, on_res: bool, tone_names=list or None) -> np.ndarray:
        """
        Function to calculate the df timestreams via the multiplication method and return an array of all the df
        timestreams for each tone either in the same ordering as the instance attributes or in the order of the provided
        tone_names list.
        :param on_res: Boolean to return the on resonance "on_res" timestream if true. If false then the off resonance
        "off_res" timestream is returned.
        :param tone_names: If None then all df timestreams are calculated, if specific tones are desired then provide a
        list of tones corresponding to the tone_name_array. e.g. ["K000", "K001", "K002"...]
        :param pca_clean: 
        """

        if tone_names is not None:
            tones = tone_names
        elif tone_names == None:
            tones = self.tone_name_array
        else:
            return print("Invalid tone_names list")

        df_timestream_array = []
        for tone_name in tones:

            tone_index = np.where(self.tone_name_array == tone_name)[0][0]
            tone_sweep_data = self.get_tone_sweep_data(tone=tone_name)

            tone_timestream_data = self.get_tone_timestream_data(tone=tone_name, on_res=on_res)

            sweep_di_df, sweep_dq_df, sweep_di2_plus_dq2, sweep_f0_index, f0 = timestream_data_tools.calculate_sweep_frequency_derivatives(
                sweep_i_array=tone_sweep_data.i,
                sweep_q_array=tone_sweep_data.q,
                sweep_f_array=tone_sweep_data.frequency_array
            )

            df_timestream = timestream_data_tools.calculate_timestream_df(
                di_df_r=sweep_di_df[sweep_f0_index],
                dq_df_r=sweep_dq_df[sweep_f0_index],
                timestream_i_array=tone_timestream_data.timestream_i_data,
                timestream_q_array=tone_timestream_data.timestream_q_data,
            )

            df_timestream_array.append(df_timestream)

        return np.array(df_timestream_array)
    

    def get_multitone_psd_array(self, resample=False, resample_bins=500, on_res=True, pca_clean=False, pca_components=1):
        """
        Function to calculate the multitone PSD data and return an array of PSD data for each tone and corresponding frequencies.

        :param resample: Boolean to exponentially resample the noise data.
        :param resample_bins: Number of frequency bins to exponentially resample into.
        :param on_res: Boolean to return the on resonance PSD if True, off resonance if False.
        :param pca_clean: If true, PCA will be used to clean the df timestreams.
        :param pca_components: Number of principle components to remove if pca_clean is true.
        """

        df_timestreams = np.real(self.get_df_timestreams(on_res=on_res, tone_names=None))

        if pca_clean:
            df_timestreams = data_processing_tools.pca_clean_timestream(
                data_array=df_timestreams,
                number_of_components=pca_components,
                plot_components=False
            )

        df_psd = []
        frequencies = []

        for count, tone_name in enumerate(self.tone_name_array):

            tone_timestream_data = self.get_tone_timestream_data(tone=tone_name, on_res=on_res)

            tone_psd, tone_frequencies = timestream_data_tools.get_mean_psd(
                number_of_timestreams=np.array([df_timestreams[count]]).shape[0],
                df_timestreams_array=np.real(np.array([df_timestreams[count]]))/tone_timestream_data.tone_frequency,
                sample_rate=tone_timestream_data.sample_rate,
                resample=resample,
                resample_bins=resample_bins,
            )

            tone_frequencies, tone_psd = hidfmux_data_tools.apply_cic2_comp_psd(
                f_arr=tone_frequencies,
                psd=tone_psd,
                fs=tone_timestream_data.sample_rate
            )

            df_psd.append(tone_psd)
            frequencies.append(tone_frequencies)


        df_psd = np.array(df_psd)
        frequencies = np.array(frequencies)

        return df_psd, frequencies
    

    def plot_timestream_quality_check(self,):
        """
        Function to plot the IQ circle overlaid with timestream on and off res data points.
        as well as the S21 vs frequency on an inset axis.
        """

        number_of_plots = self.tone_name_array.size
        n_rows = math.ceil(number_of_plots / 5)
        n_cols = 5

        total_plots = n_rows * n_cols
        figure, axes = plt.subplots(
            n_rows,
            n_cols,
            sharex=False,
            sharey=False,
            figsize=(14, 3 * n_rows),
        )

        for row in axes:
            for axis in row:
                axis.inset_axes([.6,.6,.35,.35])

        total_plots = axes.size
        # Plot each tone on own subplots:
        m = 0  # Counter to assign row value
        n = 0  # Counter to assign column

        for count, tone_name in enumerate(self.tone_name_array):

            inset_axis = get_inset_axes(axes[m][n])[0]

            on_res_timestream_data = self.get_tone_timestream_data(tone=tone_name, on_res=True)
            off_res_timestream_data = self.get_tone_timestream_data(tone=tone_name, on_res=False)

            tone_sweep_data = self.get_tone_sweep_data(tone=tone_name)
            tone_iq = tone_sweep_data.get_iq_array()
            tone_s21_db = 20*np.log10(np.abs(tone_iq))

            axes[m][n].set_title(tone_name)
            axes[m][n].plot(
                np.real(tone_iq),
                np.imag(tone_iq),
                color="k"
            )
            axes[m][n].plot(
                on_res_timestream_data.timestream_i_data,
                on_res_timestream_data.timestream_q_data,
                linestyle="none",
                marker="o",
                markersize=3,
                color="C0"
            )
            axes[m][n].plot(
                off_res_timestream_data.timestream_i_data,
                off_res_timestream_data.timestream_q_data,
                linestyle="none",
                marker="o",
                markersize=3,
                color="C1"
            )
            axes[m][n].axis("equal")

            # plot IQ circle on:
            inset_axis.plot(
                tone_sweep_data.frequency_array,
                tone_s21_db,
                color="k"
            )
            inset_axis.vlines(
                x=[on_res_timestream_data.tone_frequency, off_res_timestream_data.tone_frequency],
                ymin=np.min(tone_s21_db),
                ymax=np.max(tone_s21_db),
                linestyles=["-", "--"],
                colors=["C0", "C1"]
            )

            inset_axis.set_ylabel("S21 (dB)")
            inset_axis.set_xlabel("Frequency (MHz)")

            if n == 0:
                axes[m][n].set_ylabel("Q")
                
            if m == math.ceil(number_of_plots / 5) - 1:
                axes[m][n].set_xlabel("I")
            n += 1
            if n == 5:
                m += 1  # Move to next row
                n = 0  # Reset to first column

        try:
            remaining_subplots = total_plots - number_of_plots
            if remaining_subplots > 0:
                for remaining_column in range(remaining_subplots):
                    axes[m][n + remaining_column].remove()
        except:
            print("No axes to remove")

        plt.tight_layout()
        plt.show()