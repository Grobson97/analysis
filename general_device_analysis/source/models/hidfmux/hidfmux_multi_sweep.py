import numpy as np
from dataclasses import dataclass

from source.models.hidfmux.hidfmux_data import HidfmuxData
from source.models.resonator_measurement.sweep_data import SweepData


@dataclass
class HidfmuxMultiSweep(HidfmuxData):
    """
    Class to represent the useful information from a multi sweep of the hidfmux multi tone readout system.
    """

    calibration_iq_array: np.ndarray  # Array of the "cal_iq" data from the (for multitones this should be a 2d array).

    def get_tone_calibration_iq_data(self, tone: int or str) -> SweepData: # type: ignore
        """
        Function to return the sweep data of a specific tone in the multi sweep.

        :param tone: Either an integer representing the index of the tone in the multisweep (e.g. 0 would be the first
        tone, 100 would be the 101st tone) or the corresponding tone name in the tone_name_array (e.g. "K001").
        """

        if type(tone) == int:
            return self.calibration_iq_array[tone]

        if type(tone) == str or type(tone) ==  np.str_:
            tone_index = np.where(self.tone_name_array == tone)[0][0]
            return self.calibration_iq_array[tone_index]
