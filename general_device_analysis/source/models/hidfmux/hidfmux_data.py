import numpy as np
import math
import matplotlib.pyplot as plt
from dataclasses import dataclass

from source.models.resonator_measurement.sweep_data import SweepData
import source.util.hidfmux_transfer_functions as hidfmux_transfer_functions
import source.util.lekid_analysis_tools as lekid_analysis_tools


@dataclass
class HidfmuxData:
    """
    Class to represent the basic useful information in both a Hidfmux Multisweep and timestream.
    """

    bias_power_array: np.ndarray  # Array of tone bias powers corresponding to each KID.
    tone_name_array: np.ndarray  # Array of string tone names for each tone. e.g. "K001"
    sweep_data_array: np.ndarray  # Array of SweepData objects containing the frequency sweep and corresponding I Q values.

    def get_tone_sweep_data(self, tone: int or str) -> SweepData: # type: ignore
        """
        Function to return the sweep data of a specific tone in the multi sweep.

        :param tone: Either an integer representing the index of the tone in the multisweep (e.g. 0 would be the first
        tone, 100 would be the 101st tone) or the corresponding tone name in the tone_name_array (e.g. "K001").
        """

        if type(tone) == int:
            return self.sweep_data_array[tone]

        if type(tone) == str or type(tone) ==  np.str_:
            tone_index = np.where(self.tone_name_array == tone)[0][0]
            return self.sweep_data_array[tone_index]

    # TODO make sure conversion to db is correct.
    def get_tone_bias_power(self, tone: int or str, db: bool) -> float: # type: ignore
        """
        Function to return the bias power of a specific tone in the multi sweep.

        :param tone: Either an integer representing the index of the tone in the multisweep (e.g. 0 would be the first
        tone, 100 would be the 101st tone) or the corresponding tone name in the tone_name_array (e.g. "K001").
        :param db: If true, the bias power will be converted from DAC units to dB units.
        """

        if type(tone) == int:
            return self.bias_power_array[tone]

        if type(tone) == str or type(tone) ==  np.str_:
            tone_index = np.where(self.tone_name_array == tone)[0][0]

        power = self.bias_power_array[tone_index]

        if db:
            return hidfmux_transfer_functions.convert_dacunits_to_dbm(power)
        else:
            return power

    def get_good_kid_array(
        self,
        min_qr: float,
        max_qr: float,
        uncertainty_threshold: float,
        fit_fraction: float,
        plot_fits: bool,
    ) -> np.ndarray:
        """
        Function to identify which kids are good and which are bad according to their quality factors and whether
        there are more than one peak in the sweep.

        :param min_qr: Minimum acceptable value for the resonator quality factor.
        :param max_qr: Minimum acceptable value for the resonator quality factor.
        :param uncertainty_threshold: Value of the mean percentage 3 sigma uncertainty over which a KID is labelled as bad.
        :param fit_fraction: Fraction of data points to fit to (between 0 and 1). 1 being 100% of the data.
        :param plot_fits: Boolean to plot the fits for each sweep
        """

        good_kid_array = np.empty_like(self.tone_name_array, dtype=bool)
        reasons_array = []

        for count, sweep in enumerate(self.sweep_data_array):

            good_kid = lekid_analysis_tools.is_good_kid(
                frequency_array=sweep.frequency_array,
                iq_array=sweep.get_iq_array(),
                qc_guess=1e5,
                qi_guess=1e5,
                f0_guess=None,
                fit_fraction=fit_fraction,
                percentage_3_sigma_uncertainty_threshold=uncertainty_threshold,
                min_qr=min_qr,
                max_qr=max_qr,
                normalise=False,
                plot_graph=plot_fits,
                plot_db=True,
                plot_title=f"{self.tone_name_array[count]}",
            )

            good_kid_array[count] = good_kid[0]
            reasons_array.append(good_kid[1])

        reasons_array = np.array(reasons_array)

        # Sort and print reasons:
        indices = np.where(reasons_array == "Good KID")[0]
        print(f"Total good KIDs: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "No peak(s) found")[0]
        print(f"Total No Peaks found: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "More than one peak")[0]
        print(f"Total with more than one peak: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "Bifurcated")[0]
        print(f"Bifurcated: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "Fit uncertainty is too large")[0]
        print(f"Total fit uncertainty too large: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "Qr is too large")[0]
        print(f"Total Qr too large: {indices.size}")
        print(self.tone_name_array[indices])

        indices = np.where(reasons_array == "Qr is too small")[0]
        print(f"Total Qr too small: {indices.size}")
        print(self.tone_name_array[indices])

        return good_kid_array
    

    def plot_good_kids(self, kid_quality_array: np.ndarray, save_figure=False, save_name="") -> None:
        """
        Function to plot each kid sweep on multiple subplots with their tone names and the colour of
        the tone name indicating whether it has been identified as a good kid or a bad kid.

        :param kid_quality_array: Boolean array corresponding to whether each KID is good or bad (true/false).
        :param save_figure: Boolean to save the figure.
        :param save_name: file name to save the plot under (Must include image file type ending e.g. ".png").
        """

        n_sweeps = self.tone_name_array.shape[0]
        n_columns = 4
        n_rows = math.ceil(n_sweeps / n_columns)

        row = 0
        column = 0
        figures, axes = plt.subplots(n_rows, n_columns, figsize=(14, n_rows * 3))
        for count, sweep in enumerate(self.sweep_data_array):
            if kid_quality_array[count] == True:
                color = "g"
            else:
                color = "r"

            axes[row][column].plot(
                sweep.frequency_array * 1e-9, np.abs(sweep.get_iq_array())
            )
            axes[row][column].set_title(
                self.tone_name_array[count], color=color
            )

            if column == 0:
                axes[row][column].set_ylabel("S21 Magnitude (Arbitrary units)")

            if row == n_rows - 1:
                axes[row][column].set_xlabel("Frequency (GHz)")

            column += 1
            if column == 4:
                column = 0
                row += 1
        plt.tight_layout()
        if save_figure:
            plt.savefig(save_name)
        plt.show()
