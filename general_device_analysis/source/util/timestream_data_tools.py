"""
Functions for processing timestream data into PSD via the multiplication method.
"""

import math
import os
import re

import numpy as np
import scipy
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import gridspec
from lmfit import Model
from lmfit import Parameters
from scipy.constants import pi
from skimage.measure import block_reduce
from scipy.optimize import curve_fit 

from source.util import (
    general_plotting_tools,
    lekid_analysis_tools,
    data_processing_tools
)

from source.services import npz_load_service
from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.models.noise.noise_sweep import NoiseSweep
from source.models.noise.simple_noise_packet_psd import SimpleNoisePacketPSD
from source.models.resonator_measurement.sweep_data import SweepData
from source.models.resonator_measurement.timestream_data import TimestreamData
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import resonator_noise_analysis_tools


def calculate_sweep_frequency_derivatives(
    sweep_i_array: np.ndarray, sweep_q_array: np.ndarray, sweep_f_array: np.ndarray
):
    """
    Function to calculate the sweep data derivatives used to calculate the df/f for a noise packet via the
    multiplication method.\n
    Returns as an unpackable:\n
    di/df numpy array, dq/df numpy array, di^2 + dq^2 numpy array, f0 index of sweep, f0 value.
    \n
    :param sweep_i_array: I data from sweep array.
    :param sweep_q_array: Q data from sweep array.
    :param sweep_f_array: f data from sweep array.
    """

    # obtain derivatives of sweep data with respect to frequency
    sweep_di_df = np.diff(sweep_i_array) / np.diff(sweep_f_array)
    sweep_dq_df = np.diff(sweep_q_array) / np.diff(sweep_f_array)

    sweep_di2_plus_dq2 = sweep_di_df**2 + sweep_dq_df**2
    sweep_f0_index = np.where(sweep_di2_plus_dq2 == np.max(sweep_di2_plus_dq2))[0][0]
    f0 = sweep_f_array[sweep_f0_index]

    return sweep_di_df, sweep_dq_df, sweep_di2_plus_dq2, sweep_f0_index, f0


def calculate_timestream_df(
    di_df_r: float, dq_df_r: float, timestream_i_array: np.ndarray, timestream_q_array: np.ndarray
):
    """
    Function to return the df/f array for a timestream using the multiplication method.

    :param di_df_r: dI/df at the resonant frequency.
    :param dq_df_r: dQ/df at the resonant frequency.
    :param timestream_i_array: Timestream I values with I(f0) subtracted.
    :param timestream_q_array: Timestream Q values with Q(f0) subtracted.
    """

    bottom = di_df_r**2 + dq_df_r**2
    a = (timestream_i_array * di_df_r + timestream_q_array * dq_df_r) / bottom
    b = (-timestream_i_array * dq_df_r + timestream_q_array * di_df_r) / bottom

    return a + 1j * b


def get_mean_psd(
    number_of_timestreams: int,
    df_timestreams_array: np.ndarray,
    sample_rate: float,
    resample: bool,
    resample_bins: int,
):
    """
    Function to calculate each timestreams psd then mean average over all of them.

    Returns the mean psd and the corresponding frequencies.

    :param number_of_timestreams: Number of timestreams.
    :param df_timestreams_array: Array of all the df/f timestreams. Must be two dimensional even if there's only one
    timestream. e.g. a 1 timestream array should have shape (1, n_samples)
    :param sample_rate: Sample rate for the timestreams.
    :param resample_bins: Number of bins to use for exponential binning if resample = True.
    :param resample: Boolean to down sample the psd data.
    """

    # Get power spectral densities using scipy's periodogram function.
    psd_array = []

    if df_timestreams_array.ndim != 2:
        return print("df_timestreams_array is not 2D")

    for index in range(int(number_of_timestreams)):
        if number_of_timestreams == 1:
            (frequencies, psd) = scipy.signal.periodogram(
                df_timestreams_array, sample_rate, scaling="density"
            )
            psd_array.append(psd[0])
        else:
            (frequencies, psd) = scipy.signal.periodogram(
                df_timestreams_array[index], sample_rate, scaling="density"
            )
            psd_array.append(psd)

    # Get rid of negative indices:
    positive_frequency_mask = np.where(frequencies >= 0)[0]
    mean_psd = np.mean(psd_array, axis=0)[positive_frequency_mask] * 2
    frequencies = frequencies[positive_frequency_mask]

    if resample:
        frequencies, mean_psd, mean_psd_error = resample_noise_data_exponentially(
            frequency=frequencies, psd=mean_psd, n_bins=resample_bins
        )

    return mean_psd, frequencies


def resample_noise_data_exponentially(
    frequency: np.ndarray, psd: np.ndarray, n_bins:int=100
) -> tuple:
    """
    Resample a noise psd using exponential bin sizes. Returns the reduced frequency array and corresponding psd.
    Not statistically rigorous!!

    :param frequency: Frequency array.
    :param psd: PSD array to be down sampled.
    :param n_bins: Number of bins.
    """

    binned_frequencies = []
    binned_psd = []
    standard_error = []

    f_min = np.min(frequency) + 1e-4
    f_max = np.max(frequency)

    for bin_index in range(n_bins - 1):
        (bin_f_min, bin_f_max) = f_min * (f_max / f_min) ** (
            np.array([bin_index, bin_index + 1]) / (n_bins - 1.0)
        )
        bin_indices = np.where((frequency > bin_f_min) * (frequency <= bin_f_max))[0]

        if len(bin_indices) != 0:
            binned_frequencies.append(np.mean([bin_f_min, bin_f_max]))
            binned_psd.append(np.mean(psd[bin_indices]))
            standard_error.append(np.std(psd[bin_indices]))

    binned_frequencies = np.asarray(binned_frequencies)
    binned_psd = np.asarray(binned_psd)

    return binned_frequencies, binned_psd, standard_error


def plot_sweep_and_derivatives(
        frequency_array: np.ndarray,
        iq_array: np.ndarray,
        sweep_di_df: np.ndarray,
        sweep_dq_df: np.ndarray,
        sweep_di2_plus_dq2: np.ndarray,
        sweep_f0_index: int,
        save_figure: bool=False,
        save_name: str="",
        title: str="",
        axes: Axes=None,
    ) -> None:
        """
        Function to plot frequency sweep data and fit to the magnitude of IQ as well as plotting
        the sweep derivatives.

        :param frequency_array: Array of frequency data in Hertz.
        :param iq_array: Complex IQ data array.
        :param sweep_di_df: di/df array calculated from calculate_sweep_frequency_derivatives.
        :param sweep_dq_df: dq/df array calculated from calculate_sweep_frequency_derivatives.
        :param sweep_di2_plus_dq2: di^2 + dq^2 array calculated from the
        calculate_sweep_frequency_derivatives function.
        :param sweep_f0_index: Index corresponding to the f0 value for the tone in the frequency
        array.
        :param save_figure: Boolean to save the figure or not. Default is false.
        :param save_name: Full path to save the plot to. Must include file format e.g. ".png".
        :param title: Title to give to the plot.
        :param axes: Matplotlib Axes object to plot results to. must be initialised as:
        
        axes = plt.subplots(1, 4, figsize=(14, 4))
        """

        # Fit Lorentzian to Sweep data
        fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
            frequency_array=frequency_array,
            data_array=np.abs(iq_array),
            qc_guess=1e5,
            qi_guess=1e5,
            f0_guess=None,
            fit_fraction=0.8,
            normalise=False,
            plot_db=True,
            plot_title="",
            plot_graph=False,
        )
        s21_fit = lekid_analysis_tools.skewed_lorentzian(
            frequency_array=frequency_array,
            f0=fit_result_dict["f0"][0],
            qi=fit_result_dict["qi"][0],
            qc_real=fit_result_dict["qc_real"][0],
            qc_imag=fit_result_dict["qc_imag"][0],
            amp=fit_result_dict["amp"][0],
        )

        if axes is None:
            figure, axes = plt.subplots(1, 4, figsize=(14, 4))
        axes[0].plot(frequency_array * 1e-9, np.abs(iq_array))
        axes[0].plot(
            frequency_array * 1e-9,
            s21_fit,
            color="r",
            linestyle="--",
            linewidth=2,
            label=f"Fit:\nQr={fit_result_dict['qr'][0]:.2E}"
            f"\nQc={fit_result_dict['qc'][0]:.2E}"
            f"\nQi={fit_result_dict['qi'][0]:.2E}",
        )
        axes[0].legend()
        axes[0].set_xlabel("Frequency (GHz)")
        axes[0].set_ylabel("S21 Magnitude (Arb)")
        axes[1].plot(
            frequency_array * 1e-9,
            20 * np.log10(np.abs(iq_array)),
        )
        axes[1].set_xlabel("Frequency (GHz)")
        axes[1].set_ylabel("S21 Magnitude (dB)")
        axes[2].plot(np.real(iq_array), np.imag(iq_array))
        axes[2].set_xlabel("I (V)")
        axes[2].set_ylabel("Q (V)")
        axes[2].axis("equal")
        axes[3].plot(sweep_di_df * 1e9, label="dI'")
        axes[3].plot(sweep_dq_df * 1e9, label="dQ'")
        axes[3].plot(
            np.sqrt(sweep_di2_plus_dq2) * 1e9, label="$\sqrt{dI'^2+dQ'^2}$"
        )
        axes[3].vlines(
            sweep_f0_index,
            ymin=np.min(sweep_dq_df * 1e9),
            ymax=np.max(sweep_di2_plus_dq2 * 1e9),
            linestyle="--",
            color="k",
        )
        axes[3].set_xlabel("Index")
        axes[3].set_ylabel("d/df (V/GHz)")
        axes[3].legend()
        
        figure = plt.gcf()
        figure.suptitle(title)
        plt.tight_layout()
        if save_figure:
            plt.savefig(save_name)
        plt.show()


def get_mean_psd_from_data_objects(
        sweep_data: SweepData,
        timestream_data: TimestreamData,
        resample: bool,
        resample_bins: int,
    ) -> None:
    """
    Function to read a timestream sweep file and a timestream file, calculate df/f using the
    multiplication method then return the frequencies and Mean PSD.

    :param sweep_data: SweepData object for timestream with I and Q arrays with the following
    shape: (N repeats, X data points) and the frequency array is returned as an array with shape
    (X data points) and corresponds to all repeats.
    :param timestream_data: TimestreamData object containing 2D arrays of I and Q of shape
    (N repeats, M samples).
    :param resample_bins: Number of bins to use for exponential binning if resample = True.
    :param resample: Boolean to down sample the psd data.
    """

    # Load sweep data:
    sweep_frequency_array = sweep_data.frequency_array
    sweep_iq_array = sweep_data.i.mean(axis=0) + 1j * sweep_data.q.mean(axis=0)
    
    # Load timestream data:
    timestream_iq_array = timestream_data.timestream_i_data + 1j * timestream_data.timestream_q_data

    sampling_rate = timestream_data.sample_rate

    (
        sweep_di_df, sweep_dq_df, sweep_di2_plus_dq2, sweep_f0_index, f0
    ) = calculate_sweep_frequency_derivatives(
        sweep_i_array=np.real(sweep_iq_array),
        sweep_q_array=np.imag(sweep_iq_array),
        sweep_f_array=sweep_frequency_array,
    )

    # Get df array for each repeat of timestream IQ:
    timestream_df_array = np.empty_like(timestream_iq_array)
    for count, timestream_iq in enumerate(timestream_iq_array):
        timestream_df = calculate_timestream_df(
            di_df_r=sweep_di_df[sweep_f0_index],
            dq_df_r=sweep_dq_df[sweep_f0_index],
            timestream_i_array=np.real(timestream_iq),
            timestream_q_array=np.imag(timestream_iq),
        )
        timestream_df_array[count] = timestream_df / f0 # Take df (disregard dissipation [imag]) and divide by f0.

    mean_psd, frequencies = get_mean_psd(
        number_of_timestreams=timestream_df_array.shape[0],
        df_timestreams_array=np.real(timestream_df_array),
        sample_rate=sampling_rate,
        resample=resample,
        resample_bins=resample_bins,
    )

    return frequencies, mean_psd


def plot_psd_from_data_objects(
        sweep_data: SweepData,
        timestream_data: TimestreamData,
        resample: bool,
        resample_bins: int,
        label: str="",
        axes: Axes=None,
    ) -> None:
    """
    Function to read a timestream sweep file and a timestream file, calculate df/f using the
    multiplication method then calculate the PSD and plot this.

    :param sweep_data: SweepData object for timestream with I and Q arrays with the following
    shape: (N repeats, X data points) and the frequency array is returned as an array with shape
    (X data points) and corresponds to all repeats.
    :param timestream_data: TimestreamData object containing 2D arrays of I and Q of shape
    (N repeats, M samples).
    :param resample_bins: Number of bins to use for exponential binning if resample = True.
    :param resample: Boolean to down sample the psd data.
    :param label: Label to be given to the data series.
    :param axes: Matplotlib axes consisting of three subplots. Preferably instanciated in a similar
    way to:

    figure = plt.figure(tight_layout=True, figsize=(10, 6))
    gs = gridspec.GridSpec(3, 2, width_ratios=(0.75, 0.25), height_ratios=(0.5, 0.25, 0.25))

    axes = [
        figure.add_subplot(gs[0, :]),
        figure.add_subplot(gs[1, :-1]),
        figure.add_subplot(gs[2, :-1]),
        figure.add_subplot(gs[1:, 1:]),
    ]
    """
    sweep_iq_array = sweep_data.i.mean(axis=0) + 1j * sweep_data.q.mean(axis=0)
    timestream_iq_array = timestream_data.timestream_i_data + 1j * timestream_data.timestream_q_data


    frequencies, mean_psd = get_mean_psd_from_data_objects(
        sweep_data=sweep_data,
        timestream_data=timestream_data,
        resample=resample,
        resample_bins=resample_bins,
    )

    general_plotting_tools.plot_psd(
        psd_array=mean_psd[1:],
        frequency_array=frequencies[1:],
        sweep_iq_array=sweep_iq_array,
        timestream_iq_array=timestream_iq_array,
        axes=axes,
        label=label,
    )

    return frequencies, mean_psd


def plot_psd_from_noise_directory(directory: str, resample_bins: int=500, clean_timestream=False) -> None:
    """
    Function to plot the all timestream information (IQ timestreams, IQ sweep, PSD) for a directory
    containing a sweep file and multiple timestream files.

    :param directory: Directory path containing the noise files.
    :param resample_bins: How many bins to resample the PSD into.
    :param clean_timestreams: Boolean to remove the anomalies in a timestream.
    """

    timestream_data_array = []
    for file_index, file_name in enumerate(os.listdir(directory)):
        
        file_path = os.path.join(directory, file_name)
        if "sweep" in file_name:
            sweep_data = npz_load_service.load_timestream_sweep(file_path=file_path)
        else:
            timestream_data = npz_load_service.load_iq_timestream(file_path=file_path)
            timestream_data_array.append(timestream_data)

    figure = plt.figure(tight_layout=True, figsize=(10, 6))
    gs = gridspec.GridSpec(3, 2, width_ratios=(0.75, 0.25), height_ratios=(0.5, 0.25, 0.25))

    axes = [
        figure.add_subplot(gs[0, :]),
        figure.add_subplot(gs[1, :-1]),
        figure.add_subplot(gs[2, :-1]),
        figure.add_subplot(gs[1:, 1:]),
    ]

    figure.suptitle(f"{sweep_data.measurement_header.tone_name}")

    for timestream_data in timestream_data_array:

        if clean_timestream:
            timestream_data.timestream_i_data = data_processing_tools.remove_timestream_anomalies(
                timestream_data.timestream_i_data,
                sampling_rate=timestream_data.sample_rate,
                save_anomaly_to_file=""
            )
            timestream_data.timestream_q_data = data_processing_tools.remove_timestream_anomalies(
                timestream_data.timestream_q_data,
                sampling_rate=timestream_data.sample_rate,
                save_anomaly_to_file=""
            )

        if timestream_data.on_resonance:
            label = f"On Res: {timestream_data.sample_rate:.1E}Hz"
        else:
            label = f"Off Res: {timestream_data.sample_rate:.1E}Hz"
        plot_psd_from_data_objects(
            sweep_data=sweep_data,
            timestream_data=timestream_data,
            resample=True,
            resample_bins=resample_bins,
            axes=axes,
            label=label
        )

    return None


def load_tone_multisetting_psd(
        tone_timestream_directory: str,
        on_res: bool,
        resample: bool,
        resample_bins=200,
        remove_spikes=False,
        save_fits=False,
        tone_name="",
    ) -> tuple:
    """
    Function to sweep through a directory (for a tone) with the following structure:

    tone_timestream_directory:
        temperature_directories:
            power_directories:
                time_stream_sweep_file
                iq_timestream_files

    The sweep file and on_res/off_res timestreams from each noise directory (tone/temp/power/) are
    loaded and the mean psd from each iq timestream is calculated then combined.

    The IQ sweep is also used to check if resonator is bifurcated using the
    lekid_analysis_tools.is_bifurcated function.

    Returns a tuple with the temperature_array (np.ndarray), power_array (np.ndarray),
    frequencies_list, and mean_psd_list, bifurcated_array.

    If desired, resample each mean_psd and frequency into N=resample_bins bins.

    :param tone_timestream_directory: Path to directory for the Tone.
    :param on_res: Boolean to choose the on resonance timestreams. If false off res will be used.
    :param resample: Boolean to resample psd into N=resample_bins bins.
    :param resample_bins: Number of bins to resample into.
    :param remove_spikes: Boolean to use the psd peak removal function.
    :param save_fits: If true, the PSD fits will be saved to figures directory.
    """

    mean_psd_array = []
    frequencies_array = []
    temperature_array = []
    power_array = []
    bifurcated_array = []
    fit_results_array = []

    for temperature_directory_name in os.listdir(tone_timestream_directory):
        temperature_directory = os.path.join(tone_timestream_directory, temperature_directory_name)
        for power_directory_name in os.listdir(temperature_directory):
            current_noise_directory = os.path.join(temperature_directory, power_directory_name)

            print(f"Processing timestreams for {tone_name}: {temperature_directory_name}, {power_directory_name}...")
            timestream_data_array = []
            for file_index, file_name in enumerate(os.listdir(current_noise_directory)):
                file_path = os.path.join(current_noise_directory, file_name)

                if "sweep" in file_name.lower():
                    sweep_data = npz_load_service.load_timestream_sweep(file_path=file_path)
                elif on_res and "onres" in file_name.lower():
                    timestream_data_array.append(npz_load_service.load_iq_timestream(file_path=file_path))
                elif not on_res and "offres" in file_name.lower():
                    timestream_data_array.append(npz_load_service.load_iq_timestream(file_path=file_path))

            temperature_array.append(sweep_data.measurement_header.actual_temperature)
            power_array.append(sweep_data.measurement_header.power)
            # Get mean psd:
            # Get sweep derivatives:
            sweep_iq_array = sweep_data.i.mean(axis=0) + 1j * sweep_data.q.mean(axis=0)
            (
                sweep_di_df, sweep_dq_df, sweep_di2_plus_dq2, sweep_f0_index, f0
            ) = calculate_sweep_frequency_derivatives(
                sweep_i_array=np.real(sweep_iq_array),
                sweep_q_array=np.imag(sweep_iq_array),
                sweep_f_array=sweep_data.frequency_array,
            )

            is_bifurcated = lekid_analysis_tools.is_bifurcated(iq_array=sweep_iq_array)
            bifurcated_array.append(is_bifurcated)

            
            # Load timestream data:
            timestream_df_arrays = []
            sampling_rates = []
            for timestream_data in timestream_data_array:
                anomaly_directory = os.path.join("figures", tone_name, "time_stream_anomaly_data")
                if not os.path.exists(anomaly_directory):
                    os.makedirs(anomaly_directory)                
                sampling_rate = timestream_data.sample_rate
                i_anomaly_file_name = f"{timestream_data.measurement_header.tone_name}_{sampling_rate}Hz_timestream_i_anomalies_{timestream_data.measurement_header.power}dB_{timestream_data.measurement_header.actual_temperature*1000:.0f}mK"
                q_anomaly_file_name = f"{timestream_data.measurement_header.tone_name}_{sampling_rate}Hz_timestream_q_anomalies_{timestream_data.measurement_header.power}dB_{timestream_data.measurement_header.actual_temperature*1000:.0f}mK"
                # Get df array for each repeat of timestream IQ:
                sampling_rates.append(timestream_data.sample_rate)
                timestream_data.timestream_i_data = data_processing_tools.remove_timestream_anomalies(
                    timestream_data.timestream_i_data,
                    sampling_rate=sampling_rate,
                    # save_anomaly_to_file=os.path.join(anomaly_directory, i_anomaly_file_name)
                    save_anomaly_to_file=""
                )
                timestream_data.timestream_q_data = data_processing_tools.remove_timestream_anomalies(
                    timestream_data.timestream_q_data,
                    sampling_rate=sampling_rate,
                    # save_anomaly_to_file=os.path.join(anomaly_directory, q_anomaly_file_name)
                    save_anomaly_to_file=""
                )

                timestream_iq_array = timestream_data.timestream_i_data + 1j * timestream_data.timestream_q_data

                timestream_df_array = np.empty_like(timestream_iq_array)
                for count, timestream_iq in enumerate(timestream_iq_array):
                    timestream_df = calculate_timestream_df(
                        di_df_r=sweep_di_df[sweep_f0_index],
                        dq_df_r=sweep_dq_df[sweep_f0_index],
                        timestream_i_array=np.real(timestream_iq),
                        timestream_q_array=np.imag(timestream_iq),
                    )
                    timestream_df_array[count] = timestream_df / f0 # Take df (disregard dissipation [imag]) and divide by f0.

                timestream_df_arrays.append(timestream_df_array)

            # Now for each df array get mean psd and frequencies, then add to full arrays :
            for count, timestream_df_array in enumerate(timestream_df_arrays):
                if count == 0:
                    mean_psd, frequencies = get_mean_psd(
                        number_of_timestreams=timestream_df_array.shape[0],
                        df_timestreams_array=np.real(timestream_df_array),
                        sample_rate=sampling_rates[count],
                        resample=False,
                        resample_bins=1000,
                    )
                    mean_psd = mean_psd
                    frequencies = frequencies

                else:
                    mean_psd_1, frequencies_1 = get_mean_psd(
                        number_of_timestreams=timestream_df_array.shape[0],
                        df_timestreams_array=np.real(timestream_df_array),
                        sample_rate=sampling_rates[count],
                        resample=False,
                        resample_bins=1000,
                    )
                    mean_psd = np.append(mean_psd, mean_psd_1)
                    frequencies = np.append(frequencies, frequencies_1)

            # Sort in ascending frequencies     
            sort_order = np.argsort(frequencies)
            frequencies = frequencies[sort_order]
            mean_psd = mean_psd[sort_order]

            # Delete the DC points.
            zero_indices = np.where(frequencies==0.0)[0]
            frequencies = np.delete(frequencies, zero_indices)
            mean_psd = np.delete(mean_psd, zero_indices)
                          
            if save_fits:
                tone_directory = os.path.join("figures", tone_name, "psd_fits")
                if not os.path.exists(tone_directory):
                    os.makedirs(tone_directory)
            else:
                tone_directory = ""

            if resample:
                frequencies, mean_psd, mean_psd_error = resample_noise_data_exponentially(
                    frequency=frequencies, psd=mean_psd, n_bins=resample_bins
                )

            if remove_spikes:
                mean_psd = data_processing_tools.remove_psd_peaks(
                    frequencies, mean_psd, f_min=10, f_max=2e4, show_plot=False, prominence=0.5, iterations=3
                )


            fit_results = fit_dark_general_ffs_psd(
                frequency_array=frequencies,
                data_array=mean_psd,
                fit_min_frequency=1,
                fit_max_frequency=9e4,
                a_guess=1e-18,
                b_guess=0.5e-17,
                n_guess=1,
                c_guess=1e-19,
                roll_off_lifetime_guess=1e-5,
                plot_graph=False,
                plot_title=f"{tone_name} PSD Fit at {temperature_directory_name}, {power_directory_name}",
                save_figure=save_fits,
                save_name=os.path.join(tone_directory, f"{temperature_directory_name}_{power_directory_name}_psd_fit.png"),
                axis=None,
            )

            # In case where fit fails:
            if fit_results["a"] == None:
                print(f"PSD Fit failed for {temperature_directory_name}, {power_directory_name}")

            if len(mean_psd_array) != 0 and mean_psd.size != mean_psd_array[0].size:
                difference = mean_psd.size - mean_psd_array[0].size
                if difference > 0:
                    mean_psd = mean_psd[:-difference]
                    frequencies = frequencies[:-difference]
                if difference < 0:
                    mean_psd = np.append(mean_psd, np.full(np.abs(difference), mean_psd[-1]))
                    frequencies = np.append(frequencies, np.full(np.abs(difference), frequencies[-1]))

            frequencies_array.append(frequencies)
            mean_psd_array.append(mean_psd)
            fit_results_array.append(fit_results)
            

    temperature_array = np.round(np.array(temperature_array), decimals=3)
    power_array = np.array(power_array)
    frequencies_array = np.array(frequencies_array)
    mean_psd_array = np.array(mean_psd_array)
    bifurcated_array = np.array(bifurcated_array)
    fit_results_array = np.array(fit_results_array)

    return temperature_array, power_array, frequencies_array, mean_psd_array, bifurcated_array, fit_results_array


def general_ffs_psd(
    frequency_array: np.ndarray,
    a: float,
    b: float,
    n: float,
    c: float,
    roll_off_lifetime: float,
):
    """
    Function to calculate a general power spectral density of the fractional frequency noise of a detector. Adapted from
    paper by R. McGeehan DOI: 10.1007/s10909-018-2061-6.

    :param frequency_array: Array of N frequency points.
    :param a: Numerator term 1.
    :param b: 1/f multiplier.
    :param n: Power of 1/f term.
    :param c: Amplitude offset.
    :param roll_off_lifetime: Roll off lifetime.
    """

    term_1 = a + b * frequency_array ** (-n)
    term_2 = 1 + (2 * pi * frequency_array * roll_off_lifetime) ** 2

    return (term_1 / term_2) + c


def fit_dark_general_ffs_psd(
    frequency_array: np.array,
    fit_min_frequency: float,
    fit_max_frequency: float,
    data_array: np.array,
    a_guess: float,
    b_guess: float,
    n_guess: float,
    c_guess: float,
    roll_off_lifetime_guess: float or None,
    plot_graph=True,
    axis=None,
    plot_title="",
    save_figure=False,
    save_name="",
) -> dict:
    """
    Function to fit the roll-off frequency of a dark power spectral density of the fractional frequency noise, Sxx, in
    units of Hz-1. See R. McGeehan DOI: 10.1007/s10909-018-2061-6.

    :param frequency_array: Array of N frequency points.
    :param fit_min_frequency: Minimum frequency value to fit.
    :param fit_max_frequency: Maximum frequency value to fit.
    :param data_array: FFT Sxx data array.
    :param a_guess: Numerator term 1.
    :param b_guess: 1/f multiplier.
    :param c_guess: Power of 1/f term.
    :param n_guess: Amplitude offset.
    :param roll_off_lifetime_guess: Roll off lifetime.
    :param plot_graph: Boolean to plot the graph.
    :param plot_title: Title to give to plot, default is nothing.
    """

    mask = np.where(
        (frequency_array > fit_min_frequency) & (frequency_array < fit_max_frequency)
    )[0]
    fit_frequency_array = frequency_array[mask]

    guesses = [a_guess, b_guess, n_guess, c_guess, roll_off_lifetime_guess]

    # Increase weight around roll off frequency:
    sigma = np.full(shape=fit_frequency_array.shape, fill_value=100)
    roll_off_indices = np.where((fit_frequency_array > 100) & (fit_frequency_array < 9e4))
    np.put(sigma, roll_off_indices, 10)
    amp_noise = np.where((fit_frequency_array > 1e4) & (fit_frequency_array < 9e4))
    np.put(sigma, amp_noise, 1)

    try:
        parameter_values, parameter_covariance  = curve_fit(
            general_ffs_psd, fit_frequency_array, data_array[mask], p0=guesses, sigma=sigma
        )
    except:
        print("Unable to find suitable fit.")
        return {"a":[None, None], "b":[None, None], "n":[None, None], "c":[None, None], "roll_off_lifetime": [None, None]}
    standard_errors = np.sqrt(np.diag(parameter_covariance))

    psd_fit = general_ffs_psd(fit_frequency_array, *parameter_values)

    result_dict = {}
    result_dict["a"] = [parameter_values[0], standard_errors[0]]
    result_dict["b"] = [parameter_values[1], standard_errors[1]]
    result_dict["n"] = [parameter_values[2], standard_errors[2]]
    result_dict["c"] = [parameter_values[3], standard_errors[3]]
    result_dict["roll_off_lifetime"] = [parameter_values[4], standard_errors[4]]

    if plot_graph or save_figure:

        if axis is None:
            figure, axis = plt.subplots(1,1, figsize=(10, 6))
        axis.plot(
            frequency_array,
            data_array,
            linestyle="-",
            linewidth=2,
            color="r",
            label="Data",
            alpha=0.5,
        )
        axis.plot(
            fit_frequency_array,
            psd_fit,
            linestyle="-",
            color="k",
            label=f"Best fit:\nA={result_dict['a'][0]:.2E}"
            f"\nB={result_dict['b'][0]:.2E}"
            f"\nn={result_dict['n'][0]:.2E}"
            f"\nC={result_dict['c'][0]:.2E}"
            f"\n$\\tau$={result_dict['roll_off_lifetime'][0]:.2E}",
        )
        axis.set_xlabel("Frequency (Hz)")
        axis.set_ylabel("Sxx (Hz$^{-1}$)")
        axis.set_xscale("log")
        axis.set_yscale("log")
        axis.set_title(plot_title)
        axis.legend()
        plt.tight_layout()
        
        if save_figure:
            plt.savefig(save_name)
            if plot_graph == False:
                plt.close(figure)
        if plot_graph:
            plt.show()

    return result_dict

        
