import numpy as np
import matplotlib.pyplot as plt
import math

from scipy.stats import skewnorm



def get_number_of_clashes(
    f0_array: np.ndarray,
    qr: float,
    line_width_spacing_clash_threshold: float,
    plot_spacing=False,
) -> int:
    """
    Function to find the number of occasions that two f0's are within some threshold of number of linewidths.

    :param f0_array: Array of resonant frequencies
    :param qr: Quality factor of resonators (for line width calculation)
    :param line_width_spacing_clash_threshold: line width threshold, separations less than this are considered a clash.
    :param plot_spacing: Boolean to plot the line width separations relative to the threshold.
    :return:
    """
    f0_spacing_array = np.diff(f0_array)
    line_width_array = f0_array / qr
    line_width_spacing_array = f0_spacing_array / line_width_array[-1]
    clash_indices = np.where(
        line_width_spacing_array < line_width_spacing_clash_threshold
    )[0]

    if plot_spacing:
        plt.figure(figsize=(8, 6))
        plt.plot(line_width_spacing_array, linestyle="None", marker="o")
        plt.hlines(
            y=line_width_spacing_clash_threshold,
            xmin=0,
            xmax=line_width_spacing_array.size,
            color="r",
            label="Threshold",
        )
        plt.legend()
        plt.ylabel("F0 spacing (line widths)")
        plt.xlabel("F0 index")
        plt.show()

    return clash_indices.size


def randomize_f0_array(
    f0_array: np.ndarray,
    fab_yield: float,
    fractional_frequency_shift: float,
    ffs_sigma=1.0,
    plot_f0_arrays=False,
) -> np.ndarray:
    """
    Function to take a random sub-sample of the full f0 array according to some yield fraction then add a random scatter
    where by the distribution of the scatter is defined by a normal distribution about 0 with some standard deviation
    and a magnitude used to scale that standard deviation.

    :param f0_array: Array of resonant frequencies.
    :param fab_yield: Fraction of f0's that are randomly selected. e.g. 0.2 corresponds to 20% yield, 1 corresponds to
    100% yield.
    :param fractional_frequency_shift: The value that a 1 sigma normal distribution about zero will be scaled by. These
    values are then multiplied by the f0's, the resulting values are then added to the original f0's to yield a
    randomised f0 scatter by fractional frequency shift.
    :param ffs_sigma: Standard deviation to use for the fractional frequency shift distribution. Default is 1.
    :param plot_f0_arrays: Boolean to plot
    :return:
    """
    # Take random sub sample according to the yield:
    subsample_f0_array = np.sort(
        np.random.choice(
            a=f0_array, size=math.ceil(f0_array.size * fab_yield), replace=False
        )
    )
    # Make an array of randomly distributed fractional frequency shifts between positive and negative input fractional
    # frequency shift value
    fractional_frequency_shift_array = (
        np.random.normal(loc=0.0, scale=ffs_sigma, size=subsample_f0_array.size)
        * fractional_frequency_shift
    )
    f0_shift = subsample_f0_array * fractional_frequency_shift_array
    randomized_f0_array = np.sort(subsample_f0_array + f0_shift)

    if plot_f0_arrays:
        # Plot Scatter distribution:
        plt.figure(figsize=(8, 6))
        plt.plot(
            fractional_frequency_shift_array, linestyle="none", marker="o", markersize=1
        )
        plt.xlabel("F0 Index")
        plt.ylabel("Fractional Frequency Shift")
        plt.show()

        # Plot f0's
        plt.figure(figsize=(8, 6))
        plt.plot(
            f0_array * 1e-9,
            linestyle="none",
            marker="_",
            markersize=8,
            label="Expected",
        )
        plt.plot(
            subsample_f0_array * 1e-9,
            linestyle="none",
            marker="o",
            markersize=3,
            fillstyle="none",
            label=f"{fab_yield * 100}% Subsample",
        )
        plt.plot(
            randomized_f0_array * 1e-9,
            linestyle="none",
            marker="o",
            markersize=1,
            label=f"{fab_yield * 100}% Subsample and {fractional_frequency_shift} shift",
        )
        plt.xlabel("F0 Index")
        plt.ylabel("F0 (GHz)")
        plt.legend()
        plt.show()

    return randomized_f0_array  # Sort to keep in ascending order


def get_clash_number_array(
    f0_array: np.ndarray,
    fab_yield: float,
    fractional_frequency_shift: float,
    qr: float,
    line_width_spacing_clash_threshold: float,
    repeats: int,
    plot_density_histogram=False,
) -> np.ndarray:
    """
    Function to randomize an f0 array, sub-sampled according to a yield, then find the number of clashes. This process
    is then repeated a specified number of times. Each time the number of clashes is recorded until an array full of
    each iterations clash number is returned.

    :param f0_array: Array of resonant frequencies.
    :param fab_yield: Fraction of f0's that are randomly selected. e.g. 0.2 corresponds to 20% yield, 1 corresponds to
    100% yield.
    :param fractional_frequency_shift: The fractional frequency shift of the f0's. See randomize_f0_array description.
    :param qr: Quality factor of resonators (for line width calculation)
    :param line_width_spacing_clash_threshold: line width threshold, separations less than this are considered a clash.
    :param repeats: Number of times to repeat randomization and clash counting.
    :param plot_density_histogram: Boolean to plot the resulting probability density histogram of the clash counts.
    :return:
    """

    clash_number_array = []
    for repeat_count in range(repeats):
        randomized_f0_array = randomize_f0_array(
            f0_array=f0_array,
            fab_yield=fab_yield,
            fractional_frequency_shift=fractional_frequency_shift,
            plot_f0_arrays=False,
        )
        number_of_clashes = get_number_of_clashes(
            f0_array=randomized_f0_array,
            qr=qr,
            line_width_spacing_clash_threshold=line_width_spacing_clash_threshold,
            plot_spacing=False,
        )
        clash_number_array.append(number_of_clashes)

    clash_number_array = np.array(clash_number_array)

    if plot_density_histogram:
        clash_range = np.max(clash_number_array) - np.min(clash_number_array)
        plt.figure(figsize=(8, 6))
        plt.hist(clash_number_array, bins=clash_range, density=True)
        plt.xlabel("Number of clashes")
        plt.ylabel("Probability Density")
        plt.title(
            f"<{line_width_spacing_clash_threshold} line width clash probabilities from {repeats} repeats "
            f"of:\n{fab_yield}% yield, normally distributed {fractional_frequency_shift} fractional frequency shift"
            f" magnitude"
        )
        plt.show()

    return clash_number_array


def get_most_common_clash_count(clash_array: np.ndarray) -> np.ndarray:
    """
    Function to extract the most common number of clashes (peak of a normal distribution) for an MxN array of clash
    values. Where M is the number of different types of trials (e.g. different yield values) then N is the number of
    repeats for a given trial.

    :param clash_array: MxN array of clash values.
    :return:
    """

    most_common_clash_number_array = []

    # For each clash array get normalised count for each number of clashes:
    for count, trial in enumerate(clash_array):
        minimum_clashes = np.min(clash_array)
        maximum_clashes = np.max(clash_array) + 1
        number_of_bins = maximum_clashes - minimum_clashes

        counts, clash_bins = np.histogram(trial, bins=number_of_bins, density=False)
        most_likely_clashes_index = np.argmax(counts)
        most_common_clash_number_array.append(clash_bins[most_likely_clashes_index])

    most_common_clash_number_array = np.array(most_common_clash_number_array)

    return most_common_clash_number_array


def linewidth_spacing_cumulative_density(
        expected_f0_array: np.array,
        fab_yield: float,
        fractional_frequency_shift: float,
        qr_gamma: float,
        qr_mean: float,
        qr_sigma: float,
        histogram_bins: int,
        histogram_minimum_bin: int,
        histogram_maximum_bin: int,
        ffs_sigma=1.0,
        use_fixed_qr=False
) -> tuple:
    """
    Function to create a scattered f0 array from an expected f0 with a given yield and fractional frequency shift
    and also a skewed normally distributed Qr array given the skewness (gamma), mean, and standard deviation (sigma).

    The line width separation is then obtained from these two arrays and the cumulative frequency density is calculated
    according to the number of bins, and the bin range.

    Returns the cumulative frequency density and the corresponding count_bins

    :param expected_f0_array: Array of the expected f0's from which to derive the scattered array.
    :param fab_yield: Yield of the expected array.
    :param fractional_frequency_shift: Fractional scatter for 1 sigma distribution.
    :param qr_gamma: Skewness of the Qr distribution.
    :param qr_mean: Mean value of the Qr distribution.
    :param qr_sigma: Standard deviation of the Qr distribution.
    :param histogram_bins: Number of bins for the cumulative frequency.
    :param histogram_minimum_bin: Minimum bin for the histogram.
    :param histogram_maximum_bin: Maximum bin for the histogram.
    :param ffs_sigma: Standard deviation to use for the fractional frequency shift distribution. Default is 1.

    """
    
    # Create random scattered f0 array
    fake_scattered_array = randomize_f0_array(
        f0_array=expected_f0_array,
        fab_yield=fab_yield,
        fractional_frequency_shift=fractional_frequency_shift,
        plot_f0_arrays=False
    )

    # create fake skewed normally distributed Qr array:
    if use_fixed_qr:
        fake_qr_array=np.full(fake_scattered_array.shape, qr_mean)
    else:
        fake_qr_array = np.abs(skewnorm.rvs(a=qr_gamma, loc=qr_mean, scale=qr_sigma, size=fake_scattered_array.size)) * 1e3

    fake_line_width_spacing = np.diff(fake_scattered_array) / fake_qr_array[:-1]

    # Calculate probability density
    count, bins_count = np.histogram(fake_line_width_spacing, bins=histogram_bins, range=(histogram_minimum_bin, histogram_maximum_bin), density=False)
    probability_density = count / sum(count)
    cumulative_frequency = np.cumsum(probability_density)

    return cumulative_frequency, bins_count


def spacing_cumulative_density(
        expected_f0_array: np.array,
        fab_yield: float,
        fractional_frequency_shift: float,
        histogram_bins: int,
        histogram_minimum_bin: int,
        histogram_maximum_bin: int,
        ffs_sigma=1.0,
        repeats=1,
) -> tuple:
    """
    Function to create a scattered f0 array from an expected f0 with a given yield and fractional frequency shift.

    The f0 spacing in Hz is obtained and the cumulative frequency density is calculated
    according to the number of bins, and the bin range.

    Returns the cumulative frequency density and the corresponding count_bins

    :param expected_f0_array: Array of the expected f0's from which to derive the scattered array.
    :param fab_yield: Yield of the expected array.
    :param fractional_frequency_shift: Fractional scatter for 1 sigma distribution.
    :param histogram_bins: Number of bins for the cumulative frequency.
    :param histogram_minimum_bin: Minimum bin for the histogram.
    :param histogram_maximum_bin: Maximum bin for the histogram.
    :param ffs_sigma: Standard deviation to use for the fractional frequency shift distribution. Default is 1.
    :param repeats: The number of times to create the f0 array, calculate the cumulative frequency and then the
    mean for each bin will be returned.
    """
    
    cumulative_frequency_array = []
    for repeat in range(repeats):
        # Create random scattered f0 array
        fake_scattered_array = randomize_f0_array(
            f0_array=expected_f0_array,
            fab_yield=fab_yield,
            fractional_frequency_shift=fractional_frequency_shift,
            ffs_sigma=ffs_sigma,
            plot_f0_arrays=False
        )

        fake_spacing = np.diff(fake_scattered_array)

        # Calculate probability density
        count, bins_count = np.histogram(fake_spacing, bins=histogram_bins, range=(histogram_minimum_bin, histogram_maximum_bin), density=False)
        probability_density = count / sum(count)
        cumulative_frequency = np.cumsum(probability_density)

        cumulative_frequency_array.append(cumulative_frequency)

    cumulative_frequency_array = np.array(cumulative_frequency_array)

    if repeats > 1:
        return np.mean(cumulative_frequency_array, axis=0), bins_count
    if repeats == 1:
        return cumulative_frequency, bins_count
