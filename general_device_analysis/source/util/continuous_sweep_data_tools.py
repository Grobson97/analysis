import numpy as np
import matplotlib.pyplot as plt

from matplotlib.pyplot import Axes

from source.models.resonator_measurement.sweep_data import SweepData


def plot_sweep(
        sweep_data: SweepData, save_figure: bool=False, axis: Axes=None, label: str=""
    ) -> None:
    """
    Function to plot the continuous sweep in dB, either on a default new axis or on a provided axis then
    optionally save the figure. 

    :param sweep_data: SweepData object containing the sweep data.
    :param save_figure: boolean to save the figure or not. Plot will be saved to the plots folder
    in the top level directory under the DUT.
    :param axis: Matplotlib Axis object to optionally plot to. If none, one is created.
    """

    magnitude_db = 20 * np.log10(np.abs(sweep_data.i + 1j * sweep_data.q))

    if axis is None:
        figure, axis = plt.subplots(figsize=(10, 5))
    axis.plot(sweep_data.frequency_array[0] * 1e-9, magnitude_db.mean(axis=0), label=label)
    axis.set_title(
        sweep_data.measurement_header.dut
        + f" VNA Sweep at T = {int(sweep_data.measurement_header.actual_temperature*1000)}mK and {sweep_data.measurement_header.power} dB attenuation",
        fontsize=14
    )
    axis.set_xlabel("Frequency (GHz)", fontsize=14)
    axis.set_ylabel("S21 Magnitude (dB)", fontsize=14)
    axis.tick_params(axis="both", labelsize=12)
    axis.legend()
    if save_figure:
        plt.savefig(
            f"figures/{sweep_data.measurement_header.dut}_Continuous_Sweep_{int(sweep_data.measurement_header.actual_temperature * 1000)}mK_{sweep_data.measurement_header.power}dB.png"
        )
    
    return None