import numpy as np
import numpy as np
import source.util.hidfmux_transfer_functions as hidfmux_transfer_functions
from source.models.hidfmux.hidfmux_timestream import TimestreamData
from source.models.hidfmux.hidfmux_multi_sweep import HidfmuxMultiSweep
from source.models.hidfmux.hidfmux_timestream import HidfmuxTimestream
from source.util import hidfmux_data_tools 
from source.util import hidfmux_transfer_functions
from netCDF4 import Dataset
from scipy import signal


from scipy import interpolate


def get_best_multisweep_bias(bias_multisweep: dict) -> tuple:
    """
    Function to extract the bias amplitude that the hidfmux software has determined as the best 
    bias amplitude to use (closest to bifurcation) as an array. Also the corresponding tone
    frequencies and an array of tone names that did not KID bifurcated within the bias power sweep.  

    :param bias_multisweep: Resulting dictionary from a bias multisweep.

    Returns: bias_amplitude_array, bias_frequency, no_bifurcation
    """

    tone_name_array = bias_multisweep["info"]["name_list"]
    bias_amplitude_array = np.empty_like(tone_name_array, dtype=float)
    bias_frequency = np.empty_like(tone_name_array, dtype=float)
    no_bifurcation = []

    for count, tone_name in enumerate(tone_name_array):
        bias_amplitude_array[count] = bias_multisweep["bias_info"][tone_name]["onres"]["bias_amplitude"]
        bias_frequency[count] = bias_multisweep["bias_info"][tone_name]["onres"]["bias_freq"]
        if bias_multisweep["bias_info"][tone_name]["onres"]["bifurc"] == None:
            no_bifurcation.append(tone_name)

    return bias_amplitude_array, bias_frequency, no_bifurcation


def get_best_bias_data_indices(bias_multisweep: np.ndarray, best_bias_amplitudes: np.ndarray) -> np.ndarray:
    """
    Function that finds the index of the bias sweep which uses the best bias amplitude for each
    detector.

    :param bias_multisweep: Resulting dictionary from a bias multisweep.
    :param best_bias_amplitudes: Array of the bias amplitudes that were identified as the closest to
    bifurcation.

    Returns: Array of indices.
    """

    tone_name_array = bias_multisweep["info"]["name_list"]
    best_bias_indices = np.empty_like(tone_name_array, dtype=int)

    for count, tone_name in enumerate(tone_name_array):
        for index in range(len(bias_multisweep["data"][tone_name])):
            if bias_multisweep["data"][tone_name][index]["amp"] == best_bias_amplitudes[count]:
                best_bias_indices[count] = index

    return best_bias_indices


def rotate_iq_arrays(i_array: np.ndarray, q_array: np.ndarray, angle: float) -> tuple:
    """
    Function to rotate IQ arrays by a provided angle.
    """

    rotated_iq = (i_array + 1.j*q_array) * np.exp(1.j*angle)

    return np.real(rotated_iq), np.imag(rotated_iq)

def remove_internal_phase_shift_noise_data(
    i_array: np.ndarray,
    q_array: np.ndarray,
    tone_frequency: float,
    cal_iq_at_bias_freq=None,
    sweep_f_array=None,
    cal_iq_array=None,
) -> tuple:
    """
    Calculate and remove the phase shift incurred by latency within the FPGA, by comparing the calibrated, 'cal'
    phase at the demodulator to the digitized ('adc') phase at the demodulator.

    :param i_array: Un-corrected in-phase values from timestream.
    :param q_array: Un-corrected quadrature values from timestream.
    :param tone_frequency: Frequency that the corresponding tone was set to.
    :param cal_iq_at_bias_freq: If known, the value of the cal_iq at the tone frequency.
    :param sweep_f_array: Frequency values from the detector's corresponding sweep.
    :param cal_iq_array: "cal_iq" array from the detector's corresponding sweep.
    """
    if cal_iq_at_bias_freq is not None:
        phi_cal = np.angle(cal_iq_at_bias_freq)
    else:
        interp_cal_phase = interpolate.CubicSpline(sweep_f_array, cal_iq_array)
        phi_cal = np.angle(interp_cal_phase(tone_frequency))

    iq = i_array + 1.0j * q_array

    latency = hidfmux_transfer_functions.LATENCY
    latency_adjustment = np.pi * (
        1.0 - (2 * latency) * (tone_frequency % (1.0 / latency))
    )
    phi_adc = np.asarray([phi_raw - phi_cal for phi_raw in np.angle(iq)])
    i_corrected = abs(iq) * np.cos(phi_adc - latency_adjustment)
    q_corrected = abs(iq) * np.sin(phi_adc - latency_adjustment)

    return np.asarray(i_corrected), np.asarray(q_corrected)


def create_hidfmux_timestream_from_ledgerman(
        timestream_data: Dataset,
        tones_list: np.ndarray,
        tone_frequency_array: np.ndarray,
        multisweep: HidfmuxMultiSweep
) -> HidfmuxTimestream:
    """
    Function to extract the necessary data from a ledgerman timestream data file containing timestreams for
    multiple kids that correspond to the tone names in tones_list and tone_frequency_array as well as a
    multisweep data object containing the multisweeps for each tone to calibrate the I and Q values. 

    :param 
    
    """

    timestreams_array = np.empty(
        shape=tones_list.shape, dtype=object
    )
    bias_power_array = np.empty(
        shape=tones_list.shape, dtype=float,
    )
    sweep_data_array = np.empty(
        shape=tones_list.shape, dtype=object
    )

    for count, tone in enumerate(tones_list):
        kid_i_raw = timestream_data.variables[tone+'_I'][:]
        kid_q_raw = timestream_data.variables[tone+'_Q'][:]

        tone_bias_power = multisweep.get_tone_bias_power(
            tone=tone,
            db=False
        )
        # Get tone calibration sweep data to calibrate timestream IQ
        sweep_data = multisweep.get_tone_sweep_data(
            tone=tone
        )
        calibration_iq_array = multisweep.get_tone_calibration_iq_data(
            tone=tone
        )

        time_array = np.array(timestream_data.variables["Time"][:])

        tone_frequency = tone_frequency_array[count]

        (
            timestream_i,
            timestream_q,
        ) = hidfmux_data_tools.remove_internal_phase_shift_noise_data(
            i_array=kid_i_raw,
            q_array=kid_q_raw,
            tone_frequency=tone_frequency,
            cal_iq_at_bias_freq=None,
            sweep_f_array=sweep_data.frequency_array,
            cal_iq_array=calibration_iq_array,
        )

        timestream = TimestreamData(
            n_timestreams=1,
            sample_rate=hidfmux_transfer_functions.DEMOD_SAMPLING_FREQ,
            measurement_length=time_array[-1] - time_array[0],
            tone_frequency=tone_frequency,
            timestream_i_data=timestream_i,
            timestream_q_data=timestream_q,
            timestream_time_data=time_array,
        )

        timestreams_array[count] = timestream
        bias_power_array[count] = tone_bias_power
        sweep_data_array[count] = sweep_data
        
        print(f"Tone {tone} Processed ({count}/{tones_list.size - 1})")
    

    return HidfmuxTimestream(
        bias_power_array=bias_power_array,
        tone_name_array=tones_list,
        sweep_data_array=sweep_data_array,
        on_res_timestreams_array=timestreams_array,
        off_res_timestreams_array=timestreams_array,
    )


def cic2_response(f_arr, fs):
    '''
    Compute the response of the second-stage cascaded-integrator 
    comb (CIC) filter, as a function of frequency. This filter 
    defines the shape of each channel's frequency response, so
    the frequencies here are relative to the central frequency
    of the channel.


    Paramters
    ---------

    f_arr : list or array of frequencies within one channel bw.
        Must be less than the demodulated channel sampling frequency.
        
    '''

    

    f_arr = np.asarray(f_arr)

    if fs < 500:
        cic_factor = 6
    else:
        cic_factor = 5
    
    return np.sinc(f_arr / fs)**cic_factor


def apply_cic2_comp_psd(f_arr, psd, fs, trim=0.15):
    '''
    NOTE it is not completely correct to apply this correction to
    power data, as any random scatter in the voltage data will
    be forced positive, which may slightly skew the results!
    
    Compensate for the response of the second-stage cascaded-
    integrator comb (CIC) filter as a function of frequency in
    a power spectral density measurement.

    Because the channel response drops off quickly towards and 
    above Nyquist, results will be less trustworthy at high
    frequencies, and so this trims the last trim-percent of the 
    datapoints at high frequency.

    f_arr : list or array of frequencies within one channel bw.
        Must be less than the demodulated channel sampling frequency.
            
    psd : list or array of amplitude spectral density values as
        a function of frequency

    fs : int or float
        Sampling rate of the demodulated timestream. 

    trim : float
        Percent length of array to trim off at high frequencies,
        to avoid returning data where numerical uncertainties
        are higher in the compensation function.
        
    '''
    compensated = np.asarray(psd) / (cic2_response(np.asarray(f_arr), fs))**2

    trimind = int(np.ceil(trim * len(compensated)))
    return f_arr[:-trimind], compensated[:-trimind]


def apply_pfb_correction(pfb_samples, nco_freq, channel_freq, binlim=0.6e6, trim=True):
    '''
    Remove the spectral droop of the polyphase filterbank's window function from timestream
    data acquired by routing the samples directly out of the PFB (the 2 MSPS capture mode).

    The purpose and mechanism of this function are explained in more detail in the docs and
    in a memo + ipynb available from maclean.

    Parameters:
    -----------

    pfb_samples : arraylike
        the complex timestream data (array of time-ordered I+jQ samples)

    nco_freq : float
        numerically controlled oscillator frequency setting at which the data was 
        acquired

    channel_freq : float
        readout channel frequency setting at which the data was acquired. Expected to 
        be in real-world frequency units (ie, between 0-3GHz, rather than within -+250MHz 
        relative to the NCO)

    binlim : float, default 0.6e6
        frequency range within a PFB bin to keep data from. The bin's filter window quickly
        goes to very low gain, so we limit the region of data to keep to a range where the
        signal is not too far suppressed. Otherwise, at the extremes of the window, when we 
        remove the droop we are only amplifying noise.

    trim : bool, default True
        whether or not to trim the un-drooped output array to be centred on zero. Optional
        but makes downstream processing much simpler.

    '''

    fs = 500e6 / 256

    NFFT = 1024  # Synthetic FFT length. Actual cores are 1/4 this size.
    NTAPS = 4096  # Group delay is ~2 us. This is small enough to not tweak.

    w = signal.chebwin(NTAPS, 103) # the window function applied by the PFB
    W = np.fft.fft(w, 262144) # 2^18

    W /= np.abs(W[0]) # normalized filter response in frequency space
    W = np.fft.fftshift(W) # roll W so the zero bin is in the centre

    # now design a frequency array to go with W
    pfb_freqs = np.linspace(-500e6, 500e6, len(W))

    # interpolate the response 
    pfb_func = interpolate.interp1d(pfb_freqs, W)

    # FFT the complex timestream so we can apply the corrections
    fftfreqs = np.fft.fftshift(np.fft.fftfreq(len(pfb_samples), d=1./fs))
    fft = 2*np.fft.fftshift(np.fft.fft(np.hanning(len(pfb_samples))*pfb_samples)) / len(pfb_samples)

    #### temporary correction factor: ALL PFB PSDs ARE 1.2x LARGER THAN LOWFS ###
    fft = fft/1.2
    #print('correcting FFT by /1.2')


    

    # determine which bin this frequency is in and how far it is from the centre
    bin_centres = [x * 500e6/512 for x in range(-256, 256)]
    channel_freq_in_nco_bw = channel_freq - nco_freq
    b = abs(np.asarray(bin_centres) - channel_freq_in_nco_bw).argmin()
    this_bin_centre = bin_centres[b]
    channel_freq_in_bin = channel_freq_in_nco_bw - this_bin_centre # tone frequency relative to bin centre
    fftfreqs_in_bin = fftfreqs + channel_freq_in_bin # the FFT frequency array relative to bin centre

    # compute the scalar gain factor at this frequency
    builtin_gain_factor = pfb_func(channel_freq_in_bin)
    # remove the scalar gain factor from the FFT'd data
    fft = fft * builtin_gain_factor

    # prepare the droop correction:

    # the correction function will be centered around zero (the bin centre)
    # to avoid over-amplifying noise by un-drooping the extremes of the window's wings,
    # limit the correction region to a range of frequencies (+- binlim) close-ish to zero
    # THIS IS CURRENTLY AN ARBITRARY CHOICE AND MAY NEED TO BE CHOSEN MORE CAREFULLY IN FUTURE
    freqrange_to_correct = fftfreqs_in_bin[np.where((fftfreqs_in_bin > -binlim) & (fftfreqs_in_bin < binlim))[0]]
    specdata_to_correct = fft[np.where((fftfreqs_in_bin > -binlim) & (fftfreqs_in_bin < binlim))[0]]

    # apply the droop correction
    specdata_corrected = specdata_to_correct / pfb_func(freqrange_to_correct)

    # shift the spectrum back to its real-life frequency range:
    output_fftfreqs = freqrange_to_correct - channel_freq_in_bin

    # apply the FFT normalization to the corrected data
    #specdata_corrected = specdata_corrected / len(fft)
    # NOTE that we still need to compensate for the window function! 

    # trim the output arrays to be centred on zero
    # this is optional but reduces headaches later in the processing
    if trim:
        zerof_ind = abs(output_fftfreqs).argmin()
        shortest_range = min([zerof_ind, len(output_fftfreqs)-zerof_ind])
        specdata_corrected = np.asarray(specdata_corrected[zerof_ind-shortest_range:zerof_ind+shortest_range])
        output_fftfreqs = np.asarray(output_fftfreqs[zerof_ind-shortest_range:zerof_ind+shortest_range])

    return output_fftfreqs, specdata_corrected, builtin_gain_factor, len(pfb_samples)
        

