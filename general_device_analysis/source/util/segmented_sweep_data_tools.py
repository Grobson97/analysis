import numpy as np
import matplotlib.pyplot as plt
import math
import os
from matplotlib.pyplot import Axes
from source.models.resonator_measurement.sweep_data import SweepData
from source.models.resonator_measurement.measurement_header import ResonatorMeasurementHeader
from source.services import npz_load_service
from source.util import lekid_analysis_tools
from source.util import data_array_analysis_tools

from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.axes import Axes


def get_inset_axes(axis: Axes):
    """
    Function to get the inset axes from a matplotlib axis
    """
    return [c for c in axis.get_children()
            if isinstance(c, Axes)]


def plot_segmented_sweep(
    tone_names_array: np.array,
    sweep_data: SweepData,
    label: str="",
    save_figure: bool=False,
    axes: Axes = None,
    good_kid_list = None
) -> None:
    """
    Function to plot the segmented sweep results.

    :param tone_names_array: 1D array of tone names that correspond to each subarray of complex data.
    :param complex_arrays: 2D array, consisting of N arrays of complex data where N=number of tones x repeats.
    :param frequency_arrays: 2D array, consisting of N arrays of frequency data where N=number of tones x repeats.
    :param label: The label to give to the data series to add to the overall legend. i.e. the attenuation.
    :param save_figure: boolean to save the figure or not. Plot will be saved to the plots folder
    in the top level directory under the DUT.
    :param good_kid_list: Boolean list, if element is True, subplot label is green, if false then red. If None, subplot titles are black.
    :param axes: Matplotlib Axes object to plot the results to. If None, a new figure will be
    created. Axes must be defined as follows with inset axes:

    number_of_plots = tone_names_array.size
    n_rows = math.ceil(number_of_plots / 4)
    n_cols = 4

    total_plots = n_rows * n_cols
    figure, axes = plt.subplots(
        n_rows,
        n_cols,
        sharex=False,
        sharey=False,
        figsize=(14, 3 * math.ceil(number_of_plots / 4)),
    )

    for row in axes:
        for axis in row:
            axis.inset_axes([.6,.6,.35,.35])

    """
    
    number_of_plots = tone_names_array.size
    if axes is None:
        n_rows = math.ceil(number_of_plots / 4)
        n_cols = 4

        figure, axes = plt.subplots(
            n_rows,
            n_cols,
            sharex=False,
            sharey=False,
            figsize=(14, 3 * math.ceil(number_of_plots / 4)),
        )
        if number_of_plots < 5:
            axes = np.array([axes])
        for row in axes:
            for axis in row:
                axis.inset_axes([.6,.6,.35,.35])

    total_plots = axes.size
    # Plot each tone on own subplots:
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    mean_i = sweep_data.i.mean(axis=0)
    mean_q = sweep_data.q.mean(axis=0)

    magnitude_db = 20 * np.log10(np.abs(mean_i + 1j * mean_q))
    frequency_array = sweep_data.frequency_array[0]

    tone_title_color = "k"

    for count, tone_name in enumerate(tone_names_array):

        inset_axis = get_inset_axes(axes[m][n])[0]

        if label == "tone_names":
            label = f"{sweep_data.measurement_header.power} /{count}"

        axes[m][n].plot(
            frequency_array[count] * 1e-6,
            magnitude_db[count],
            label=label
        )

        # plot IQ circle on inset axis:
        inset_axis.plot(mean_i[count], mean_q[count],
        )

        inset_axis.set_ylabel("Q")
        inset_axis.set_xlabel("I")
        inset_axis.axis("equal")

        if good_kid_list is not None:
            if good_kid_list[count]:
                tone_title_color = "g"
            if not good_kid_list[count]:
                tone_title_color = "r"

        axes[m][n].set_title(tone_name, color=tone_title_color)

        if n == 0:
            axes[m][n].set_ylabel("S21 (dB)")
        if m == math.ceil(number_of_plots / 4) - 1:
            axes[m][n].set_xlabel("Frequency (MHz)")

        n += 1
        if n == 4:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    try:
        remaining_subplots = total_plots - number_of_plots
        if remaining_subplots > 0:
            for remaining_column in range(remaining_subplots):
                axes[m][n + remaining_column].remove()
    except:
        print("No axes to remove")

    handles, labels = axes[0][0].get_legend_handles_labels()
    figure = plt.gcf()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="upper center",
        ncol=4,
    )
    plt.tight_layout(rect=(0, 0, 1, 0.95))

    if save_figure:
        plt.savefig(
            f"figures/{sweep_data.measurement_header.dut}_segmented_sweep_{int(sweep_data.measurement_header.actual_temperature * 1000)}mK_{sweep_data.measurement_header.power}dB.png"
        )

    return None


def get_inset_axes(axis: Axes):
    """
    Function to get the inset axes from a matplotlib axis
    """
    return [c for c in axis.get_children()
            if isinstance(c, Axes)]



def plot_and_fit_segmented_sweep(
    tone_names_array: np.array,
    sweep_data: SweepData,
    save_figure: bool=False,
    axes: Axes = None,
) -> tuple:
    """
    Function to plot the segmented sweep and fit to each resonance.

    Returns arrays of f0, qr, qc, and qi.

    :param tone_names_array: 1D array of tone names that correspond to each subarray of complex data.
    :param complex_arrays: 2D array, consisting of N arrays of complex data where N=number of tones x repeats.
    :param frequency_arrays: 2D array, consisting of N arrays of frequency data where N=number of tones x repeats.
    :param label: The label to give to the data series to add to the overall legend. i.e. the attenuation.
    :param save_figure: boolean to save the figure or not. Plot will be saved to the plots folder
    in the top level directory under the DUT.
    :param axes: Matplotlib Axes object to plot the results to. If None, a new figure will be
    created. Axes must be defined as follows with inset axes:

    number_of_plots = tone_names_array.size
    n_rows = math.ceil(number_of_plots / 4)
    n_cols = 4

    total_plots = n_rows * n_cols
    figure, axes = plt.subplots(
        n_rows,
        n_cols,
        sharex=False,
        sharey=False,
        figsize=(14, 3 * math.ceil(number_of_plots / 4)),
    )

    """
    
    number_of_plots = tone_names_array.size
    if axes is None:
        n_rows = math.ceil(number_of_plots / 4)
        n_cols = 4

        figure, axes = plt.subplots(
            n_rows,
            n_cols,
            sharex=False,
            sharey=False,
            figsize=(14, 3 * math.ceil(number_of_plots / 4)),
        )

    total_plots = axes.size
    if tone_names_array.size < 4:
        axes=[axes]
    # Plot each tone on own subplots:
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    shape = sweep_data.frequency_array.shape[0], sweep_data.frequency_array.shape[1], 2
    f0_array = np.empty(shape=shape)
    qr_array = np.empty(shape=shape)
    qc_array = np.empty(shape=shape)
    qi_array = np.empty(shape=shape)

    for repeat, frequency_array in enumerate(sweep_data.frequency_array):

        current_i = sweep_data.i[repeat]
        current_q = sweep_data.q[repeat]
        current_complex_iq = current_i + 1j * current_q

        for count, tone_name in enumerate(tone_names_array):
            
            try:
                fit_result_dict, fit = lekid_analysis_tools.fit_non_linear_lorentzian(
                    frequency_array=frequency_array[count],
                    data_array=current_complex_iq[count],
                    plot_graph=False,
                    plot_db=False,
                    plot_title=f"KID {count}",
                )

            except:
                fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
                    frequency_array=frequency_array[count],
                    data_array=np.abs(current_complex_iq[count]),
                    qc_guess=5e4,
                    qi_guess=1e5,
                    f0_guess=None,
                    fit_fraction=0.98,
                    plot_graph=False,
                    normalise=False,
                    plot_db=True,
                    save_figure=False,
                    save_name="",
                    plot_title=f"KID {count}",
                )
                fit = lekid_analysis_tools.skewed_lorentzian(
                    frequency_array=frequency_array[count],
                    f0=fit_result_dict["f0"][0],
                    qi=fit_result_dict["qi"][0],
                    qc_real=fit_result_dict["qc_real"][0],
                    qc_imag=fit_result_dict["qc_imag"][0],
                    amp=fit_result_dict["amp"][0]
                )
                fit_result_dict["anl"] = [np.nan, np.nan]

            f0_array[repeat][count] = np.asarray(fit_result_dict["f0"])
            qr_array[repeat][count] = np.asarray(fit_result_dict["qr"])
            qc_array[repeat][count] = np.asarray(fit_result_dict["qc_real"])
            qi_array[repeat][count] = np.asarray(fit_result_dict["qi"])

            if repeat == 0:
                axes[m][n].plot(
                    frequency_array[count] * 1e-6,
                    20 * np.log10(np.abs(current_complex_iq[count])),
                    linestyle="none",
                    marker="o",
                    color="tab:blue",
                    markersize="1",
                    label=f"Data"
                )

                axes[m][n].plot(
                    frequency_array[count] * 1e-6,
                    20 * np.log10(np.abs(fit)),
                    linewidth=2,
                    linestyle="--",
                    color="k",
                    label="Fit"
                )

                if fit_result_dict["qi"][0] > 1e6:
                    qi = fit_result_dict["qi"][0]
                    print(f"{tone_name}: Qi = {qi:.2E}")

                axes[m][n].set_title(tone_name)
                handles, labels = axes[m][n].get_legend_handles_labels()

                axes[m][n].legend(
                    handles=handles,
                    labels=["Data", f"Fit:\nQr={fit_result_dict['qr'][0]:.2E}"
                    f"\nQc={fit_result_dict['qc_real'][0]:.2E}"
                    f"\nQi={fit_result_dict['qi'][0]:.2E}"
                    f"\nanl={fit_result_dict['anl'][0]:.2f}"],
                    loc="upper right",
                    ncol=1,
                )

                if n == 0:
                    axes[m][n].set_ylabel("S21 (dB)")
                if m == math.ceil(number_of_plots / 4) - 1:
                    axes[m][n].set_xlabel("Frequency (MHz)")

                n += 1
                if n == 4:
                    m += 1  # Move to next row
                    n = 0  # Reset to first column

    try:
        remaining_subplots = total_plots - number_of_plots
        if remaining_subplots > 0:
            for remaining_column in range(remaining_subplots):
                axes[m][n + remaining_column].remove()
    except:
        print("No axes to remove")

    figure = plt.gcf()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="upper center",
        ncol=4,
    )
    plt.tight_layout(rect=(0, 0, 1, 0.95))

    if repeat > 0:
        f0_array = np.stack([f0_array.mean(axis=0)[:, 0], f0_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
        qr_array = np.stack([qr_array.mean(axis=0)[:, 0], qr_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
        qc_array = np.stack([qc_array.mean(axis=0)[:, 0], qc_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
        qi_array = np.stack([qi_array.mean(axis=0)[:, 0], qi_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)

    else: 
        f0_array = f0_array[0]
        qr_array = qr_array[0]
        qc_array = qc_array[0]
        qi_array = qi_array[0]


    if save_figure:
        plt.savefig(
            f"figures/{sweep_data.measurement_header.dut}_segmented_sweep_fits_{int(sweep_data.measurement_header.actual_temperature * 1000)}mK_{sweep_data.measurement_header.power}dB.png"
        )

    return f0_array, qr_array, qc_array, qi_array



def plot_q_histogram(
        measurement_header: ResonatorMeasurementHeader,
        qr_array: np.ndarray,
        qc_array: np.ndarray,
        qi_array: np.ndarray,
        n_bins: int,
        title: str="",
        save_figure: bool=False,
        axes: Axes=None,
    ):
    """
    Function to plot subplot histograms of the Qr, Qc and Qi distributions from given arrays split
    into n_bins bins.

    :param measurement_header: Measurement header for current set of data.
    :param qr_array: Array of Qr values.
    :param qc_array: Array of Qc values.
    :param qi_array: Array of Qi values.
    :param n_bins: Number of bins to split Q's into.
    :param save_figure: Boolean to save the figure.
    """

    qr_mask_min_bound = 1e2,
    qr_mask_max_bound =  1e6,
    qc_mask_min_bound = 1e2,
    qc_mask_max_bound =  1e6,
    qi_mask_min_bound = 1e2,
    qi_mask_max_bound =  1e6,

    # Plot histogram of Q distribution:

    # Create mask to remove data from averaging calculations based on invalid Q values.
    # Mask each qr, qc, qi array:
    mask = data_array_analysis_tools.make_inequality_data_mask(
        qr_array, min_bound=qr_mask_min_bound, max_bound=qr_mask_max_bound, mask=None
    )
    mask = data_array_analysis_tools.make_inequality_data_mask(
        qc_array, min_bound=qc_mask_min_bound, max_bound=qc_mask_max_bound, mask=mask
    )
    mask = data_array_analysis_tools.make_inequality_data_mask(
        qi_array, min_bound=qi_mask_min_bound, max_bound=qi_mask_max_bound, mask=mask
    )
    mean_qr = np.mean(qr_array[mask])
    mean_qc = np.mean(qc_array[mask])
    mean_qi = np.mean(qi_array[mask])

    if axes is None:
        figure, axes = plt.subplots(nrows=1, ncols=3, figsize=(10, 4))
    _, qr_bins = np.histogram(np.log10(qr_array + 1), bins=n_bins)
    _, qi_bins = np.histogram(np.log10(qi_array + 1), bins=n_bins)
    _, qc_bins = np.histogram(np.log10(qc_array + 1), bins=n_bins)
    axes[0].hist(qr_array, bins=10**qr_bins, histtype='step', linewidth=4, color="tab:blue")
    axes[1].hist(qc_array, bins=10**qc_bins, histtype='step', linewidth=4, color="tab:orange")
    axes[2].hist(qi_array, bins=10**qi_bins, histtype='step', linewidth=4, color="tab:green")

    axes[0].vlines(mean_qr, ymin=0, ymax=axes[0].get_ylim()[1], linestyle="--", color="k", label=f"mean={mean_qr:.2E}\n$\sigma$={np.std(qr_array[mask]):.2E}")
    axes[1].vlines(mean_qc, ymin=0, ymax=axes[1].get_ylim()[1], linestyle="--", color="k", label=f"mean={mean_qc:.2E}\n$\sigma$={np.std(qc_array[mask]):.2E}")
    axes[2].vlines(mean_qi, ymin=0, ymax=axes[2].get_ylim()[1], linestyle="--", color="k", label=f"mean={mean_qi:.2E}\n$\sigma$={np.std(qi_array[mask]):.2E}")

    axes[0].set_xlabel("Qr", fontsize=14)
    axes[0].set_ylabel("Count", fontsize=14)
    axes[0].set_xscale("log")

    axes[1].set_xlabel("Qc", fontsize=14)
    axes[1].set_xscale("log")

    axes[2].set_xlabel("Qi", fontsize=14)
    axes[2].set_xscale("log")

    if title == "":
        title = f"Q factor histograms for {measurement_header.dut} at T={int(measurement_header.actual_temperature * 1000)}mK, {measurement_header.power}dB"

    for axis in axes:
        axis.tick_params(axis='both', labelsize=12)
        axis.legend()

    plt.suptitle(title)
    plt.tight_layout(rect=(0, 0, 1, 0.95))

    if save_figure:
        plt.savefig(
            f"figures/{measurement_header.dut}_q_histograms_{int(measurement_header.actual_temperature * 1000)}mK_{measurement_header.power}dB.png"
        )

    print(f"Mean Qr: {mean_qr:.2E} +/- {np.std(qr_array[mask]):.2E}")
    print(f"Mean Qc: {mean_qc:.2E} +/- {np.std(qc_array[mask]):.2E}")
    print(f"Mean Qi: {mean_qi:.2E} +/- {np.std(qi_array[mask]):.2E}")
    print(f"Data points removed by mask: {qr_array.size - qr_array[mask].size}")

    return None


def fit_multiple_segmented_sweeps(
        directory: str,
        plot_fits: bool=False,
        save_fits: bool=False     
) -> dict:
        """
        Function to sweep through the segmented sweep files in a directory and fit lorentzians to all the resonators
        to extract f0, qr, qc, qi and return a dictionary containing arrays for the f0, qr, qc, qi, attenuation and temperature.

        :param directory: Directory containing segmented sweep files which contain the word "segmented" in the file name.
        :param plot_fits: Optional boolean to plot the fits for each individual detector. WARNING: This will take a long time
        for lots of resonators.
        """

        result_dict = {
                "tone_names": [],
                "temperature": [],
                "power": [],
                "f0": [],
                "qr": [],
                "qc": [],
                "qi": [],
                "anl": []
        }

        for file_index, file_name in enumerate(os.listdir(directory)):
                
            file_path = os.path.join(directory, file_name)

            if ("segmented" in file_name.lower()) and (".npz" in file_name):

                print(f"Fitting {file_name}...") 

                tone_names, tone_frequencies, sweep_data = npz_load_service.load_segmented_sweep(file_path)
                
                result_dict["tone_names"].append(tone_names)
                
                # Pull the temperature and power from sweep data header:
                temperatures = np.full_like(
                        a=tone_frequencies,
                        fill_value=sweep_data.measurement_header.actual_temperature
                )
                temperatures = np.round(temperatures, decimals=3)
                powers = np.full_like(
                        a=tone_frequencies,
                        fill_value=sweep_data.measurement_header.power
                )
                result_dict["temperature"].append(temperatures)
                result_dict["power"].append(powers)

                shape = sweep_data.frequency_array.shape[0], sweep_data.frequency_array.shape[1], 2
                f0_array = np.empty(shape=shape)
                qr_array = np.empty(shape=shape)
                qc_array = np.empty(shape=shape)
                qi_array = np.empty(shape=shape)
                anl_array = np.empty(shape=shape)

                for repeat, frequency_array in enumerate(sweep_data.frequency_array):

                    current_i = sweep_data.i[repeat]
                    current_q = sweep_data.q[repeat]
                    current_complex_iq = current_i + 1j * current_q

                    for count, tone_name in enumerate(tone_names):

                        if save_fits:
                            tone_directory = os.path.join("figures", tone_name, "vna_fits")
                            if not os.path.exists(tone_directory):
                                os.makedirs(tone_directory)

                        try:
                            print(1/0)
                            fit_result_dict, fit = lekid_analysis_tools.fit_non_linear_lorentzian(
                                frequency_array=frequency_array[count],
                                data_array=current_complex_iq[count],
                                plot_graph=plot_fits,
                                plot_db=True,
                                save_figure=save_fits,
                                save_name=os.path.join("figures", tone_name, "vna_fits", f"resonance_fit_{temperatures[0]*1000}mK_{powers[0]}dB_{repeat}.png"),
                                plot_title=f"{tone_name}: {temperatures[0]*1000}mK, {powers[0]}dB, repeat:{repeat}",
                            )
                            anl_array[repeat][count] = np.asarray(fit_result_dict["anl"])
                            
                        except:
                            print(f"Non linear fit failed for {file_name}:{tone_name}.")
                            fit_result_dict = lekid_analysis_tools.fit_skewed_lorentzian(
                                frequency_array=frequency_array[count],
                                data_array=np.abs(current_complex_iq[count]),
                                qc_guess=5e4,
                                qi_guess=1e5,
                                f0_guess=None,
                                fit_fraction=0.98,
                                plot_graph=plot_fits,
                                normalise=False,
                                plot_db=True,
                                save_figure=save_fits,
                                save_name=os.path.join("figures", tone_name, "vna_fits", f"resonance_fit_{temperatures[0]*1000}mK_{powers[0]}dB_{repeat}.png"),
                                plot_title=f"{tone_name}: {temperatures[0]*1000}mK, {powers[0]}dB, repeat:{repeat}",
                            )
                            anl_array[repeat][count] = np.asarray([None, None])

                        f0_array[repeat][count] = np.asarray(fit_result_dict["f0"])
                        qr_array[repeat][count] = np.asarray(fit_result_dict["qr"])
                        qc_array[repeat][count] = np.asarray(fit_result_dict["qc_real"])
                        qi_array[repeat][count] = np.asarray(fit_result_dict["qi"])
                        

                # Take mean and standard error of fit results.
                if repeat > 0:
                    f0_array = np.stack([f0_array.mean(axis=0)[:, 0], f0_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
                    qr_array = np.stack([qr_array.mean(axis=0)[:, 0], qr_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
                    qc_array = np.stack([qc_array.mean(axis=0)[:, 0], qc_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
                    qi_array = np.stack([qi_array.mean(axis=0)[:, 0], qi_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)
                    anl_array = np.stack([anl_array.mean(axis=0)[:, 0], anl_array.std(axis=0)[:, 0]/np.sqrt(repeat + 1)], axis=1)

                else: 
                    f0_array = f0_array[0]
                    qr_array = qr_array[0]
                    qc_array = qc_array[0]
                    qi_array = qi_array[0]
                    anl_array = anl_array[0]
            else:
                continue

            result_dict["f0"].append(f0_array)
            result_dict["qr"].append(qr_array)
            result_dict["qc"].append(qc_array)
            result_dict["qi"].append(qi_array)
            result_dict["anl"].append(anl_array)
                        
            print(f"File {file_index} complete.")

        for key in result_dict.keys():
                result_dict[key] = np.array(result_dict[key])


        result_dict["f0"] = result_dict["f0"].reshape(result_dict["f0"].shape[0] * result_dict["f0"].shape[1], 2)
        result_dict["qr"] = result_dict["qr"].reshape(result_dict["qr"].shape[0] * result_dict["qr"].shape[1], 2)
        result_dict["qc"] = result_dict["qc"].reshape(result_dict["qc"].shape[0] * result_dict["qc"].shape[1], 2)
        result_dict["qi"] = result_dict["qi"].reshape(result_dict["qi"].shape[0] * result_dict["qi"].shape[1], 2)
        result_dict["anl"] = result_dict["anl"].reshape(result_dict["anl"].shape[0] * result_dict["anl"].shape[1], 2)

        result_dict["tone_names"] = result_dict["tone_names"].flatten()
        result_dict["power"] = result_dict["power"].flatten()
        result_dict["temperature"] = result_dict["temperature"].flatten()

        temperature_order = np.argsort(result_dict["temperature"])

        for key in result_dict.keys():
            result_dict[key] = result_dict[key][temperature_order]

        return result_dict 

