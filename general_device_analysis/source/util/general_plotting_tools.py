import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.axes import Axes
from matplotlib import gridspec
import math



def plot_multi_setting_fit_results(
    temperature_array: np.ndarray,
    power_array: np.ndarray,
    tone_names_array: np.ndarray,
    f0_array: np.ndarray,
    qc_array: np.ndarray,
    qi_array: np.ndarray,
    anl_array: np.ndarray=None,
):
    """
    Function to take corresponding arrays and plot the f0, qc, qi as functions of power and
    temperature for each KID on subplots.

    Note, arrays for f0, Qc, Qi should have shapes [tone_names * number of files, 2], where the
    each value in the first axis is the value and the error.
    e.g. f0_array = np.array([value_1, error_1], [value_2, error_2], [value_2, error_2])

    :param temperature_array: Array of temperatures in Kelvin.
    :param power_array: Array of powers/attenuation in dB.
    :param tone_names_array: Array of tone names.
    :param f0_array: array of f0's.
    :param qc_array: array of Qc's.
    :param qi_array: array of Qi's.
    :param anl_array: Array of non linearity coefficients. Can be set to None.
    """
    
    # Find unique temperatures and powers:
    temperature_array = np.round(temperature_array, decimals=3) # Round to nearest mK
    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(power_array)

    temperature_map = plt.get_cmap('plasma', unique_temperatures.size)
    power_map = plt.get_cmap('viridis', unique_powers.size)

    unique_tone_names = np.unique(tone_names_array)
    number_of_tones = unique_tone_names.size

    # Plot parameters as a function of power:
    n_plots = 3
    if anl_array is not None:
        n_plots = 4
    figure, axes = plt.subplots(
        number_of_tones, n_plots, sharex=True, sharey=False, figsize=(12, number_of_tones * n_plots)
    )
    axes[0][0].set_title("F0 (GHz)")
    axes[0][1].set_title("Qc (x10$^4$)")
    axes[0][2].set_title("Qi (x10$^4$)")
    if anl_array is not None:
        axes[0][3].set_title("Non-linear Coefficient")

    for tone_count, tone_name in enumerate(unique_tone_names):

        # Get indices for current tone:
        tone_indices = np.where(tone_names_array == tone_name)
        
        for temperature in unique_temperatures:
            
            # Get indices of current temperature within current tone subset:
            temperature_indices = np.where(temperature_array[tone_indices] == temperature)

            temperature_position = np.where(np.sort(unique_temperatures) == temperature)[0]

            sorted_series_indices = np.argsort(power_array[tone_indices][temperature_indices])
            power_series = power_array[tone_indices][temperature_indices][sorted_series_indices]

            axes[tone_count][0].set_ylabel(f"{tone_name}")

            axes[tone_count][0].errorbar(
                power_series,
                f0_array[tone_indices][temperature_indices][:, 0][sorted_series_indices] * 1e-9,
                yerr=f0_array[tone_indices][temperature_indices][:, 1][sorted_series_indices] * 1e-9,
                linestyle="--",
                marker="o",
                color=temperature_map(temperature_position),
                label=f"{int(temperature*1000)}mK",
            )


            axes[tone_count][1].errorbar(
                power_series,
                qc_array[tone_indices][temperature_indices][:, 0][sorted_series_indices] * 1e-4,
                yerr=qc_array[tone_indices][temperature_indices][:, 1][sorted_series_indices] * 1e-4,
                linestyle="--",
                marker="o",
                color=temperature_map(temperature_position),
                label=f"{int(temperature*1000)}mK",
            )
            # axes[tone_count][1].set_yscale("log")

            axes[tone_count][2].errorbar(
                power_series,
                qi_array[tone_indices][temperature_indices][:, 0][sorted_series_indices] * 1e-4,
                yerr=qi_array[tone_indices][temperature_indices][:, 1][sorted_series_indices] * 1e-4,
                linestyle="--",
                marker="o",
                color=temperature_map(temperature_position),
                label=f"{int(temperature*1000)}mK",
            )
            # axes[tone_count][2].set_yscale("log")

            if anl_array is not None:
                axes[tone_count][3].errorbar(
                    power_series,
                    anl_array[tone_indices][temperature_indices][:, 0][sorted_series_indices],
                    yerr=anl_array[tone_indices][temperature_indices][:, 1][sorted_series_indices],
                    linestyle="--",
                    marker="o",
                    color=temperature_map(temperature_position),
                    label=f"{int(temperature*1000)}mK",
                )



    # Set last x labels to Power.
    axes[tone_count][0].set_xlabel("Power (dB)")
    axes[tone_count][1].set_xlabel("Power (dB)")
    axes[tone_count][2].set_xlabel("Power (dB)")
    if anl_array is not None:
        axes[tone_count][3].set_xlabel("Power (dB)")

    handles, labels = axes[0][0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="center right",
        ncol=1,
    )

    plt.show()


    # Plot parameters as a function of temperature:
    figure, axes = plt.subplots(
        number_of_tones, n_plots, sharex=True, sharey=False, figsize=(12, number_of_tones * n_plots)
    )
    axes[0][0].set_title("F0 (GHz)")
    axes[0][1].set_title("Qc (x10$^4$)")
    axes[0][2].set_title("Qi (x10$^4$)")
    if anl_array is not None:
        axes[0][3].set_title("Non-linear Coefficient")

    for tone_count, tone_name in enumerate(unique_tone_names):

        # Get indices for current tone:
        tone_indices = np.where(tone_names_array == tone_name)
        
        for power in unique_powers:
            
            # Get indices of current temperature within current tone subset:
            power_indices = np.where(power_array[tone_indices] == power)

            power_position = np.where(np.sort(unique_powers) == power)[0]

            sorted_series_indices = np.argsort(temperature_array[tone_indices][power_indices])
            temperature_series = temperature_array[tone_indices][power_indices][sorted_series_indices] * 1000

            axes[tone_count][0].set_ylabel(f"{tone_name}")

            axes[tone_count][0].errorbar(
                temperature_series,
                f0_array[tone_indices][power_indices][:, 0][sorted_series_indices] * 1e-9,
                yerr=f0_array[tone_indices][power_indices][:, 1][sorted_series_indices] * 1e-9,
                linestyle="--",
                marker="o",
                color=power_map(power_position),
                label=f"{power}dB",
            )


            axes[tone_count][1].errorbar(
                temperature_series,
                qc_array[tone_indices][power_indices][:, 0][sorted_series_indices] * 1e-4,
                yerr=qc_array[tone_indices][power_indices][:, 1][sorted_series_indices] * 1e-4,
                linestyle="--",
                marker="o",
                color=power_map(power_position),
                label=f"{power}dB",
            )
            # axes[tone_count][1].set_yscale("log")

            axes[tone_count][2].errorbar(
                temperature_series,
                qi_array[tone_indices][power_indices][:, 0][sorted_series_indices] * 1e-4,
                yerr=qi_array[tone_indices][power_indices][:, 1][sorted_series_indices] * 1e-4,
                linestyle="--",
                marker="o",
                color=power_map(power_position),
                label=f"{power}dB",
            )
            # axes[tone_count][2].set_yscale("log")

            if anl_array is not None:
                axes[tone_count][3].errorbar(
                    temperature_series,
                    anl_array[tone_indices][power_indices][:, 0][sorted_series_indices],
                    yerr=anl_array[tone_indices][power_indices][:, 1][sorted_series_indices],
                    linestyle="--",
                    marker="o",
                    color=power_map(power_position),
                    label=f"{power}dB",
                )

    # Set last x labels to Power.
    axes[tone_count][0].set_xlabel("Temperature (mK)")
    axes[tone_count][1].set_xlabel("Temperature (mK)")
    axes[tone_count][2].set_xlabel("Temperature (mK)")
    if anl_array is not None:
        axes[tone_count][3].set_xlabel("Temperature (mK)")

    handles, labels = axes[0][0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc="center right",
        ncol=1,
    )

    plt.show()

    return None


def plot_multisetting_q_overview(
    temperature_array: np.ndarray,
    power_array: np.ndarray,
    tone_names_array: np.ndarray,
    qc_array: np.ndarray,
    qi_array: np.ndarray,
    axes,
):
    
    """
    Function to take corresponding arrays and plot the qc and qi as functions of power and
    temperature on four subplots, one for Qc and Qi as a function of power and the same as a 
    function of temperature.

    Note, arrays for f0, Qc, Qi should have shapes [tone_names * number of files, 2], where the
    each value in the first axis is the value and the error.
    e.g. f0_array = np.array([value_1, error_1], [value_2, error_2], [value_2, error_2])

    :param temperature_array: Array of temperatures in Kelvin.
    :param power_array: Array of powers/attenuation in dB.
    :param tone_names_array: Array of tone names.
    :param f0_array: array of f0's.
    :param qc_array: array of Qc's.
    :param qi_array: array of Qi's.
    :param axes: Matplotlib Axes object containing two subplots.
    """

    # Find unique temperatures and powers:
    temperature_array = np.round(temperature_array, decimals=3) # Round to nearest mK
    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(power_array)

    temperature_map = plt.get_cmap('plasma', unique_temperatures.size)
    power_map = plt.get_cmap('viridis', unique_powers.size)

    unique_tone_names = np.unique(tone_names_array)
    number_of_tones = unique_tone_names.size

    if axes is None:
        figure, axes = plt.subplots(nrows=2, ncols=2, figsize=(12, 8))

    for tone_name in unique_tone_names:

        # Get indices for current tone:
        tone_indices = np.where(tone_names_array == tone_name)
        
        for count, temperature in enumerate(unique_temperatures):
            
            # Get indices of current temperature within current tone subset:
            temperature_indices = np.where(temperature_array[tone_indices] == temperature)

            sorted_series_indices = np.argsort(power_array[tone_indices][temperature_indices])
            power_series = power_array[tone_indices][temperature_indices][sorted_series_indices]

            axes[0][0].plot(
                power_series,
                qc_array[tone_indices][temperature_indices][:, 0][sorted_series_indices],
                linestyle="--",
                marker="none",
                color=temperature_map(count),
                alpha=0.75,
            )
            axes[0][1].plot(
                power_series,
                qi_array[tone_indices][temperature_indices][:, 0][sorted_series_indices],
                linestyle="--",
                marker="none",
                color=temperature_map(count),
                alpha=0.75,
            )

        for count, power in enumerate(unique_powers):
            
            # Get indices of current temperature within current tone subset:
            power_indices = np.where(power_array[tone_indices] == power)

            sorted_series_indices = np.argsort(temperature_array[tone_indices][power_indices])
            temperature_series = temperature_array[tone_indices][power_indices][sorted_series_indices] * 1000

            axes[1][0].plot(
                temperature_series,
                qc_array[tone_indices][power_indices][:, 0][sorted_series_indices],
                linestyle="--",
                marker="none",
                color=power_map(count),
                alpha=0.75,
            )
            axes[1][1].plot(
                temperature_series,
                qc_array[tone_indices][power_indices][:, 0][sorted_series_indices],
                linestyle="--",
                marker="none",
                color=power_map(count),
                alpha=0.75,
            )

    axes[0][0].set_xlabel("Power (dB)")
    axes[0][0].set_ylabel("Qc")
    axes[0][0].set_yscale("log")

    axes[0][1].set_xlabel("Power (dB)")
    axes[0][1].set_ylabel("Qi")
    axes[0][1].set_yscale("log")

    axes[1][0].set_xlabel("Temperature (mK)")
    axes[1][0].set_ylabel("Qc")
    axes[1][0].set_yscale("log")

    axes[1][1].set_xlabel("Temperature (mK)")
    axes[1][1].set_ylabel("Qi")
    axes[1][1].set_yscale("log")

    plt.tight_layout()
    plt.show()



def plot_multisetting_df_overview(
    temperature_array: np.ndarray,
    power_array: np.ndarray,
    tone_names_array: np.ndarray,
    f0_array: np.ndarray,
    axes,
):
    
    """
    Function to take corresponding arrays and plot the change in f0 (df) as functions of power and
    temperature on two subplots, one for a function of power and  a 
    function of temperature.

    Note, arrays for f0 should have shapes [tone_names * number of files, 2], where the
    each value in the first axis is the value and the error.
    e.g. f0_array = np.array([value_1, error_1], [value_2, error_2], [value_2, error_2])

    :param temperature_array: Array of temperatures in Kelvin.
    :param power_array: Array of powers/attenuation in dB.
    :param tone_names_array: Array of tone names.
    :param f0_array: array of f0's.
    :param qc_array: array of Qc's.
    :param qi_array: array of Qi's.
    :param axes: Matplotlib Axes object containing two subplots.
    """

    # Find unique temperatures and powers:
    temperature_array = np.round(temperature_array, decimals=3) # Round to nearest mK
    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(power_array)

    temperature_map = plt.get_cmap('plasma', unique_temperatures.size)
    power_map = plt.get_cmap('viridis', unique_powers.size)

    unique_tone_names = np.unique(tone_names_array)
    number_of_tones = unique_tone_names.size

    if axes is None:
        figure, axes = plt.subplots(nrows=2, ncols=1, figsize=(10, 6))

    for tone_name in unique_tone_names:

        # Get indices for current tone:
        tone_indices = np.where(tone_names_array == tone_name)
        
        for count, temperature in enumerate(unique_temperatures):
            
            # Get indices of current temperature within current tone subset:
            temperature_indices = np.where(temperature_array[tone_indices] == temperature)

            sorted_series_indices = np.argsort(power_array[tone_indices][temperature_indices])
            power_series = power_array[tone_indices][temperature_indices][sorted_series_indices]

            f0_0 = f0_array[tone_indices][temperature_indices][:, 0][sorted_series_indices][0]
            axes[0].plot(
                power_series,
                (f0_array[tone_indices][temperature_indices][:, 0][sorted_series_indices] - f0_0) * 1e-3,
                linestyle="--",
                marker="none",
                color=temperature_map(count),
                alpha=0.75,
                label=f"{temperature*1000}mK"
            )


        for count, power in enumerate(unique_powers):
            
            # Get indices of current temperature within current tone subset:
            power_indices = np.where(power_array[tone_indices] == power)

            sorted_series_indices = np.argsort(temperature_array[tone_indices][power_indices])
            temperature_series = temperature_array[tone_indices][power_indices][sorted_series_indices] * 1000

            f0_0 = f0_array[tone_indices][power_indices][:, 0][sorted_series_indices][0]
            axes[1].plot(
                temperature_series,
                (f0_array[tone_indices][power_indices][:, 0][sorted_series_indices] - f0_0) * 1e-3,
                linestyle="--",
                marker="none",
                color=power_map(count),
                alpha=0.75,
                label=f"{power}dB"
            )

    axes[0].set_xlabel("Power (dB)")
    axes[0].set_ylabel("df (kHz)")
    handles, labels = axes[0].get_legend_handles_labels()
    unique_labels = np.unique(labels)
    unique_label_indices = []
    for unique_label in unique_labels:
        unique_label_indices.append(np.where(np.array(labels) == unique_label)[0][0])
    axes[0].legend(
        handles=np.array(handles)[unique_label_indices].tolist(),
        labels=unique_labels.tolist(),
    )

    axes[1].set_xlabel("Temperature (mK)")
    axes[1].set_ylabel("df (kHz)")
    handles, labels = axes[1].get_legend_handles_labels()
    unique_labels = np.unique(labels)
    unique_label_indices = []
    for unique_label in unique_labels:
        unique_label_indices.append(np.where(np.array(labels) == unique_label)[0][0])
    axes[1].legend(
        handles=np.array(handles)[unique_label_indices].tolist(),
        labels=unique_labels.tolist(),
    )
    plt.tight_layout()
    plt.show()


def plot_psd(
    psd_array: np.ndarray,
    frequency_array: np.ndarray,
    sweep_iq_array: np.ndarray,
    timestream_iq_array: np.ndarray,
    label="",
    axes: Axes=None,
) -> None:
    """
    Function to plot the PSD (Sxx) in units of 1/Hz, as well as the IQ timestream, and the IQ
    points on top of the sweep IQ circle.

    :param psd_array: Array of PSD values in units of 1/Hz.
    :param frequency_array: Array of corresponding frequency values.
    :param sweep_iq_array: Timestream sweep IQ array.
    :param timestream_iq_array: Array of timestream IQ values.
    :param label: Label to be given to the data series.
    :param axes: Matplotlib axes consisting of three subplots. Preferably instanciated in a similar
    way to:

    figure = plt.figure(tight_layout=True, figsize=(10, 6))
    gs = gridspec.GridSpec(3, 2, width_ratios=(0.75, 0.25), height_ratios=(0.5, 0.25, 0.25))

    axes = [
        figure.add_subplot(gs[0, :]),
        figure.add_subplot(gs[1, :-1]),
        figure.add_subplot(gs[2, :-1]),
        figure.add_subplot(gs[1:, 1:]),
    ]

    """

    
    if axes is None:
        figure = plt.figure(tight_layout=True, figsize=(10, 6))
        gs = gridspec.GridSpec(3, 2, width_ratios=(0.75, 0.25), height_ratios=(0.5, 0.25, 0.25))

        axes = [
            figure.add_subplot(gs[0, :]),
            figure.add_subplot(gs[1, :-1]),
            figure.add_subplot(gs[2, :-1]),
            figure.add_subplot(gs[1:, 1:]),
        ]

    axes[0].plot(frequency_array, psd_array, label=label)
    for count, timestream_iq in enumerate(timestream_iq_array):
        axes[1].plot(np.real(timestream_iq), label=f"{label}/{count}")
        axes[2].plot(np.imag(timestream_iq))
        axes[3].plot(np.real(timestream_iq), np.imag(timestream_iq), label=label)

    axes[3].plot(np.real(sweep_iq_array), np.imag(sweep_iq_array), color="k", linewidth=0.5,)
    
    axes[0].set_xlabel("Frequency (Hz)")
    axes[0].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
    axes[0].set_xscale("log")
    axes[0].set_yscale("log")

    axes[1].set_ylabel("I (V)")
    axes[2].set_ylabel("Q (V)")
    axes[2].set_xlabel("Sample Index")

    axes[3].set_xlabel("I (V)")
    axes[3].set_ylabel("Q (V)")
    axes[3].set_aspect("equal")

    legend = axes[0].get_legend()
    if legend is not None:
        legend.remove() # Remove old legend if there is one.
    axes[0].legend()
    
    legend = axes[1].get_legend()
    if legend is not None:
        legend.remove() # Remove old legend if there is one.
    
    handles_1, labels_1 = axes[1].get_legend_handles_labels()
    figure = plt.gcf()
    figure.legend(
        handles=handles_1,
        labels=labels_1,
        bbox_to_anchor=(0.95, 0.01),
        ncols=4,
    )
    plt.subplots_adjust(bottom=0.3, right=1.0, top=1.0)
    plt.tight_layout()


    return None


def plot_multi_setting_psd(
    temperature_array: np.ndarray,
    power_array: np.ndarray,
    frequencies_array: list,
    psd_array: list,
    bifurcated_array=None,
    title="",
):
    """
    Function to take corresponding arrays of temperature, power and the corresponding frequencies and
    psd for a given resonator then plot each temperature's PSDs on a subplot and each power's PSDs on
    a subplot.

    :param temperature_array: Array of temperatures in Kelvin.
    :param power_array: Array of powers/attenuation in dB.
    :param frequencies_array: List of arrays of frequencies for each temperature/power combination.
    :param psd_array: List of arrays of PSDs for each temperature/power combination.
    :param bifurcated_array: Array of booleans indicating whether a PSD is from a bifurcated
    resonator.
    """
    
    # Find unique temperatures and powers:
    unique_temperatures = np.unique(temperature_array)
    unique_powers = np.unique(power_array)

    temperature_map = plt.get_cmap('plasma', unique_temperatures.size)
    power_map = plt.get_cmap('viridis', unique_powers.size)

    number_of_plots = unique_temperatures.size
    n_rows = math.ceil(unique_temperatures.size/2)
    figure, temperature_axes = plt.subplots(
        nrows=n_rows, ncols=2, sharex=True, sharey=False, figsize=(12, n_rows * 3),
    )
    figure.suptitle(title)

    total_plots = temperature_axes.size
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    for count, temperature in enumerate(unique_temperatures):

        temperature_indices = np.where(temperature_array == temperature)[0]

        for power_count, power in enumerate(unique_powers):
            power_index = np.where(power_array[temperature_indices] == power)[0]
            index = int(temperature_indices[power_index][0])

            if bifurcated_array[index] == True:
                linestyle="--"
                linewidth=0.5
            else:
                linestyle="-"
                linewidth=1
            temperature_axes[m][n].plot(
                frequencies_array[index],
                psd_array[index],
                label=f"{power}dB",
                color=power_map(power_count),
                linestyle=linestyle,
                linewidth=linewidth
            )

        temperature_axes[m][n].set_title(f"{temperature*1000}mK")
        temperature_axes[m][n].set_xscale("log")
        temperature_axes[m][n].set_yscale("log")
        if n == 0:
            temperature_axes[m][n].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
        if m == math.ceil(number_of_plots / 2) - 1:
            temperature_axes[m][n].set_xlabel("Frequency (Hz)")

        n += 1
        if n == 2:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    handles, labels = temperature_axes[0][0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc='lower center',
        ncol=8,
    )
    plt.tight_layout()
    figure.subplots_adjust(bottom=0.1)

    try:
        remaining_subplots = total_plots - number_of_plots
        if remaining_subplots > 0:
            for remaining_column in range(remaining_subplots):
                temperature_axes[m][n + remaining_column].remove()
                temperature_axes[m-1][n].set_xlabel("Frequency (Hz)")
    except:
        print("No axes to remove")


    number_of_plots = unique_powers.size
    n_rows = math.ceil(unique_powers.size/2)
    figure, power_axes = plt.subplots(
        nrows=n_rows, ncols=2, sharex=True, sharey=False, figsize=(12, n_rows * 3),
    )
    figure.suptitle(title)
    total_plots = power_axes.size
    m = 0  # Counter to assign row value
    n = 0  # Counter to assign column

    for count, power in enumerate(unique_powers):

        power_indices = np.where(power_array == power)[0]

        for temperature_count, temperature in enumerate(unique_temperatures):
            temperature_index = np.where(temperature_array[power_indices] == temperature)[0]
            index = int(power_indices[temperature_index][0])

            if bifurcated_array[index] == True:
                linestyle="--"
                linewidth=0.5
            else:
                linestyle="-"
                linewidth=1
            power_axes[m][n].plot(
                frequencies_array[index],
                psd_array[index],
                label=f"{temperature*1000}mK",
                color=temperature_map(temperature_count),
                linestyle=linestyle,
                linewidth=linewidth
            )

        power_axes[m][n].set_title(f"{power}dB")
        power_axes[m][n].set_xscale("log")
        power_axes[m][n].set_yscale("log")
        if n == 0:
            power_axes[m][n].set_ylabel("S$_{xx}$ (Hz$^{-1}$)")
        if m == math.ceil(number_of_plots / 2) - 1:
            power_axes[m][n].set_xlabel("Frequency (Hz)")

        n += 1
        if n == 2:
            m += 1  # Move to next row
            n = 0  # Reset to first column

    handles, labels = power_axes[0][0].get_legend_handles_labels()
    figure.legend(
        handles=handles,
        labels=labels,
        loc='lower center',
        ncol=8,
    )
    plt.tight_layout()
    figure.subplots_adjust(bottom=0.1)


    try:
        remaining_subplots = total_plots - number_of_plots
        if remaining_subplots > 0:
            for remaining_column in range(remaining_subplots):
                power_axes[m][n + remaining_column].remove()
                power_axes[m-1][n].set_xlabel("Frequency (Hz)")
    except:
        print("No axes to remove")

    return None






