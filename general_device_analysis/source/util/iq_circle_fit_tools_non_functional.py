"""
Functions to be used for circle fitting resonator IQ circles... Still on going, currently not functional.
"""

from scipy import optimize
from lmfit import Model
from lmfit import Parameters
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial import Polynomial

from source.models.iq_circle_fit.cable_delay import CableDelay
from source.models.iq_circle_fit.iq_circle_fit import IQCircleFit
from source.models.resonator_measurement.sweep_data import SweepData


def find_resonance_index(i_array: np.ndarray, q_array: np.ndarray) -> np.ndarray[int]:
    """
    Function to find the index in the I and Q arrays corresponding to the resonant frequency, e.g. the smallest value
    in the magnitude array.

    :param i_array: real component of the IQ data.
    :param q_array: imaginary component of the IQ data.
    :return:
    """

    magnitude_array = np.abs(i_array + 1j * q_array)
    plt.figure(figsize=(8, 6))
    plt.plot(magnitude_array)
    plt.show()

    return np.argmin(magnitude_array)


def find_off_resonance_index_array(
    i_array: np.ndarray, q_array: np.ndarray, threshold: float
) -> np.ndarray:
    """
    Function to find the indices in the I and Q arrays corresponding to the values that are off resonance, set by the
    threshold.
    in the magnitude array.

    :param i_array: real component of the IQ data.
    :param q_array: imaginary component of the IQ data.
    :param threshold: Threshold for off resonance. Must be value between 0 and 1. 0 will include all values, 1 will
    include the largest value.
    :return: Array of indexes where values are above the threshold.
    """

    magnitude_array = np.abs(i_array + 1j * q_array)
    value_range = np.max(magnitude_array) - np.min(magnitude_array)
    normalised_mag_array = magnitude_array / value_range - np.min(
        magnitude_array / value_range
    )
    off_resonance_index_array = np.argwhere(normalised_mag_array > threshold)

    return off_resonance_index_array


def plot_iq(iq_data: np.ndarray, save_plot: bool, plot_title: str, save_name: str):
    """
    :param iq_data: complex data array to plot I and Q: I+iQ.
    :param save_plot: Boolean to save plot.
    :param plot_title: Title of the plot.
    :param save_name: name to save plot with, end with ".png".
    :return:
    """
    plt.figure(figsize=(6, 6))
    plt.plot(
        np.real(iq_data), np.imag(iq_data), linestyle="--", marker="o", markersize="4"
    )
    plt.xlabel("I")
    plt.ylabel("Q")
    plt.axis("equal")
    plt.title(plot_title)
    plt.show()
    if save_plot:
        plt.savefig(save_name)


def cable_delay(
    frequency_array: np.ndarray, amplitude: float, tau: float
) -> np.ndarray:
    """
    Function to calculate the cable delay term for a transfer coefficient.
    :param frequency_array: Array of frequencies
    :param amplitude: Amplitude of cable delay
    :param tau: Cable delay time
    :return:
    """

    return amplitude * np.exp(-2 * np.pi * 1j * frequency_array * tau)


def fit_cable_delay(
    frequency_array: np.array,
    complex_data_array: np.array,
    amplitude_guess: float,
    tau_guess: float,
    cable_index: int,
    plot_graph=True,
) -> CableDelay:
    """

    :param frequency_array:
    :param complex_data_array: Array of complex data.
    :param amplitude_guess:
    :param tau_guess:
    :param cable_index: Indices to define how many data points at the start and end of data array are used for fitting.
    :param plot_graph: Boolean to plot graph of the fit
    :return CableDelay instance with the fit amplitude and tau.
    """

    data_amplitude = np.abs(complex_data_array)
    data_phase_angle = np.angle(complex_data_array)
    data_exponential = data_amplitude * np.exp(1j * data_phase_angle)

    # Clip data:
    data_to_fit = np.concatenate(
        [data_exponential[:cable_index], data_exponential[-cable_index:]]
    )
    frequency_to_fit = np.concatenate(
        [frequency_array[:cable_index], frequency_array[-cable_index:]]
    )

    # define model
    cable_delay_model = Model(cable_delay)

    # define parameters:
    params = Parameters()
    params.add("amplitude", value=amplitude_guess, min=0, vary=True)
    params.add("tau", value=tau_guess, vary=True)

    result = cable_delay_model.fit(
        data=data_to_fit, params=params, frequency_array=frequency_to_fit
    )
    fit_amplitude = (result.best_values["amplitude"],)
    fit_tau = (result.best_values["tau"],)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(
            np.real(data_exponential),
            np.imag(data_exponential),
            linestyle="none",
            marker="o",
            fillstyle="none",
            label="Data",
        )
        plt.plot(
            np.real(data_to_fit),
            np.imag(data_to_fit),
            linestyle="none",
            marker="o",
            fillstyle="none",
            label="Data for fit",
        )
        dummy_frequencies = np.linspace(
            start=np.min(frequency_array) - (5 / fit_tau[0]),
            stop=np.max(frequency_array) + (5 / fit_tau[0]),
            num=1000,
        )
        best_fit_circle = cable_delay(dummy_frequencies, fit_amplitude[0], fit_tau[0])
        best_fit = cable_delay(frequency_array, fit_amplitude[0], fit_tau[0])
        plt.plot(
            np.real(best_fit),
            np.imag(best_fit),
            linestyle="none",
            marker="o",
            fillstyle="none",
            label=f"Best fit: Amplitude = {fit_amplitude[0]: .2E}, "
            + r"$\tau$"
            + f" = {fit_tau[0]: .2E}",
        )
        plt.plot(
            np.real(best_fit_circle), np.imag(best_fit_circle), label=f"Best fit circle"
        )
        plt.xlabel("Q")
        plt.ylabel("I")
        plt.title("Cable delay fit")
        plt.axis("equal")
        plt.legend()
        plt.show()

    return CableDelay(fit_amplitude[0], fit_tau[0])


def calculate_r(
    x_array: np.ndarray, y_array: np.ndarray, x_centre: float, y_centre: float
):
    """
    calculate the distance of each 2D points from the center (xc, yc)

    :param x_array: numpy array of x data
    :param y_array: numpy array of y data
    :param x_centre: centre of circle x coordinate
    :param y_centre: centre of circle y coordinate
    :return:
    """
    return np.sqrt((x_array - x_centre) ** 2 + (y_array - y_centre) ** 2)


def f(
    centre,
    x_array: np.ndarray,
    y_array: np.ndarray,
):
    """calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc)"""
    ri = calculate_r(*centre, x_array, y_array)
    return ri - ri.mean()


def circle_fit(x_array: np.ndarray, y_array: np.ndarray) -> IQCircleFit:
    """
    Fits circle to data using least square method shown in scipy.optimize.

    :param x_array: array of x data to fit.
    :param y_array: array of y data to fit.
    :return: circle fit parameters as a IQCircleFit object with none type values for rotation_angle and
    phase_polynomial.
    """
    # centre guess:
    x_mean = np.mean(x_array)
    y_mean = np.mean(y_array)
    centre_guess = x_mean, y_mean
    fit_centre, ier = optimize.leastsq(f, centre_guess, args=(x_array, y_array))
    x_fit_centre, y_fit_centre = fit_centre
    r_array = calculate_r(x_array, y_array, x_fit_centre, y_fit_centre)
    r_mean = r_array.mean()
    residual = np.sum((r_array - r_mean) ** 2)

    print(
        f"circle fit complete. Fit parameters:\nx_centre={x_fit_centre: .1E}\ny_centre={y_fit_centre:.1E}\nR={r_mean:.1E}\nResidual={residual:.1E}"
    )

    return IQCircleFit(i_centre=x_fit_centre, q_centre=y_fit_centre, radius=r_mean)


def plot_circle(x_centre: float, y_centre: float, r: float, label: str) -> None:
    """
    Function to plot a circle with given centre coordinates and radius to the current plt.figure.

    :param x_centre: x coordinate of circle centre.
    :param y_centre: y coordinate of circle centre.
    :param r: radius of circle
    :param label: Label to be given to the circle data series.
    :return: none
    """
    circle_theta_array = np.linspace(-np.pi, np.pi, 180)
    circle_x_array = x_centre + r * np.cos(circle_theta_array)
    circle_y_array = y_centre + r * np.sin(circle_theta_array)
    plt.plot(circle_x_array, circle_y_array, label="Circle Fit")

    return None


def rotate_2d_matrix(array: np.ndarray, angle: float) -> np.ndarray:
    """
    Function to rotate a 2d matrix by a specified angle in degrees
    :param array: 2D numpy array to be rotated
    :param angle: angle to rotate by.
    :return: Rotated Matrix.
    """

    rotation_matrix = np.array(
        ((np.cos(angle), -np.sin(angle)), (np.sin(angle), np.cos(angle)))
    )

    return rotation_matrix.dot(array)


def discontinuity_presence(array: np.ndarray, threshold: float) -> bool or None:
    """
    Function to check if there is a discontinuity in the array by looking for large jumps between values. The threshold
    for which determines how big a gap counts as a discontinuity is set by the threshold.

    :param array: 1D Numpy array to be checked for discontinuities.
    :param threshold: Value determining how big a jump counts as a discontinuity
    :return: boolean, True if there is a discontinuity, false if not.
    """

    difference_array = np.diff(array)
    # Check if all difference values are below threshold.
    if np.abs(np.where(difference_array > threshold)[0].size) == 0:
        return False
    # Check if any difference values are above threshold.
    if np.abs(np.where(difference_array > threshold)[0].size) > 0:
        return True
    else:
        print("Error: Discontinuity Presence checker failed in iq_circle_fit_tools")
        return None


def fit_polynomial(x_data: np.array, y_data: np.array, degree: int, plot_graph: True):
    """
    Function to fit a polynomial of a specified degree to x and y data. Returns a numpy poly1d object.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    :return poly1d object containing the fit coefficients.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial_fit = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial_fit(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree 6",
        )
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.show()

    return polynomial_fit


def calibrate_iq_points_to_circle_fit(
    i: np.ndarray or float,
    q: np.ndarray or float,
    tone_frequency: float,
    iq_circle_fit: IQCircleFit,
):
    """
    Function to remove a cable delay then centre, normalise and rotate an IQ value pair according to a circle fit.

    :param i: In phase (I) value or array to be calibrated.
    :param q: Quadrature (Q) value or array to be calibrated.
    :param tone_frequency: Frequency of the readout tone.
    :param iq_circle_fit: IQCircleFit instance for the corresponding IQ sweep circle fit.
    """

    iq = i + 1j * q
    cable_delay_value = iq_circle_fit.cable_delay.calculate_delay_at_frequency(
        tone_frequency
    )
    iq = iq / cable_delay_value
    i = np.real(iq)
    q = np.imag(iq)

    i, q = iq_circle_fit.centre_and_normalise_iq(i=i, q=q)

    # Rotate IQ loop to put resonance at (-1, 0)
    rotated_iq = rotate_2d_matrix(np.array([i, q]), iq_circle_fit.rotation_angle)

    return rotated_iq[0], rotated_iq[1]


def process_sweep_data_with_circle_fit(
    sweep_data: SweepData,
    phase_polynomial_degree: int,
    phase_fit_fraction: float,
    plot_cable_delay_fit=True,
    plot_calibrated_data=True,
    plot_phase_polynomial_fit=True,
    save_figure=False,
):
    """
    Function to take a SweepData object and extract circle fit information and return an IQCircleFit object. Cable delay
    is removed from IQ data, then a circle is fit to the data, then centers, normalises and rotates the data. A phase is
    calculated for each data point then a polynomial fit of phase to frequency is obtained. An IQCircleFit object is
    then returned with the data from this process.

    :param sweep_data: SweepData object of the VNA sweep data.
    :param phase_polynomial_degree: Degree of polynomial fit to us to fit the phase frequency plot.
    :param phase_fit_fraction: Fraction of data range to use when fitting the phase frequency polynomial.
    :param plot_cable_delay_fit: Boolean to plot a the fit of the cable delay.
    :param plot_calibrated_data: Boolean to plot the IQ circle after removing cable delay, centering, normalising and
    rotating.
    :param plot_phase_polynomial_fit: Boolean to plot the polynomial fit of the phase frequency plot.
    :param save_figure: Boolean to save the plotted figure.
    """
    # Section to circle fit the sweep data.

    cable_delay = fit_cable_delay(
        frequency_array=sweep_data.frequency_array,
        complex_data_array=sweep_data.get_iq_array(),
        amplitude_guess=np.sqrt(sweep_data.i[0] ** 2 + sweep_data.q[0] ** 2),
        tau_guess=80e-9,
        cable_index=40,
        plot_graph=plot_cable_delay_fit,
    )

    # Remove cable delay
    cable_delay_values = cable_delay.calculate_delay_at_frequency(
        sweep_data.frequency_array
    )
    sweep_iq_array = sweep_data.get_iq_array() / cable_delay_values

    sweep_i_array = np.real(sweep_iq_array)
    sweep_q_array = np.imag(sweep_iq_array)

    # Fit circle and find centre and radius:
    iq_circle_fit = circle_fit(sweep_i_array, sweep_q_array)
    sweep_i_array, sweep_q_array = iq_circle_fit.centre_and_normalise_iq(
        sweep_i_array, sweep_q_array
    )

    # set cable delay of circle fit object to cable delay fit result.
    iq_circle_fit.cable_delay = cable_delay

    resonance_index = find_resonance_index(sweep_i_array, sweep_q_array)
    # Find angle of resonance w.r.t. centre of circle:
    resonance_angle = np.angle(
        sweep_i_array[resonance_index] + 1j * sweep_q_array[resonance_index], deg=True
    )

    # Rotate IQ loop to put resonance at (-1, 0)
    rotated_iq_array = rotate_2d_matrix(
        np.array([sweep_i_array, sweep_q_array]), resonance_angle
    )
    sweep_i_array_rotated = rotated_iq_array[0, :]
    sweep_q_array_rotated = rotated_iq_array[1, :]
    phase_array = np.angle(sweep_i_array_rotated + 1j * sweep_q_array_rotated, deg=True)

    rotated_angle = resonance_angle
    # Adjust rotation slightly to get continues phase vs frequency
    while discontinuity_presence(phase_array, threshold=125):
        rotated_iq_array = rotate_2d_matrix(
            np.array([sweep_i_array_rotated, sweep_q_array_rotated]), 0.1
        )
        sweep_i_array_rotated = rotated_iq_array[0, :]
        sweep_q_array_rotated = rotated_iq_array[1, :]
        phase_array = np.angle(
            sweep_i_array_rotated + 1j * sweep_q_array_rotated, deg=True
        )
        rotated_angle += 0.1

    iq_circle_fit.rotation_angle = rotated_angle
    iq_circle_fit.calibrated_i_array = sweep_i_array_rotated
    iq_circle_fit.calibrated_q_array = sweep_q_array_rotated

    if plot_calibrated_data:
        plt.figure(figsize=(8, 6))
        plt.plot(sweep_i_array_rotated, sweep_q_array_rotated)
        plt.xlabel("I (V)")
        plt.ylabel("Q (V)")
        plt.title(
            "IQ circle after cable delay is removed, and data is normalised, centred and rotated."
        )
        plt.axis("equal")
        plt.tight_layout()
        if save_figure:
            plt.savefig("..\\..\\Figures\\" + f"circle_fit_calibrated_sweep_data")
        plt.show()

    # fit polynomial to map phase angle to frequency
    data_fraction = int(sweep_data.frequency_array.size * phase_fit_fraction / 2)
    iq_circle_fit.phase_polynomial = fit_polynomial(
        phase_array[resonance_index - data_fraction : resonance_index + data_fraction],
        sweep_data.frequency_array[
            resonance_index - data_fraction : resonance_index + data_fraction
        ],
        degree=phase_polynomial_degree,
        plot_graph=plot_phase_polynomial_fit,
    )

    return iq_circle_fit
