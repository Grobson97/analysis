"""
Tools file for functions to help fit noise data such as Sxx or NEP from measured devices.
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy
from lmfit import Model
from lmfit import Parameters
from scipy.constants import pi
from skimage.measure import block_reduce


def remove_psd_peaks(
    frequency_array: np.ndarray,
    data_array: np.ndarray,
    max_iterations=1,
    iteration=0,
    show_plots=False,
) -> tuple:
    """
    Function to find peaks in the PSD data array and remove from both frequency and data array. Whilst peaks are still
    found, function calls itself recursively.

    :param frequency_array: Array of frequencies that correspond to the data array.
    :param data_array: Array of data to look for peaks in and remove those indices.
    :param max_iterations: Number of iterations to recursively call function.
    :param iteration: The current iteration, when called by the user it should always be 0.

    returns tuple of processed frequency array, processed data array
    """
    peaks = scipy.signal.find_peaks(
        np.log(data_array),
        height=None,
        distance=None,
        prominence=1,
        width=None,
        wlen=None,
        rel_height=None,
        plateau_size=None,
    )[0]

    processed_frequencies = np.delete(frequency_array, peaks)
    processed_data = np.delete(data_array, peaks)

    if show_plots:
        plt.figure()
        plt.plot(frequency_array, data_array, label="Original Data")
        plt.plot(processed_frequencies, processed_data, label="Data removed")
        plt.plot(
            frequency_array[peaks],
            data_array[peaks],
            linestyle="none",
            marker="o",
            color="r",
            label="Peaks",
            markersize=2,
        )
        plt.legend()
        plt.xscale("log")
        plt.yscale("log")
        plt.show()

    # See if processed data still has peaks
    peaks = scipy.signal.find_peaks(
        np.log(data_array),
        height=None,
        distance=None,
        prominence=1,
        width=None,
        wlen=None,
        rel_height=None,
        plateau_size=None,
    )[0]

    iteration += 1
    if (peaks.size > 0) & (iteration < max_iterations):
        processed_frequencies, processed_data = remove_psd_peaks(
            processed_frequencies,
            processed_data,
            max_iterations=max_iterations,
            iteration=iteration,
        )

    return processed_frequencies, processed_data


def general_ffs_psd(
    frequency_array: np.ndarray,
    a: float,
    b: float,
    n: float,
    c: float,
    roll_off_lifetime: float,
):
    """
    Function to calculate a general power spectral density of the fractional frequency noise of a detector. Adapted from
    paper by R. McGeehan DOI: 10.1007/s10909-018-2061-6.

    :param frequency_array: Array of N frequency points.
    :param a: Numerator term 1.
    :param b: 1/f multiplier.
    :param n: Power of 1/f term.
    :param c: Amplitude offset.
    :param roll_off_lifetime: Roll off lifetime.
    """

    term_1 = a + b * frequency_array ** (-n)
    term_2 = 1 + (2 * pi * frequency_array * roll_off_lifetime) ** 2

    return (term_1 / term_2) + c


def fit_dark_general_ffs_psd(
    frequency_array: np.array,
    fit_min_frequency: float,
    fit_max_frequency: float,
    data_array: np.array,
    a_guess: float,
    b_guess: float,
    n_guess: float,
    c_guess: float,
    roll_off_lifetime_guess: float or None,
    plot_graph=True,
    plot_title="",
) -> dict:
    """
    Function to fit the roll-off frequency of a dark power spectral density of the fractional frequency noise, Sxx, in
    units of Hz-1. See R. McGeehan DOI: 10.1007/s10909-018-2061-6.

    :param frequency_array: Array of N frequency points.
    :param fit_min_frequency: Minimum frequency value to fit.
    :param fit_max_frequency: Maximum frequency value to fit.
    :param data_array: FFT Sxx data array.
    :param a_guess: Numerator term 1.
    :param b_guess: 1/f multiplier.
    :param c_guess: Power of 1/f term.
    :param n_guess: Amplitude offset.
    :param roll_off_lifetime_guess: Roll off lifetime.
    :param plot_graph: Boolean to plot the graph.
    :param plot_title: Title to give to plot, default is nothing.
    """

    mask = np.where(
        (frequency_array > fit_min_frequency) & (frequency_array < fit_max_frequency)
    )
    fit_frequency_array = frequency_array[mask]

    # Define model:
    psd_model = Model(general_ffs_psd)

    # Define Parameters:
    params = Parameters()
    params.add("a", value=a_guess, vary=True)
    params.add("b", value=b_guess, vary=True)
    params.add("n", value=n_guess, vary=True)
    params.add(
        "c", value=c_guess, vary=True
    )  # Guesses white noise to be value at highest frequency
    params.add("roll_off_lifetime", value=roll_off_lifetime_guess, vary=True, min=0)

    # l_model.fit
    result = psd_model.fit(
        data=data_array[mask],
        params=params,
        frequency_array=fit_frequency_array,
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    if plot_graph:

        f_array = np.linspace(
            start=fit_frequency_array[0],
            stop=fit_frequency_array[-1],
            num=10000,
        )

        psd_fit = general_ffs_psd(
            frequency_array=f_array,
            a=result_dict["a"][0],
            b=result_dict["b"][0],
            n=result_dict["n"][0],
            c=result_dict["c"][0],
            roll_off_lifetime=result_dict["roll_off_lifetime"][0],
        )

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            data_array,
            linestyle="-",
            linewidth=2,
            color="r",
            label="Data",
        )
        plt.plot(
            f_array,
            psd_fit,
            linestyle="-",
            color="k",
            label=f"Best fit:\nA={result_dict['a'][0]:.2E}"
            f"\nB={result_dict['b'][0]:.2E}"
            f"\nn={result_dict['n'][0]:.2E}"
            f"\nC={result_dict['c'][0]:.2E}"
            f"\n$\\tau$={result_dict['roll_off_lifetime'][0]:.2E}",
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Sxx (Hz$^{-1}$)")
        plt.xscale("log")
        plt.yscale("log")
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return result_dict


def down_sample_noise_data_exponentially(
    frequency: np.ndarray, psd: np.ndarray, n_bins=100
) -> tuple:
    """
    downsample a noise psd using exponential bin sizes. Returns the reduced frequency array and corresponding psd.
    Not statistically rigorous!!

    :param frequency: Frequency array.
    :param psd: PSD array to be down sampled.
    :param n_bins: Number of bins.
    """

    binned_frequencies = []
    binned_psd = []
    standard_error = []

    f_min = frequency[0] + 10e-3
    f_max = frequency[-1]

    for bin_index in range(n_bins - 1):
        (bin_f_min, bin_f_max) = f_min * (f_max / f_min) ** (
            np.array([bin_index, bin_index + 1]) / (n_bins - 1.0)
        )
        bin_indices = np.where((frequency > bin_f_min) * (frequency <= bin_f_max))[0]

        if len(bin_indices) != 0:
            binned_frequencies.append(np.mean([bin_f_min, bin_f_max]))
            binned_psd.append(np.mean(psd[bin_indices]))
            standard_error.append(np.std(psd[bin_indices]))

    binned_frequencies = np.asarray(binned_frequencies)
    binned_psd = np.asarray(binned_psd)

    return binned_frequencies, binned_psd, standard_error


def get_mean_psd(
    number_of_timestreams: int,
    df_timestreams_array: np.ndarray,
    sample_rate: float,
    down_sample: bool,
    down_sample_bins: int,
):
    """
    Function to calculate each timestreams psd then mean average over all of them.

    Returns the mean psd and the corresponding frequencies.

    :param number_of_timestreams: Number of timestreams.
    :param df_timestreams_array: Array of all the df/f timestreams. Must be two dimensional even if there's only one
    timestream. e.g. a 1 timestream array should have shape (1, n_samples)
    :param sample_rate: Sample rate for the timestreams.
    :param down_sample_bins: Number of bins to use for exponential binning if down_sample_psd = True.
    :param down_sample: Boolean to down sample the psd data.
    """

    # Get power spectral densities using scipy's periodogram function.
    psd_array = []

    if df_timestreams_array.ndim != 2:
        return print("df_timestreams_array is not 2D")

    # window_size =
    for index in range(int(number_of_timestreams)):
        if number_of_timestreams == 1:
            (frequencies, psd) = scipy.signal.periodogram(
                df_timestreams_array, sample_rate, scaling="density"
            )
            psd_array.append(psd[0])
        else:
            (frequencies, psd) = scipy.signal.periodogram(
                df_timestreams_array[index], sample_rate, scaling="density"
            )
            psd_array.append(psd)

    # Get rid of negative indices:
    positive_frequency_mask = np.where(frequencies >= 0)[0]
    mean_psd = np.mean(psd_array, axis=0)[positive_frequency_mask] * 2
    frequencies = frequencies[positive_frequency_mask]

    if down_sample:
        frequencies, mean_psd, mean_psd_error = down_sample_noise_data_exponentially(
            frequency=frequencies, psd=mean_psd, n_bins=down_sample_bins
        )

    return mean_psd, frequencies
