import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors, colorbar
import pickle
import math
from matplotlib import gridspec
from lmfit import Model
from lmfit import Parameters
import scipy
from scipy.io import netcdf
from scipy.signal import find_peaks
import matplotlib as mpl
from source.util import segmented_sweep_data_tools


def fft_lorentzian(
        x: np.ndarray,
        nu_0: float,
        time_constant: float,
        scaler: float,
        offset: float
    ) -> np.ndarray:
    """
    Function for an exponentially decaying cosine (The FFT of a lorentzian peak).

    :param x: X axis of decaying function. For FTS measurements this is the OPD.
    :param nu_0: Resonant frequency in wavenumber (cm-1).
    :param time_constant: Decay constant (cm).
    :param scaler: Sets the magnitude of the interferogram.
    :param offset: Phase term in case of misalignnment/incorrect zero position. Should be zero.
    """
    return scaler * np.exp(-x/time_constant) * np.cos(2 * np.pi * nu_0 * x + offset)


def fit_interferogram(
    x: np.array,
    nu_0_guess: float,
    data_array: np.array,
) -> dict:
    """
    Function to fit an interferogram for the resonant frequency (nu_0) in wavenumber (cm-1) and the decay constant (cm).

    :param x: X axis of decaying function. For FTS measurements this is the OPD.
    :param nu_0_guess: Resonant frequency guess in wavenumber (cm-1).
    :param data_array: y-axis of interferogram (For MKIDs this is usually df/f).
    """
    
    # Define model:
    model = Model(fft_lorentzian)

    # Define Parameters:
    params = Parameters()
    params.add("time_constant", value=0.5e-1, vary=True)
    params.add("nu_0", value=nu_0_guess, vary=True, min=0)
    params.add("scaler", value=np.max(data_array), vary=True)
    params.add("offset", value=np.pi/8, vary=True)

    try:
        result = model.fit(
            data=data_array,
            params=params,
            x=x,
        )
    except:
        print("Fit failed")
        return None
    result_dict = {}

    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    return result_dict


def remove_slope(x: np.ndarray, y: np.ndarray, plot: bool=False) -> np.ndarray:
    """
    Function to remove a global linear trend from data.

    :param x: X data.
    :param y: Y data.
    :param plot: Option to plot the before and after correction.
    """
    coefficients, covariance = np.polyfit(x, y, 1, cov=True)
    polynomial = np.poly1d(coefficients)

    corrected_y = y - polynomial(x)

    if plot:
        plt.figure(figsize=(6,4))
        plt.plot(x, y, alpha=0.3, label="Uncorrected")
        plt.plot(x, corrected_y, alpha=0.3, label="Corrected")
        axis = plt.gca()
        plt.hlines(y=0, xmin=axis.get_xlim()[0], xmax=axis.get_xlim()[1], linestyle="--", color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.show()

    return corrected_y


def butterfly_interferogram(
    interferogram_y: np.ndarray,
    x: np.ndarray,
    plot: bool=False
) -> np.ndarray:
    """
    Function to "butterfly" an interferogram such that zero path difference position is
    at the start and end of the array and extrema in the center.
    
    :param interferogram_y: Y axis of the interferogram (For MKIDs this is usually df/f).
    :param x: Corresponding optical path difference with zero position in the center.
    :param plot: Option to plot the input and output interferograms.
    """

    zero_position_index = np.where(abs(x)==min(abs(x)))[0][0] #assume ZPD location is good. Can be fixed later.
    dx = np.mean(np.diff(x)) # step between x points.

    positive_end = interferogram_y[zero_position_index:]
    negative_end = interferogram_y[:zero_position_index]
    
    # Ensure symmetric:
    if len(positive_end) > len(negative_end):
        positive_end = positive_end[:len(negative_end)]
    else:
        negative_end = negative_end[:len(positive_end)]
        
    butterfly = np.append(positive_end, negative_end) #butterfly, scipy/numpy expects the 0th `frequency' first (ZPD signal)

    #subtract the 0-frequency term
    butterfly -= np.median(butterfly)

    if plot:
        plt.figure(figsize=(6,4))
        plt.plot(interferogram_y, label='Regular IFGM order', alpha=0.3)
        plt.plot(butterfly, label='Butterflied', alpha=0.3)
        plt.legend(loc='best')
        plt.xlabel('Index')
        plt.show()

    return butterfly


def fft_opd_to_frequency(
    butterfly_interferogram: np.ndarray,
    path_difference: np.ndarray,
    plot: bool=False,
    x_lim: tuple=(100, 200)
) -> tuple:
    """
    Function to take the FFT of a butterflied interferogram and convert the path difference to frequency
    axis (using n points and step size).

    Returns the complex spectral amplitude of the interferogram and the frequency axis in GHz and removes
    the negative frequency points.

    :param butterfly_interferogram: Y axis of the interferogram that is in the butterflied format
    (typically df/f).
    :param path_difference: Path difference in cm.
    :param plot: Option to plot the spectral amplitudes.
    :param x_lim: Tuple containing the limits for the x axis on the plot.
    """

    dx = np.mean(np.diff(path_difference)) # step between x points.

    spectral_amplitude = np.fft.fft(butterfly_interferogram)
    frequency_axis = np.fft.fftfreq(len(spectral_amplitude), d=dx) * 29.9792458 #GHz

    #Put the data back into a sensible order
    sort_order = np.argsort(frequency_axis)
    spectral_amplitude = spectral_amplitude[sort_order]
    frequency_axis = frequency_axis[sort_order]

    if plot:
        plt.figure()
        plt.plot(frequency_axis, spectral_amplitude.real, label='Real')
        plt.plot(frequency_axis, spectral_amplitude.imag, label='Imaginary')
        plt.xlabel('Frequency [GHz]')
        plt.ylabel('Spectral amplitude [df/f]')
        plt.legend(loc='best')
        plt.xlim(x_lim[0], x_lim[1])
        plt.show()

    positive_indices = np.where(frequency_axis>=0)[0]

    return spectral_amplitude[positive_indices], frequency_axis[positive_indices]


def ifft_frequency_to_opd(
    spectral_amplitude: np.ndarray,
    frequencies: np.ndarray,
    plot: bool=False        
) -> tuple:
    """
    Function to take the inverse fourier transform of spectral amplitudes and corresponding
    positive frequencies in GHz and return the interferogram and path difference in cm.

    Returns a tuple of the interferogram and the path difference in cm.

    :param spectral_amplitude: Real array of spectral amplitudes from an FFT of an interferogram.
    Must just be the positive frequency values.
    :param frequencies: Frequency axis of the spectral amplitudes. Must be the positive values in GHz.
    :param plot: Option to plot the result.
    """

    amplitudes_for_ifft = np.append(
        spectral_amplitude,
        np.flip(spectral_amplitude)
    )

    interferogram = np.fft.fftshift(np.fft.ifft(amplitudes_for_ifft))
    path_difference = np.fft.fftshift(
        np.fft.fftfreq(interferogram.size, d=np.mean(np.diff(frequencies))/29.9792458) # (cm)
    )
    if plot:
        plt.figure(figsize=(6,4))
        plt.plot(path_difference, np.real(interferogram))
        plt.xlabel("Path difference (cm)")
        plt.ylabel("response (arb)")
        plt.show()

    return np.real(interferogram), path_difference


def phase_correct_spectral_amplitudes(
    spectral_amplitude: np.ndarray,
    frequencies: np.ndarray,
    peak_index_span: int=4,
    plot: bool=True,
    min_frequency: float=100.0,
    max_frequency: float=200.0
) -> np.ndarray:
    """
    Function to take the complex spectral amplitudes from an FFT of an interferogram that correspond
    to positive frequencies, find the peak index, fit a linear term to the phase in that region
    and then multiply the spectral amplitudes by this linear phase offset.

    Returns the phase corrected spectral amplitudes.

    :param spectral_amplitude: Complex array of spectral amplitudes from an FFT of an interferogram.
    Must just be the positive frequency values.
    :param frequencies: Frequency axis of the spectral amplitudes. Must be the positive values in GHz.
    :param peak_index_span: Number of indices either side of the peak index to include in linear fit.
    :param plot: Option to plot the procedure results.
    :param min_frequency: Minimum frequency for the plots (GHz) 
    :param max_frequency: Maximum frequency for the plots (GHz) 
    """
    
    # Find spectral peak index
    peak_index = np.argmax(np.abs(spectral_amplitude))
    phase = np.arctan2(spectral_amplitude.imag, spectral_amplitude.real)
    unwrapped_phase = np.unwrap(phase)

    # isolate data points around peak to fit linear to remove linear term.
    peak_frequencies = frequencies[peak_index - peak_index_span: peak_index + peak_index_span]
    peak_phase = unwrapped_phase[peak_index - peak_index_span: peak_index +peak_index_span]

    # Fit linear to peak region:
    weights = abs(spectral_amplitude)[peak_index - peak_index_span: peak_index +peak_index_span]
    coefficients, covariance = np.polyfit(peak_frequencies, np.unwrap(peak_phase), 1, w=weights, cov=True)
    polynomial = np.poly1d(coefficients)

    # Create mask in region we're interested in for plotting
    mask = np.where((frequencies > min_frequency) * (frequencies < max_frequency))
    linear_fit = polynomial(frequencies)

    # Create exponential function from linear fit:
    phase_correction_function = np.exp(-1j*linear_fit)
    # Correct spectral amplitudes:
    corrected_spectral_amplitudes = spectral_amplitude * phase_correction_function

    if plot:
        figure, axes = plt.subplots(ncols=2, nrows=1, figsize=(12, 4))
        axes[0].plot(
            frequencies,
            unwrapped_phase,
            color="C0",
            label="Full Phase"
        )
        axes[0].plot(
            peak_frequencies,
            peak_phase,
            color="C1",
            label="Peak region (fit data)"
        )
        axes[0].plot(frequencies[mask], linear_fit[mask], linestyle="--", color="k", linewidth=1, label="fit")
        axes[0].set_xlabel('Frequency [GHz]')
        axes[0].set_ylabel('Unwrapped Phase [rad]')
        axes[0].legend()
        axes[0].set_xlim(min_frequency, max_frequency)
        axes[0].set_ylim(np.min(unwrapped_phase[mask]), np.max(unwrapped_phase[mask]))

        axes[1].plot(frequencies[mask], spectral_amplitude.real[mask], color="C0", linestyle=":")
        axes[1].plot(frequencies[mask], spectral_amplitude.imag[mask], color="C1", linestyle=":")
        axes[1].plot(frequencies[mask], corrected_spectral_amplitudes.real[mask], linestyle="-", label='Real')
        axes[1].plot(frequencies[mask], corrected_spectral_amplitudes.imag[mask], linestyle="-", label='Imaginary')
        axes[1].plot([], [], linestyle="-", color="k", label="Corrected")
        axes[1].plot([], [], linestyle=":", color="k", label="Uncorrected")
        axes[1].set_xlabel('Frequency [GHz]')
        axes[1].set_ylabel('Spectral amplitude [df/f]')
        axes[1].legend(loc='best')
        axes[1].set_xlim(min_frequency, max_frequency)
        plt.show()

    return corrected_spectral_amplitudes


def check_if_spectral(
    spectral_amplitude: np.ndarray,
    frequencies: np.ndarray,
    sigma_cut_off: float,
    in_band_limit: tuple,
    out_band_limit: tuple
):
    """
    Function to check if spectral amplitudes contain a distinct spectral peak. This is done by
    comparing the max value in band to the out of band standard deviation (max/std).

    Return True if spectral or false if not.

    :param spectral_amplitude: Complex array of spectral amplitudes from an FFT of an interferogram.
    Must just be the positive frequency values.
    :param frequencies: Frequency axis of the spectral amplitudes. Must be the positive values in GHz.
    :param sigma_cut_off: If signal to noise is less than sigma cut off, then considered not spectral
    if greater than then True is returned.
    :param in_band_limit: Min and max frequency bounds defining the in band range (GHz). e.g. (100, 200).
    :param out_band_limit: Min and max frequency bounds defining the out of band range (GHz). e.g. (300, 400).
    """

    spectral_amplitude_mag = np.abs(spectral_amplitude)
    in_band = np.where(np.logical_and(frequencies>=in_band_limit[0], frequencies<=in_band_limit[1]))
    out_band = np.where(np.logical_and(frequencies>=out_band_limit[0], frequencies>=out_band_limit[1]))
    approx_signal_to_noise = (max(spectral_amplitude_mag[in_band])/np.std(spectral_amplitude_mag[out_band]))
    
    if approx_signal_to_noise >= sigma_cut_off:
        return True
    if approx_signal_to_noise <= sigma_cut_off:
        return False
 

def process_interferogram(
    interferogram: np.ndarray,
    path_difference: np.ndarray,
    in_band_limit: tuple,
    out_band_limit: tuple,
    sigma_noise_cut_off: float,
    fit_interferograms: bool
) -> dict:
    """
    Function to take an uncorrected interferogram and path difference in cm and does the following:
    1. Remove a linear slope (lop sided interferograms)
    2. Take the fft to frequency space.
    3. Checks if the result is spectral by checking the in band maximum is significantly greater than
    the out of band standard deviation.
    4. If spectral: Removes the linear phase offset from the spectral amplitudes.
    5. Saves corrected spectral amplitudes and corresponding frequencies (GHz) within the band to dictionary.
    6. Takes the inverse FFT of the corrected spectrum and saves the corrected interferogram and corresponding
    path difference in cm to the dictionary.

    :param interferogram: Y axis of the interferogram (For MKIDs this is usually df/f).
    :param path_difference: Optical path difference corresponding to interferogram values in cm.
    :param in_band_limit: Min and max frequency bounds defining the in band range (GHz). e.g. (100, 200).
    :param out_band_limit: Min and max frequency bounds defining the out of band range (GHz). e.g. (300, 400).
    :param sigma_noise_cut_off: If signal to noise is less than sigma cut off, then considered not spectral
    if greater than then True is returned.
    :param fit_interferograms: Option to fit the interferograms if the interferogram is flagged as spectral.
    """

    dictionary = {}

    flat_df_f = remove_slope(x=path_difference, y=interferogram, plot=False)
    butterfly_df_f = butterfly_interferogram(
        interferogram_y=flat_df_f,
        x=path_difference,
        plot=False,
    )
    spectral_amplitude, frequencies = fft_opd_to_frequency(
        butterfly_interferogram=butterfly_df_f,
        path_difference=path_difference,
        plot=False,
        x_lim=in_band_limit
    )

    is_spectral = check_if_spectral(
        spectral_amplitude=spectral_amplitude,
        frequencies=frequencies,
        sigma_cut_off=sigma_noise_cut_off,
        in_band_limit=in_band_limit,
        out_band_limit=out_band_limit
    )

    if is_spectral:
        try:
            phase_corrected_spectral_amplitude = phase_correct_spectral_amplitudes(
                spectral_amplitude=spectral_amplitude,
                frequencies=frequencies,
                peak_index_span=4,
                min_frequency=in_band_limit[0],
                max_frequency=in_band_limit[1],
                plot=False
            )
            phase_corrected_spectral_amplitude = np.real(phase_corrected_spectral_amplitude)
        except:
            print("Phase correction failed")
            phase_corrected_spectral_amplitude = np.real(spectral_amplitude)
            is_spectral = False
    else:
        print("Not spectral")
        phase_corrected_spectral_amplitude = np.real(spectral_amplitude)

    interferogram, path_difference = ifft_frequency_to_opd(
        spectral_amplitude=phase_corrected_spectral_amplitude,
        frequencies=frequencies,
        plot=False
    )

    in_band_mask = np.where(np.logical_and(frequencies>=in_band_limit[0], frequencies<=in_band_limit[1]))
    peak_index = np.argmax(phase_corrected_spectral_amplitude[in_band_mask])
    peak_frequency = frequencies[in_band_mask][peak_index]
    peak_amplitude = phase_corrected_spectral_amplitude[in_band_mask][peak_index]

    if is_spectral and fit_interferograms:
        positive_path_difference_indices = path_difference >= 0
        fit_result = fit_interferogram(
            x=path_difference[positive_path_difference_indices],
            nu_0_guess=peak_frequency / 30,
            data_array=interferogram[positive_path_difference_indices]
        )
        dictionary["interferogram_fit"] = fit_result


    dictionary["is_spectral"] = is_spectral
    dictionary["spectral_amplitudes"] = phase_corrected_spectral_amplitude[in_band_mask]
    dictionary["frequencies"] = frequencies[in_band_mask]
    dictionary["interferogram"] = interferogram
    dictionary["path_difference"] = path_difference
    dictionary["peak_frequency"] = peak_frequency
    dictionary["peak_amplitude"] = peak_amplitude

    return dictionary


def plot_spectra_from_dictionary(
    tones_list: np.ndarray,
    tone_data_dictionary_array: dict,
    out_band_limit: tuple,
    title: str
) -> None:
    """
    Function to plot the spectra from an array of tone data dictionaries output by the
    process_interferogram function.

    :param tone_data_dictionary: Array of tone data dictionaries, each with the following keys:
    "interferogram_fit", "is_spectral", "spectral_amplitudes", "frequencies", "interferogram",
    "path_difference", "peak_frequency", "peak_amplitude", ". 
    :param out_band_limit: tuple of min and max frequencies to pull the noise standard deviation from.
    :param title: Title for the plot.
    """

    spectral_count = 0
    cmap = plt.get_cmap('turbo')
    color_normaliser =  mpl.colors.Normalize(vmin=100, vmax=200)

    figure = plt.figure(tight_layout=True, figsize=(10, 8))
    gs = gridspec.GridSpec(4, 1, height_ratios=(0.19, 0.27, 0.27, 0.27))

    axis_1 = figure.add_subplot(gs[1])
    axis_0 = figure.add_subplot(gs[0], sharex=axis_1)
    axis_2 = figure.add_subplot(gs[2], sharex=axis_1)
    axis_3 = figure.add_subplot(gs[3], sharex=axis_1)


    plt.setp(axis_0.get_xticklabels(), visible=False)
    plt.setp(axis_1.get_xticklabels(), visible=False)
    plt.setp(axis_2.get_xticklabels(), visible=False)

    s_n_array = []
    all_normalised_spectra = []

    x = 0
    for count, tone in enumerate(tones_list):
        frequencies = tone_data_dictionary_array[count]["frequencies"]
        spectrum = np.abs(tone_data_dictionary_array[count]["spectral_amplitudes"])
        noise_range = np.where((frequencies >= out_band_limit[0]) & (frequencies <= out_band_limit[1]))
        noise_std = np.std(spectrum[noise_range])
        signal_to_noise = spectrum/noise_std
        normalised_spectrum = spectrum/np.max(spectrum)

        label=""

        if tone_data_dictionary_array[count]["is_spectral"] and not tone_data_dictionary_array[count]["cross_pol"] :
            s_n_array.append(signal_to_noise)
            all_normalised_spectra.append(normalised_spectrum)
            peak_frequency = tone_data_dictionary_array[count]["peak_frequency"]
            color = cmap(color_normaliser(peak_frequency))
            alpha = 0.3
            spectral_count += 1

            axis_0.scatter(peak_frequency, noise_std, color=color)
            axis_3.fill_between(
                x=frequencies,
                y1=normalised_spectrum,
                color=color,
                alpha=alpha
            )
               
        else:
            color = "k"
            alpha=0.05
            if x == 0:
                label = "Noise"
                x+=1

        axis_1.fill_between(
            x=frequencies,
            y1=spectrum,
            color=color,
            alpha=alpha,
            label=label
        )
        axis_2.fill_between(
            x=frequencies,
            y1=signal_to_noise,
            color=color,
            alpha=alpha,
            label=label
        )

    summed_all_normalised_spectra = np.sum(np.array(all_normalised_spectra), axis=0)
    normalised_spectral_coverage = summed_all_normalised_spectra/np.max(summed_all_normalised_spectra)
    axis_3.plot(frequencies, normalised_spectral_coverage, color="k", linestyle="--", label="Normalised spectral coverage")

    print(f"Mean spectral S/N = {np.mean(s_n_array):.1f} +/- {np.std(s_n_array):.1f}")

    axis_0.set_yscale("log")
    axis_0.grid(axis="y")
    axis_0.set_ylabel("Noise STD\n(df/f)")
    axis_1.set_ylabel("Spectral signal (df/f)")
    axis_1.legend()
    axis_2.set_ylabel("Signal to Noise")
    axis_2.legend()
    axis_3.set_ylabel("Normalised signal")
    axis_3.set_xlabel("Spectral Frequency (GHz)")
    axis_3.legend()
    plt.suptitle(title + f", total spectral channels: {spectral_count}, total noisy channels: {tones_list.size - spectral_count}")
    plt.subplots_adjust(hspace=0.0)
    plt.tight_layout()
    plt.show()

    return normalised_spectral_coverage, frequencies


def simple_lorentzian(
    frequency_array: np.array, a: float, bw: float, f0: float, c: float
) -> np.array:
    """
    Function defining amp very simple model of amp lorentzian.
    Returns

    :param np.array frequency_array: Array of N frequency points.
    :param float a: Amplitude.
    :param float bw: Full width half maximum.
    :param float f0: Resonant frequency.
    :param float c: Offset in y.
    """
    x = (frequency_array - f0) / (0.5 * bw)
    lorentzian = a / (1 + x**2) + c
    return lorentzian