import numpy as np


class SParamTransferFunctions:
    """
    Class of static methods to convert s parameter matrices to other formats.
    """

    @staticmethod
    def complex_to_magnitude(
        real_s_array: np.ndarray, imag_s_array: np.ndarray
    ) -> np.ndarray:
        """
        Function to combine the real and imaginary arrays of an S parameter measurement and convert into a magnitude
        array.

        :param real_s_array: Real part of S parameter.
        :param imag_s_array: Imaginary part of S parameter.
        """

        return np.abs(real_s_array + 1j * imag_s_array)

    @staticmethod
    def magnitude_to_db(magnitude_array) -> np.ndarray:
        """
        Function to convert a magnitude array to a dB array.

        :param magnitude_array: Magnitude array.
        """

        return 20 * np.log10(magnitude_array)

    @staticmethod
    def complex_to_db(real_s_array: np.ndarray, imag_s_array: np.ndarray) -> np.ndarray:
        """
        Function to convert the real and imaginary arrays of an S parameter measurement into a magnitude array in dB.

        :param real_s_array: Real part of S parameter.
        :param imag_s_array: Imaginary part of S parameter.
        """

        return 20 * np.log10(np.abs(real_s_array + 1j * imag_s_array))
