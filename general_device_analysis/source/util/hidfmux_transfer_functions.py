"""
NB: This is copied from the hidfmux library... careful with updating to newer version.
Transfer functions for various hardware items. VERY MUCH UNDER CONSTRUCTION;
things here will change often as we figure out the best way to handle these.

Update June 2022:
    -switch to using standard non-freq dependent mezzanine in/out TF
"""

import numpy as np
from scipy import interpolate

# __all__ = ['']

ADC_RESOLUTION = 23  # bits
VOLTS_PER_ROC = 1.0 / (2**ADC_RESOLUTION - 1)

DAC_FS = 0  # dBm

CREST_FACTOR = 3.5

LATENCY = 1.19664e-6

INPUT_LINE_GAIN = -65  ## THIS IS NOT THE RIGHT PLACE FOR THIS BUT JUST FOR NOW

TERMINATION = 50.0

COMB_SAMPLING_FREQ = 500e6

DEMOD_SAMPLING_FREQ = (
    COMB_SAMPLING_FREQ / 256 / 64 / 64
)  # Hz ### AS OF R1.6 THIS IS NO LONGER FIXED AT THIS FREQUENCY

DDS_PHASE_ACC_NBITS = 32  # bits
FREQ_QUANTUM = COMB_SAMPLING_FREQ / 256 / 2**DDS_PHASE_ACC_NBITS

BASE_FREQUENCY = COMB_SAMPLING_FREQ / 256 / 2**12

EST_ADC_NOISE_FLOOR_ROC = (
    90  # approx ADC input white noise level, in units of readout counts
)


def get_demod_sampling_freq(d):
    fir_stage = d.get_fir_stage()
    demod_sampling_freq = 500e6 / (256 * 64 * 2**fir_stage)
    return demod_sampling_freq


####################
### SYSTEM UNITS ###
####################


def convert_dacunits_to_dbm(dacunits):
    """
    Convert the "normalized DAC unit" carrier amplitudes to
    a value in dBm. Assumes the system at this point is
    50-ohm balanced.

    Parameters
    ----------

    dacunits : carrier value in DAC units

    Returns
    -------

    (float) equivalent power in dBm

    """

    return DAC_FS + 20.0 * np.log10(dacunits)


def convert_roc_to_volts(roc):
    """
    Convenience function for converting a measured value in readout
    counts (ROCs) to voltage units.

    ROCs are a measure of voltage.

    Parameters
    ----------

    roc : measured quantity in readout counts (ROCs)


    Returns
    -------

    (float) value in volts
    """

    return roc * VOLTS_PER_ROC


def convert_roc_to_dbm(roc, termination=50.0):
    """
    Convenience function for converting a measured value in readout
    counts (ROCs) to log power units.

    ROCs are a measure of voltage, so an input termination is
    required (default 50 ohm).

    Parameters
    ----------

    roc : measured quantity in readout counts (ROCs)

    termination : the system termination resistance in ohms.
        Default is 50.

    Returns
    -------

    (float) value in dbm
    """

    volts = convert_roc_to_volts(roc)
    dbm = convert_volts_to_dbm(volts, termination)
    return dbm


#####################
### GENERAL UNITS ###
#####################


def convert_volts_to_watts(volts, termination=50.0):
    """
    Convenience function for converting a signal amplitude in
    volts (for a sinusoidal signal) to  power units.

    NOTE that the conversion from volts amplitude to power is
    done using the root-mean-square voltage.

    Parameters
    ----------

    volts : signal amplitude in volts

    termination : the system termination resistance in ohms.
        Default is 50.

    Returns
    -------

    (float) value in watts
    """

    v_rms = volts / np.sqrt(2.0)
    watts = v_rms**2 / termination

    return watts


def convert_volts_to_dbm(volts, termination=50.0):
    """
    Convenience function for converting a signal amplitude in
    volts (for a sinusoidal signal) to log power units.

    NOTE that the conversion from volts amplitude to power is
    done using the root-mean-square voltage.

    Parameters
    ----------

    volts : signal amplitude in volts

    termination : the system termination resistance in ohms.
        Default is 50.

    Returns
    -------

    (float) value in dbm
    """

    watts = convert_volts_to_watts(volts, termination)
    return 10.0 * np.log10(watts * 1e3)


def convert_dbm_to_volts(dbm, termination=50.0):
    """
    Convenience function for converting log power units to
    volts peak-amplitude.

    Parameters
    ----------

    dbm : power in dBm

    termination : the system termination resistance in ohms.
        Default is 50.
    """

    watts = 10.0 ** (dbm / 10.0) * 1e-3
    volts_rms = np.sqrt(watts * termination)
    volts_peak_amplitude = volts_rms * np.sqrt(2)
    return volts_peak_amplitude


###########################
### DIGITAL FILTER COMP ###
###########################


def cic2_response(f_arr, fs):
    """
    Compute the response of the second-stage cascaded-integrator
    comb (CIC) filter, as a function of frequency. This filter
    defines the shape of each channel's frequency response, so
    the frequencies here are relative to the central frequency
    of the channel.


    Paramters
    ---------

    f_arr : list or array of frequencies within one channel bw.
        Must be less than the demodulated channel sampling frequency.

    """

    f_arr = np.asarray(f_arr)

    if fs < 500:
        cic_factor = 6
    else:
        cic_factor = 5

    return np.sinc(f_arr / fs) ** cic_factor


def apply_cic2_comp_asd(f_arr, asd, fs, trim=0.15):
    """
    Compensate for the response of the second-stage cascaded-
    integrator comb (CIC) filter as a function of frequency in
    an amplitude spectral density measurement.

    Because the channel response drops off quickly towards and
    above Nyquist, results will be less trustworthy at high
    frequencies.

    Parameters
    ----------

    f_arr : list or array of frequencies within one channel bw.
        Must be less than the demodulated channel sampling frequency.

    asd : list or array of amplitude spectral density values as
        a function of frequency

    fs : int or float
        Sampling rate of the demodulated timestream.

    trim : float
        Percent length of array to trim off at high frequencies,
        to avoid returning data where numerical uncertainties
        are higher in the compensation function.

    """
    compensated = np.asarray(asd) / cic2_response(np.asarray(f_arr), fs)
    trimind = int(np.ceil(trim * len(compensated)))

    return f_arr[:-trimind], compensated[:-trimind]


def apply_cic2_comp_psd(f_arr, psd, fs, trim=0.15):
    """
    NOTE it is not completely correct to apply this correction to
    power data, as any random scatter in the voltage data will
    be forced positive, which may slightly skew the results!

    Compensate for the response of the second-stage cascaded-
    integrator comb (CIC) filter as a function of frequency in
    a power spectral density measurement.

    Because the channel response drops off quickly towards and
    above Nyquist, results will be less trustworthy at high
    frequencies, and so this trims the last trim-percent of the
    datapoints at high frequency.

    f_arr : list or array of frequencies within one channel bw.
        Must be less than the demodulated channel sampling frequency.

    psd : list or array of amplitude spectral density values as
        a function of frequency

    fs : int or float
        Sampling rate of the demodulated timestream.

    trim : float
        Percent length of array to trim off at high frequencies,
        to avoid returning data where numerical uncertainties
        are higher in the compensation function.

    """
    compensated = np.asarray(psd) / (cic2_response(np.asarray(f_arr), fs)) ** 2

    trimind = int(np.ceil(trim * len(compensated)))
    return f_arr[:-trimind], compensated[:-trimind]


#################################
### SYSTEM TRANSFER FUNCTIONS ###
#################################


def get_available_transferfunctions():
    return


def get_current_transferfunction():
    return
