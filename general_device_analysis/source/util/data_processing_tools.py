"""
Functions to help with any pre-analysis data processing such as time stream cleaning.

"""

import numpy as np
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from scipy import signal


def pca_clean_timestream(data_array: np.ndarray, number_of_components: int, plot_components=False):
    """
    Function to identify principal components in simultaneous timestreams and remove them from the data.

    :param data_array: Numpy array containing N timestreams each with M data points of shape (N, M). Timestreams must be
     real.
    :param number_of_components: Number of principle components to remove from data.
    :param plot_components: Boolean to plot the principle components.
    """
    number_of_timestreams = data_array.shape[0]
    standard_deviation = np.nanstd(data_array, axis=1)
    standard_deviation[standard_deviation == 0] = 1
    data_array = (data_array.T / standard_deviation).T

    pca = PCA(n_components=number_of_components, svd_solver="full")
    print("Starting PCA")
    result = pca.fit(data_array.T)
    print("PCA Complete")
    loadings = pca.components_.T
    components = result.fit_transform(data_array.T)

    for component in range(number_of_components):
        print("cleaning component", component, end="\r")
        for timestream_index in range(number_of_timestreams):
            data_array[timestream_index, :] -= (
                components[:, component] * loadings[timestream_index, component]
            )
    data_array = (data_array.T * standard_deviation).T

    if plot_components:
        labels = []
        for n in range(1, number_of_components + 1):
            labels.append(f"Component {n}")
        plt.figure(figsize=(8, 6))
        plt.plot(components)
        plt.legend(labels)
        plt.show()

    return data_array


def get_principle_components(data_array: np.ndarray, number_of_components: int, plot_components=False):
    """
    Function to identify principal components in simultaneous timestreams return the array of components.

    :param data_array: Numpy array containing N timestreams each with M data points of shape (N, M). Timestreams must be
     real.
    :param number_of_components: Number of principle components to remove from data.
    :param plot_components: Boolean to plot the principle components.
    """
    number_of_timestreams = data_array.shape[0]
    standard_deviation = np.nanstd(data_array, axis=1)
    standard_deviation[standard_deviation == 0] = 1
    data_array = (data_array.T / standard_deviation).T

    pca = PCA(n_components=number_of_components, svd_solver="full")
    print("Starting PCA")
    result = pca.fit(data_array.T)
    print("PCA Complete")
    components = result.fit_transform(data_array.T)

    if plot_components:
        labels = []
        for n in range(1, number_of_components + 1):
            labels.append(f"Component {n}")
        plt.figure(figsize=(8, 6))
        plt.plot(components)
        plt.legend(labels)
        plt.show()

    return components


def apply_notch_filter(timestream_data, notch_frequency, notch_q, sampling_rate):
    
    b_notch, a_notch = signal.iirnotch(notch_frequency, notch_q, sampling_rate)

    freq, h = signal.freqz(b_notch, a_notch, fs = sampling_rate)
    
    notched_timestream_data = np.empty_like(timestream_data)
    if timestream_data.shape[0] != 1:
        for count, timestream in enumerate(timestream_data):
            notched_timestream = signal.filtfilt(b_notch, a_notch, timestream)
            notched_timestream_data[count] = notched_timestream
        return notched_timestream_data
    else:
        return  signal.filtfilt(b_notch, a_notch, timestream_data)
    


import scipy
def remove_psd_peaks(frequency_data, psd_data, f_min, f_max, prominence=0.5, show_plot=False, iterations=1):
    """
    Function to find peaks in a logarithmic psd spectra between frequency bounds and set those data points equal
    to the mean of the 10 values either side.
    """
    
    new_psd_data = np.copy(psd_data)
    for iteration in range(iterations):
        peak_indices = scipy.signal.find_peaks(np.log10(new_psd_data), prominence=prominence)[0]
        # Get peak indices within range:
        peak_indices_in_range = peak_indices[np.where((frequency_data[peak_indices] > f_min) & (frequency_data[peak_indices] < f_max))[0]]
        if peak_indices_in_range.size > 0:
            
            for peak_index in peak_indices_in_range:
                values_above = new_psd_data[peak_index + 1: peak_index + 11]
                values_below = new_psd_data[peak_index - 10: peak_index]
                new_psd_data[peak_index] = np.mean([values_above, values_below])
        else:
            print("No suitable peaks found")

    if show_plot:
        plt.figure(figsize=(10, 4))
        plt.plot(frequency_data, psd_data, alpha=0.5, label="Original Data")
        plt.plot(frequency_data, new_psd_data, alpha=0.5, label="Peak removed")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("PSD data")
        plt.yscale("log")
        plt.xscale("log")
        plt.legend()
        plt.show()
    
    return new_psd_data

    

def remove_timestream_anomalies_1d(timestream: np.ndarray, sampling_rate, save_anomaly_to_file="") -> np.ndarray:
    """
    Function to take a single 1D timestream array, normalise the timestream array by the mean then find
    where there are values larger than 6x the standard deviation and set these values to the mean of
    the timestream. Returns a copy of the original timestream with the corrections applied.

    :param timestream_data: Array of timestream data, should be a 1D or 2D un-complex data array.
    :param sampling_rate: Sampling rate of the timestream.
    :param save_anomaly_to_file: If not "", the anomaly timestream data will be saved to the file
    with the provided name. Must include .npz at the end.
    """

    corrected_timestream = np.copy(timestream)

    # Find where anomalies are:
    normalised_timestream = timestream/np.mean(timestream) - 1
    standard_deviation = np.std(normalised_timestream)
    anomaly_indices = np.where(np.abs(normalised_timestream) > 6*standard_deviation)[0]

    # If there are anomalies, sort into separate event groups if necessary and pick the one that corresponds to the largest anomaly.
    if anomaly_indices.size > 0:
        group_start_indices = np.where(np.diff(anomaly_indices) > 10)[0] + 1
        group_start_indices = np.insert(group_start_indices, 0, 0)

        max_anomaly_indices = []
        for count, group_start_index in enumerate(group_start_indices):

            if group_start_index == group_start_indices[-1]:
                current_anomaly_group = anomaly_indices[group_start_index:]
            else:
                current_anomaly_group = anomaly_indices[group_start_index: group_start_indices[count + 1]]

            max_index_in_group = np.argmax(np.abs(normalised_timestream)[current_anomaly_group])
            max_anomaly_indices.append(anomaly_indices[max_index_in_group])

        # Add correction to anomalous region:
        anomaly_data = []
        for anomaly_count, anomaly_index in enumerate(max_anomaly_indices):
            corrected_timestream[anomaly_index - 30:anomaly_index+30] = np.mean(timestream)
            anomaly_data.append(timestream[anomaly_index - 100: anomaly_index + 100])

        if save_anomaly_to_file != "":
            try:
                np.savez(
                    file=save_anomaly_to_file,
                    sampling_rate=sampling_rate,
                    anomaly_data=np.array(anomaly_data)
                )
            except:
                print("Failed to save anomaly data")
    
    return corrected_timestream


def remove_timestream_anomalies(timestream_data: np.ndarray, sampling_rate, save_anomaly_to_file="") -> np.ndarray:
    """
    Function to take a timestream array, take the gradient of the normalised timestream then find
    where the magnitude of this gradient is larger than 5 x the standard deviation and set these
    values to the mean of the timestream. Returns a copy of the original timestream with the
    corrections applied.

    :param timestream_data: Array of timestream data, should be a 1D or 2D un-complex data array.
    :param sampling_rate: Sampling rate of the timestream.
    :param save_anomaly_to_file: If not "", the anomaly timestream data will be saved to the file
    with the provided name. Must include .npz at the end.
    """

    full_corrected_timestream = np.copy(timestream_data)
    
    for repeat, timestream in enumerate(timestream_data):
        if save_anomaly_to_file != "":
            save_anomaly_to_file = save_anomaly_to_file + f"_{repeat}"
        corrected_timestream = remove_timestream_anomalies_1d(
            timestream=timestream,
            sampling_rate=sampling_rate,
            save_anomaly_to_file=save_anomaly_to_file
        )

        full_corrected_timestream[repeat] = corrected_timestream
  
    return full_corrected_timestream

    
