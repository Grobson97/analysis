"""
Functions for processing timestream data into PSD via the multiplication method.
"""

import math
import os
import re

import numpy as np
import scipy
import matplotlib.pyplot as plt

from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.models.noise.noise_sweep import NoiseSweep
from source.models.noise.simple_noise_packet_psd import SimpleNoisePacketPSD
from source.models.resonator_measurement.sweep_data import SweepData
from source.models.resonator_measurement.timestream_data import TimestreamData
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import resonator_noise_analysis_tools


def calculate_timestream_df(
    di_df_r: float, dq_df_r: float, timestream_i_array: np.ndarray, timestream_q_array: np.ndarray
):
    """
    Function to return the df/f array for a timestream using the multiplication method.

    :param di_df_r: dI/df at the resonant frequency.
    :param dq_df_r: dQ/df at the resonant frequency.
    :param timestream_i_array: Timestream I values.
    :param timestream_q_array: Timestream Q values.
    """

    bottom = di_df_r**2 + dq_df_r**2
    a = (timestream_i_array * di_df_r + timestream_q_array * dq_df_r) / bottom
    b = (-timestream_i_array * dq_df_r + timestream_q_array * di_df_r) / bottom

    return a + 1j * b


def get_single_tone_noise_packet_psd(
    directory: str,
    down_sample_psd: bool,
    down_sample_bins: int,
    plot_sweep_figure=True,
    save_figures=False,
    save_to_directory="..\\..\\Figures",
) -> NoisePacketPSD:
    """
    Function to calculate the psd of a noise packet obtained from a single tone measurement, and return an instance of
    NoisePacketPSD. Directory must contain sweep data "Sweep.fits" and high and low resolution timestreams for both
    on resonance and off resonance.
    If desired, the psd will be down sampled with exponential bins.

    :param directory: string path to directory containing noise packet.
    :param down_sample_psd: Boolean to down sample the psd data.
    :param down_sample_bins: Number of bins to use for exponential binning if down_sample_psd = True.
    :param plot_sweep_figure: Boolean to plot the sweep figure.
    :param save_figures: Boolean to save the figures.
    :param save_to_directory: String for directory path to save figures to.

    """

    sweep_filename = "Sweep.fits"  # This shouldn't change.
    sweep_file_path = os.path.join(directory, sweep_filename)
    # Factor to downsample psd data by.

    # Create instance of FitsSweep class:
    fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=sweep_file_path)
    sweep_data = FitsDataUnpackService.unpack_sweep_data(
        fits_sweep_data=fits_sweep.data
    )

    noise_sweep = NoiseSweep(
        dut=fits_sweep.header.dut,
        tone_name=fits_sweep.header.tone_name,
        temperature=fits_sweep.header.set_temperature,
        input_attenuation=fits_sweep.header.input_attenuation,
        sweep_data=sweep_data,
    )
    if plot_sweep_figure:
        noise_sweep.plot_sweep(
            save_figure=save_figures, save_to_directory=save_to_directory
        )

    # Loop to find the two sample frequencies to identify which is high res and which is low res.
    sample_frequencies = []
    for file_name in os.listdir(directory):
        if "TS" in file_name:
            string_search_result = re.search("TS_(.*)_Hz", file_name)
            sample_frequency = float(string_search_result.group(1))
            if sample_frequency not in sample_frequencies:
                sample_frequencies.append(int(sample_frequency))

    mean_psd_data_dict = {"Label": [], "Mean PSD Data": [], "Frequencies": []}
    for file_name in os.listdir(directory):
        if "TS" in file_name:
            timestream_file_path = os.path.join(directory, file_name)
            # Create instance of FitsCW
            fits_cw = FitsFileLoadService.load_fits_cw_file(
                file_path=timestream_file_path
            )
            cw_data = FitsDataUnpackService.unpack_cw_data(fits_cw)

            psd_label = ""
            if str(max(sample_frequencies)) in file_name:
                psd_label += "High Frequency, "
            else:
                psd_label += "Low Frequency, "
            if "ON" in file_name:
                psd_label += "On Res"
            else:
                psd_label += "Off Res"

            df_timestreams_array = []
            for index in range(int(cw_data.n_timestreams)):
                # Get I and Q timestream data:
                i = cw_data.timestream_i_data[index]
                q = cw_data.timestream_q_data[index]

                df_f = calculate_timestream_df(
                    di_df_r=noise_sweep.di_df_r,
                    dq_df_r=noise_sweep.dq_df_r,
                    timestream_i_array=i,
                    timestream_q_array=q,
                )
                df_timestreams_array.append(df_f / noise_sweep.sweep_f0)

            df_timestreams_array = np.array(df_timestreams_array)

            mean_psd, frequencies = resonator_noise_analysis_tools.get_mean_psd(
                number_of_timestreams=cw_data.n_timestreams,
                df_timestreams_array=df_timestreams_array,
                sample_rate=cw_data.sample_rate,
                down_sample=down_sample_psd,
                down_sample_bins=down_sample_bins,
            )

            mean_psd_data_dict["Label"].append(psd_label)
            mean_psd_data_dict["Mean PSD Data"].append(mean_psd)
            mean_psd_data_dict["Frequencies"].append(frequencies)

    on_res_low_index = mean_psd_data_dict["Label"].index("Low Frequency, On Res")
    off_res_low_index = mean_psd_data_dict["Label"].index("Low Frequency, Off Res")
    on_res_high_index = mean_psd_data_dict["Label"].index("High Frequency, On Res")
    off_res_high_index = mean_psd_data_dict["Label"].index("High Frequency, Off Res")

    noise_packet_psd = NoisePacketPSD(
        dut=fits_sweep.header.dut,
        tone_name=fits_sweep.header.tone_name,
        temperature=fits_sweep.header.set_temperature,
        input_attenuation=fits_sweep.header.input_attenuation,
        noise_sweep=noise_sweep,
        psd_on_res_low=mean_psd_data_dict["Mean PSD Data"][on_res_low_index],
        on_res_low_frequencies=mean_psd_data_dict["Frequencies"][on_res_low_index],
        psd_off_res_low=mean_psd_data_dict["Mean PSD Data"][off_res_low_index],
        off_res_low_frequencies=mean_psd_data_dict["Frequencies"][off_res_low_index],
        psd_on_res_high=mean_psd_data_dict["Mean PSD Data"][on_res_high_index],
        on_res_high_frequencies=mean_psd_data_dict["Frequencies"][on_res_high_index],
        psd_off_res_high=mean_psd_data_dict["Mean PSD Data"][off_res_high_index],
        off_res_high_frequencies=mean_psd_data_dict["Frequencies"][off_res_high_index],
    )

    return noise_packet_psd


# TODO refactor this function to stop repeating code.
def get_simple_noise_packet_psd(
    noise_sweep: NoiseSweep,
    on_res_timestream: TimestreamData,
    off_res_timestream: TimestreamData,
    down_sample_psd: bool,
    down_sample_bins: int,
) -> SimpleNoisePacketPSD:
    """
    Function to take a NoiseSweep object and it's corresponding on and off resonance Timestreams and calculate the PSD's
    returning a SimpleNoisePacketPSD.

    If desired, the psd will be down sampled with exponential bins.

    :param noise_sweep: NoiseSweep object containing the sweep information for the resonator.
    :param on_res_timestream: On resonance timestream data.
    :param off_res_timestream: Off resonance timestream data.
    :param down_sample_psd: Boolean to down sample the psd data.
    :param down_sample_bins: Number of bins to use for exponential binning if down_sample_psd = True.
    """

    # On resonance:
    on_res_df_timestreams_array = []
    for index in range(int(on_res_timestream.n_timestreams)):
        # Get I and Q timestream data:
        if on_res_timestream.n_timestreams == 1:
            i = on_res_timestream.timestream_i_data
            q = on_res_timestream.timestream_q_data
        else:
            i = on_res_timestream.timestream_i_data[index]
            q = on_res_timestream.timestream_q_data[index]

        df_f = calculate_timestream_df(
            di_df_r=noise_sweep.di_df_r,
            dq_df_r=noise_sweep.dq_df_r,
            timestream_i_array=i,
            timestream_q_array=q,
        )
        on_res_df_timestreams_array.append(df_f / noise_sweep.sweep_f0)

    on_res_df_timestreams_array = np.array(on_res_df_timestreams_array)

    on_res_mean_psd, on_res_frequencies = resonator_noise_analysis_tools.get_mean_psd(
        number_of_timestreams=on_res_timestream.n_timestreams,
        df_timestreams_array=on_res_df_timestreams_array,
        sample_rate=on_res_timestream.sample_rate,
        down_sample=down_sample_psd,
        down_sample_bins=down_sample_bins,
    )

    # Off resonance:
    off_res_df_timestreams_array = []
    for index in range(int(off_res_timestream.n_timestreams)):
        # Get I and Q timestream data:
        if off_res_timestream.n_timestreams == 1:
            i = off_res_timestream.timestream_i_data
            q = off_res_timestream.timestream_q_data
        else:
            i = off_res_timestream.timestream_i_data[index]
            q = off_res_timestream.timestream_q_data[index]

        df_f = calculate_timestream_df(
            di_df_r=noise_sweep.di_df_r,
            dq_df_r=noise_sweep.dq_df_r,
            timestream_i_array=i,
            timestream_q_array=q,
        )
        off_res_df_timestreams_array.append(df_f / noise_sweep.sweep_f0)

    off_res_df_timestreams_array = np.array(off_res_df_timestreams_array)

    off_res_mean_psd, off_res_frequencies = resonator_noise_analysis_tools.get_mean_psd(
        number_of_timestreams=off_res_timestream.n_timestreams,
        df_timestreams_array=off_res_df_timestreams_array,
        sample_rate=off_res_timestream.sample_rate,
        down_sample=down_sample_psd,
        down_sample_bins=down_sample_bins,
    )

    return SimpleNoisePacketPSD(
        dut=noise_sweep.dut,
        tone_name=noise_sweep.tone_name,
        temperature=noise_sweep.temperature,
        input_attenuation=noise_sweep.input_attenuation,
        sampling_rate=on_res_timestream.sample_rate,
        noise_sweep=noise_sweep,
        on_res_psd=on_res_mean_psd,
        on_res_frequencies=on_res_frequencies,
        off_res_psd=off_res_mean_psd,
        off_res_frequencies=off_res_frequencies,
    )
