"""
General functions for fitting to/analysing data. (Not resonator specific).
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


def fit_linear_regression(
    x_data: np.ndarray,
    y_data: np.ndarray,
    plot_graph=True,
    plot_title="",
    x_label="X Data",
    y_label="Y Data",
):
    """
    Function to fit a linear regression to x and y data and return the scipy LinregressResult object. See
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html for attributes and methods.

    :param x_data: X data to fit to.
    :param y_data: Y data to fit to.
    :param plot_graph: Boolean to
    :param x_label: Label for x axis of plot.
    :param y_label: Label for y axis of plot.
    :return: result: LinregressResult instance. The return value is an object with the following attributes:
        slope: float
            Slope of the regression line.
        intercept: float
            Intercept of the regression line.
        rvalue: float
            The Pearson correlation coefficient. The square of ``rvalue``
            is equal to the coefficient of determination.
        pvalue: float
            The p-value for a hypothesis test whose null hypothesis is
            that the slope is zero, using Wald Test with t-distribution of
            the test statistic. See `alternative` above for alternative
            hypotheses.
        stderr: float
            Standard error of the estimated slope (gradient), under the
            assumption of residual normality.
        intercept_stderr: float
            Standard error of the estimated intercept, under the assumption
            of residual normality.
    """

    result = stats.linregress(x_data, y_data)

    if plot_graph:
        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", label="Data")
        plt.plot(
            x_data,
            result.intercept + result.slope * x_data,
            color="r",
            label="y = mx + c fit with:\nm=%.2e\nc=%.2e\nR$^2$=%.2e\n$\sigma_m$=%.2e\n$\sigma_c$=%.2e"
            % (
                result.slope,
                result.intercept,
                result.rvalue**2,
                result.stderr,
                result.intercept_stderr,
            ),
        )
        plt.title(plot_title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.legend()
        plt.show()

    return result


def fit_polynomial(x_data: np.array, y_data: np.array, degree: int, plot_graph: True):
    """
    Function to fit a polynomial of a specified degree to x and y data and return the polynomial.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial


def fit_and_interpolate_polynomial(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and interpolate
    at a given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate a y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial(xi)


def fit_polynomial_find_minima(
    x_data: np.array, y_data: np.array, degree: int, plot_graph: True
):
    """
    Function to fit a polynomial of a specified degree to x and y data and locate it's minimum points.
    returns the poly1D polynomial fit object and the minimum points.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    critical_points = polynomial.deriv().r

    # Find real critical points within the x_data bounds:
    valid_critical_points = []
    for x in critical_points:
        if x.imag == 0 and np.min(x_data) < x.real < np.max(x_data):
            valid_critical_points.append(x)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(
            valid_critical_points,
            np.min(polynomial(valid_critical_points)),
            np.max(y_data),
            color="k",
        )
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial, valid_critical_points
