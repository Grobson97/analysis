U
    �#e�  �                   @   s�   d dl Zd dlmZ d dlmZ dejejd�dd	�Zej	ej	e
dd
�dd�Zej	ej	ee
dd�dd�Zej	ej	e
dd
�dd�ZdS )�    N)�statsT� �X Data�Y Data)�x_data�y_datac              
   C   s�   t �| |�}|r�tjdd� tj| |dddd� tj| |j|j|   dd|j|j|jd	 |j|j	f d
� t�
|� t�|� t�|� t��  t��  |S )aY  
    Function to fit a linear regression to x and y data and return the scipy LinregressResult object. See
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html for attributes and methods.

    :param x_data: X data to fit to.
    :param y_data: Y data to fit to.
    :param plot_graph: Boolean to
    :param x_label: Label for x axis of plot.
    :param y_label: Label for y axis of plot.
    :return: result: LinregressResult instance. The return value is an object with the following attributes:
        slope: float
            Slope of the regression line.
        intercept: float
            Intercept of the regression line.
        rvalue: float
            The Pearson correlation coefficient. The square of ``rvalue``
            is equal to the coefficient of determination.
        pvalue: float
            The p-value for a hypothesis test whose null hypothesis is
            that the slope is zero, using Wald Test with t-distribution of
            the test statistic. See `alternative` above for alternative
            hypotheses.
        stderr: float
            Standard error of the estimated slope (gradient), under the
            assumption of residual normality.
        intercept_stderr: float
            Standard error of the estimated intercept, under the assumption
            of residual normality.
    ��   �   �Zfigsize�none�o�Data)�	linestyle�marker�label�rzMy = mx + c fit with:
m=%.2e
c=%.2e
R$^2$=%.2e
$\sigma_m$=%.2e
$\sigma_c$=%.2e�   )�colorr   )r   Z
linregress�plt�figure�plotZ	interceptZslopeZrvalue�stderrZintercept_stderr�title�xlabel�ylabel�legend�show)r   r   �
plot_graphZ
plot_titleZx_labelZy_label�result� r    �^/home/kidslab/Documents/Analysis/general_device_analysis/source/util/general_analysis_tools.py�fit_linear_regression   s.    &���


r"   )r   r   �degreer   c                 C   s�   t �| ||�}t �|�}|r�t �t �| �t �| �t �| �d �}||�}tjdd� tj	| |ddddd� tj	||d	d
dt
|� d� t�d� t�d� t��  t��  t��  |S )aH  
    Function to fit a polynomial of a specified degree to x and y data and return the polynomial.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    �d   r   r   r   r   �br   �r   r   r   r   �-r   �Fit with polynomial of degree �r   r   r   �X�Y)�np�polyfit�poly1d�linspace�min�max�sizer   r   r   �strr   r   r   �tight_layoutr   )r   r   r#   r   �coefficients�
polynomial�x_points�y_fitr    r    r!   �fit_polynomialG   s(    

$
�

r9   )r   r   �xir#   r   c           	      C   s�   t �| ||�}t �|�}|r�t �t �| �t �| �t �| �d �}||�}tjdd� tj	| |ddddd� tj	||d	d
dt
|� d� tj|t �|�t �|�dd� tj||�t �| �t �| �dd� t�d� t�d� t��  t��  t��  ||�S )a�  
    Function to fit a polynomial of a specified degree to x and y data and interpolate
    at a given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate a y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    r$   r   r   r   r   r%   r   r&   r'   r   r(   r)   �k�r   r*   r+   )r,   r-   r.   r/   r0   r1   r2   r   r   r   r3   �vlinesZhlinesr   r   r   r4   r   )	r   r   r:   r#   r   r5   r6   r7   r8   r    r    r!   �fit_and_interpolate_polynomialj   s,    
$
�"

r>   c                 C   s0  t �| ||�}t �|�}|�� j}g }|D ]>}|jdkr*t �| �|j  k rZt �| �k r*n q*|�	|� q*|�r(t �
t �| �t �| �t �| �d �}	||	�}
tjdd� tj| |ddddd	� tj|	|
d
ddt|� d� tj|t �||��t �|�dd� t�d� t�d� t��  t��  t��  ||fS )a�  
    Function to fit a polynomial of a specified degree to x and y data and locate it's minimum points.
    returns the poly1D polynomial fit object and the minimum points.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    r   r$   r   r   r   r   r%   r   r&   r'   r   r(   r)   r;   r<   r*   r+   )r,   r-   r.   Zderivr   �imagr0   �realr1   �appendr/   r2   r   r   r   r3   r=   r   r   r   r4   r   )r   r   r#   r   r5   r6   Zcritical_pointsZvalid_critical_points�xr7   r8   r    r    r!   �fit_polynomial_find_minima�   s>    

0$
��

rC   )Tr   r   r   )Znumpyr,   Zmatplotlib.pyplotZpyplotr   Zscipyr   Zndarrayr"   Zarray�intr9   �floatr>   rC   r    r    r    r!   �<module>   s,       ��A$    �*   �