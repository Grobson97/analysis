"""
Custom functions to help with manipulating data arrays.... Currently quite niche, maybe redundant in the future or will
be added to.
"""

import numpy as np


def make_inequality_data_mask(
    data_array: np.ndarray,
    min_bound: float,
    max_bound: float,
    mask: np.ndarray or None,
) -> np.ndarray:
    """
    Function to create a boolean mask array for data values that are within the specified bounds. If no mask is given,
    the original mask used is a True array of the same shape as data_array. If a mask is given, this mask is used and
    indices that correspond to a data point outside the bounds is made False.

    :param data_array: Data array from which to check values are within the bounds.
    :param min_bound: Minimum value for inequality.
    :param max_bound: Maximum value for inequaity.
    :param mask: Numpy boolean array from which the function alter values to false. If no mask given, full array of True
    is used.
    """

    if mask is None:
        mask = np.full(data_array.shape, True)

    above_max_indices = np.where(data_array > max_bound)
    mask[above_max_indices] = False
    below_min_indices = np.where(data_array < min_bound)
    mask[below_min_indices] = False

    return mask
