"""
Functions for analysing IQ data/S21 from resonators.
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks
from lmfit import Model
from lmfit import Parameters
from source.models import fitting
from scipy.special import j0
from scipy.special import digamma
from scipy.constants import (e, pi, k, hbar)



def fit_non_linear_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    plot_graph=True,
    plot_db=True,
    save_figure=False,
    save_name="",
    plot_title="",
) -> dict:
    """
    Function to fit a skewed lorentzian model to S21 magnitude data.\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qr"] = [value, error].
    Result dictionary keys: dict_keys(['f0', 'qr', 'qc_real', 'qc_imag', 'qi', 'anl']).
    Where the mean percentage uncertainty is just a single float value.

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    fit, fitparams, result, guess, baseline = fitting.fit_nonlinear_resonator(
        f=frequency_array, z=data_array, offres_linewidths=5, baseline_polyorder=1
    )

    result_dict = {}

    result_dict["f0"] = [fitparams["f0"].value, fitparams["f0"].stderr]
    result_dict["qr"] = [fitparams["Qr"].value, fitparams["Qr"].stderr]
    result_dict["qc_real"] = [fitparams["Qe_real"].value, fitparams["Qe_real"].stderr]
    result_dict["qc_imag"] = [fitparams["Qe_imag"].value, fitparams["Qe_imag"].stderr]
    result_dict["qi"] = [fitparams["Qi_dummy"].value, fitparams["Qi_dummy"].stderr]
    result_dict["anl"] = [fitparams["anl"].value, fitparams["anl"].stderr]

    if plot_graph or save_figure:

        if plot_db:
            y_label = "S21 Magnitude (plot_db)"
            data_array = 20 * np.log10(np.abs(data_array))
            fit = 20 * np.log10(np.abs(fit))

        if not plot_db:
            y_label = "S21 Magnitude"

        figure = plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            data_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            frequency_array,
            fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nQi=%0.2e\nf0=%0.2e"
            % (
                result_dict["qr"][0],
                result_dict["qc_real"][0],
                result_dict["qi"][0],
                result_dict["f0"][0],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()

        if save_figure:
            plt.savefig(save_name)
            if plot_graph == False:
                plt.close(figure)
        if plot_graph:
            plt.show()

    return result_dict, fit


########################################################################################################################


def lorentzian_s21(
    frequency_array: np.ndarray, qr: float, qc: float, f0: float, a=1.0
) -> np.array:
    """
    Function defining the model of amp lorentzian resonance in an S21 array.

    :param np.array frequency_array: Array of N frequency points.
    :param float qr: Resonator quality factor.
    :param float qc: Coupling quality factor.
    :param float f0: Resonant frequency.
    :param float a: Amplitude/ background level.
    """
    x = (frequency_array - f0) / f0
    b = 2.0 * qr * x
    real_s21 = a - (qr / qc) * (1.0 / (b**2 + 1.0))
    imaginary_s21 = (qr / qc) * (b / (b**2 + 1.0))
    magnitude_s21 = np.sqrt(real_s21**2 + imaginary_s21**2)
    return magnitude_s21


def fit_lorentzian_s21(
    frequency_array: np.array,
    s21_array: np.array,
    qr_guess: float,
    qc_guess: float,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> list:
    """
    Function to fit qr, qc and f0 to amp  somewhat simple lorentzian resonance in S21.\n
    Returns fit parameters as amp list: [qr, qc, f0, amp].

    :param frequency_array: Array of N frequency points.
    :param s21_array: Complex array of N S21 data points.
    :param qr_guess: Guess of resonators quality factor.
    :param qc_guess: Guess of coupling quality factor.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S21 in plot_db.
    :param plot_title: Title to give to the plot. Default is an empty string.

    """

    s21_mag_array = np.abs(s21_array)

    f0_index = np.argmin(s21_mag_array)  # index of minimum point.
    resonance_data_points = (
        300  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(lorentzian_s21)

    # Define Parameters:
    params = Parameters()
    params.add("qc", value=qc_guess, min=0, vary=True)
    params.add("qr", value=qr_guess, expr="qc")
    params.add(
        "f0", value=frequency_array[np.argmin(s21_mag_array)], min=0, vary=True
    )  # Guesses f0 is min of data_array
    params.add(
        "amp", value=np.mean(np.abs(s21_array)), vary=True
    )  # Guesses background level as mean of data_array.

    result = l_model.fit(
        data=np.abs(s21_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    best_fit_parameters = [
        result.best_values["qr"],
        result.best_values["qc"],
        result.best_values["f0"],
        result.best_values["amp"],
    ]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=100000,
        )

        lorentzian_fit = lorentzian_s21(
            frequency_array=f_array,
            qr=result.best_values["qr"],
            qc=result.best_values["qc"],
            f0=result.best_values["f0"],
            a=result.best_values["amp"],
        )

        y_label = "S21 Magnitude"
        if plot_db:
            y_label = "S21 Magnitude  (plot_db)"
            s21_mag_array = 20 * np.log10(s21_mag_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            s21_mag_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nf0=%0.2e"
            % (
                result.best_values["qr"],
                result.best_values["qc"],
                result.best_values["f0"],
            ),
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return best_fit_parameters


########################################################################################################################


def simple_lorentzian(
    frequency_array: np.array, a: float, bw: float, f0: float, c: float
) -> np.array:
    """
    Function defining amp very simple model of amp lorentzian.
    Returns

    :param np.array frequency_array: Array of N frequency points.
    :param float a: Amplitude.
    :param float bw: Full width half maximum.
    :param float f0: Resonant frequency.
    :param float c: Offset in y.
    """
    x = (frequency_array - f0) / (0.5 * bw)
    lorentzian = a / (1 + x**2) + c
    return lorentzian


def fit_simple_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    bw_guess: np.array,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> dict:
    """
    Function to fit amp very simple lorentzian bw, f0, amp, amp to amp resonance in S21.\n
    Returns fit parameters as amp list: [q, bw, f0, amp, amp].

    :param frequency_array: Array of N frequency points.
    :param data_array: Complex array of N S21 data points.
    :param bw_guess: Guess for the bandwidth of the lorentzian.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    magnitude_array = np.abs(data_array)

    f0_index = np.argmax(magnitude_array)  # index of minimum point.
    resonance_data_points = (
        500  # define number of data points either side of resonance to sample
    )
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points
    if start_index < 0:
        start_index = 0
    if end_index > frequency_array.size:
        end_index = -1

    # Define model:
    l_model = Model(simple_lorentzian)

    # Define Parameters:
    params = Parameters()
    params.add("bw", value=bw_guess, vary=True)
    params.add("amp", value=np.max(magnitude_array), vary=True)
    params.add("f0", value=frequency_array[np.argmax(magnitude_array)], vary=True)
    # Guesses background level as mean of data_array.
    params.add("c", value=np.mean(np.abs(magnitude_array)), vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(magnitude_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    result_dict["q"] = [
        result_dict["f0"][0] / result_dict["bw"][0],
        (result_dict["f0"][0] / result_dict["bw"][0])
        * np.sqrt(
            (result_dict["f0"][1] / result_dict["f0"][0]) ** 2
            + (result_dict["bw"][1] / result_dict["bw"][0]) ** 2
        ),
    ]

    if plot_graph:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=10000,
        )

        lorentzian_fit = simple_lorentzian(
            frequency_array=f_array,
            a=result_dict["amp"][0],
            bw=result_dict["bw"][0],
            f0=result_dict["f0"][0],
            c=result_dict["c"][0],
        )

        y_label = "S21 Magnitude"
        if plot_db:
            y_label = "S21 Magnitude  (plot_db)"
            magnitude_array = 20 * np.log10(magnitude_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)

        plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            magnitude_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nq=%0.2e" % result_dict["q"][0],
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()
        plt.show()

    return result_dict


########################################################################################################################


def skewed_lorentzian(
    frequency_array: np.ndarray,
    f0: float,
    qi: float,
    qc_real: float,
    qc_imag: float,
    amp: float,
) -> np.ndarray:
    """
    Model of a skewed lorentzian using a complex qc.

    :param frequency_array: Array of N frequency points.
    :param f0: Resonant frequency
    :param qi: Loss quality factor
    :param qc_real: Real coupling quality factor
    :param qc_imag: Imaginary coupling quality factor
    :param amp: Amplitude
    :return:
    """

    x = (frequency_array - f0) / f0
    qc = (abs(qc_real + 1j * qc_imag) ** 2 / qc_real,)
    qr = 1.0 / ((1.0 / qc[0]) + (1.0 / qi))
    return abs(amp * (1 - qr / (qc_real + 1j * qc_imag) / (1 + 2j * qr * x)))


def fit_skewed_lorentzian(
    frequency_array: np.array,
    data_array: np.array,
    qc_guess: float,
    qi_guess: float,
    f0_guess: float or None,
    fit_fraction: float,
    normalise=True,
    plot_graph=True,
    plot_db=True,
    plot_title="",
    save_figure=False,
    save_name="",
) -> dict:
    """
    Function to fit a skewed lorentzian model to S21 magnitude data.\n
    Returns dictionary of fit parameters with standard errors, e.g. result_dict["qr"] = [value, error].
    Result dictionary keys: dict_keys(['f0', 'qr', 'qc_real', 'qc_imag', 'amp', 'qc', 'qi', 'mean_3_sigma_percentage_uncertainty']).
    Where the mean percentage uncertainty is just a single float value.

    :param frequency_array: Array of N frequency points.
    :param data_array: Magnitude array of N S21 data points.
    :param qc_guess: Guess for the qr of the lorentzian.
    :param qi_guess: Guess for the loss q of the lorentzian.
    :param f0_guess: Guess for the resonant frequency.
    :param fit_fraction: Fraction of data points to fit to (between 0 and 1). 1 being 100% of the data.
    :param normalise: Boolean to normalise the data_array array before fitting.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.

    """

    # TODO Add logic to identify colliding kids.
    if normalise:
        data_array = data_array / np.max(data_array)

    f0_index = np.argmin(data_array)  # index of minimum point.
    resonance_data_points = round(
        (len(data_array) * fit_fraction) / 2
    )  # define number of data points either side of resonance to sample
    start_index = f0_index - resonance_data_points
    end_index = f0_index + resonance_data_points

    if start_index < 0:
        end_index -= start_index  # Add clipped data points to end index
        start_index = 0

    if end_index > frequency_array.size - 1:
        start_index -= (
            end_index - frequency_array.size - 1
        )  # Add clipped data points to start index
        end_index = -1

    # Define model:
    l_model = Model(skewed_lorentzian)

    # Allow for user input f0_guess
    f0_first_guess = frequency_array[f0_index]
    if f0_guess is not None:
        f0_first_guess = f0_guess

    # Define Parameters:
    params = Parameters()
    params.add("f0", value=f0_first_guess, vary=True)
    params.add("qi", value=qi_guess, vary=True, min=1)
    params.add("qc_real", value=qc_guess, vary=True, min=1)
    params.add("qc_imag", value=100.0, vary=True)
    # Guesses background level as mean of data_array.
    params.add("amp", value=data_array[-1], vary=True)

    # l_model.fit
    result = l_model.fit(
        data=np.abs(data_array)[start_index:end_index],
        params=params,
        frequency_array=frequency_array[start_index:end_index],
    )

    # uncertainty interval to 3 sigma:
    uncertainty = result.eval_uncertainty(sigma=3)
    upper_uncertainty_line = result.best_fit + uncertainty
    lower_uncertainty_line = result.best_fit - uncertainty

    mean_percentage_confidence = np.mean((uncertainty / result.best_fit) * 100)
    # print(f"Percentage uncertainty: {mean_percentage_confidence}")

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]

    result_dict["mean_3_sigma_percentage_uncertainty"] = mean_percentage_confidence

    try:
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] + 1j * result_dict["qc_imag"][0]) ** 2
            / result_dict["qc_real"][0],
            np.sqrt(
                result_dict["qc_real"][1] ** 2
                + (
                    2
                    * result_dict["qc_imag"][1]
                    * result_dict["qc_imag"][0]
                    / result_dict["qc_real"][0]
                )
                ** 2
                + result_dict["qc_real"][1]
                * result_dict["qc_imag"][0] ** 2
                / result_dict["qc_real"][0] ** 2
            ),
        ]
    except TypeError:
        print(
            f"{plot_title} - Possible error: The fit value for Qc has None type standard errors."
        )
        result_dict["qc"] = [
            abs(result_dict["qc_real"][0] ** 2 + 1j * result_dict["qc_imag"][0])
            / result_dict["qc_real"][0],
            None,
        ]

    qr = 1 / ((1 / result_dict["qc"][0]) + (1 / result_dict["qi"][0]))
    try:
        result_dict["qr"] = [
            qr,
            np.sqrt(
                qr**4
                * (
                    result_dict["qc"][1] ** 2 / result_dict["qc"][0] ** 4
                    + result_dict["qi"][1] ** 2 / result_dict["qi"][0] ** 4
                )
            ),
        ]
    except TypeError:
        print(
            f"{plot_title} - Possible error: The fit value for Qi or Qc has None type standard errors."
        )
        result_dict["qr"] = [qr, None]

    if plot_graph or save_figure:

        f_array = np.linspace(
            start=frequency_array[start_index],
            stop=frequency_array[end_index],
            num=1000000,
        )

        lorentzian_fit = skewed_lorentzian(
            frequency_array=f_array,
            f0=result_dict["f0"][0],
            qi=result_dict["qi"][0],
            qc_real=result_dict["qc_real"][0],
            qc_imag=result_dict["qc_imag"][0],
            amp=result_dict["amp"][0],
        )

        if plot_db:
            y_label = "S21 Magnitude (plot_db)"
            data_array = 20 * np.log10(data_array)
            lorentzian_fit = 20 * np.log10(lorentzian_fit)
            uncertainty = 20 * np.log10(uncertainty)
            upper_uncertainty_line = 20 * np.log10(upper_uncertainty_line)
            lower_uncertainty_line = 20 * np.log10(lower_uncertainty_line)

        if not plot_db:
            y_label = "S21 Magnitude"

        figure = plt.figure(figsize=(8, 6))
        plt.plot(
            frequency_array,
            data_array,
            linestyle="none",
            marker="o",
            markersize=2,
            color="b",
            label="Data",
        )
        plt.plot(
            f_array,
            lorentzian_fit,
            linestyle="-",
            color="r",
            label="Best fit:\nqr=%0.2e\nqc=%0.2e\nQi=%0.2e\nf0=%0.2e"
            % (
                result_dict["qr"][0],
                result_dict["qc"][0],
                result_dict["qi"][0],
                result_dict["f0"][0],
            ),
        )
        plt.fill_between(
            frequency_array[start_index:end_index],
            upper_uncertainty_line,
            lower_uncertainty_line,
            color="#888888",
            label="3$\sigma$ Confidence Interval",
        )
        plt.xlabel("Frequency (Hz)")
        plt.ylabel(y_label)
        plt.title(plot_title)
        plt.legend()
        plt.tight_layout()

        if save_figure:
            plt.savefig(save_name)
            if plot_graph == False:
                plt.close(figure)
        if plot_graph:
            plt.show()
        plt.show()

    return result_dict


########################################################################################################################


def interpolate_poly_fit(
    x_data: np.array, y_data: np.array, xi: float, degree: int, plot_graph: True
):
    """
    Function to fit amp polynomial of amp specified degree to x and y data and interpolate
    at amp given value.

    :param np.array x_data: Data for x values.
    :param np.array y_data: Data for y values.
    :param float xi: x value at which to interpolate amp y value.
    :param int degree: Degree of polynomial to use for fitting.
    :param bool plot_graph: Boolean to plot the graph of the fit.
    """

    coefficients = np.polyfit(x_data, y_data, degree)
    polynomial = np.poly1d(coefficients)

    if plot_graph:
        x_points = np.linspace(np.min(x_data), np.max(x_data), np.size(x_data) * 100)
        y_fit = polynomial(x_points)

        plt.figure(figsize=(8, 6))
        plt.plot(x_data, y_data, linestyle="none", marker="o", color="b", label="Data")
        plt.plot(
            x_points,
            y_fit,
            linestyle="-",
            color="r",
            label="Fit with polynomial of degree " + str(degree),
        )
        plt.vlines(xi, np.min(y_data), np.max(y_data), color="k")
        plt.hlines(polynomial(xi), np.min(x_data), np.max(x_data), color="k")
        plt.xlabel("X")
        plt.ylabel("Y")
        plt.legend()
        plt.tight_layout()
        plt.show()

    return polynomial(xi)


def is_good_kid(
    frequency_array: np.array,
    iq_array: np.array,
    qc_guess: float,
    qi_guess: float,
    f0_guess: float or None, # type: ignore
    fit_fraction: float,
    percentage_3_sigma_uncertainty_threshold: float,
    min_qr: float,
    max_qr: float,
    normalise=True,
    plot_graph=True,
    plot_db=True,
    plot_title="",
) -> tuple:
    """
    Function to fit a skewed lorentzian to a resonator sweep and determine whether it is a good kid or a bad kid.
    Returns a tuple of a boolean if kid is good or not and the reason. (True/False, Reason).

    :param frequency_array: Array of N frequency points.
    :param iq_array: complex IQ array of N sweep data points.
    :param qc_guess: Guess for the qr of the lorentzian.
    :param qi_guess: Guess for the loss q of the lorentzian.
    :param f0_guess: Guess for the resonant frequency.
    :param fit_fraction: Fraction of data points to fit to (between 0 and 1). 1 being 100% of the data.
    :param percentage_3_sigma_uncertainty_threshold: Value of the mean percentage 3 sigma uncertainty over which a KID is labelled as bad.
    :param min_qr: Minimum allowed quality factor of resonator.
    :param max_qr: Maximum quality factor of resonator.
    :param normalise: Boolean to normalise the data_array array before fitting.
    :param plot_graph: Boolean command to plot amp graph of the fit.
    :param plot_db: Boolean command to plot S Parameter in plot_db.
    :param plot_title: Title to give to plot. Default is an empty string.)
    """

    magnitude_array = np.abs(iq_array)

    reason = "Good KID"  # Variable to store the reason the kid has been identified as bad.

    # Ensure there's more than one peak in sweep
    peaks, _ = find_peaks(x=-20*np.log10(magnitude_array), prominence=0.5)
    if len(peaks) == 0:
        reason = "No peak(s) found"
        return False, reason
    if len(peaks) > 1:
        reason = "More than one peak"
        return False, reason
    if is_bifurcated(iq_array=iq_array):
        print("Bad KID: ")
        reason = "Bifurcated"
        return False, reason

    fit_result = fit_skewed_lorentzian(
        frequency_array=frequency_array,
        data_array=magnitude_array,
        qc_guess=qc_guess,
        qi_guess=qi_guess,
        f0_guess=f0_guess,
        fit_fraction=fit_fraction,
        normalise=normalise,
        plot_graph=plot_graph,
        plot_db=plot_db,
        plot_title=plot_title,
    )

    if (
        fit_result["mean_3_sigma_percentage_uncertainty"]
        > percentage_3_sigma_uncertainty_threshold
    ):
        reason = "Fit uncertainty is too large"
        return False, reason
    if fit_result["qr"][0] > max_qr:
        reason = "Qr is too large"
        return False, reason
    if fit_result["qr"][0] < min_qr:
        reason = "Qr is too small"
        return False, reason
    else:  # If Nne of the above conditions are met then KID is good.
        return True, reason


def is_bifurcated(iq_array: np.ndarray) -> bool:
    """
    Function to check if the KID is bifurcated by searching for discontinuities in the IQ circle. Identified as a
    large change in dI and dQ relative to the mean dI, dQ.

    :param iq_array: Complex numpy array of I and Q values from a NoiseSweep.
    """

    dq_magnitude = np.abs(np.diff(np.real(iq_array)))
    di_magnitude = np.abs(np.diff(np.imag(iq_array)))

    discontinuity_index_q = np.where(dq_magnitude > 50 * np.mean(dq_magnitude))
    discontinuity_index_i = np.where(di_magnitude > 50 * np.mean(di_magnitude))

    bifurcated = False
    if (discontinuity_index_q[0].size > 0) or (discontinuity_index_i[0].size > 0):
        bifurcated = True

    return bifurcated



def calculate_s_2(
        temperature: float,
        frequency: float,
        energy_gap: float
) -> float:
    """
    Calculates the S2 parameter from mattis bardeen.

    :param critical_temperature: Critical temperature of superconductor.
    :param temperature: Bath temperature of superconductor.
    :param frequency: Frequency of the microwave readout.
    :return:
    """

    xi = (hbar * pi * frequency) / (k * temperature)

    term_1 = np.sqrt((2 * energy_gap) / (pi * k * temperature))
    term_2 = np.exp(-xi) * j0(xi)

    return 1 + term_1 * term_2


def calculate_sweep_frequency_derivatives(
    sweep_i_array: np.ndarray, sweep_q_array: np.ndarray, sweep_f_array: np.ndarray
):
    """
    Function to calculate the sweep data derivatives used to calculate the df/f for a noise packet via the
    multiplication method.\n
    Returns as an unpackable:\n
    di/df numpy array, dq/df numpy array, di^2 + dq^2 numpy array, f0 index of sweep, f0 value.
    \n
    :param sweep_i_array: I data from sweep array.
    :param sweep_q_array: Q data from sweep array.
    :param sweep_f_array: f data from sweep array.
    """

    # obtain derivatives of sweep data with respect to frequency
    sweep_di_df = np.diff(sweep_i_array) / np.diff(sweep_f_array)
    sweep_dq_df = np.diff(sweep_q_array) / np.diff(sweep_f_array)

    sweep_di2_plus_dq2 = sweep_di_df**2 + sweep_dq_df**2
    sweep_f0_index = np.where(sweep_di2_plus_dq2 == np.max(sweep_di2_plus_dq2))[0][0]
    f0 = sweep_f_array[sweep_f0_index]

    return sweep_di_df, sweep_dq_df, sweep_di2_plus_dq2, sweep_f0_index, f0


def theory_ffs_tls(
    temperature: np.ndarray,
    frequency: float,
    f_delta_0: float,
):

    tls_term_1 = (f_delta_0/pi) * (np.real(digamma(0.5 - (hbar*frequency)/(1j*k*temperature))) - np.log((hbar*frequency)/(k*temperature)))

    return tls_term_1

def theory_ffs(
    temperature: np.ndarray,
    alpha: float,
    energy_gap: float,
    single_spin_dos: float,
    frequency: float,
    f_delta_0: float,
    zero_offset: float,
):
    """
    Calculates the expected fractional frequency shift of a resonator with a given energy gap,
    kinetic inductance fraction (alpha), single spin density of states and frequency at zero temp.
    """

    # Standard term:
    s_2 = calculate_s_2(temperature=temperature, frequency=frequency, energy_gap=energy_gap)
    n_qp =  2 * single_spin_dos * np.sqrt(2*pi*k*temperature*energy_gap) * np.exp(-energy_gap/(k*temperature))

    #TLS term:
    tls_term = theory_ffs_tls(
        temperature=temperature,
        frequency=frequency,
        f_delta_0=f_delta_0,
    )

    return zero_offset + -1 * (alpha * s_2 * n_qp) / (4 * energy_gap * single_spin_dos)  + tls_term


def fit_ffs(
    temperature: np.ndarray,
    ffs_data: np.ndarray,
    alpha_guess: float,
    f_delta_0_guess: float,
    energy_gap: float,
    single_spin_dos: float,
    frequency: float,
    plot_fit=False
):
    """
    Fits the fractional frequency.
    """
    
    # Define model:
    model = Model(theory_ffs)

    # Define Parameters:
    params = Parameters()
    params.add("alpha", value=alpha_guess, vary=True)
    params.add("f_delta_0", value=f_delta_0_guess, vary=True)
    params.add("energy_gap", value=energy_gap, vary=False)
    params.add("single_spin_dos", value=single_spin_dos, vary=False)
    params.add("frequency", value=frequency, vary=False)
    params.add("zero_offset", value=ffs_data[0], vary=True)

    # l_model.fit
    result = model.fit(
        data=ffs_data,
        params=params,
        temperature=temperature,
    )

    result_dict = {}
    for name, parameter in result.params.items():
        result_dict[name] = [parameter.value, parameter.stderr]
        
    
    if plot_fit:
        plt.figure()
        plt.plot(temperature, ffs_data, linestyle="none", marker="o")
        plt.plot(temperature, result.best_fit, linestyle="--", color="k")
        plt.show()

    return result_dict
    

