# General Device Analysis

This is a self-contained project directory to analyse data taken with a VNA and single tone homodyne readout operated via the Labview Automeasure program, or data taken by a multitone readout using the Hidfmux Ice Board system.

Script specific details for use should be found in the preamble of each script. Below in "Data Structure" is a guideline on the expected data structure for each readout system.

## Setup

1. Pull the github project "Analysis".

2. Create a python virtual environment within the general_device_analysis directory
   ```
   python -m venv venv
   ```
3. Install the project requirements. After activating the venv in the command line:
   ```
   pip install -r /path/to/requirements.txt
   ```
4. Run whichever example script you like, or make your own.

## Data Structure

In many scripts a directory is required rather than a specific data file.

### VNA / Homodyne Automat data:

The following is the data structure and naming conventions required for all data files from a measurement for a test device named "test_device".

The term Experiment is used to describe a set of measurements taken on a device. This might be a full data set containing a continuous vna sweep, tones list, segmented vna sweeps and noise packets at various temperatures and powers:
```
test_device
-> Experiment <date>
   -> KID<tone number>
      -> Set Temperature 1
         -> Set Power 1
            -> Sweep.fits
            -> TS_<low sampling rate>_Hz_OFF_RES_x<repeats>.fits
            -> TS_<low sampling rate>_Hz_ON_RES_x<repeats>.fits
            -> TS_<high sampling rate>_Hz_OFF_RES_x<repeats>.fits
            -> TS_<high sampling rate>_Hz_ON_RES_x<repeats>.fits
         -> Set Power 2
            -> Sweep.fits
            -> TS_<low sampling rate>_Hz_OFF_RES_x<repeats>.fits
            -> TS_<low sampling rate>_Hz_ON_RES_x<repeats>.fits
            -> TS_<high sampling rate>_Hz_OFF_RES_x<repeats>.fits
            -> TS_<high sampling rate>_Hz_ON_RES_x<repeats>.fits
   -> VNA_Sweeps
      -> S21_Continuous_BTEMP<set temperature>_mK_POW<BB power>_dB_ATT<attenuation>.fits  (An example continuous VNA sweep)
      -> S21_Continuous_BTEMP<set temperature>_mK_POW<BB power>_dB_ATT<attenuation>.fits  (Another example continuous VNA sweep)
      -> S21_Segmented_BTEMP<set temperature>_mK_POW<BB power>_dB_ATT<attenuation>.fits  (An example segmented VNA sweep for the full tones list)
      -> TonesList_<device_name>.txt
      -> TonesList_<device_name>.fits
      -> segmented_multi_setting_sweeps   (Directory containing segmented sweeps for the KIDs measured in the automat)
         -> S21_Segmented_BTEMP0100_mK_POW0.0_dB_ATT42.0_dB.fits
         -> S21_Segmented_BTEMP0100_mK_POW0.0_dB_ATT42.0_dB_1.fits  (repeat)
         -> S21_Segmented_BTEMP0140_mK_POW0.0_dB_ATT44.0_dB.fits
         -> S21_Segmented_BTEMP0140_mK_POW0.0_dB_ATT44.0_dB_1.fits  (repeat)
```
