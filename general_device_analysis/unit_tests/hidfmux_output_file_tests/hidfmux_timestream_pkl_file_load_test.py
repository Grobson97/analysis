import os

import numpy as np
from matplotlib import pyplot as plt

from source.services.hidfmux_pkl_file_load_service import HidfmuxFileLoadService
from source.models.hidfmux.hidfmux_multi_sweep import HidfmuxMultiSweep


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Ice_board_data\140mK_0p005_amp",
    tone_to_plot="K171",
):

    timestream_file_name = "noise_timestream_" + tone_to_plot + ".pkl"
    multisweep_file_name = "res_detail_multi_sweep.pkl"

    multi_sweep_path = os.path.join(directory, multisweep_file_name)
    multisweep = HidfmuxFileLoadService.load_pkl_multi_sweep_file(multi_sweep_path)

    timestream_path = os.path.join(directory, timestream_file_name)

    (
        on_res_timestream,
        off_res_timestream,
    ) = HidfmuxFileLoadService.load_pkl_timestream_file(
        timestream_file_path=timestream_path,
        multisweep=multisweep,
        tone_name=tone_to_plot,
    )

    figure, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(
        on_res_timestream.timestream_time_data,
        on_res_timestream.timestream_i_data,
        label="On-Res",
    )
    axes[0].plot(
        off_res_timestream.timestream_time_data,
        off_res_timestream.timestream_i_data,
        label="Off-Res",
    )
    axes[0].set_ylabel("I (V)")

    axes[1].plot(
        on_res_timestream.timestream_time_data,
        on_res_timestream.timestream_q_data,
        label="On-Res",
    )
    axes[1].plot(
        off_res_timestream.timestream_time_data,
        off_res_timestream.timestream_q_data,
        label="Off-Res",
    )
    axes[1].set_ylabel("Q (V)")
    axes[1].set_xlabel("Time (s)")
    axes[1].legend()
    plt.show()


if __name__ == "__main__":
    main()
