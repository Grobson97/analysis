import os

import numpy as np
from matplotlib import pyplot as plt

from source.services.hidfmux_pkl_file_load_service import HidfmuxFileLoadService
from source.models.hidfmux.hidfmux_multi_sweep import HidfmuxMultiSweep


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\slim_v2_anlw001_idc_stepdown\Ice_board_data\140mK_0p005_amp",
    file_name="multi_sweep.pkl",
    tone_to_plot="K171",
):
    multi_sweep_path = os.path.join(directory, file_name)

    multi_sweep = HidfmuxFileLoadService.load_pkl_multi_sweep_file(multi_sweep_path)

    tone_sweep_data = multi_sweep.get_tone_sweep_data(tone=tone_to_plot)
    plt.figure(figsize=(8, 6))
    plt.plot(
        tone_sweep_data.frequency_array * 1e-9,
        np.abs(tone_sweep_data.get_iq_array()),
    )
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("|IQ|")
    plt.show()


if __name__ == "__main__":
    main()
