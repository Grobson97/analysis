import os.path
import re

import numpy as np
import matplotlib.pyplot as plt

from source.models.noise.noise_sweep import NoiseSweep
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.services.hidfmux_pkl_file_load_service import HidfmuxFileLoadService
from source.util import noise_multiplication_method_tools


multi_tone_directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Ice_board_data\140mK_0p001_amp"
dut = "slim_v2_anlw001_ppcaps"
temperature = 0.14
save_figures = False

multi_tone_timestreams = HidfmuxFileLoadService.load_multi_tone_pkl_timestreams(
    timestream_directory=multi_tone_directory
)

print("timestreams complete")
