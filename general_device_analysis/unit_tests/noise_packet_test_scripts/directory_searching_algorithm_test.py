import os.path

from source.services.fits_file_load_service import FitsFileLoadService


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Data\20230816_Dark_Data_Prelim\KID_K172",
    save_figures=True,
):

    temperature_directories = os.listdir(directory)

    power_directories = []
    for temperature_directory in temperature_directories:
        power_directory_list = os.listdir(
            os.path.join(directory, temperature_directory)
        )

        for power_directory in power_directory_list:
            if power_directory not in power_directories:
                power_directories.append(power_directory)

    noise_packet_list = []
    temperature_array = []
    input_attenuation_array = []

    for temperature_directory in temperature_directories:
        temperature = float(temperature_directory[16:19])
        for power_directory in power_directories:
            power = float(power_directory[16:-2])
            path = os.path.join(directory, temperature_directory, power_directory)

            print(f"Getting data for {temperature}mK, {power}dB")

            sweep_filename = "Sweep.fits"  # This shouldn't change.
            sweep_file_path = os.path.join(path, sweep_filename)

            try:
                # Create instance of FitsVNAS21Sweep class:
                fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(
                    file_path=sweep_file_path
                )
            except:
                print(f"No data for {temperature}mK, {power}dB")
            else:
                print(f"Data for {temperature}mK, {power}dB complete")


if __name__ == "__main__":
    main()
