import os
import re

import numpy as np
from matplotlib import pyplot as plt

from source.models.noise.noise_sweep import NoiseSweep
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService

directory = r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Data\20230830_Dark_Data_Prelim\KID_K168\Set_Temperature_180_mK\Set_Attenuation_48dB"
sweep_filename = "Sweep.fits"  # This shouldn't change.
sweep_file_path = os.path.join(directory, sweep_filename)
# Factor to downsample psd data by.

# Create instance of FitsVNAS21Sweep class:
fits_sweep = FitsFileLoadService.load_fits_kid_sweep_file(file_path=sweep_file_path)
sweep_data = FitsDataUnpackService.unpack_sweep_data(fits_sweep_data=fits_sweep.data)
noise_sweep = NoiseSweep(
    dut=fits_sweep.header.dut,
    tone_name=fits_sweep.header.tone_name,
    temperature=fits_sweep.header.set_temperature,
    input_attenuation=fits_sweep.header.input_attenuation,
    sweep_data=sweep_data,
)

noise_sweep.plot_sweep()

dq_magnitude = np.abs(np.diff(noise_sweep.sweep_data.i))
di_magnitude = np.abs(np.diff(noise_sweep.sweep_data.q))

discontinuity_index_q = np.where(dq_magnitude > 100 * np.mean(dq_magnitude))
discontinuity_index_i = np.where(di_magnitude > 100 * np.mean(di_magnitude))

if (discontinuity_index_q[0].size > 0) & (discontinuity_index_i[0].size > 0):
    if discontinuity_index_q[0] == discontinuity_index_i[0]:
        bifurcated = True
else:
    bifurcated = False

plt.figure(figsize=(8, 6))
plt.plot(np.diff(noise_sweep.sweep_data.i), label="dQ")
plt.plot(np.diff(noise_sweep.sweep_data.q), label="dI")
plt.plot(
    np.diff(noise_sweep.sweep_data.i)[discontinuity_index_q],
    linestyle="none",
    marker="o",
    color="r",
)
plt.plot(
    np.diff(noise_sweep.sweep_data.q)[discontinuity_index_i],
    linestyle="none",
    marker="o",
    color="r",
)
plt.legend()
plt.show()
