import os.path
import re

import numpy as np
import matplotlib.pyplot as plt
import scipy

from skimage.measure import block_reduce
from source.models.noise.noise_packet_psd import NoisePacketPSD
from source.services.fits_data_unpack_service import FitsDataUnpackService
from source.services.fits_file_load_service import FitsFileLoadService
from source.util import noise_multiplication_method_tools
from scipy.fft import fft, fftfreq, fftshift


def main(
    directory=r"C:\Users\c2047423\OneDrive - Cardiff University\Documents\Gethin - PhD\Device Data\SPT-SLIM\v2_anlw001_ppcaps\Data\20230829_Dark_Data_Prelim\KID_K018\Set_Temperature_100_mK\Set_Attenuation_54dB",
    save_figures=True,
    plot_low_frequencies=False,
):

    try:
        noise_packet_psd = (
            noise_multiplication_method_tools.get_single_tone_noise_packet_psd(
                directory=directory,
                down_sample_psd=True,
                down_sample_bins=100,
            )
        )
    except:
        print(f"Error with getting PSD for noise packet")
    else:
        print("PSD complete")

    plt.figure(figsize=(10, 6))

    plt.plot(
        noise_packet_psd.on_res_low_frequencies,
        noise_packet_psd.psd_on_res_low,
        label="On Res Low Freq",
    )

    plt.plot(
        noise_packet_psd.on_res_high_frequencies,
        noise_packet_psd.psd_on_res_high,
        label="On Res High Freq",
    )

    plt.plot(
        noise_packet_psd.off_res_low_frequencies,
        noise_packet_psd.psd_off_res_low,
        label="Off Res Low Freq",
    )

    plt.plot(
        noise_packet_psd.off_res_high_frequencies,
        noise_packet_psd.psd_off_res_high,
        label="Off Res High Freq",
    )

    plt.plot(
        noise_packet_psd.psd_on_res_combined_frequencies,
        noise_packet_psd.psd_on_res_combined,
        label="Combined On Res",
    )

    plt.plot(
        noise_packet_psd.psd_off_res_combined_frequencies,
        noise_packet_psd.psd_off_res_combined,
        label="Combined Off Res",
    )

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("S$_{xx}$ (Hz$^{-1}$)")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
