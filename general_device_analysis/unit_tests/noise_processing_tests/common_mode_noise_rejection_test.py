import numpy as np
import matplotlib.pyplot as plt
from source.util import resonator_noise_analysis_tools as resonator_noise_analysis_tools

if __name__ == "__main__":
    # Create example PSD and frequency arrays (replace these with your actual data).
    n_channels = 3
    n_frequencies = 100
    psd_array = np.random.rand(n_channels, n_frequencies)
    single_freq_array = np.linspace(0, 1000, n_frequencies)
    freq_array = np.split(np.tile(single_freq_array, n_channels), n_channels)

    # Define the frequency range to remove (adjust as needed).
    noise_freq_range = (100, 200)

    # Apply common mode noise rejection.
    cleaned_psd_array = resonator_noise_analysis_tools.common_mode_noise_rejection(
        psd_array, freq_array, noise_freq_range
    )

    plt.figure(figsize=(8, 6))
    for channel in range(n_channels):
        plt.plot(freq_array[channel], psd_array[channel], label="Original Data")
        plt.plot(freq_array[channel], cleaned_psd_array[channel], label="Cleaned Data")
    plt.legend()
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("PSD")
    plt.show()

    # Print the cleaned PSDs.
    print("Cleaned PSD Array:")
    print(cleaned_psd_array)
